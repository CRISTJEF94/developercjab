({
  fetchFinancialStatements: function(component, event, helper) {
    var idRecord = component.get('v.recordId');
    component.set('v.mycolumns', [
      {
        label: $A.get('{!$Label.c.Lc_arce_Period}'),
        fieldName: 'arce__economic_month_info_number__c',
        type: 'text'
      },
      {
        label: $A.get('{!$Label.c.Lc_arce_Date}'),
        fieldName: 'arce__financial_statement_end_date__c',
        type: 'text'
      },
      {
        label: $A.get('{!$Label.c.Lc_arce_Certification}'),
        fieldName: 'arce__ffss_certification_type__c',
        type: 'text'
      },
      {
        label: $A.get('{!$Label.c.Lc_arce_Type}'),
        fieldName: 'arce__ffss_submitted_type__c',
        type: 'text'
      },
      {
        label: $A.get('{!$Label.c.Lc_arce_ValidForRating}'),
        fieldName: 'arce__ffss_valid_type__c',
        type: 'text'
      },
      {
        label: $A.get('{!$Label.c.Lc_arce_AdjustedType}'),
        fieldName: 'arce__ffss_adjusted_type__c',
        type: 'text'
      }
    ]);

    var action = component.get('c.fetchFinancialStatements');
    action.setParams({ varRecord: idRecord });
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === 'SUCCESS') {
        component.set('v.acctList', response.getReturnValue());
        component.set('v.btnStmIsDisabled', false);
      } else if (state === 'ERROR') {
        var errors = response.getError();
        var errorMsg = (errors[0] && errors[0].message) ?
          errors[0].message :
          $A.get('{!$Label.c.Cls_arce_GRP_servError}');
        component.set('v.ffssErrorMsg', errorMsg);
      }
    });
    $A.enqueueAction(action);
  },
  consultEngine: function(component, event, helper) {
    return new Promise(function(resolve, reject) {
      var action = component.get('c.callEngineFinancialState');
      var idRecord = component.get('v.recordId');
      var listIDS = component.get('v.selectedRows2');
      var msg = '';
      var errorMsg = '';
      var warningMsg = '';
      action.setParams({ recordId: idRecord, financialIdList: listIDS });
      component.set('v.msgTable', msg);
      component.set('v.errorMsgTable', errorMsg);
      component.set('v.btnStmIsDisabled', true);
      component.set('v.warningMsgTable', warningMsg);
      if (listIDS.length === 0) {
        component.set('v.msgTable', 'no');
        component.set('v.errorMsgTable', $A.get('{!$Label.c.Arc_Gen_NotEEFF}'));
        component.set('v.btnStmIsDisabled', false);
        resolve();
      } else {
        action.setCallback(this, function(response) {
          var state = response.getState();
          if (state === 'SUCCESS') {
            var resp = response.getReturnValue();
            if (resp.ratiosStatus === 'Success') {
              msg = 'si';
              var ratingValidFFSS = resp.ratingValidFFSS;
              component.set('v.ffssValid', ratingValidFFSS);
              component.set('v.fsServiceId', resp.fsServiceId);
              helper.checkValidFFSS(component, resp);
              resolve();
            } else {
              msg = 'no';
              errorMsg = resp.ratiosStatus;
              warningMsg = resp.ratiosErrorMessage;
              component.set('v.warningMsgTable', warningMsg);
              resolve();
            }
          } else {
            msg = 'no';
            errorMsg = $A.get('{!$Label.c.Lc_arce_engineError}');
            resolve();
          }
          component.set('v.msgTable', msg);
          component.set('v.errorMsgTable', errorMsg);
          component.set('v.btnStmIsDisabled', false);
        });
      }
      $A.enqueueAction(action);
    });
  },
  checkValidFFSS: function(component, resp) {
    if (resp.ratingValidFFSS === 'yes') {
      var ffssValidId = resp.ratingValidatedFS;
      component.set('v.ffssValidId', ffssValidId);
    }
  },
  consultFSdetails: function(component, event, helper) {
    var action = component.get('c.consultFSdetails');
    action.setParams({
      recordId: component.get('v.recordId'),
      fsServiceId: component.get('v.fsServiceId')
    });
    action.setCallback(this, function(response) {
      if (response.getState() === 'SUCCESS') {
        let resp = response.getReturnValue();
        if (resp.gblSuccessOperation === true && resp.gblRespServiceCode === 200 && component.get('v.msgTable') === 'si' && component.get('v.ffssValid') === 'yes') {
          helper.toastMessage(component, event, 'SUCCESS', $A.get('{!$Label.c.Arc_Gen_Toast_Success}'));
        } else if (resp.gblSuccessOperation === false) {
          helper.toastMessage(component, event, 'ERROR', 'Error: ' + resp.gblResulError);
        } else if (resp.gblSuccessOperation === true && resp.gblRespServiceCode !== 200 && component.get('v.msgTable') === 'si' && component.get('v.ffssValid') === 'yes') {
          helper.toastMessage(component, event, 'ERROR', 'Error: ' + resp.gblRespServiceCode);
        }
      }
      var tabRefresh = $A.get('e.dyfr:SaveObject_evt');
      tabRefresh.setParams({
        'recordId': component.get('v.recordId')
      });
      tabRefresh.fire();
    });
    $A.enqueueAction(action);
  },
  toastMessage: function(component, event, type, message) {
    var toastMess = $A.get('e.force:showToast');
    toastMess.setParams({
      'title': '',
      'type': type,
      'mode': 'sticky',
      'duration': '8000',
      'message': message
    });
    toastMess.fire();
  }
});