({
  initEng: function(component, event, helper) {
    helper.fetchFinancialStatements(component, event, helper);  // Fetch FFSS related to AHA.
  },
  UpdateSelectedRows: function(component, event, helper) {
    var selectedRows = event.getParam('selectedRows');
    var setRows = [];
    for (var i = 0; i < selectedRows.length; i++) {
      setRows.push(selectedRows[i].arce__financial_statement_id__c);
    }
    component.set('v.selectedRows2', setRows);
  },
  callTablesEngine: function(component, event, helper) {
    component.set('v.isSaving', true);
    let promise = helper.consultEngine(component, event, helper);
    promise.then(function(resolve, reject) {
      helper.consultFSdetails(component, event, helper);
      component.set('v.isSaving', false);
    }).catch(function() {
      component.set('v.isSaving', false);
    });
  }
});