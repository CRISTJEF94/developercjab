({
  hinit: function(component, event) {
    var action = component.get('c.getProducts');
    action.setParams({
      'tipologia': component.get('v.tipology')
    });
    action.setCallback(this, function(response) {
      var estado = response.getState();
      if (estado === 'SUCCESS') {
        var respuesta = response.getReturnValue();
        component.set('v.listProducts', respuesta.gblProductResp);
        component.set('v.spinnerStatus', false);
        if (respuesta.gblProductResp.length < 1) {
          component.set('v.error', true);
        }
      }
    });
    $A.enqueueAction(action);
  },
  honSubmit: function(component, event, helper) {
    var fields = event.getParam('fields');
    let term = fields.arce__current_apprv_limit_term_type__c;
    let importValue = fields.arce__current_proposed_amount__c;
    if (!term || !importValue || importValue < 0) {
      component.set('v.error', true);
      component.set('v.errorMessageFields', true);
      setTimeout($A.getCallback(function() {
        component.set('v.error', false);
        component.set('v.errorMessageId', false);
      }), 5000);
    } else {
      if (component.get('v.inputClass') === 'default') {
        helper.insertRecordsDefault(component, event, helper);
      } else {
        helper.insertAutoRecords(component, event, helper);
      }
    }
  },
  insertRecordsDefault: function(component, event, helper) {
    var fields = event.getParam('fields');
    let ProductId = component.get('v.selectedChild');
    fields.arce__account_has_analysis_id__c = component.get('v.accHasAId');
    fields.arce__limits_exposures_parent_id__c = component.get('v.parentId');
    fields.arce__Product_id__c = ProductId;
    var campos1 = this.update2ZeroFields(fields);
    component.find('myform').submit(campos1);
  },
  insertAutoRecords: function(component, event, helper) {
    var fields = event.getParam('fields');
    fields.arce__account_has_analysis_id__c = component.get('v.accHasAId');
    fields.arce__limits_exposures_parent_id__c = component.get('v.parentId');
    fields.arce__Product_id__c = component.get('v.selectedChild');
    component.find('myform').submit(fields);
  },
  update2ZeroFields: function(fields) {
    var fieldExposures = ['arce__last_approved_amount__c', 'arce__curr_approved_commited_amount__c', 'arce__curr_apprv_uncommited_amount__c',
      'arce__current_formalized_amount__c', 'arce__outstanding_amount__c', 'arce__current_proposed_amount__c', 'arce__current_approved_amount__c'];
    for (var i = 0; i < fieldExposures.length; i++) {
      for (var fiel in fields) {
        if (fiel === fieldExposures[i] && (fields[fiel] < 0 || !fields[fiel])) {
          fields[fiel] = 0;
        }
      }
    }
    return fields;
  },
  deactivateValidFlag: function(component) {
    var action = component.get('c.desactivateValidFlag');
    action.setParams({
      'recordId': component.get('v.accHasAId'),
      'desactivate': 'desactivate'
    });
    return new Promise(function(resolve, reject) {
      action.setCallback(this, function(response) {
        var state1 = response.getState();
        if (state1 === 'SUCCESS') {
          var respo = response.getReturnValue();
          if (respo.successResponse) {
            resolve();
          } else {
            var toastEvent = $A.get('e.force:showToast');
            toastEvent.setParams({
              'type': 'SUCCESS',
              'title': 'Success!',
              'message': $A.get('$Label.c.Arc_Gen_Record_Update_Success')
            });
            toastEvent.fire();
            reject();
          }
        }
      });

      $A.enqueueAction(action);
    });
  },
  updateLimitsFromService: function(component, event, helper, newRecordId) {
    var action = component.get('c.insertProducts');
    action.setParams({
      'accHasAId': component.get('v.accHasAId'),
      'tipologia': component.get('v.tipology'),
      'prodId': component.get('v.selectedChild'),
      'recordId': newRecordId
    });
    return new Promise(function(resolve, reject) {
      action.setCallback(this, function(response) {
        if (response.getState() === 'SUCCESS') {
          let resp = response.getReturnValue();
          if (resp.successResponse === true) {
            resolve();
          } else {
            helper.toastMessage(component, event, 'ERROR', resp.errorResponse);
            reject();
          }
        } else {
          helper.toastMessage(component, event, 'ERROR', $A.get('$Label.c.Arc_Gen_ApexCallError'));
          reject();
        }
      });
      $A.enqueueAction(action);
    });
  },
  toastMessage: function(component, event, type, message) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'type': type,
      'title': '',
      'message': message
    });
    toastEvent.fire();
  }
});