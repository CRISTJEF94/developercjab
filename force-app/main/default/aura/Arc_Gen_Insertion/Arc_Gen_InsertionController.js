({
  init: function(component, event, helper) {
    helper.hinit(component, event);
  },
  onSubmit: function(component, event, helper) {
    event.preventDefault();
    helper.honSubmit(component, event, helper);
  },
  closeError: function(component, event, helper) {
    component.set('v.error', false);
  },
  handleChange: function(component, event, helper) {
    component.set('v.selectedChild', event.getParam('value'));
    component.set('v.editMode', 'edit');
  },
  handleSuccess: function(component, event, helper) {
    Promise.all([
      helper.deactivateValidFlag(component),
      helper.updateLimitsFromService(component, event, helper, event.getParam('id'))
    ]).then($A.getCallback(function() {
      $A.get('e.force:refreshView').fire();
    })).catch($A.getCallback(function() {
      component.find('overlayLib').notifyClose();
    }));
  }
});