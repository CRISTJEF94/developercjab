({
  doInit: function(component, event, helper) {
    var analysisId = component.get('v.hasRecordId');
    helper.getRatingData(component, analysisId);
  },
  validateRating: function(component, event, helper) {
    component.set('v.success', 'loading');
    var analysisId = component.get('v.hasRecordId');
    var ratingId = component.get('v.ratingId');
    window.setTimeout($A.getCallback(function() {
      helper.validating(component, analysisId, ratingId);
    }), 2500);
  }
});