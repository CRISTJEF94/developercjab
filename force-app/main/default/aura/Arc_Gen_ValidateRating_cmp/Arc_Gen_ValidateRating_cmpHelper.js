({
  getRatingData: function(component, analysisId) {
    var action = component.get('c.getRatingData');
    action.setParams({
      analysisId: analysisId
    });
    action.setCallback(this, function(response) {
      var resp = response.getReturnValue();
      var state = response.getState();
      if (state === 'SUCCESS') {
        component.set('v.ratingId', resp.ratingId);
        component.set('v.ratingFinal', resp.ratingFinal);
        component.set('v.ratingScore', resp.ratingScore);
      } else {
        var mensaje = resp.errorMessage;
        var resultsToast = $A.get('e.force:showToast');
        resultsToast.setParams({
          'title': $A.get('{!$Label.c.Lc_arce_newAnalysisError}'),
          'type': 'error',
          'message': mensaje,
          'duration': '8000'
        });
        resultsToast.fire();
        $A.get('e.force:closeQuickAction').fire();
      }
    });
    $A.enqueueAction(action);
  },
  validating: function(component, analysisId, ratingId) {
    var action = component.get('c.changeStatus');
    action.setParams({
      analysisId: analysisId,
      ratingId: ratingId
    });
    action.setCallback(this, function(response) {
      var resp = response.getReturnValue();
      var state = response.getState();
      if (state === 'SUCCESS') {
        component.set('v.success', 'yes');
        window.setTimeout($A.getCallback(function() {
          $A.get('e.force:refreshView').fire();
        }), 2500);
      } else {
        var mensaje = resp.serviceMessage;
        component.set('v.success', 'no');
        component.set('v.message', mensaje);
        var resultsToast = $A.get('e.force:showToast');
        resultsToast.setParams({
          'title': $A.get('{!$Label.c.Lc_arce_newAnalysisError}'),
          'type': 'error',
          'message': mensaje,
          'duration': '8000'
        });
        resultsToast.fire();
        $A.get('e.force:closeQuickAction').fire();
      }
    });
    $A.enqueueAction(action);
  }
});