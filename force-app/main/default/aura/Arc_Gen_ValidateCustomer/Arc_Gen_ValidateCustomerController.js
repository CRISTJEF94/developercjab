({
  init: function(component, event, helper) {
    component.set('v.show', true);
    helper.hvalidate(component, event);
  },
  close: function(component, event) {
    component.set('v.show', false);
    component.set('v.validation', false);
  },
  closeError: function(component, event) {
    component.set('v.error', false);
  }
});