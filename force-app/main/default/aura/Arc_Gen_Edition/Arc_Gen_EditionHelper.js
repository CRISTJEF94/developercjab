({
  hsaveFields: function(component, event) {
    event.preventDefault();
    var fields = event.getParam('fields');
    var campos1 = [];
    if (component.get('v.producto') === true) {
      let term = fields.arce__current_apprv_limit_term_type__c;
      let importValue = fields.arce__current_proposed_amount__c;
      if (!term || !importValue || importValue < 0) {
        component.set('v.error', true);
        setTimeout($A.getCallback(function() {
          component.set('v.error', false);
        }), 5000);
      } else {
        campos1 = this.update2ZeroFields1(fields);
        component.find('EditForm').submit(campos1);
      }
    } else if (component.get('v.tipology') === 'TP_0013' || component.get('v.tipology') === 'TP_0003' || component.get('v.tipology') === 'TP_0006') {
      component.set('v.sumTypo', true);
      campos1 = this.update2ZeroFields1(fields);
      component.find('EditForm').submit(campos1);
    } else {
      campos1 = this.update2ZeroFields1(fields);
      component.find('EditForm').submit(campos1);
    }
  },
  hhandleSaveSuccess: function(component) {
    var action = component.get('c.desactivateValidFlag');
    action.setParams({
      'recordId': component.get('v.accHasAId'),
      'desactivate': 'desactivate'
    });
    action.setCallback(this, function(response) {
      var state1 = response.getState();
      if (state1 === 'SUCCESS') {
        var respo = response.getReturnValue();
        if (respo.successResponse) {
          $A.get('e.force:refreshView').fire();
        } else {
          var toastEvent = $A.get('e.force:showToast');
          toastEvent.setParams({
            'type': 'SUCCESS',
            'title': 'Success!',
            'message': $A.get('$Label.c.Arc_Gen_Record_Update_Success')
          });
          toastEvent.fire();
          component.find('overlayLib').notifyClose();
        }
      }
    });
    $A.enqueueAction(action);
  },
  sumTypos: function(component) {
    if (component.get('v.sumTypo') === true) {
      var action = component.get('c.sumTypologies');
      action.setParams({
        'recordId': component.get('v.accHasAId')
      });
      $A.enqueueAction(action);
    }
  },
  update2ZeroFields1: function(fields) {
    var fieldsExposure = ['arce__last_approved_amount__c', 'arce__curr_approved_commited_amount__c', 'arce__curr_apprv_uncommited_amount__c',
      'arce__current_formalized_amount__c', 'arce__outstanding_amount__c', 'arce__current_proposed_amount__c', 'arce__current_approved_amount__c'];
    for (var i = 0; i < fieldsExposure.length; i++) {
      for (var field in fields) {
        if (field === fieldsExposure[i] && (fields[field] < 0 || !fields[field])) {
          fields[field] = 0;
        }
      }
    }
    return fields;
  }
});