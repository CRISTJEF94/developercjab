({
  saveFields: function(component, event, helper) {
    event.preventDefault();
    helper.hsaveFields(component, event);
  },
  closeError: function(component, event, helper) {
    component.set('v.error', false);
  },
  close: function(component, event, helper) {
    component.find('overlayLib').notifyClose();
  },
  handleSaveSuccess: function(component, event, helper) {
    helper.sumTypos(component);
    helper.hhandleSaveSuccess(component);
  }
});