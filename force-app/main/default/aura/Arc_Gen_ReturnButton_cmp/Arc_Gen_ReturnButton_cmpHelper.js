({
  initializeComponent: function(cmp, evt, helper) {
    return new Promise(function(resolve, reject) {
		console.log('llego aqui');
      let promise1 = helper.initDelegation(cmp, evt, helper);
      promise1.then(function() {
		console.log('PASO PROMISE 1');
        let promise2 = helper.initIdentification(cmp, evt, helper);
        promise2.then(function() {
          resolve();
        });
      });
    });
  },
  initDelegation: function(cmp, evt, helper) {
	console.log('initDelegation:LLEGO');
    return new Promise(function(resolve, reject) {
      var inputAttributes = cmp.get('v.inputAttributes');
      var action = cmp.get('c.initDelegation');
      action.setParams({
        accHasAnalysisId: inputAttributes.recordId
      });
	  console.log('inputAttributes:'+inputAttributes);
      action.setCallback(this, function(response) {
        var state = response.getState();
		console.log('state:'+state);
        if (state === 'SUCCESS') {
			console.log('response.getReturnValue():'+response.getReturnValue());
          var resp = JSON.parse(response.getReturnValue());
          if (resp.codStatus === 200) {
            if (resp.lstAmbits.length === 1) {
              cmp.set('v.delegationWrapper', response.getReturnValue());
              cmp.set('v.listAmbits', resp.lstAmbits);
			  console.log('response.getReturnValue():'+response.getReturnValue());
              if (resp.msgInfo !== '') {
                helper.showToast('success', resp.msgInfo);
              }
              resolve();
            } else if (resp.lstAmbits.length > 1) {
              helper.errorExecute(cmp, evt, helper, 'Error: Se ha devuelto más de un ámbito');
              reject();
            }
          } else if (resp.codStatus === 500) {
            helper.errorExecute(cmp, evt, helper, resp.msgInfo);
            reject();
          }
        } else {
          helper.errorExecute(cmp, evt, helper, response.getError()[0].message);
          reject();
        }
      });
      $A.enqueueAction(action);
    });
  },
  initIdentification: function(cmp, evt, helper) {
	console.log('PASO A initIdentification');
    return new Promise(function(resolve, reject) {
      var action = cmp.get('c.initIdentification');
      action.setParams({
        wrapper: cmp.get('v.delegationWrapper')
      });
      action.setCallback(this, function(response) {
        console.log('response.getState():'+response.getState());
		var state = response.getState();
        if (state === 'SUCCESS') {
          var resp = response.getReturnValue();
          cmp.set('v.userId', resp);
          resolve();
        } else {
          helper.errorExecute(cmp, evt, helper, response.getError()[0].message);
          reject();
        }
      });
      $A.enqueueAction(action);
    });
  },
  evaluateIdentification: function(cmp, evt, helper, reason) {
    var inputAttributes = cmp.get('v.inputAttributes');
    var action = cmp.get('c.evaluateIdentification');
    action.setParams({
      accHasAnalysisId: inputAttributes.recordId,
      wrapper: cmp.get('v.delegationWrapper'),
      userId: cmp.get('v.userId'),
      reason: reason
    });
    action.setCallback(this, function(response) {
		console.log('response.getState():'+response.getState());
      var state = response.getState();
      if (state === 'SUCCESS') {
		console.log('response.getReturnValue():'+response.getReturnValue());
        var resp = JSON.parse(response.getReturnValue());
        if (resp.codStatus === 200) {
          if (resp.msgInfo !== '') {
            helper.showToast('success', resp.msgInfo);
          }
          helper.refreshTab(cmp, evt, helper);
		  console.log('PASO helper.refreshTab');
          helper.closeModal(cmp);
        } else if (resp.codStatus === 500) {
          if (resp.msgInfo !== '' || resp.msgInfo !== undefined) {
            helper.errorExecute(cmp, evt, helper, resp.msgInfo);
          }
        }
      } else {
        helper.errorExecute(cmp, evt, helper, response.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  },
  showToast: function(type, message) {
    var toastEventUE = $A.get('e.force:showToast');
    toastEventUE.setParams({
      'title': '',
      'type': type,
      'mode': 'sticky',
      'duration': '8000',
      'message': message
    });
    toastEventUE.fire();
  },
  closeModal: function(cmp) {
	console.log('ENTRO closeModal');
    cmp.destroy();
  },
  errorExecute: function(cmp, evt, helper, msgInfo) {
    if (msgInfo !== '') {
      helper.showToast('error', msgInfo);
    }
    helper.refreshTab(cmp, evt, helper);
    helper.closeModal(cmp);
  },
  refreshTab: function(cmp, evt, helper) {
    let refreshEvt = $A.get('e.c:Arc_Gen_QVCDRefresh_evt');
    refreshEvt.fire();
  }
});