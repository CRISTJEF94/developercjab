({
  getTemplate: function(component) {
    var action = component.get('c.getTabsJson');
    action.setParams({
      recordId: component.get('v.idSelect')
    });
    action.setCallback(this, function(response) {
      component.set('v.view', true);
      var state = response.getState();
      if (component.isValid() && state === 'SUCCESS') {
        var resp = JSON.parse(response.getReturnValue());
        if (resp.gblResultResponse) {
          var statusButtons = (resp.gblPermissionEdit === 'true') ? 'Show' : 'Hide';
          var statusButtonsForQVCD = (resp.gblPermissionEdit === 'true') ? true : false;
          var appEvent = $A.get('e.c:Arc_Gen_QVCDEvent');
          appEvent.setParams({'permission': statusButtonsForQVCD});
          appEvent.fire();
          component.set('v.sectionsLts', resp.lstNamesTemplates);
          component.set('v.dynaAtt', '{"lightningEdit":"' + resp.gblPermissionEdit + '", "cWrappButtons":"' + statusButtons + '", "columnReduction":"' + resp.columnReduction + '"}');
          var edit = '{"style":"brand","unactiveStyle":"hidden","active":' + resp.gblPermissionEdit + '}';
          component.set('v.permissionEdit', edit);
          var managePolicies = this.policieTabStatus(resp.lstNamesTemplates);
          var compEvent = $A.get('e.c:Arc_Gen_TabSetEvent');
          compEvent.setParams({'policies': managePolicies});
          compEvent.fire();
        } else {
          var toastError = $A.get('e.force:showToast');
          toastError.setParams({
            'title': 'Error!',
            'type': 'error',
            'mode': 'sticky',
            'duration': '8000',
            'message': $A.get('{!$Label.c.Arc_Gen_UnknownError}') + ': ' + resp.gblDescriptionResponse
          });
          toastError.fire();
        }
      } else {
        var toastEventUE = $A.get('e.force:showToast');
        toastEventUE.setParams({
          'title': 'Error!',
          'type': 'error',
          'mode': 'sticky',
          'duration': '8000',
          'message': $A.get('{!$Label.c.Arc_Gen_UnknownError}') + ': ' + JSON.stringify(response.error)
        });
        toastEventUE.fire();
      }
    });
    $A.enqueueAction(action);
  },
  policieTabStatus: function(jSONResp) {
    var statusPolicies = false;
    for (var i in jSONResp) {
      if (jSONResp[i].nameSection === 'Policies' && jSONResp[i].isVisible === true) {
        statusPolicies = true;
      }
    }
    return statusPolicies;
  },
  callClassCompletitud: function(cmp, evt, helper) {
    return new Promise(function(resolve, reject) {
      var action = cmp.get('c.callTemplateAnalysisJson');
      let recordId = evt.getParam('recordId');
      action.setParams({
        'recordId': recordId
      });
      action.setCallback(this, function(response) {
        if (cmp.isValid() && response.getState() === 'SUCCESS') {
          var resp = response.getReturnValue();
          var jsonParse = JSON.parse(resp.gblDescriptionResponse);
          cmp.set('v.jsonResponse', jsonParse);
          cmp.set('v.changeArceState', resp.changeStatus);
          cmp.set('v.unitMessage', resp.unitChangeResponse);
          resolve('Resolved');
        } else {
          reject('Rejected');
        }
      });
      $A.enqueueAction(action);
    });
  },
  calculatePercentage: function(cmp, evt, helper) {
    return new Promise(function(resolve, reject) {
      var jsonParse = cmp.get('v.jsonResponse');
      var lista = cmp.get('v.sectionsLts');
      var indexAux = [];
      var aux;
      for (var i in lista) {
        if (lista[i] !== undefined) {
          lista[i].percent = jsonParse[i].percent;
          if (lista[i].isVisible !== jsonParse[i].isVisible) {
            aux = i;
            lista[i].isVisible = jsonParse[i].isVisible;
          }
          if (i > aux) {
            indexAux.push(lista[i].isVisible);
            lista[i].isVisible = false;
          }
        }
      }
      cmp.set('v.lstIndexAux', indexAux);
      cmp.set('v.sectionsLts', lista);
      cmp.set('v.indexAux', aux);
      resolve('Resolved');
    });
  },
  restorePercentage: function(cmp, evt, helper) {
    return new Promise(function(resolve, reject) {
      var aux;
      if (cmp.get('v.indexAux') !== undefined) {
        aux = cmp.get('v.indexAux');
      } else {
        aux = undefined;
      }
      var indexAux = cmp.get('v.lstIndexAux');
      var lista = cmp.get('v.sectionsLts');
      var contador = 0;
      for (var j in lista) {
        if (j > aux && aux !== undefined) {
          lista[j].isVisible = indexAux[contador];
          contador++;
        }
      }
      resolve(lista);
    });
  },
  showToast: function(cmp, evt, message) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'type': 'SUCCESS',
      'title': 'Success!',
      'message': message
    });
    toastEvent.fire();
  }
});