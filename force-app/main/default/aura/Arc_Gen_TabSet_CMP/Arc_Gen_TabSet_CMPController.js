({
  getItemEvent: function(cmp, event, helper) {
    cmp.set('v.view', false);
    cmp.set('v.idSelect', event.getParam('IdItem'));
    cmp.set('v.selectedTab', null);
    helper.getTemplate(cmp);
  },
  update: function(component, event, helper) {
    component.destroy();
  },
  handleSaveEvent: function(cmp, evt, helper) {
    let promise = helper.callClassCompletitud(cmp, evt);
    promise.then(function(resolve, reject) {
      let promise2 = helper.calculatePercentage(cmp, evt);
      promise2.then(function(resolve2) {
        let promise3 = helper.restorePercentage(cmp, evt);
        promise3.then(function(resolve3) {
          if (resolve3) {
            var changeMiniStattus = cmp.get('v.changeArceState');
            if (changeMiniStattus === true) {
              $A.get('e.force:refreshView').fire();
            } else {
              var managePolicies = helper.policieTabStatus(resolve3);
              var compEvent = $A.get('e.c:Arc_Gen_TabSetEvent');
              compEvent.setParams({'policies': managePolicies });
              compEvent.fire();
              cmp.set('v.sectionsLts', resolve3);
            }
            if (cmp.get('v.unitMessage') !== 'SameUnit') {
              helper.showToast(cmp, evt, cmp.get('v.unitMessage'));
            }
          }
        });
      });
    });
  },
  handleSelect: function(cmp, event, helper) {
    cmp.set('v.selectedTab', event.getParam('id'));
  }
});