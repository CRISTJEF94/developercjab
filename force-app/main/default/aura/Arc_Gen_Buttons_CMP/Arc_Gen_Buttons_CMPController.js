({
  getItemEvent: function(cmp, event, helper) {
    cmp.set('v.view', false);
    cmp.set('v.idSelect', event.getParam('IdItem'));
    cmp.set('v.view', true);
  },
  update: function(component, event, helper) {
    component.destroy();
  },
  handleTabEvent: function(cmp, event, helper) {
    var policies = event.getParam('policies');
    var buttons = cmp.get('v.buttonsIdentifier');
    var buttonStyle = cmp.get('v.buttonsStyle');
    var buttonAlig = cmp.get('v.buttonsAlig');
    var buttonsLts = buttons.split(',');
    var buttonStyleLts = buttonStyle.split(',');
    var buttonAligLts = buttonAlig.split(',');
    var aux = 0;
    if (policies === false) {
      for (var i in buttonsLts) {
        if (buttonsLts[i] === $A.get('$Label.c.Arc_Gen_DelBtnValClient')) {
          buttonsLts[i] = null;
          buttonStyleLts[i] = null;
          buttonAligLts[i] = null;
          cmp.set('v.indexAux', i);
        }
      }
      cmp.set('v.buttonsIdentifier', buttonsLts.toString());
      cmp.set('v.buttonsAlig', buttonAligLts.toString());
      cmp.set('v.buttonsStyle', buttonStyleLts.toString());
    } else {
      for (var j in buttonsLts) {
        if (buttonsLts[j] === $A.get('$Label.c.Arc_Gen_DelBtnValClient')) {
          aux++;
        }
      }
      if (aux === 0) {
        let index = cmp.get('v.indexAux');
        buttonsLts[index] = $A.get('$Label.c.Arc_Gen_DelBtnValClient');
        buttonStyleLts[index] = $A.get('$Label.c.Lc_arce_buttonStyle');
        buttonAligLts[index] = $A.get('$Label.c.Lc_arce_buttonAlignment');
        cmp.set('v.buttonsIdentifier', buttonsLts.toString());
        cmp.set('v.buttonsStyle', buttonStyleLts.toString());
        cmp.set('v.buttonsAlig', buttonAligLts.toString());
      }
    }
    cmp.set('v.view', false);
    cmp.set('v.view', true);
  }
});