({
  init: function(component, event, helper) {
    helper.getTableData(component, event);
    helper.getHeaderDate(component, event);
  },
  handleRowAction: function(component, event, helper) {
    var action = event.getParam('action');
    switch (action.name) {
      case 'insert':
        helper.insertRecords(component, event);
        break;
      case 'edit':
        helper.editRecords(component, event);
        break;
      case 'show':
        helper.editRecords(component, event);
        break;
      case 'delete':
        var row = event.getParam('row');
        component.set('v.row', row);
        component.set('v.show', true);
        break;
      default:
        helper.editRecords(component, event);
        break;
    }
  },
  close: function(component, event, helper) {
    component.set('v.show', false);
  },
  accept: function(component, event, helper) {
    var row = component.get('v.row');
    helper.deleteRecords(component, event, row);
    component.set('v.show', false);
  },
  handleRefreshEvt: function(component, event, helper) {
    if (event.getParam('table') === 'PolicieTable') {
      var action = component.get('c.changeServiceFlag');
      action.setParams({
        'recordId': component.get('v.recordId')
      });
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === 'SUCCESS') {
          let resp = response.getReturnValue();
          if (resp.successResponse === true) {
            helper.getTableData(component, event);
            helper.toastMessages('SUCCESS', 'SUCCESS');
            var tabRefresh = $A.get('e.dyfr:SaveObject_evt');
            tabRefresh.setParams({
              'recordId': component.get('v.recordId')
            });
            tabRefresh.fire();
          } else {
            helper.toastMessages('ERROR', resp.errorResponse);
          }
        } else {
          helper.toastMessages('ERROR', $A.get('$Label.c.Arc_Gen_ApexCallError'));
        }
      });
      $A.enqueueAction(action);
    }
  }
});