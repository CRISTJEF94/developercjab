({
  initHandler: function(component, event, helper) {
    var actionCall = component.get('c.getGroupId');
    actionCall.setParams({
      recordId: component.get('v.recordId')
    });
    actionCall.setCallback(this, function(response) {
      var state = response.getState();
      if (state === 'SUCCESS') {
        var resp = response.getReturnValue();
        if (resp.arce__wf_status_id__c === '01') {
          component.set('v.idGroup', resp.arce__Group__c);
          component.set('v.load', true);
        }
      }
    });
    $A.enqueueAction(actionCall);
  }
});