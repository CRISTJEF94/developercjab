({
  doInit: function(component, event, helper) {
    component.set('v.spinner', 'true');
    var analysisId = component.get('v.hasRecordId');
    helper.getCustomerId(component, analysisId);
    window.setTimeout($A.getCallback(function() {
      var customerId = component.get('v.customerId');
      helper.callRatingEngine(component, analysisId, customerId);
    }), 2500);
  }
});