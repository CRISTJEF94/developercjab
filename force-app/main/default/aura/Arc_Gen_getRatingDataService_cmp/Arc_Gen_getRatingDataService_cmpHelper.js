({
  getCustomerId: function(component, analysisId) {
    var action = component.get('c.getCustomerData');
    action.setParams({
      analysisId: analysisId
    });
    action.setCallback(this, function(response) {
      var resp = response.getReturnValue();
      var state = response.getState();
      if (state === 'SUCCESS') {
        component.set('v.customerName', resp.customerName);
        component.set('v.customerId', resp.customerId);
      } else {
        var mensaje = resp.errorMessage;
        var resultsToast = $A.get('e.force:showToast');
        resultsToast.setParams({
          'title': $A.get('{!$Label.c.Lc_arce_newAnalysisError}'),
          'type': 'error',
          'message': mensaje,
          'duration': '8000'
        });
        resultsToast.fire();
        $A.get('e.force:closeQuickAction').fire();
      }
    });
    $A.enqueueAction(action);
  },
  callRatingEngine: function(component, analysisId, customerId) {
    var action = component.get('c.setupRating');
    action.setParams({
      analysisId: analysisId,
      customerId: customerId,
      serviceMock: null
    });
    action.setCallback(this, function(response) {
      var resp = response.getReturnValue();
      var state = response.getState();
      if (state === 'SUCCESS') {
        if (resp.serviceCode === '200' && resp.saveStatus === 'true') {
          component.set('v.message', $A.get('{!$Label.c.Lc_arce_successAndCloseWindow}'));
          component.set('v.success', 'yes');
          this.refreshRating(component);
        } else if (resp.serviceCode !== '200') {
          var toastErrorService = $A.get('e.force:showToast');
          component.set('v.message', $A.get('{!$Label.c.Lc_arce_newAnalysisError}') + resp.serviceMessage);
          component.set('v.success', 'no');
        }
        if (resp.saveStatus === 'false') {
          component.set('v.message', $A.get('{!$Label.c.Lc_arce_newAnalysisError}') + resp.saveMessage);
          component.set('v.success', 'no');
        }
      } else {
        var resultsToast = $A.get('e.force:showToast');
        component.set('v.message', $A.get('{!$Label.c.Lc_arce_newAnalysisError}'));
        component.set('v.success', 'no');
      }
      component.set('v.spinner', 'false');
      $A.get('e.force:closeQuickAction').fire();
    });
    $A.enqueueAction(action);
  },
  refreshRating: function(component) {
    var tabRefresh = $A.get('e.dyfr:SaveObject_evt');
    tabRefresh.setParams({
      'recordId': component.get('v.recordId')
    });
    tabRefresh.fire();
  }
});