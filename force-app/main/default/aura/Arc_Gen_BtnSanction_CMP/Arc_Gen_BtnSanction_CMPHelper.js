({
  initializeComponent: function(cmp, evt, helper) {
    return new Promise(function(resolve, reject) {
      let promise = helper.initDelegation(cmp, evt, helper);
      promise.then(function() {
        resolve();
      });
    });
  },
  initDelegation: function(cmp, evt, helper) {
    return new Promise(function(resolve, reject) {
      var inputAttributes = cmp.get('v.inputAttributes');
      var action = cmp.get('c.initDelegation');
      action.setParams({
        accHasAnalysisId: inputAttributes.recordId
      });
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === 'SUCCESS') {
          var resp = JSON.parse(response.getReturnValue());
          if (resp.codStatus === 200) {
            cmp.set('v.delegationWrapper', response.getReturnValue());
            cmp.set('v.listAmbits', resp.lstAmbits);
            cmp.set('v.listActions', resp.lstActions);
            resolve();
          } else if (resp.codStatus === 500) {
            helper.executeError(cmp, evt, helper, resp.msgInfo);
            reject();
          }
        } else {
          helper.executeError(cmp, evt, helper, response.getError()[0].message);
          reject();
        }
      });
      $A.enqueueAction(action);
    });
  },
  validatesave: function(cmp, helper) {
    var descValue = cmp.find('descText').get('v.value');
    var opt = typeof (cmp.get('v.selectedAction')) === 'undefined' ? '' : cmp.get('v.selectedAction');
    var sancDate = cmp.get('v.sancdate');
    var sancDateCometee = cmp.get('v.sancdateCometee');
    var idUserSelected = cmp.get('v.idUserSelected');

    if (!descValue) {
      descValue = '';
    }

    if (sancDate !== '' && sancDate !== null && sancDateCometee !== '' && sancDateCometee !== null && idUserSelected !== '' && descValue !== '' && opt !== '') {
      helper.checkAmbitValidation(cmp);
    } else {
      cmp.set('v.isVisibleBtn', true);
    }
  },
  checkAmbitValidation: function(cmp) {
    if (cmp.get('v.lstAmbitVisibility')) {
      if (cmp.get('v.selectedAmbit') !== undefined) {
        cmp.set('v.isVisibleBtn', false);
      }
    } else {
      cmp.set('v.isVisibleBtn', false);
    }
  },
  saveAction: function(cmp, evt, helper) {
    var inputAttributes = cmp.get('v.inputAttributes');
    var actionCall = cmp.get('c.saveScopeOfSanction');
    var params = {};
    params = {
      'recordId': inputAttributes.recordId,
      'idUserSelected': cmp.get('v.idUserSelected'),
      'userSelected': cmp.get('v.userSelected'),
      'ambit': cmp.get('v.selectedAmbit'),
      'sanction': cmp.get('v.selectedAction'),
      'descText': cmp.find('descText').get('v.value'),
      'sancDate': cmp.get('v.sancdate'),
      'sancdateCometee': cmp.get('v.sancdateCometee'),
    };
    actionCall.setParams({
      'wrapper': cmp.get('v.delegationWrapper'),
      data: params
    });
    actionCall.setCallback(this, function(response) {
      var state = response.getState();
      if (state === 'SUCCESS') {
        var resp = JSON.parse(response.getReturnValue());
        if (resp.codStatus === 200) {
          if (resp.msgInfo !== '') {
            helper.showToast('success', resp.msgInfo);
          }
          helper.refreshTab(cmp, evt, helper);
          helper.cancelAction(cmp);
        } else if (resp.codStatus === 500) {
          helper.executeError(cmp, evt, helper, resp.msgInfo);
        }
      } else {
        helper.executeError(cmp, evt, helper, response.getError()[0].message);
      }
    });
    $A.enqueueAction(actionCall);
  },
  showToast: function(type, message) {
    var toastEventUE = $A.get('e.force:showToast');
    toastEventUE.setParams({
      'title': '',
      'type': type,
      'mode': 'sticky',
      'duration': '8000',
      'message': message
    });
    toastEventUE.fire();
  },
  cancelAction: function(cmp) {
    cmp.set('v.isVisibleBtn', true);
    cmp.set('v.spinnerStatus', true);
    cmp.destroy();
  },
  executeError: function(cmp, evt, helper, msgError) {
    helper.showToast('error', msgError);
    helper.cancelAction(cmp);
  },
  refreshTab: function(cmp, evt, helper) {
    let refreshEvt = $A.get('e.c:Arc_Gen_QVCDRefresh_evt');
    refreshEvt.fire();
  }
});