({
  doInit: function(component, event, helper) {
    var componentTarget = component.find('changespin');
    $A.util.addClass(componentTarget, 'getingdata');
    let promise1 = helper.getAHA(component, event, helper);
    promise1.then(function() {
      let promise2 = helper.listParticipant(component, event, helper);
      promise2.then(function() {
        helper.listCustomers(component, event, helper);
        component.set('v.refreshSpinner', false);
      });
    });
  },
  closeWindow: function(component, event, helper) {
    helper.cancelAction(component, event, helper);
  }
});