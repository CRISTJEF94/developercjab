({
  getAHA: function(component, event, helper) {
    return new Promise(function(resolve, reject) {
      var action = component.get('c.getAHARefresh');
      action.setParams({
        'recordId': component.get('v.inputAttributes').recordId
      });
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === 'SUCCESS') {
          var resp = response.getReturnValue();
          var groupid = '';
          var groupnumber = '';
          var ahaswithoutgroup = [];
          var ahaswithoutgroupnumber = [];
          var groupname = '';
          for (var x of resp) {
            if (x.ahaObj.arce__group_asset_header_type__c === '1') {
              groupid = x.ahaObj.Id;
              groupnumber = x.accWrapperObj.accNumber;
              groupname = x.accWrapperObj.name;
            } else {
              ahaswithoutgroup.push(x.ahaObj.Id);
              ahaswithoutgroupnumber.push(x.accWrapperObj.accNumber);
            }
          }
          component.set('v.groupid', groupid);
          component.set('v.groupname', groupname);
          component.set('v.groupnumber', groupnumber);
          component.set('v.ahaswithoutgroup', ahaswithoutgroup);
          component.set('v.ahaswithoutgroupnumber', ahaswithoutgroupnumber);
          resolve();
        } else {
          helper.cancelAction(component, helper);
          reject();
        }
      });
      $A.enqueueAction(action);
    });
  },
  listParticipant: function(component, event, helper) {
    return new Promise(function(resolve, reject) {
      var componentTarget = component.find('changespin');
      $A.util.removeClass(componentTarget, 'getingdata');
      $A.util.addClass(componentTarget, 'listparticip');

      var action = component.get('c.getListParticipants');
      action.setParams({
        'encryptedgroup': component.get('v.groupnumber'),
        'ahaswithoutgroup': component.get('v.ahaswithoutgroup'),
        'ahaswithoutgroupnumber': component.get('v.ahaswithoutgroupnumber'),
        'recordId': component.get('v.groupid')
      });
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === 'SUCCESS') {
          var resp = JSON.parse(response.getReturnValue());
          if (resp.servicecallerror || resp.errormessage) {
            component.set('v.refreshSpinner', 'false');
            component.set('v.refreshMessage', $A.get('{!$Label.c.Arc_Gen_SpinnerMessageError}') + '..');
            reject();
          } else if (resp.error204message) {
            component.set('v.refreshSpinner', 'false');
            component.set('v.refreshMessage', resp.error204message + '..');
            reject();
          } else {
            component.set('v.listparticipant', response.getReturnValue());
            resolve();
          }
        } else {
          component.set('v.refreshSpinner', false);
          component.set('v.refreshMessage', $A.get('{!$Label.c.Arc_Gen_SpinnerMessageError}') + '..');
          reject();
        }
      });
      $A.enqueueAction(action);
    });
  },
  listCustomers: function(component, event, helper) {
    var idSelected = component.get('v.inputAttributes').recordId;
    let action = component.get('c.callListCustomers');
    action.setParams({
      recordId: idSelected
    });
    action.setCallback(this, function(response) {
      var state = response.getState();
      var resp = response.getReturnValue();
      if (state === 'SUCCESS') {
        if (resp.message && resp.serviceStatus === 'KO') {
          component.set('v.refreshSpinner', false);
          component.set('v.refreshMessage', $A.get('{!$Label.c.Arc_Gen_SpinnerMessageError}') + '.....');
          helper.cancelAction(component);
          component.set('v.resolvelistcust', false);
        } else {
          component.set('v.resolvelistcust', true);
          component.set('v.refreshMessage', 'The ARCE has refreshed properly, you can close the modal');
        }
      } else {
        component.set('v.refreshSpinner', false);
        component.set('v.refreshMessage', $A.get('{!$Label.c.Arc_Gen_SpinnerMessageError}') + '.....');
        component.set('v.resolvelistcust', false);
        helper.cancelAction(component);
      }
    });
    $A.enqueueAction(action);
  },
  cancelAction: function(component) {
    window.location.reload();
  },
  refreshTab: function(component) {
    let refreshEvt = $A.get('e.c:Arc_Gen_QVCDRefresh_evt');
    refreshEvt.fire();
  }
});