({
  initializeComponent: function(cmp, evt, helper) {
    return new Promise(function(resolve, reject) {
      let promise = helper.validateRating(cmp, evt, helper);
      promise.then(function() {
        let promise1 = helper.initDelegation(cmp, evt, helper);
        promise1.then(function() {
          resolve();
        });
      });
    });
  },
  validateRating: function(cmp, evt, helper) {
    return new Promise(function(resolve, reject) {
      cmp.set('v.show', true);
      let inputAttributes = cmp.get('v.inputAttributes');
      var action = cmp.get('c.validateRatingInPreparation');
      action.setParams({
        accHasAnalysisId: inputAttributes.recordId
      });
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === 'SUCCESS') {
          var res = response.getReturnValue();
          if (res === true) {
            resolve();
          } else {
            helper.executeError(cmp, evt, helper, 'Validate rating has value : ' + res);
            reject();
          }
        } else {
          helper.executeError(cmp, evt, helper, response.getError()[0].message);
          reject();
        }
      });
      $A.enqueueAction(action);
    });
  },
  initDelegation: function(cmp, evt, helper) {
    return new Promise(function(resolve, reject) {
      var inputAttributes = cmp.get('v.inputAttributes');
      var action = cmp.get('c.initDelegation');
      action.setParams({
        accHasAnalysisId: inputAttributes.recordId
      });
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === 'SUCCESS') {
          var resp = JSON.parse(response.getReturnValue());
          if (resp.codStatus === 200) {
            cmp.set('v.delegationWrapper', response.getReturnValue());
            cmp.set('v.listAmbits', resp.lstAmbits);
            resolve();
          } else if (resp.codStatus === 500) {
            helper.executeError(cmp, evt, helper, resp.msgInfo);
            reject();
          }
        } else {
          cmp.set('v.visibilityButtons', '{cancel: ' + cmp.get('v.visibilityButtons').cancel + ', save: true, continue: true}');
          helper.executeError(cmp, evt, helper, response.getError()[0].message);
          reject();
        }
      });
      $A.enqueueAction(action);
    });
  },
  evaluateDelegation: function(cmp, evt, helper) {
    var inputAttributes = cmp.get('v.inputAttributes');
    var action = cmp.get('c.evaluateDelegation');
    action.setParams({
      wrapper: cmp.get('v.delegationWrapper'),
      accHasAnalysisId: inputAttributes.recordId
    });
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === 'SUCCESS') {
        var res = JSON.parse(response.getReturnValue());
        if (res.codStatus === 200) {
          cmp.set('v.spinnerStatus', false);
        } else if (res.codStatus === 201) {
          if (res.msgInfo !== '') {
            helper.showToast('success', res.msgInfo);
          }
          helper.cancelAction(cmp);
        } else if (res.codStatus === 500) {
          cmp.set('v.visibilityButtons', '{cancel: ' + cmp.get('v.visibilityButtons').cancel + ', save: true, continue: true}');
          helper.executeError(cmp, evt, helper, res.msgInfo);
        }
      } else {
        helper.executeError(cmp, evt, helper, response.getError()[0].message);
        cmp.set('v.visibilityButtons', '{cancel: ' + cmp.get('v.visibilityButtons').cancel + ', save: true, continue: true}');
      }
    });
    $A.enqueueAction(action);
  },
  btnSaveAction: function(cmp, evt, helper) {
    var inputAttributes = cmp.get('v.inputAttributes');
    var action = cmp.get('c.saveAction');
    action.setParams({
      wrapper: cmp.get('v.delegationWrapper'),
      accHasAnalysisId: inputAttributes.recordId,
      ambit: cmp.get('v.ambit')
    });
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === 'SUCCESS') {
        var resp = JSON.parse(response.getReturnValue());
        if (resp.codStatus === 200 || resp.codStatus === 201) {
          if (resp.msgInfo !== '') {
            helper.showToast('success', resp.msgInfo);
          }
          helper.refreshTab(cmp, evt, helper);
          helper.cancelAction(cmp);
        } else {
          cmp.set('v.visibilityButtons', '{cancel: ' + cmp.get('v.visibilityButtons').cancel + ', save: true, continue: true}');
          helper.executeError(cmp, evt, helper, resp.msgInfo);
        }
      } else {
        helper.executeError(cmp, evt, helper, response.getError()[0].message);
        cmp.set('v.visibilityButtons', '{cancel: ' + cmp.get('v.visibilityButtons').cancel + ', save: true, continue: true}');
      }
    });
    $A.enqueueAction(action);
  },
  showToast: function(type, message) {
    var toastEventUE = $A.get('e.force:showToast');
    toastEventUE.setParams({
      'title': '',
      'type': type,
      'mode': 'sticky',
      'duration': '8000',
      'message': message
    });
    toastEventUE.fire();
  },
  cancelAction: function(cmp) {
    cmp.destroy();
  },
  executeError: function(cmp, evt, helper, msgError) {
    helper.showToast('error', msgError);
    helper.cancelAction(cmp);
  },
  refreshTab: function(component, event, helper) {
    let refreshEvt = $A.get('e.c:Arc_Gen_QVCDRefresh_evt');
    refreshEvt.fire();
  }
});