({
  init: function(cmp, evt, helper) {
    let promise = helper.initializeComponent(cmp, evt, helper);
    promise.then(function(resolve) {
      helper.evaluateDelegation(cmp, evt, helper);
    });
  },
  cancelAction: function(cmp, evt, helper) {
    helper.cancelAction(cmp, evt, helper);
  },
  setAmbitValue: function(cmp, evt, helper) {
    cmp.set('v.ambit', evt.getParam('value'));
    cmp.set('v.visibilityButtons', '{cancel: ' + cmp.get('v.visibilityButtons').  ancel + ', save: false, continue: true}');
  },
  btnSaveAction: function(cmp, evt, helper) {
    helper.btnSaveAction(cmp, evt, helper);
  }
});