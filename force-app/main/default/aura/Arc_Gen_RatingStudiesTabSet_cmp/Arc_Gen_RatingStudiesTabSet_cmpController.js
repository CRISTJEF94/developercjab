({
  selectedRow: function(component, event, helper) {
    component.set('v.spinner', 'true');
    component.set('v.view', 'false');
    helper.doSelectedRow(component, event);
  }
});