({
  doSelectedRow: function(component, event) {
    var ffssId = event.getParam('recordId');
    var action = component.get('c.getAnalyzedClient');
    action.setParams({
      ffssId: ffssId
    });
    action.setCallback(this, function(response) {
      var resp = response.getReturnValue();
      var state = response.getState();
      if (state === 'SUCCESS') {
        component.set('v.idSelect', resp.ratingId);
        component.set('v.template', resp.templateName);
        component.set('v.success', 'yes');
        component.set('v.view', 'true');
      } else {
        component.set('v.success', 'no');
      }
      component.set('v.spinner', 'false');
    });
    $A.enqueueAction(action);
  }
});