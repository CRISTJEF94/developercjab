({
  getfullaccountforservices: function(component, event, helper) {
    var action = component.get('c.getaccdataforservices');
    action.setParams({
      recordId: component.get('v.hasRecordId')
    });
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === 'SUCCESS') {
        component.set('v.firstaccountforfilldata', response.getReturnValue());
        helper.chainingpromisesforservice(component, event, helper);
      }
    });
    $A.enqueueAction(action);
  },
  chainingpromisesforservice: function(component, event, helper) {
    var account = component.get('v.firstaccountforfilldata');
    /**
        * @Description: String with value "Group"
    */
    var S_GROUP = 'GROUP';
    /**
        * @Description: String with value "Client"
    */
    var CLIENT = 'SUBSIDIARY';
    if (account.participantType === CLIENT) {
      let grpid = account.accParentId;
      let clientorgroupnumber = account.accNumber;
      let economicpar = helper.economicpartservice(component, clientorgroupnumber);
      economicpar.then(function(result) {
        let listpart = helper.listparticipant(component, result);
        listpart.then(function() {
          let groupstruc = helper.groupstructure(component, component.get('v.listparticipant'), component.get('v.economicparticipant'), clientorgroupnumber, helper);
          groupstruc.then(function() {
            let getpreviousArce = helper.getpreviousArce(component, helper);
            getpreviousArce.then(function() {});
          });
        });
      });
    } else if (account.participantType === S_GROUP) {
      //when is group or orphan
      let grpid = account.accId;
      let clientorgroupnumber = account.accNumber;
      component.set('v.groupId', grpid);
      component.set('v.idofarceexecutor', component.get('v.hasRecordId'));
      let listpart = helper.listparticipant(component, clientorgroupnumber);
      listpart.then(function() {
        let groupstruc = helper.groupstructure(component, component.get('v.listparticipant'), component.get('v.economicparticipant'), clientorgroupnumber, helper);
        groupstruc.then(function() {
          let getpreviousArce = helper.getpreviousArce(component, helper);
          getpreviousArce.then(function() {});
        });
      });
    }
  },
  listparticipant: function(component, result) {
    var componentTarget = component.find('changespin');
    $A.util.addClass(componentTarget, 'listp');
    $A.util.removeClass(componentTarget, 'economicp');
    var listpartaction = component.get('c.listparticipant');
    return new Promise(function(resolve, reject) {
      listpartaction.setParams({
        'encryptedgroup': result
      });
      listpartaction.setCallback(this, function(response) {
        var state = response.getState();
        if (state === 'SUCCESS') {
          component.set('v.listparticipant', response.getReturnValue());
          var listparticipantsdetails = JSON.parse(response.getReturnValue());
          if (listparticipantsdetails.customersdata || listparticipantsdetails.error204message) {
            resolve();
          } else if (listparticipantsdetails.servicecallerror || listparticipantsdetails.errormessage) {
            component.set('v.nextButtonDisabled', true);
            component.set('v.refreshSpinner', 'false');
            component.set('v.showmessage', true);
            component.set('v.showall', false);
            component.set('v.economicpartmessage', $A.get('{!$Label.c.Arc_Gen_SpinnerMessageError}') + '..');
            reject();
          }
        } else {
          component.set('v.nextButtonDisabled', true);
          component.set('v.refreshSpinner', 'false');
          component.set('v.showmessage', true);
          component.set('v.showall', false);
          component.set('v.economicpartmessage', $A.get('{!$Label.c.Arc_Gen_SpinnerMessageError}') + '..');
          reject();
        }
      });
      $A.enqueueAction(listpartaction);
    });
  },
  /**exectution of economic participant service **/
  economicpartservice: function(component, result) {
    component.set('v.refreshSpinner', true);
    var componentTarget = component.find('changespin');
    $A.util.addClass(componentTarget, 'economicp');
    var econpartaction = component.get('c.economicarticipants');
    return new Promise(function(resolve, reject) {
      econpartaction.setParams({
        'encryptedClient': result
      });
      econpartaction.setCallback(this, function(response) {
        var state = response.getState();
        if (state === 'SUCCESS') {
          component.set('v.economicparticipant', response.getReturnValue());
          var economicparticipants = JSON.parse(response.getReturnValue());
          component.set('v.isorphan', economicparticipants.isorphan === null ? false : economicparticipants.isorphan);
          if (economicparticipants.groupinfo.groupid === result) {
            component.set('v.isorphan', economicparticipants.isorphan);
            component.set('v.orphanNumber', economicparticipants.groupinfo.groupid);
          }
          if (economicparticipants.groupinfo) {
            resolve(economicparticipants.groupinfo.groupid);
          } else if (economicparticipants.errormessage || economicparticipants.servicecallerror) {
            component.set('v.nextButtonDisabled', true);
            component.set('v.showmessage', true);
            component.set('v.showall', false);
            component.set('v.economicpartmessage', $A.get('{!$Label.c.Arc_Gen_SpinnerMessageError}') + '.');
            component.set('v.refreshSpinner', 'false');
            reject();
          }
        } else {
          component.set('v.nextButtonDisabled', true);
          component.set('v.refreshSpinner', 'false');
          component.set('v.showmessage', true);
          component.set('v.showall', false);
          component.set('v.economicpartmessage', $A.get('{!$Label.c.Arc_Gen_SpinnerMessageError}') + '.');
        }
      });
      $A.enqueueAction(econpartaction);
    });
  },
  groupstructure: function(component, listparticipant, economicparticipant, clientorgroupnumber, helper) {
    var componentTarget = component.find('changespin');
    $A.util.removeClass(componentTarget, 'economicp');
    $A.util.addClass(componentTarget, 'spins');
    component.set('v.modalHeader', $A.get('{!$Label.c.Lc_arce_GroupStructure}'));
    var groupstructure = component.get('c.constructgroupstructure');
	console.log('listparticipant:'+listparticipant);
	console.log('economicparticipant:'+economicparticipant);
	console.log('clientorgroupnumber:'+clientorgroupnumber);
	console.log('component.get(v.isorphan):'+component.get('v.isorphan'));
    return new Promise(function(resolve, reject) {
      groupstructure.setParams({
        'listparticipant': listparticipant,
        'economicparticipant': economicparticipant,
        'accountNumber': clientorgroupnumber,
        'isOrphan': component.get('v.isorphan')
      });
      groupstructure.setCallback(this, function(response) {
        var state = response.getState();
        if (state === 'SUCCESS') {
          var resp = JSON.parse(response.getReturnValue());
          component.set('v.accounts', JSON.stringify(resp.participantsOnline));
          component.set('v.idofarceexecutor', resp.groupID);
          if (component.get('v.isorphan') && resp.noGroupsInSf === false) {
            component.set('v.groupId', resp.orphanId);
            resolve(resp);
            component.set('v.refreshSpinner', 'false');
            window.setTimeout(function() {
              helper.setButtonVisibility(component);
            }, 1000);
          } else if (resp.noGroupsInSf === false) {
            component.set('v.groupId', resp.groupID);
            resolve(resp);
            component.set('v.refreshSpinner', 'false');
            window.setTimeout(function() {
              helper.setButtonVisibility(component);
            }, 1000);
          } else {
            component.set('v.refreshSpinner', 'false');
            component.set('v.showmessage', true);
            component.set('v.showall', false);
            component.set('v.economicpartmessage', $A.get('{!$Label.c.Arc_Gen_NoGroupInARCE}'));
            component.set('v.nextButtonDisabled', true);
          }
        } else {
          component.set('v.nextButtonDisabled', true);
          component.set('v.refreshSpinner', 'false');
          component.set('v.showmessage', true);
          component.set('v.showall', false);
          component.set('v.economicpartmessage', $A.get('{!$Label.c.Arc_Gen_SpinnerMessageError}') + '...');
        }
      });
      $A.enqueueAction(groupstructure);
    });
  },
  setButtonVisibility: function(component) {
    if (component.get('v.currentStep') === '3') {
      component.set('v.nextButtonDisabled', true);
    } else {
      component.set('v.nextButtonDisabled', false);
    }
  },
  getpreviousArce: function(component, helper) {
    var previousarce = component.get('c.getPreviousArce');
    return new Promise(function(resolve, reject) {
      var accountsWrapp = component.get('v.accounts');
      previousarce.setParams({
        'customerId': component.get('v.hasRecordId'),
        'accountswraper': accountsWrapp
      });
      previousarce.setCallback(this, function(response) {
        var state = response.getState();
        if (state === 'SUCCESS') {
          var arceDataList = response.getReturnValue();
          if (arceDataList[0] === '') {
            component.set('v.NewAnalysis', true);
            component.set('v.isnewanalysis', true);
            component.set('v.clientNumber', arceDataList[2]);
            component.set('v.clientOrGroup', arceDataList[5]);
            resolve();
          } else if (arceDataList[0] !== '') {
            component.set('v.isnewanalysis', false);
            component.set('v.analysisId', arceDataList[0]);
            if (arceDataList[1] === '01') {
              component.set('v.nextButtonDisabled', true);
              component.set('v.clientNumber', arceDataList[2]);
              component.set('v.bankId', arceDataList[3]);
              component.set('v.clientNumber', arceDataList[2]);
              component.set('v.clientOrGroup', arceDataList[5]);
              component.set('v.currentStep', '3');
              component.set('v.refreshSpinner', 'false');
              resolve();
            } else if (arceDataList[1] === '10') {
              component.set('v.clientNumber', arceDataList[2]);
              component.set('v.bankId', arceDataList[3]);
              component.set('v.clientNumber', arceDataList[2]);
              component.set('v.clientOrGroup', arceDataList[5]);
              component.set('v.currentStep', '2');
              component.set('v.refreshSpinner', 'false');
              component.set('v.modalHeader', $A.get('{!$Label.c.Lc_arce_selectAnalysisType}'));
              resolve();
            } else {
              component.set('v.refreshSpinner', 'false');
              helper.redirectAnalysis(component);
              resolve();
            }
          }
        } else {
          reject();
        }
      });
      $A.enqueueAction(previousarce);
    });
  },
  controlNextButton: function(component, event, helper) {
    var buttonDisabled = false;
    var getCurrentStep = component.get('v.currentStep');
    switch (getCurrentStep) {
      case '1':
        buttonDisabled = false;
        break;
      case '2':
        break;
      case '3':
        var warningTotalList = component.get('v.warningTotalList');
        var numberOfChecked = component.get('v.warningListChecked');
        if (numberOfChecked !== warningTotalList) {
          buttonDisabled = true;
        }
        break;
      case '4':
        var typeOfSelectedSector = typeof(component.get('v.selectedSector'));
        if (typeOfSelectedSector === 'undefined') {
          buttonDisabled = true;
        }
        break;
      case '5':
        break;
    }
    component.set('v.nextButtonDisabled', buttonDisabled);
  },
  newAnalysis: function(component, event, helper) {
    var recordId = component.get('v.hasRecordId');
    var sObjectName = component.get('v.sObjectName');
    var newanalysis = component.get('c.setanalysis');
    return new Promise(function(resolve, reject) {
      newanalysis.setParams({
        'recordId': recordId,
        'isorphan': component.get('v.isorphan'),
        'orphanNumber': component.get('v.orphanNumber'),
        'accounts': component.get('v.accounts')
      });
      newanalysis.setCallback(this, function(response) {
        var state = response.getState();
        if (state === 'SUCCESS') {
          var resp = JSON.parse(response.getReturnValue());
          if (resp.status === 'NUEVO') {
            component.set('v.existentAnalysis', 'false');
          } else if (resp.status === 'EXISTENTE') {
            component.set('v.existentAnalysis', 'true');
          }
          component.set('v.clientNumber', resp.clientnumber);
          component.set('v.analysisId', resp.analysisId);
          component.set('v.refreshSpinner', 'false');
          resolve();
        } else {
          var resultsToast = $A.get('e.force:showToast');
          resultsToast.setParams({
            'title': $A.get('{!$Label.c.Lc_arce_newAnalysisError}'),
            'type': 'error',
            'message': response.getError()[0].message,
            'duration': '8000'
          });
          resultsToast.fire();
          $A.get('e.force:closeQuickAction').fire();
          reject();
        }
        component.set('v.currentStep', '3');
        component.set('v.nextButtonDisabled', true);
        component.set('v.modalHeader', $A.get('{!$Label.c.Lc_arce_RequiredInfo}'));
      });
      $A.enqueueAction(newanalysis);
    });
  },
  updatesArceToPreparing: function(component, event, helper) {
    return new Promise(function(resolve, reject) {
      component.set('v.refreshSpinner', true);
      var componentTarget = component.find('changespin');
      $A.util.addClass(componentTarget, 'spinupdate');
      var analysisId = component.get('v.analysisId');
      var action = component.get('c.updateArceToPreparing');
      action.setParams({
        'arceId': analysisId
      });
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === 'SUCCESS') {
          component.set('v.refreshSpinner', false);
          resolve();
        } else {
          component.set('v.refreshSpinner', false);
          reject();
        }
      });
      $A.enqueueAction(action);
    });
  },
  setSector: function(component, event, helper) {
    component.set('v.refreshSpinner', true);
    var componentTarget = component.find('changespin');
    $A.util.addClass(componentTarget, 'sector');
    var selectedSector = component.get('v.selectedSector');
    var analysisId = component.get('v.analysisId');
    var groupId = component.get('v.groupId');
    var customerNumber = component.get('v.customerNumber');
    var action = component.get('c.setClientSector');
    action.setParams({
      analysisId: analysisId,
      selectedSector: selectedSector,
      clientId: groupId
    });
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === 'SUCCESS') {
        var resp = response.getReturnValue();
        if (!resp.sectorResultResponse) {
          var toastError = $A.get('e.force:showToast');
          toastError.setParams({
            'title': $A.get('{!$Label.c.Lc_arce_newAnalysisError}'),
            'type': 'error',
            'mode': 'sticky',
            'duration': '8000',
            'message': resp.sectorDescriptionResponse
          });
          toastError.fire();
        }
        component.set('v.refreshSpinner', false);
      } else {
        var toastEventUE = $A.get('e.force:showToast');
        toastEventUE.setParams({
          'title': $A.get('{!$Label.c.Lc_arce_newAnalysisError}'),
          'type': 'error',
          'mode': 'sticky',
          'duration': '8000',
          'message': JSON.stringify(response.error)
        });
        toastEventUE.fire();
        component.set('v.refreshSpinner', false);
      }
      component.set('v.currentStep', '5');
      component.set('v.modalHeader', $A.get('{!$Label.c.Arc_Gen_ToContinue}'));
    });
    this.controlNextButton(component, event, helper);
    $A.enqueueAction(action);
  },
  callPathService: function(component, event, helper) {
    var analysisId = component.get('v.analysisId');
    var customerId = component.get('v.idofarceexecutor');
    var action = component.get('c.callPathService');
    action.setParams({
      analysisId: analysisId,
      customerId: customerId,
      'isorphan': component.get('v.isorphan')
    });
    action.setCallback(this, function(response) {
      var state = response.getState();
      var resp = response.getReturnValue();
      if (state === 'SUCCESS') {
        if (resp.serviceCode === '200') {
          var resultsToast = $A.get('e.force:showToast');
          resultsToast.setParams({
            'title': $A.get('{!$Label.c.Lc_arce_newAnalysisSuccess}'),
            'type': 'success',
            'message': $A.get('{!$Label.c.Lc_arce_PathSuccess}'),
            'duration': '5000'
          });
          resultsToast.fire();
        } else {
          var serviceErrorToast = $A.get('e.force:showToast');
          serviceErrorToast.setParams({
            'title': $A.get('{!$Label.c.Lc_arce_newAnalysisError}'),
            'type': 'error',
            'message': $A.get('{!$Label.c.Lc_arce_PathServiceError}') + resp.serviceMessage,
            'duration': '5000'
          });
          serviceErrorToast.fire();
        }
      } else {
        var listCustErrorToast = $A.get('e.force:showToast');
        listCustErrorToast.setParams({
          'title': $A.get('{!$Label.c.Lc_arce_newAnalysisError}'),
          'type': 'error',
          'message': $A.get('{!$Label.c.Lc_arce_PathGeneralError}'),
          'duration': '5000'
        });
        listCustErrorToast.fire();
      }
    });
    $A.enqueueAction(action);
  },
  redirectAnalysis: function(component) {
    var analysisId = component.get('v.analysisId');
    $A.get('e.force:closeQuickAction').fire();
    var navEvt = $A.get('e.force:navigateToSObject');
    navEvt.setParams({
      'recordId': analysisId
    });
    navEvt.fire();
    var resultsToast = $A.get('e.force:showToast');
    resultsToast.setParams({
      'title': $A.get('{!$Label.c.Lc_arce_newAnalysisSuccess}'),
      'type': 'success',
      'message': $A.get('{!$Label.c.Lc_arce_redirectingURL}'),
      'duration': '5000'
    });
    resultsToast.fire();
  },
  callPersistenceService: function(component, helper) {
    return new Promise(function(resolve, reject) {
      component.set('v.refreshSpinner', true);
      var componentTarget = component.find('changespin');
      $A.util.addClass(componentTarget, 'persistance');
      var analysisId = component.get('v.analysisId');
      var action = component.get('c.callPersistence');
      action.setParams({
        analysisId: analysisId
      });
      action.setCallback(this, function(response) {
        var state = response.getState();
        var resp = response.getReturnValue();
        if (state === 'SUCCESS') {
          if (resp.serviceCode === '201') {
            var resultsToast = $A.get('e.force:showToast');
            resultsToast.setParams({
              'title': $A.get('{!$Label.c.Lc_arce_newAnalysisSuccess}'),
              'type': 'success',
              'message': $A.get('{!$Label.c.Lc_arce_ListCustSuccess}'),
              'duration': '5000'
            });
            resultsToast.fire();
          }
          component.set('v.refreshSpinner', false);
          resolve();
        } else {
          component.set('v.nextButtonDisabled', true);
          component.set('v.refreshSpinner', false);
          component.set('v.nextButtonDisabled', true);
          component.set('v.showmessage', true);
          component.set('v.showall', false);
          component.set('v.economicpartmessage', $A.get('{!$Label.c.Arc_Gen_OraclePersistenceError}') + '..');
        }
      });
      $A.enqueueAction(action);
    });
  },
  setHeaderLabel: function(component, event, helper) {
    var getCurrentStep = component.get('v.currentStep');
    switch (getCurrentStep) {
      case '1':
        component.set('v.modalHeader', $A.get('{!$Label.c.Lc_arce_GroupStructure}'));
        break;
      case '2':
        component.set('v.modalHeader', $A.get('{!$Label.c.Lc_arce_selectAnalysisType}'));
        break;
      case '3':
        component.set('v.modalHeader', $A.get('{!$Label.c.Lc_arce_RequiredInfo}'));
        break;
      case '4':
        component.set('v.modalHeader', $A.get('{!$Label.c.Lc_arce_clientSectorText}'));
        break;
      case '5':
        component.set('v.modalHeader', $A.get('{!$Label.c.Lc_arce_SectorServiceResponse}'));
        break;
    }
  }
});