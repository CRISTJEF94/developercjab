({
  initHandler: function(component, event, helper) {
    helper.getfullaccountforservices(component, event, helper);
    const sectors = [{
      'label': $A.get('$Label.c.Lc_arce_sectorGeneric'),
      'value': 's-01'
    }, {
      'label': $A.get('$Label.c.Lc_arce_sectorConstruction'),
      'value': 's-02'
    }];
    component.set('v.options', sectors);
  },
  moveNext: function(component, event, helper) {
    var getCurrentStep = component.get('v.currentStep');
    var componentTargets = component.find('changespin');
    var nextStep = '';
    if (getCurrentStep === '1') {
      component.set('v.showmessage', false);
      component.set('v.showall', true);
      component.set('v.currentStep', '2');
      component.set('v.modalHeader', $A.get('{!$Label.c.Lc_arce_selectAnalysisType}'));
    } else if (getCurrentStep === '2') {
      if (component.get('v.NewAnalysis')) {
        component.set('v.refreshSpinner', true);
        var componentTarget = component.find('changespin');
        $A.util.addClass(componentTarget, 'spinarce');
        helper.newAnalysis(component, event, helper);
      }
    } else if (getCurrentStep === '3') {
      component.set('v.refreshSpinner', true);
      $A.util.addClass(componentTargets, 'spinupdate');
      component.set('v.modalHeader', $A.get('{!$Label.c.Lc_arce_clientSectorText}'));
      component.set('v.currentStep', '4');
      let persistance = helper.callPersistenceService(component, helper);
      persistance.then(function() {
        helper.controlNextButton(component, event, helper);
      });
    } else if (getCurrentStep === '4') {
      component.set('v.refreshSpinner', true);
      $A.util.addClass(componentTargets, 'spinupdate');
      let updatearce = helper.updatesArceToPreparing(component, helper);
      updatearce.then(function() {
        let sector = helper.setSector(component, helper);
      });
    }
  },
  moveBack: function(component, event, helper) {
    var getCurrentStep = component.get('v.currentStep');
    if (getCurrentStep === '2') {
      component.set('v.currentStep', '1');
    } else if (getCurrentStep === '3') {
      component.set('v.currentStep', '2');
    } else if (getCurrentStep === '4') {
      component.set('v.currentStep', '3');
    } else if (getCurrentStep === '5') {
      component.set('v.currentStep', '4');
    }
  },
  handleRadioClick: function(cmp, evt, helper) {
    cmp.set('v.analysisType', evt.getSource().get('v.value'));
  },
  handleChangeWarningList: function(component, event, helper) {
    var numberOfChecked = component.get('v.warningListChecked');
    var isChecked = event.getParam('checked');
    if (isChecked === true) {
      numberOfChecked++;
    } else {
      numberOfChecked--;
    }
    component.set('v.warningListChecked', numberOfChecked);
    helper.controlNextButton(component, event, helper);
  },
  handleChangeSectorList: function(component, event, helper) {
    var selectedOptionValue = event.getParam('value');
    var getCurrentStep = component.get('v.currentStep');
    component.set('v.selectedSector', selectedOptionValue);
    helper.controlNextButton(component, event, helper);
  },
  closeModel: function(component, event, helper) {
    $A.get('e.force:closeQuickAction').fire();
  },
  finish: function(component, event, helper) {
    helper.callPathService(component, event, helper);
    window.setTimeout($A.getCallback(function() {
      helper.redirectAnalysis(component);
    }), 2500);
  },
});