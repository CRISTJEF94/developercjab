({
  init: function(component, event, helper) {
    helper.hinit(component, event);
  },
  handleRowAction: function(component, event, helper) {
    var action = event.getParam('action');
    switch (action.name) {
      case 'edit':
        helper.edit(component, event);
        break;
      case 'insert':
        helper.insert(component, event);
        break;
      case 'delete':
        helper.delete(component, event);
        break;
    }
  },
  refreshTable: function(component, event, helper) {
    helper.hinit(component, event);
  },
  handleRefreshEvt: function(component, event, helper) {
    if (event.getParam('table') === 'PositionTable') {
      helper.hinit(component, event);
      if (component.get('v.automaticFunction') === 'true') {
        helper.toastMes(component, event, 'SUCCESS', 'SUCCESS');
        var tabRefresh = $A.get('e.dyfr:SaveObject_evt');
        tabRefresh.setParams({
          'recordId': component.get('v.recordId')
        });
        tabRefresh.fire();
      }
    }
  }
});