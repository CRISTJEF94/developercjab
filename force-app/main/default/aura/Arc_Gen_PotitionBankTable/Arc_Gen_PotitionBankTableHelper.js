({
  hinit: function(component, event) {
    var jsonData = {};
    var action = component.get('c.dataResponse');
    action.setParams({
      'recordId': component.get('v.recordId'),
      'inputClass': component.get('v.inputClass')
    });
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === 'SUCCESS') {
        jsonData = response.getReturnValue();
        if (jsonData.successResponse === true) {
          var columns = jsonData.jsonResponse.columns;
          var data = jsonData.jsonResponse.data;
          columns = this.validateActions(component, event, columns);
          component.set('v.columns', columns);
          component.set('v.rows', data);
          component.set('v.automaticFunction', data[0].info.automatic);
        } else {
          this.toastMes(component, event, 'ERROR', jsonData.errorResponse);
        }
      }
    });
    $A.enqueueAction(action);
  },
  validateActions: function(component, event, columns) {
    if (component.get('v.insert') === 'true' && component.get('v.edit') === 'true' && component.get('v.delete') === 'true') {
      columns = this.setAction(component, columns);
    }
    return columns;
  },
  setAction: function(component, columns) {
    var actions = this.getRowActions.bind(this, component);
    columns.push({ type: 'action', typeAttributes: { rowActions: actions } });
    return columns;
  },
  getRowActions: function(component, row, doneCallback) {
    var actions = [];
    var activateInsert = { label: 'Insert', name: 'insert' };
    var activateEdit = { label: 'Edit', name: 'edit' };
    var activateDelete = { label: 'Delete', name: 'delete' };
    if (row.info.clientType !== 'Group') {
      activateInsert.disabled = row.info.automatic === 'true' ? true : false;
      activateEdit.disabled = false;
      activateDelete.disabled = row.info.automatic === 'true' ? true : false;
    } else {
      activateInsert.disabled = true;
      activateEdit.disabled = false;
      activateDelete.disabled = true;
    }
    actions.push(activateInsert);
    actions.push(activateEdit);
    actions.push(activateDelete);
    setTimeout($A.getCallback(function() {
      doneCallback(actions);
    }), 200);
  },
  edit: function(component, event) {
    let row = event.getParam('row');
    var params = {
      'exposureId': row.name,
      'accHasAnId': component.get('v.recordId'),
      'recordTypeId': row.recordTypeId
    };
    $A.createComponent(
      'c:Arc_Gen_PotitionTableEdition',
      params,
      function(html, status, errorMessage) {
        if (status === 'SUCCESS') {
          component.find('overlayLibra').showCustomModal({
            header: $A.get('$Label.c.Arc_Gen_Edit_Record'),
            body: html,
            showCloseButton: true,
            closeCallback: function() {
              var refreshVar = component.get('v.refreshVariable');
              component.set('v.refreshVariable', !refreshVar);
            }
          });
        }
      });
  },
  insert: function(component, event) {
    var action = component.get('c.getRecordTypeId');
    action.setParams({
      'recordTypeDevName': 'RPS_0001',
      'inputClass': component.get('v.inputClass')
    });
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === 'SUCCESS') {
        var resp = response.getReturnValue();
        var params = {
          'apiNameObject': 'arce__risk_position_summary__c',
          'recordTypeId': resp.recorTypeId,
          'recordId': component.get('v.recordId')
        };
        $A.createComponent(
          'c:Arc_Gen_PotitionTableInsertion',
          params,
          function(html, status, errorMessage) {
            if (status === 'SUCCESS') {
              component.find('overlayLibra').showCustomModal({
                header: $A.get('$Label.c.Arc_Gen_Insert_Record'),
                body: html,
                showCloseButton: true,
                closeCallback: function() {
                  var refreshVar = component.get('v.refreshVariable');
                  component.set('v.refreshVariable', !refreshVar);
                }
              });
            }
          }
        );
      }
    });
    $A.enqueueAction(action);
  },
  delete: function(component, event) {
    let row = event.getParam('row');
    var action = component.get('c.deleteRecord');
    action.setParams({
      'recordId': row.name,
      'inputClass': component.get('v.inputClass')
    });
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === 'SUCCESS') {
        var resp = response.getReturnValue();
        if (resp.successResponse === true) {
          var refreshVar = component.get('v.refreshVariable');
          component.set('v.refreshVariable', !refreshVar);
          this.toastMes(component, event, 'SUCCESS', 'SUCCESS');
        } else {
          this.toastMes(component, event, 'ERROR', resp.errorResponse);
        }
      } else {
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
          'type': 'ERROR',
          'title': 'ERROR!',
          'message': $A.get('$Label.c.Arc_Gen_ApexCallError')
        });
        toastEvent.fire();
      }
    });
    $A.enqueueAction(action);
  },
  toastMes: function(component, event, messageType, messageInfo) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      'type': messageType,
      'title': '',
      'message': messageType === 'SUCCESS' ? $A.get('$Label.c.Arc_Gen_Record_Update_Success') : messageInfo
    });
    toastEvent.fire();
  }
});