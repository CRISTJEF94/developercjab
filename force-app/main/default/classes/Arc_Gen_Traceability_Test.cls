/**********************************************************************************
@Name                   Arc_Gen_Traceability
@Desarrollado por:      BBVA
@Author                 Angel Fuertes Gomez
@Date                   14/05/2019
@Proyecto:              ARCE
@Group                  ARCE
*************************************************************************************
@Description            Test Class to traceability events.
*************************************************************************************
@Changes                (most recent first)
14/05/2019              Creation
/************************************************************************************/
@isTest public class Arc_Gen_Traceability_Test {
    @testSetup static void setup() {
        final arce__Analysis__c newAnalysis = new arce__Analysis__c();
        newAnalysis.Name = 'Analysis';
        insert newAnalysis;

        final arce__Account_has_Analysis__c children = new arce__Account_has_Analysis__c();
        children.arce__Analysis__c = newAnalysis.Id;
        children.arce__Stage__c = '3';
        children.arce__Rating__c = 'AA';
        children.arce__InReview__c = true;
        insert children;
    }
    @isTest static void saveEventTest() {
        final arce__Analysis__c arce = [SELECT Id FROM arce__Analysis__c limit 1];
        Test.startTest();
        Arc_Gen_Traceability.saveEvent(arce.Id, 'Test', 'approve', 'Comments');
        final List<dwp_cvad__Action_Audit__c> audit = [SELECT Id FROM dwp_cvad__Action_Audit__c limit 5];
        System.assertEquals(false,audit.isEmpty(),'If traceability records exist');
        Test.stopTest();
    }
}