/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_getRatingDataService_data
* @Author   Eduardo Efraín Hernández Rendón  eduardoefrain.hernandez.contractor@bbva.com
* @Date     Created: 30/4/2019
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Contains the logic to assign the different rating variables to salesforce fields
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |30/4/2019 eduardoefrain.hernandez.contractor@bbva.com
*             Class creation.
* |23/7/2019  eduardoefrain.hernandez.contractor@bbva.com
*             Added comments and localism
* |2019-12-02 german.sanchez.perez.contractor@bbva.com | franciscojavier.bueno@bbva.com
*             Api names modified with the correct name on business glossary
* |2020-02-27 javier.soto.carrascosa@bbva.com
*             Fix Account Wrapper for rating engine
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_getRatingDataService_data {
/**
* @Class: SaveResult
* @Description: Wrapper that contain the information of a DML Result
* @author BBVA
*/
    public class SaveResult {
        /**
        * @Description: Status of the DML operation
        */
        public String status {get;set;}
        /**
        * @Description: Message if the DML operation fails
        */
        public String message {get;set;}
    }
/**
*-------------------------------------------------------------------------------
* @description Method that calls a service using the iaso component
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId
* @param String customerId
* @return Arc_Gen_getIASOResponse.serviceResponse - Wrapper that contains the response of the called service
* @example public Arc_Gen_getIASOResponse.serviceResponse callRatingService(String analysisId,String customerId)
**/
    public Arc_Gen_getIASOResponse.serviceResponse callRatingService(String parameters, String serviceName) {
        Arc_Gen_getIASOResponse.serviceResponse response = new Arc_Gen_getIASOResponse.serviceResponse();
        response = Arc_Gen_getIASOResponse.calloutIASO(serviceName, parameters);
        Return response;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that gets the valid FFSS from an analyzed client
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId
* @return List<arce__Financial_Statements__c>
* @example public List<arce__Financial_Statements__c> getValidFFSS(String analysisId)
**/
    public List<arce__Financial_Statements__c> getValidFFSS(String analysisId) {
        List<arce__Account_has_Analysis__c> analysis = [SELECT arce__ffss_for_rating_id__c FROM arce__Account_has_Analysis__c WHERE Id =: analysisId LIMIT 1];
        String validId = analysis[0].arce__ffss_for_rating_id__c;
        List<arce__Financial_Statements__c> validFinancialState = [SELECT id,arce__ffss_valid_type__c,arce__rating_id__c,arce__ffss_withRating_type__c FROM arce__Financial_Statements__c WHERE Id =:validId LIMIT 1];
        Return validFinancialState;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that gets the name and the accountNumber of a client
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId
* @return List<String> - Position[0] = Name, Position[1] = AccountNumber
* @example public static List<String> getCustomerData(String analysisId)
**/
    public static List<String> getCustomerData(String analysisId) {
        List<String> customerData = new List<String>();
        arce__Account_has_Analysis__c analysis = [SELECT arce__Customer__c FROM arce__Account_has_Analysis__c WHERE id =: analysisId LIMIT 1];
        final Map<Id, Arc_Gen_Account_Wrapper> mapWrap = Arc_Gen_Account_Locator.getAccountInfoById(new List<Id>{analysis.arce__Customer__c});
        customerData.add(mapWrap.get(analysis.arce__Customer__c).name);
        customerData.add(mapWrap.get(analysis.arce__Customer__c).accNumber);
        Return customerData;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that gets the AccountHasAnalysis from its Id
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId
* @return List<arce__Account_has_Analysis__c>
* @example public List<arce__Account_has_Analysis__c> getAccountHasAnalysis(String analysisId)
**/
    public List<arce__Account_has_Analysis__c> getAccountHasAnalysis(String analysisId) {
        return Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<String>{analysisId});
    }
/**
*-------------------------------------------------------------------------------
* @description Method that gets the existen rating variables of an analyzed client
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId
* @param String ratingId
* @return List<arce__rating_variables_detail__c>
* @example public List<arce__rating_variables_detail__c> getExistentVariables(String analysisId,String ratingId)
**/
    public List<arce__rating_variables_detail__c> getExistentVariables(String analysisId,String ratingId) {
        return [SELECT id,arce__parent_code_number__c,arce__modifier_mix_type__c FROM arce__rating_variables_detail__c WHERE arce__account_has_analysis_id__c =: analysisId AND arce__rating_id__c =: ratingId];
    }
/**
*-------------------------------------------------------------------------------
* @description Method that gets the current rating of an analyzed client
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId
* @return arce__rating__c
* @example public arce__rating__c getCurrentRating(String analysisId)
**/
    public arce__rating__c getCurrentRating(String analysisId) {
        arce__Account_has_Analysis__c analysis = [SELECT arce__ffss_for_rating_id__r.arce__rating_id__c FROM arce__Account_has_Analysis__c WHERE Id =: analysisId];
        String ratingId = analysis.arce__ffss_for_rating_id__r.arce__rating_id__c;
        arce__rating__c currentRating = Arc_Gen_Rating_data.generalRatingData(ratingId);
        Return currentRating;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that gets the leverage results
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId
* @return arce__Account_has_Analysis__c
* @example public arce__Account_has_Analysis__c getLeveragedResults(String analysisId)
**/
    public arce__Account_has_Analysis__c getLeveragedResults(String analysisId) {
        return [SELECT id,arce__anlys_wkfl_sbanlys_status_type__c,arce__ll_before_adj_ind_type__c,arce__ll_before_adj_clsfn_type__c,arce__ll_after_adj_ind_type__c,arce__ll_after_adj_clsfn_type__c FROM arce__Account_has_Analysis__c WHERE Id =: analysisId][0];
    }
/**
*-------------------------------------------------------------------------------
* @description Method that sets an empty rating record
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @return arce__rating_variables_detail__c
* @example public arce__rating_variables_detail__c setRatingVariableObj()
**/
    public arce__rating_variables_detail__c setRatingVariableObj() {
        arce__rating_variables_detail__c ratingObj = new arce__rating_variables_detail__c();
        Return ratingObj;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that update a single record
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param sObject recordToUpdate
* @return SaveResult - A wrapper with the results of a DML operation
* @example public SaveResult updateRecord(sObject recordToUpdate)
**/
    public SaveResult updateRecord(sObject recordToUpdate) {
        final SaveResult updateResults = new SaveResult();
        try {
            updateResults.status = 'true';
            update(recordToUpdate);
        } catch(DmlException ex) {
            updateResults.status = 'false';
            updateResults.message = ex.getMessage();
        }
        Return updateResults;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that update a list of sObjects
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param List<sObject> recordsToUpdate
* @return SaveResult - A wrapper with the results of a DML operation
* @example public SaveResult updateRecords(List<sObject> recordsToUpdate) {
**/
    public SaveResult updateRecords(List<sObject> recordsToUpdate) {
        final SaveResult updateResults = new SaveResult();
        try {
            updateResults.status = 'true';
            update(recordsToUpdate);
        } catch(DmlException ex) {
            updateResults.status = 'false';
            updateResults.message = ex.getMessage();
        }
        Return updateResults;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that inserts a list of sObjects
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param List<sObject> recordsToInsert
* @return SaveResult - A wrapper with the results of a DML operation
* @example public SaveResult insertRecords(List<sObject> recordsToInsert)
**/
    public SaveResult insertRecords(List<sObject> recordsToInsert) {
        final SaveResult insertResults = new SaveResult();
        try {
            insertResults.status = 'true';
            insert(recordsToInsert);
        } catch(DmlException ex) {
            insertResults.status = 'false';
            insertResults.message = ex.getMessage();
        }

        Return insertResults;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that deletes a list of sObjects
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param List<sObject> recordsToDelete
* @return SaveResult - A wrapper with the results of a DML operation
* @example public SaveResult deleteRecords(List<sObject> recordsToDelete)
**/
    public void deleteRecords(List<sObject> recordsToDelete) {
        delete(recordsToDelete);
    }
}