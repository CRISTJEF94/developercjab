/**
* @File Name          : Arc_Gen_TestLeverage_data.cls
* @Description        : Contains the logic to set the proposed limit of the group to a client field
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE Team
* @Last Modified By   : luisruben.quinto.munoz@bbva.com
* @Last Modified On   : 23/7/2019 19:32:50
* @Changes
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    30/4/2019 18:00:36   eduardoefrain.hernandez.contractor@bbva.com     Initial Version
* 1.1    7/5/2019 23:40:12   diego.miguel.contractor@bbva.com     Added comments
**/
public with sharing class Arc_Gen_SetPoliciesVariables_service {
/**
*-------------------------------------------------------------------------------
* @description Method that set the proposed limit of the group to a client field
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param List<String> analysisId
* @example public static leverageTestResponse setupLeverage(String analysisId)
**/
    @InvocableMethod(label='Set Proposed Amount')
    public static void setupPoliciesVariables(List<String> analysisId) {
        Arc_Gen_SetPoliciesVariables_data locator = new Arc_Gen_SetPoliciesVariables_data();
        List<arce__Account_has_Analysis__c> groupStructure = locator.getGroupStructure(analysisId[0]);
        arce__limits_exposures__c limitExposure = locator.getLimitExposures(analysisId[0]);
        for(arce__Account_has_Analysis__c item : groupStructure) {
            item.arce__current_proposed_local_amount__c = limitExposure.arce__current_proposed_amount__c;
        }
        locator.updateRecords(groupStructure);
    }
}