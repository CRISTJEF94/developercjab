/**
* --------------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_NAHA_Service
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2019-10-12
* @Group    ARCE
* --------------------------------------------------------------------------------------------------------
* @Description Service class for Arc_Gen_NAHACtrl
* --------------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-10-12 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* |2020-01-15 javier.soto.carrascosa@bbva.com
*             Add support for account update from Account Wrapper interface
* |2020-01-29 javier.soto.carrascosa@bbva.com
*             Do not allow to add element in carrusel if risk assessment fails
* --------------------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_NAHA_Service {
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   mariohumberto.ramirez.contractor@bbva.com
    * @Date     2019-10-12
    * @param void
    * @return void
    * @example Arc_Gen_NAHA_Service Service = new Arc_Gen_NAHA_Service()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_NAHA_Service() {

    }
    /*----------------------------------------------------------------------------------------------------
    *@Description Naha Service
    * ----------------------------------------------------------------------------------------------------
    * @Author   LUIS RUBEN QUINTO MUÑOZ
    * @Date     2019-07-28
    * @param    Id idRecord - Id of the Account has Analysis selected
    * @param    String field - Field selected
    * @param    Boolean value - If is true, the analyzed client is visible in the carousel
    * @return   qvcd.GBL_CardDetails_Ctrl.CardPagerWrapper - Wrapper that contains the response of the
    *           quick view card
    * @example  validElementInCarousel(Id idRecord, String field, boolean value)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static qvcd.GBL_CardDetails_Ctrl.CardPagerWrapper validElementInCarousel(Id idRecord, String field, boolean value) {
        qvcd.GBL_CardDetails_Ctrl.CardPagerWrapper response = new qvcd.GBL_CardDetails_Ctrl.CardPagerWrapper();
        final List<Arc_Gen_ServiceAndSaveResponse> results = new List<Arc_Gen_ServiceAndSaveResponse>();
        final Arc_Gen_ListCustomersService_service service = new Arc_Gen_ListCustomersService_service();
        final Arc_Gen_getPathDataService_data locator = new Arc_Gen_getPathDataService_data();
        Arc_Gen_ServiceAndSaveResponse listCust = new Arc_Gen_ServiceAndSaveResponse();
        Arc_Gen_ServiceAndSaveResponse riskAss = new Arc_Gen_ServiceAndSaveResponse();
        Arc_Gen_ServiceAndSaveResponse pahtEngine = new Arc_Gen_ServiceAndSaveResponse();
        final Map<Id, Map<String,Object>> accUp = new Map<Id, Map<String,Object>>();
        Arc_Gen_PersistanceArceId_service servicePers = new Arc_Gen_PersistanceArceId_service();
        response.gblResultResponse = true;
        final List<String> clientNumber = idRecord == null ? null : Arc_Gen_getPathDataService_service.getCustomerData(idRecord);
        arce__account_has_analysis__c listup = new arce__account_has_analysis__c(Id = idRecord);
        try {
            riskAss = servicePers.setupRiskAssessments(clientNumber[0],clientNumber[1]);
            Arc_Gen_GenericUtilities.populateObjFromMap(listup, riskAss.updatefields);
            results.add(riskAss);
        } catch (Exception callEx) {
            response.gblResultResponse = false;
            response.gblDescriptionResponse = callEx.getMessage();
        }
        try {
            listCust = service.setupListCustomers(clientNumber[2], false);
            results.add(listCust);
            accUp.put((Id)clientNumber[1],listCust.updatefields);
        } catch (Exception callEx) {
            response.gblDescriptionResponse = callEx.getMessage();
        }
        try {
            pahtEngine = Arc_Gen_NAHA_Service_Helper.callPathService(idRecord);
            results.add(pahtEngine);
            Arc_Gen_GenericUtilities.populateObjFromMap(listup, pahtEngine.updatefields);
        } catch (Exception callEx) {
            response.gblDescriptionResponse = callEx.getMessage();
        }
        try {
            locator.updateRecords(new List<arce__account_has_analysis__c>{listup});
            Arc_Gen_Account_Locator.accountUpdate(accUp);
            Arc_Gen_Expandible_Table_Service.verifyTypologiesInserted(idRecord);
            Arc_Gen_PotitionBankTable_Service.verifyDataInserted(idRecord);
        } catch (Exception ex) {
            response.gblDescriptionResponse = ex.getMessage();
        }
        return response;
    }
    /*----------------------------------------------------------------------------------------------------
    *@Description validates permission to add element to carrusel with arce allocation component
    * ----------------------------------------------------------------------------------------------------
    * @Author   Javier Soto Carrascosa
    * @Date     2020-01-29
    * @param    Id idRecord - Id of the Account has Analysis selected
    * @return    Boolean value - If is true, the analyzed cliente can be added or removed
    * @example  public static boolean validateAddElement(Id idRecord)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static boolean validateAddElement(Id idRecord) {
        final List<ID> records = new List<ID>();
        records.add(idRecord);
        final Type inter = Type.forName('arcealloc.Allocation_Service');
        final boolean custAlloc = String.isNotBlank(String.valueOf(inter)) ? ((Map<Id,boolean>) ((Callable) inter.newInstance()).call('checkBulkPrivileges', new Map<String, Object> {'accHasAnlysIdsLst' => records})).get(idRecord) : true ;
        return custAlloc;
    }
}