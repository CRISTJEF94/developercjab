/*------------------------------------------------------------------
* @FileName: Arc_Gen_ProposeInPreparation_Test.cls
* @Author:        Angel Fuertes Gomez - angel.fuertes2@bbva.com / BBVA DWP
* @Group:       ARCE - BBVA Bancomer
* @Description:   test for method class Arc_Gen_ProposeInPreparation_controller.
* @Project:         ARCE - BBVA Bancomer
* @Changes:
*_______________________________________________________________________________________
*Version    Date           Author                               Description
*1.0        26/04/2019     Angel Fuertes Gomez                      Creation.
*1.1        28/06/2019     Ismael Obregon Uribe                     Add function of rating.
-----------------------------------------------------------------------------------------*/
@isTest
public with sharing class Arc_Gen_ProposeInPreparation_Test {
    /**
    * @Description: String with external id of test group
    */
    static final string GROUP_ID = 'G000001';
    /**
    * @Description: String with external id of test subsidiary
    */
    static final string SUBSIDIARY_ID = 'C000001';
    /**
    * @Description: String with external id of test subsidiary
    */
    static final string SUBSIDIARY_ID2 = 'C000002';
    /**
    * @Method:      method that creates the data to use in the test.
    * @Description: testing method.
    */
    String accHasId;
    /*
        @Description: Exception error tests KO
    */
    static final String SCRIPT_EXCEPTION = 'Script-thrown exception';
    /*
        @Description: Name of the sector
    */
    static final String SECTOR_NAME = 's-01';
    /**
    * @Method:      method that creates the data to use in the test.
    * @Description: testing method.
    */
    @testSetup static void setup() {
        Arc_UtilitysDataTest_tst.setupAcccounts();
        final List<sObject> iasoCnfList = Arc_UtilitysDataTest_tst.crearIasoUrlsCustomSettings();
        insert iasoCnfList;

        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{GROUP_ID,SUBSIDIARY_ID,SUBSIDIARY_ID2});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(GROUP_ID);
        final Arc_Gen_Account_Wrapper childAccount = groupAccWrapper.get(SUBSIDIARY_ID);
        final Arc_Gen_Account_Wrapper childAccount2 = groupAccWrapper.get(SUBSIDIARY_ID2);

        final arce__Sector__c newSector = arc_UtilitysDataTest_tst.crearSector('Generic', '100;300;400', SECTOR_NAME, null);
        insert newSector;

        arce__Analysis__c newArce = Arc_UtilitysDataTest_tst.crearArceAnalysis('Arce Analysis', null, groupAccount.accId);
        newArce.arce__Stage__c = '1';
        newArce.arce__analysis_customer_relation_type__c = '01';
        insert newArce;

        arce__Account_has_Analysis__c newAnalysis = Arc_UtilitysDataTest_tst.crearAccHasAnalysis(newSector.id, newArce.Id, groupAccount.accId, SECTOR_NAME);
        newAnalysis.arce__group_asset_header_type__c = '1';
        newAnalysis.arce__InReview__c = true;
        newAnalysis.arce__path__c = 'MAC';
        insert newAnalysis;

        arce__Account_has_Analysis__c newAnalysis2 = Arc_UtilitysDataTest_tst.crearAccHasAnalysis(newSector.id, newArce.Id, childAccount.accId, SECTOR_NAME);
        newAnalysis2.arce__InReview__c = true;
        newAnalysis2.arce__path__c = 'MAC';
        insert newAnalysis2;

        arce__Account_has_Analysis__c newAnalysis3 = Arc_UtilitysDataTest_tst.crearAccHasAnalysis(newSector.id, newArce.Id, childAccount2.accId, SECTOR_NAME);
        newAnalysis3.arce__InReview__c = true;
        newAnalysis3.arce__path__c = 'MAC';
        insert newAnalysis3;

        final arce__rating__c rating = Arc_UtilitysDataTest_tst.crearRating(null);
        insert rating;

        arce__Financial_Statements__c finStatement = Arc_UtilitysDataTest_tst.crearFinStatement(groupAccount.accId, newAnalysis2.id, rating.id, null);
        finStatement.arce__financial_statement_end_date__c = Date.today();
        finStatement.arce__ffss_valid_for_rating_type__c = '1';
        insert finStatement;

        newAnalysis2.arce__ffss_for_rating_id__c = finStatement.Id;
        Update newAnalysis2;

        Arc_UtilitysDataTest_tst.createAssessmentData();
        Arc_UtilitysDataTest_tst.createBankingRelationshipDepData();
        Arc_UtilitysDataTest_tst.createBasicDataData();
        Arc_UtilitysDataTest_tst.createFinancialRiskData();
        Arc_UtilitysDataTest_tst.createBusinessRiskData();
        Arc_UtilitysDataTest_tst.createIndustryAnalysisData();
        Arc_UtilitysDataTest_tst.createPolicies();
    }
    /**
    * @Method:      test for method validateRatingInPreparationTest.
    * @Description: testing method.
    */
    @isTest static void validateRatingInPreparationTestOK() {
        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{GROUP_ID,SUBSIDIARY_ID});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(GROUP_ID);
        final arce__Sector__c newSector = Arc_UtilitysDataTest_tst.crearSector(SECTOR_NAME, '100', SECTOR_NAME, null);
        insert newSector;
        final arce__Analysis__c newArce = Arc_UtilitysDataTest_tst.crearArceAnalysis('Arce Analysis', null, groupAccount.accId);
        insert newArce;
        final arce__Account_has_Analysis__c newAnalysis = Arc_UtilitysDataTest_tst.crearAccHasAnalysis(newSector.id, newArce.Id, groupAccount.accId, SECTOR_NAME);
        insert newAnalysis;
        final arce__rating__c rating = Arc_UtilitysDataTest_tst.crearRating(null);
        insert rating;
        final arce__Financial_Statements__c finStatement = Arc_UtilitysDataTest_tst.crearFinStatement(groupAccount.accId, newAnalysis.id, rating.id, null);
        finStatement.arce__financial_statement_end_date__c = Date.today();
        finStatement.arce__ffss_valid_type__c = '1';
        insert finStatement;
        final arce__Financial_Statements__c finStatement2 = Arc_UtilitysDataTest_tst.crearFinStatement(groupAccount.accId, newAnalysis.id, rating.id, null);
        finStatement2.arce__financial_statement_end_date__c = Date.today();
        insert finStatement2;
        final String[] states = New List<String>();
        states.add(finStatement.Id);
        Test.startTest();
        final arce__Account_has_Analysis__c aac = [SELECT Id FROM arce__Account_has_Analysis__c where arce__group_asset_header_type__c = '1' ORDER by id desc limit 1];
        final Boolean flagTest = Arc_Gen_ProposeInPreparation_controller.validateRatingInPreparation(aac.Id);
        System.assertEquals(false,flagTest,'Returns a boolean value (true) if the rating is invalid');
        Test.stopTest();
    }
    /**
    * @Method:      test for method validateRatingInPreparationTest.
    * @Description: testing method.
    */
    @isTest static void validateRatingInPreparationTestKO() {
        try {
            final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{GROUP_ID,SUBSIDIARY_ID});
            final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(GROUP_ID);
            final arce__Sector__c newSector = Arc_UtilitysDataTest_tst.crearSector(SECTOR_NAME, '100', SECTOR_NAME, null);
            insert newSector;
            final arce__Analysis__c newArce = Arc_UtilitysDataTest_tst.crearArceAnalysis('Arce Analysis', null, groupAccount.accId);
            insert newArce;
            final arce__Account_has_Analysis__c newAnalysis = Arc_UtilitysDataTest_tst.crearAccHasAnalysis(newSector.id, newArce.Id, groupAccount.accId, SECTOR_NAME);
            insert newAnalysis;
            final arce__rating__c rating = Arc_UtilitysDataTest_tst.crearRating(null);
            insert rating;
            final arce__Financial_Statements__c finStatement = Arc_UtilitysDataTest_tst.crearFinStatement(groupAccount.accId, newAnalysis.id, rating.id, null);
            finStatement.arce__financial_statement_end_date__c = Date.today();
            finStatement.arce__ffss_valid_type__c = '1';
            insert finStatement;
            final arce__Financial_Statements__c finStatement2 = Arc_UtilitysDataTest_tst.crearFinStatement(groupAccount.accId, newAnalysis.id, rating.id, null);
            finStatement2.arce__financial_statement_end_date__c = Date.today();
            insert finStatement2;
            final String[] states = New List<String>();
            states.add(finStatement.Id);
            Test.startTest();
            Arc_Gen_ProposeInPreparation_controller.validateRatingInPreparation(newArce.Id);
            Test.stopTest();
        } catch(Exception ex) {
            System.assert(ex.getMessage().contains('Script'), SCRIPT_EXCEPTION);
        }
    }
    /**
    * @Method:      test for method initDelegation (Arc_Gen_ProposeInPreparation_controller)
    * @Description: testing method.
    */
    @isTest static void initDelegationTestOK() {
        final arce__Account_has_Analysis__c accHasAnalysis = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
        Test.startTest();
        final String str = Arc_Gen_ProposeInPreparation_controller.initDelegation(accHasAnalysis.Id);
        final Arc_Gen_Delegation_Wrapper wrapperSerialize = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) str, Arc_Gen_Delegation_Wrapper.class);
        System.assertEquals('1', wrapperSerialize.sanctionAmbit, 'Assert sanction ambit of the wrapper');
        Test.stopTest();
    }
    /**
    * @Method:      test for method initDelegation (Arc_Gen_ProposeInPreparation_controller)
    * @Description: testing method.
    */
    @isTest static void initDelegationTestKO() {
        try {
            final arce__Analysis__c analysis = [SELECT Id, Name FROM arce__Analysis__c WHERE Name = 'Arce Analysis' LIMIT 1];
            Test.startTest();
            Arc_Gen_ProposeInPreparation_controller.initDelegation(analysis.Id);
            Test.stopTest();
        } catch(Exception ex) {
            System.assert(ex.getMessage().contains('Script'), SCRIPT_EXCEPTION);
        }
    }
    /**
    * @Method:      test for method evaluateDelegation OK (Arc_Gen_ProposeInPreparation_controller)
    * @Description: testing method.
    */
    @isTest static void evaluateDelegationTestOK() {
        final arce__Account_has_Analysis__c accHasAnalysis = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
        Test.startTest();
        final String wrapperStr = Arc_Gen_ProposeInPreparation_controller.initDelegation(accHasAnalysis.Id);
        final String wrapperRetStr = Arc_Gen_ProposeInPreparation_controller.evaluateDelegation(wrapperStr, accHasAnalysis.Id);
        final Arc_Gen_Delegation_Wrapper wrapperSerialize = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) wrapperRetStr, Arc_Gen_Delegation_Wrapper.class);
        System.assertEquals('1', wrapperSerialize.sanctionAmbit, 'Sanction ambit equal to 1');
        Test.stopTest();
    }
    /**
    * @Method:      test for method evaluateDelegation KO (Arc_Gen_ProposeInPreparation_controller)
    * @Description: testing method.
    */
    @isTest static void evaluateDelegationTestKO() {
        try {
            final arce__Analysis__c analysis = [SELECT Id, Name FROM arce__Analysis__c WHERE Name = 'Arce Analysis' LIMIT 1];
            final arce__Account_has_Analysis__c accHasAnalysis = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
            Test.startTest();
            final String wrapperStr = Arc_Gen_ProposeInPreparation_controller.initDelegation(accHasAnalysis.Id);
            final Arc_Gen_Delegation_Wrapper wrapperSerialize = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) wrapperStr, Arc_Gen_Delegation_Wrapper.class);
            Arc_Gen_ProposeInPreparation_controller.evaluateDelegation(wrapperSerialize, analysis.Id);
            Test.stopTest();
        } catch(Exception ex) {
            System.assert(ex.getMessage().contains('Script'), SCRIPT_EXCEPTION);
        }
    }
    /**
    * @Method:      test for method saveAction OK (Arc_Gen_ProposeInPreparation_controller)
    * @Description: testing method.
    */
    @isTest static void saveActionTestAmbitUserOK() {
        final arce__Account_has_Analysis__c miniArce = [SELECT Id FROM arce__Account_has_Analysis__c limit 1];
        Test.startTest();
        final Arc_Gen_Delegation_Wrapper wrapper = Arc_UtilitysDataTest_tst.crearDelegationWrapper();
        final String res = Arc_Gen_ProposeInPreparation_controller.saveAction(JSON.serialize(wrapper), miniArce.Id, '1');
        final Arc_Gen_Delegation_Wrapper wrapperSerialize = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) res, Arc_Gen_Delegation_Wrapper.class);
        System.assertEquals(500, wrapperSerialize.codStatus, 'Status delegation wrapper OK');
        Test.stopTest();
    }
    /**
    * @Method:      test for method saveAction OK (Arc_Gen_ProposeInPreparation_controller)
    * @Description: testing method.
    */
    @isTest static void saveActionTestOK() {
        final arce__Account_has_Analysis__c miniArce = [SELECT Id FROM arce__Account_has_Analysis__c limit 1];
        Test.startTest();
        final Arc_Gen_Delegation_Wrapper wrapper = Arc_UtilitysDataTest_tst.crearDelegationWrapper();
        final String res = Arc_Gen_ProposeInPreparation_controller.saveAction(JSON.serialize(wrapper), miniArce.Id, '2');
        final Arc_Gen_Delegation_Wrapper wrapperSerialize = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) res, Arc_Gen_Delegation_Wrapper.class);
        System.assertEquals(201, wrapperSerialize.codStatus, 'Status delegation wrapper OK');
        Test.stopTest();
    }
    /**
    * @Method:      test for method saveAction KO (Arc_Gen_ProposeInPreparation_controller)
    * @Description: testing method.
    */
    @isTest static void saveActionTestKO() {
        try {
            final arce__Account_has_Analysis__c miniArce = [SELECT Id FROM arce__Account_has_Analysis__c limit 1];
            Test.startTest();
            final Arc_Gen_Delegation_Wrapper wrapper = Arc_UtilitysDataTest_tst.crearDelegationWrapper();
            Arc_Gen_ProposeInPreparation_controller.saveAction(wrapper, miniArce.Id, '1');
            Test.stopTest();
        } catch(Exception ex) {
            System.assert(ex.getMessage().contains('Script'), SCRIPT_EXCEPTION);
        }
    }
}