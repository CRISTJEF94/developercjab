/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_getRatingDataService_service
* @Author   Eduardo Efraín Hernández Rendón  eduardoefrain.hernandez.contractor@bbva.com
* @Date     Created: 30/4/2019
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Contains the logic to assign the different rating variables to salesforce fields
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |30/4/2019 eduardoefrain.hernandez.contractor@bbva.com
*             Class creation.
* |23/7/2019  eduardoefrain.hernandez.contractor@bbva.com
*             Refactor
* |26/9/2019  javier.soto.carrascosa@bbva.com
*             Remove mock
* |22/10/2019 eduardoefrain.hernandez.contractor@bbva.com
*             Refactor
* |2019-12-02 german.sanchez.perez.contractor@bbva.com | franciscojavier.bueno@bbva.com
*             Api names modified with the correct name on business glossary
* |24-01-2020 juanmanuel.perez.ortiz.contractor@bbva.com
*             Change logic of parameters used in ASO services
* -----------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_getRatingDataService_service {
    /**
    * @Description: Name of the EncryptionFlag custom metadata
    */
    private final static String ENCRYPTION_FLAG = 'EncryptionFlag';
    /**
    * @Description: Name of the bankId custom setting
    */
    private final static String BANK_ID = '0001';
    /**
    * @Description: Instance of the data access class
    */
    private static Arc_Gen_getRatingDataService_data locator = new Arc_Gen_getRatingDataService_data();
/**
*-------------------------------------------------------------------------------
* @description Method that gets the response from the service class
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId
* @param String customerId
* @return Arc_Gen_ServiceAndSaveResponse - A wrapper with the result of a DML operation and service call
* @example public static Arc_Gen_ServiceAndSaveResponse setupRating(String analysisId,String customerId)
**/
    public static Arc_Gen_ServiceAndSaveResponse setupRating(String analysisId, String customerId, String mockCode, String serviceMock) {
        Arc_Gen_ServiceAndSaveResponse serviceAndSaveResp = new Arc_Gen_ServiceAndSaveResponse();
        Arc_Gen_getRatingDataService_helper.HelperParameters helperWrapper = new Arc_Gen_getRatingDataService_helper.HelperParameters();
        String customerNumber = customerId;
        if(Boolean.valueOf(Arc_Gen_Arceconfigs_locator.getConfigurationInfo(ENCRYPTION_FLAG)[0].Value1__c)) {
            customerNumber = Arc_Gen_CallEncryptService.getEncryptedClient(customerNumber);
        }
        List<arce__Financial_Statements__c> validFinancialSt = locator.getValidFFSS(analysisId);
        helperWrapper.analysisId = analysisId;
        helperWrapper.customerNumber = customerNumber;
        helperWrapper.validFinancialSt = validFinancialSt;
        helperWrapper.bankId = BANK_ID;
        helperWrapper.serviceMock = serviceMock;
        helperWrapper.mockCode = mockCode;
        if(validFinancialSt[0].arce__ffss_valid_type__c == '1' && customerNumber != System.Label.Cls_arce_GRP_glbError) {
            serviceAndSaveResp = Arc_Gen_getRatingDataService_helper.processSuccess(helperWrapper);
        } else {
            serviceAndSaveResp.saveStatus = 'false';
            serviceAndSaveResp.saveMessage = customerNumber == 'false' ? System.Label.Cls_arce_EncryptClientError : System.Label.Cls_arce_RatingNoValidFFSS;
        }
        Return serviceAndSaveResp;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that obtains the customer data form the data class
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId
* @return List<String>
* @example public static List<String> getCustomerData(String analysisId)
**/
    public static List<String> getCustomerData(String analysisId) {
        List<String> customerData = new List<String>();
        customerData = Arc_Gen_getRatingDataService_data.getCustomerData(analysisId);
        Return customerData;
    }
}