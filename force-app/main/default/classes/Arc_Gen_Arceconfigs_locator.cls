/**
* @File Name          : public class Arc_Gen_Arceconfigs_locator.cls
* @Description        : Class that obtains the data of Arce configurations
* @Author             : javier.soto.carrascosa@bbva.com
* @Group              : ARCE Team
* @Last Modified By   : javier.soto.carrascosa@bbva.com
* @Last Modified On   : 21/10/2019 9:05:00
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author                 Modification
*==============================================================================
* 1.0    21/10/2019 9:05:00   javier.soto.carrascosa@bbva.com     Initial Version
**/
public without sharing class Arc_Gen_Arceconfigs_locator {
    private Arc_Gen_Arceconfigs_locator() {
    }
/**
*-------------------------------------------------------------------------------
* @description  Get Configuration Information
*--------------------------------------------------------------------------------
* @author javier.soto.carrascosa@bbva.com
* @date 21/10/2019 14:50:32
* @param configurationName - String with the configuration name
* @return Arce_Config__mdt
* @example public Arce_Config__mdt getConfigurationInfo(String configurationName)
*--------------------------------------------------------------------------------
**/
    public static List<Arce_Config__mdt> getConfigurationInfo(String configurationName) {
        List<Arce_Config__mdt> mdtConfigList = new list<Arce_Config__mdt>([SELECT Id, Value1__c FROM Arce_Config__mdt where DeveloperName=:configurationName]);
        if (mdtConfigList.isEmpty()) {
            mdtConfigList = new List<Arce_Config__mdt>();
        }
        return mdtConfigList;
    }
}