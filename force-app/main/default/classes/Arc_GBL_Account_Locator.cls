/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_GBL_Account_Locator
* @Author   juanignacio.hita.contractor@bbva.com
* @Date     Created: 15/11/2019
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Class "Arc_GBL_Account_Locator"
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |2019-11-15 juanignacio.hita.contractor@bbva.com
*             Class creation
* |2019-11-29 manuelhugo.castillo.contractor@bbva.com
*             Add methods 'getAccountByAccNumber','accountsForLookup'
* |2020-01-06 mariohumberto.ramirez.contractor@bbva.com
*             Fix method getParticipantType, added orphan validation
* |2020-01-15 javier.soto.carrascosa@bbva.com
*             Add accountUpdate, createGroup methods
* |2020-01-28 mariohumberto.ramirez.contractor@bbva.com
*             Add random values to the account wrapper
* |2020-01-29 javier.soto.carrascosa@bbva.com
*             Add docnumber, doctype remove orphan value
* |2020-01-29 juanmanuel.perez.ortiz.contractor@bbva.com
*             Remove companyEcoAct, accActivity, rtDevName from logic of account wrapper
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_GBL_Account_Locator implements Arc_Gen_Account_Interface {
    /**
        * @Description: String with value "Group"
    */
    static final string S_GROUP = 'GROUP';
    /**
        * @Description: String with value "Client"
    */
    static final string CLIENT = 'SUBSIDIARY';
    /**
    *-------------------------------------------------------------------------------
    * @description  Method "getAccountInfoById" that retrieves full user information from a list of account Id
    --------------------------------------------------------------------------------
    * @author juanignacio.hita.contractor@bbva.com
    * @date 2019-11-15
    * @param List<Id> listAccountId
    * @return   Map with Account Wrappers
    * @example Map<Id, Arc_Gen_Account_Wrapper> list = Arc_GBL_Account_Locator.getAccountInfoById(listAccountId);
    **/
    public Map<Id, Arc_Gen_Account_Wrapper> getAccountInfoById(List<Id> listAccountId) {
        Map<Id, Arc_Gen_Account_Wrapper> mapWrapper = new Map<Id, Arc_Gen_Account_Wrapper>();
        try {
            final List<Account> lstAcc = [SELECT Id, CurrencyIsoCode, Name, AccountNumber, OwnerId, RecordTypeId, company_economic_activity_desc__c,
                company_economic_activity_id__c, bank_id__c, bbva_financial_debt_share_per__c, bbva_total_debt_amount__c, CreatedById, participant_seniority_age_number__c,
                economic_capital_amount__c, NumberOfEmployees, controlled_by_sponsor_type__c, taxpayer_id__c, company_foundation_date__c, hats_risk_qualification_number__c,
                country_name__c, LastModifiedById, financial_debt_total_amount__c, ParentId, capital_provision_per__c, Rating,stage_collective_type__c, subsector_desc__c,
                RecordType.DeveloperName
                FROM Account WHERE Id =: listAccountId];
            for(Account acc : lstAcc) {
                final Arc_Gen_Account_Wrapper wrapper = new Arc_Gen_Account_Wrapper();
                wrapper.accId = acc.Id;
                wrapper.accNumber = acc.AccountNumber;
                wrapper.name = acc.Name;
                wrapper.bankId = acc.bank_id__c;
                wrapper.participantType = acc.ParentId == null ? S_GROUP : CLIENT;
                wrapper.docType = 'EXAMPLE';
                wrapper.docNumber = '0000000A';
                wrapper.participantOwnerId = acc.OwnerId;
                wrapper.accParentId = acc.ParentId;
                wrapper.currentLimit = math.random() * 100;
                wrapper.commited = math.random() * 100;
                wrapper.unCommited = math.random() * 100;
                wrapper.outstanding =  math.random() * 100;
                wrapper.currencyType = 'EUR';
                wrapper.unit = 'UNIT';
                mapWrapper.put(acc.Id, wrapper);
            }
        } catch(Exception ex) {
            final Arc_Gen_Account_Wrapper wrapper = new Arc_Gen_Account_Wrapper();
            wrapper.error = ex.getMessage();
            mapWrapper.put(null, wrapper);
        }
        return mapWrapper;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description  Method "getAccountByAccNumber" gets all account fields filtered by AccountNumber
    --------------------------------------------------------------------------------
    * @author manuelhugo.castillo.contractor@bbva.com
    * @date 2019-11-25
    * @param List<String> listAccountNum
    * @return   Map with Account Wrappers
    * @example Map<Id, Arc_Gen_Account_Wrapper> list = Arc_GBL_Account_Locator.getAccountByAccNumber(listAccountNum);
    **/
    public Map<String, Arc_Gen_Account_Wrapper> getAccountByAccNumber(List<String> listAccountNum) {
        final Map<String, Arc_Gen_Account_Wrapper> mapWrapper = new Map<String, Arc_Gen_Account_Wrapper>();
        try {
            final List<Account> lstAcc = [SELECT Id, CurrencyIsoCode, Name, AccountNumber, OwnerId, RecordTypeId, company_economic_activity_desc__c,
                company_economic_activity_id__c, bank_id__c, bbva_financial_debt_share_per__c, bbva_total_debt_amount__c, CreatedById, participant_seniority_age_number__c,
                economic_capital_amount__c, NumberOfEmployees, controlled_by_sponsor_type__c, taxpayer_id__c, company_foundation_date__c, hats_risk_qualification_number__c,
                country_name__c, LastModifiedById, financial_debt_total_amount__c, ParentId, capital_provision_per__c, Rating,stage_collective_type__c, subsector_desc__c
                FROM Account WHERE AccountNumber IN: listAccountNum];
            for(Account acc : lstAcc) {
                final Arc_Gen_Account_Wrapper wrapper = new Arc_Gen_Account_Wrapper();
                wrapper.accId = acc.Id;
                wrapper.name = acc.Name;
                wrapper.accNumber = acc.AccountNumber;
                wrapper.bankId = acc.bank_id__c;
                wrapper.docType = 'EXAMPLE';
                wrapper.docNumber = '0000000A';
                wrapper.participantType = acc.ParentId == null ? S_GROUP : CLIENT;
                wrapper.participantOwnerId = acc.OwnerId;
                wrapper.accParentId = acc.ParentId;
                mapWrapper.put(acc.AccountNumber, wrapper);
            }
        } catch(Exception ex) {
            final Arc_Gen_Account_Wrapper wrapper = new Arc_Gen_Account_Wrapper();
            wrapper.error = ex.getMessage();
            mapWrapper.put(null, wrapper);
        }
        return mapWrapper;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description  Method "accountsForLookup" gets account fields filtered by searchWord
    --------------------------------------------------------------------------------
    * @author manuelhugo.castillo.contractor@bbva.com
    * @date 2019-11-25
    * @param String searchWord
    * @return   Account Wrappers List
    * @example List<Arc_Gen_Account_Wrapper> map = Arc_GBL_Account_Locator.accountsForLookup(String searchWord)
    **/
    public List<Arc_Gen_Account_Wrapper> accountsForLookup(String searchWord) {
        final List<Arc_Gen_Account_Wrapper> lstAccWrapper = new List<Arc_Gen_Account_Wrapper>();
        final String searchQuery = 'FIND \'' + String.escapeSingleQuotes(searchWord) + '\' IN ALL FIELDS RETURNING Account (id, name, OwnerId, ParentId)';
        final List < List < Account >> accountsforshow = search.query(searchQuery);
        for(Account acc : accountsforshow[0]) {
            final Arc_Gen_Account_Wrapper wrapper = new Arc_Gen_Account_Wrapper();
            wrapper.accId = acc.Id;
            wrapper.name = acc.Name;
            wrapper.participantOwnerId = acc.OwnerId;
            lstAccWrapper.add(wrapper);
        }
        return lstAccWrapper;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description  Method "getClientsByGroup" that retrieves Account Wrappers filter by Group
    --------------------------------------------------------------------------------
    * @author manuelhugo.castillo.contractor@bbva.com
    * @date 2019-12-05
    * @param List<Id> lstGroupId
    * @return  Account Wrappers List
    * @example List<Arc_Gen_Account_Wrapper> list = Arc_GBL_Account_Locator.getClientsByGroup(lstGroupId);
    **/
    public List<Arc_Gen_Account_Wrapper> getClientsByGroup(List<Id> lstGroupId) {
        final List<Arc_Gen_Account_Wrapper> lstAccWrapper = new List<Arc_Gen_Account_Wrapper>();
        try {
            final List<Account> lstAcc = [SELECT Id, CurrencyIsoCode, Name, AccountNumber, OwnerId, RecordTypeId, company_economic_activity_desc__c,
                company_economic_activity_id__c, bank_id__c, bbva_financial_debt_share_per__c, bbva_total_debt_amount__c, CreatedById, participant_seniority_age_number__c,
                economic_capital_amount__c, NumberOfEmployees, controlled_by_sponsor_type__c, taxpayer_id__c, company_foundation_date__c, hats_risk_qualification_number__c,
                country_name__c, LastModifiedById, financial_debt_total_amount__c, ParentId, capital_provision_per__c, Rating,stage_collective_type__c, subsector_desc__c,
                RecordType.DeveloperName
                FROM Account WHERE ParentId = : lstGroupId];
            for(Account acc : lstAcc) {
                final Arc_Gen_Account_Wrapper wrapper = new Arc_Gen_Account_Wrapper();
                wrapper.accId = acc.Id;
                wrapper.name = acc.Name;
                wrapper.accNumber = acc.AccountNumber;
                wrapper.accParentId = acc.ParentId;
                lstAccWrapper.add(wrapper);
            }
        } catch(Exception ex) {
            final Arc_Gen_Account_Wrapper wrapper = new Arc_Gen_Account_Wrapper();
            wrapper.error = ex.getMessage();
            lstAccWrapper.add(wrapper);
        }
        return lstAccWrapper;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description  Method "accountUpdate" updates a map of Account Ids and attributes
    --------------------------------------------------------------------------------
    * @author javier.soto.carrascosa@bbva.com
    * @date 2020-01-15
    * @param Map<Id, Map<String,String>> lstAccUp
    * @return   Arc_Gen_ServiceAndSaveResponse
    * @example Arc_Gen_ServiceAndSaveResponse accountUpdate(Map<Id, Map<String,String>> lstAccUp)
    **/
    public static Arc_Gen_ServiceAndSaveResponse accountUpdate(Map<Id, Map<String,Object>> lstAccUp) {
        Arc_Gen_ServiceAndSaveResponse result = new Arc_Gen_ServiceAndSaveResponse();
        List<Account> lstUp = new List<Account>();
        for (Id accId : lstAccUp.keySet()) {
        Account accUp = new Account(Id=accId);
        accUp = putFieldsFromMap(accUp,lstAccUp.get(accId));
        lstUp.add(accUp);
        }
        try {
            result.saveStatus = 'true';
            final List<Database.SaveResult> sare = database.update(lstUp);
            result.saveMessage = Json.serialize(sare);
        } catch (Exception ex) {
            result.saveStatus = 'false';
            result.saveMessage = ex.getMessage();
        }
        return result;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description  Method "createGroup" create group account
    --------------------------------------------------------------------------------
    * @author javier.soto.carrascosa@bbva.com
    * @date 2020-01-15
    * @param Map<String,String> accAttr
    * @return  Arc_Gen_ServiceAndSaveResponse
    * @example Arc_Gen_ServiceAndSaveResponse createGroup(Map<String,String> accAttr)
    **/
    public static Arc_Gen_ServiceAndSaveResponse createGroup(Map<String,Object> accAttr) {
        Arc_Gen_ServiceAndSaveResponse result = new Arc_Gen_ServiceAndSaveResponse();
        Account grpAccount = new Account();
        grpAccount = putFieldsFromMap(grpAccount,accAttr);
        try {
            result.saveStatus = 'true';
            Database.SaveResult sare = database.insert(grpAccount);
            result.saveMessage = Json.serialize(sare);
            result.createdRsr = new List<Id>{sare.getId()};
        } catch (Exception ex) {
            result.saveStatus = 'false';
            result.saveMessage = ex.getMessage();
        }
        return result;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description  Method "putFieldsFromMap" retrieves updated object from input fields and values
    --------------------------------------------------------------------------------
    * @author javier.soto.carrascosa@bbva.com
    * @date 2020-01-15
    * @param Account acc
    * @param Map<String,String> mapValues
    * @return   Account
    * @example Account putFieldsFromMap(Account acc,Map<String,String> mapValues)
    **/
    private static Account putFieldsFromMap(Account acc, Map<String,Object> mapValues) {
        for (String fieldName : mapValues.keySet()) {
            acc.put(fieldName, mapValues.get(fieldName));
        }
        return acc;
    }
}