/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Workflow_Helper
* @Author   Juan Ignacio Hita Manso juanignacio.hita.contractor@bbva.com
* @Date     Created: 2020-01-07
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Helper class local execute method of workflow
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-01-07 juanignacio.hita.contractor@bbva.com
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
public class Arc_GBL_Workflow_Helper {
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   juanignacio.hita.contractor@bbva.com
    * @Date     Created: 07/01/2019
    * @param void
    * @return void
    * @example Arc_GBL_Workflow_Helper data = new Arc_GBL_Workflow_Helper()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_GBL_Workflow_Helper() {

    }
    /**
    *-------------------------------------------------------------------------------
    * @description scheduleAction : Schedule action method
    *--------------------------------------------------------------------------------
    * @date		07/01/2020
    * @author	juanignacio.hita.contractor@bbva.com
    * @param    Arc_Gen_Delegation_Wrapper : wrapper
    * @return   Arc_Gen_Delegation_Wrapper
    * @example	Arc_Gen_Workflow_Helper.scheduleAction(Arc_Gen_Delegation_Wrapper wrapper);
    */
    public static Arc_Gen_Delegation_Wrapper scheduleAction(Arc_Gen_Delegation_Wrapper wrapper) {
        try {
            List<String> valuesSelected = new List<String>{'1'};
            List<Map<String,String>> lstPicklist = Arc_Gen_GenericUtilities.getPicklistValuesLabels('arce__Analysis__c','arce__bbva_committees_type__c', valuesSelected);
            wrapper.lstComittees = lstPicklist;
            wrapper.codStatus = 200;
        } catch (Exception e) {
            wrapper.codStatus = 500;
            wrapper.msgInfo = 'Error ScheduleAction : ' + e.getMessage();
        }
		System.debug('wrapper:'+wrapper);
        return wrapper;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description proposeAction : Propose action method
    *--------------------------------------------------------------------------------
    * @date		08/01/2020
    * @author	juanignacio.hita.contractor@bbva.com
    * @param    Arc_Gen_Delegation_Wrapper : wrapper
    * @return   Arc_Gen_Delegation_Wrapper
    * @example	Arc_Gen_Workflow_Helper.proposeAction(Arc_Gen_Delegation_Wrapper wrapper);
    */
    public static Arc_Gen_Delegation_Wrapper proposeAction(Arc_Gen_Delegation_Wrapper wrapper) {
        try {
            List<String> valuesSelected = new List<String>{'1', '2', '3'};
            List<Map<String,String>> lstPicklist = Arc_Gen_GenericUtilities.getPicklistValuesLabels('arce__Analysis__c','arce__anlys_wkfl_edit_br_level_type__c', valuesSelected);
            wrapper.lstAmbits = lstPicklist;
            wrapper.sanctionAmbit = '1';
            wrapper.hasDelegation = 'NO';
            wrapper.codStatus = 200;
        } catch (Exception e) {
            wrapper.codStatus = 500;
            wrapper.msgInfo = 'Error ProposeAction : ' + e.getMessage();
        }
		System.debug('wrapper:'+wrapper);
        return wrapper;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description proposeAction : Propose action method
    *--------------------------------------------------------------------------------
    * @date		08/01/2020
    * @author	juanignacio.hita.contractor@bbva.com
    * @param    Arc_Gen_Delegation_Wrapper : wrapper
    * @return   Arc_Gen_Delegation_Wrapper
    * @example	Arc_Gen_Workflow_Helper.proposeAction(Arc_Gen_Delegation_Wrapper wrapper);
    */
    public static Arc_Gen_Delegation_Wrapper returnAction(Arc_Gen_Delegation_Wrapper wrapper) {
        try {
            List<String> valuesSelected = new List<String>{'2'};
            List<Map<String,String>> lstPicklist = Arc_Gen_GenericUtilities.getPicklistValuesLabels('arce__Analysis__c','arce__anlys_wkfl_edit_br_level_type__c', valuesSelected);
            wrapper.lstAmbits = lstPicklist;
            wrapper.codStatus = 200; //old value 500
        } catch (Exception e) {
            wrapper.codStatus = 500;
            wrapper.msgInfo = 'Error ReturnAction : ' + e.getMessage();
        }
		System.debug('wrapper:'+wrapper);
        return wrapper;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description sanctionAction : Sanction action method
    *--------------------------------------------------------------------------------
    * @date		22/01/2020
    * @author	juanignacio.hita.contractor@bbva.com
    * @param    Arc_Gen_Delegation_Wrapper : wrapper
    * @return   Arc_Gen_Delegation_Wrapper
    * @example	Arc_Gen_Workflow_Helper.sanctionAction(Arc_Gen_Delegation_Wrapper wrapper);
    */
    public static Arc_Gen_Delegation_Wrapper sanctionAction(Arc_Gen_Delegation_Wrapper wrapper) {
        try {
            List<String> valSelAmbit = new List<String>{'2'};
            List<String> valSelSanction = new List<String>{'1', '2', '3', '4', '5', '6'};
            List<Map<String,String>> lstMapAmbits = Arc_Gen_GenericUtilities.getPicklistValuesLabels('arce__Analysis__c','arce__anlys_wkfl_edit_br_level_type__c', valSelAmbit);
            List<Map<String,String>> lstMapSanction = Arc_Gen_GenericUtilities.getPicklistValuesLabels('arce__Analysis__c','arce__anlys_wkfl_sanction_rslt_type__c', valSelSanction);
            wrapper.lstAmbits = lstMapAmbits;
            wrapper.lstActions = lstMapSanction;
            wrapper.codStatus = 200;
        } catch (Exception e) {
            wrapper.codStatus = 500;
            wrapper.msgInfo = 'Error SanctionAction : ' + e.getMessage();
        }
		System.debug('wrapper:'+wrapper);
        return wrapper;
    }
}