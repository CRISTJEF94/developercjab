/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Auto_ExpTable_Service
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2020-01-28
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Service class for policie table
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2020-01-28 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_Auto_ExpTable_Service implements Arc_Gen_Expandible_Table_Interface {
    /**
        * @Description: Param to call limits service
    */
    static final string S_GROUP = 'GROUP';
    /**
        * @Description: Param to call limits service
    */
    static final string SUBSIDIARY = 'SUBSIDIARY';
    /**
    * --------------------------------------------------------------------------------------
    * @Description get data to build table
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-01-28
    * @param recordId - id of the acc has analysis object
    * @return Arc_Gen_DataTable - wrapper with the info to build the table
    * @example getData(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static Arc_Gen_DataTable getData(Id recordId) {
        Arc_Gen_DataTable dataJson = new Arc_Gen_DataTable();
        if (Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<String>{(String)recordId})[0].arce__call_limit_service__c == false) {
            Arc_Gen_Auto_ExpTable_Service_Helper.fillServiceData(recordId);
        }
        dataJson.columns = Arc_Gen_Expandible_Table_Service.getColumns(recordId);
        dataJson.data = Arc_Gen_Expandible_Table_Service.buildNestedData(recordId);
        return dataJson;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Change the flag to consult limit service
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-01-28
    * @param recordId - id of the acc has analysis object
    * @return void
    * @example changeServiceFlag(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static void changeServiceFlag(Id recordId) {
        List<arce__Account_has_Analysis__c> acchasAn = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<String>{(String)recordId});
        acchasAn[0].arce__call_limit_service__c = false;
        Arc_Gen_AccHasAnalysis_Data.updateAccHasAnalysis(acchasAn);
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description get a list of products
    * --------------------------------------------------------------------------------------
    * @param tipologia - name of the typology
    * @return lista.
    * @example getProductsService(tipologia)
    * --------------------------------------------------------------------------------------
    **/
    public static Map<String,Double> insertProducts(String accHasAId, String tipologia, String prodId) {
        Map<String,Double> prodResp = new Map<String,Double>();
        Map<String,Arc_Gen_Limits_Service.LimitsResponse> limitRespMap = new Map<String,Arc_Gen_Limits_Service.LimitsResponse>();
        final String typoOfCustomer = Arc_Gen_GenericUtilities.getTypeOfCustomer(accHasAId) == 'Group' ? S_GROUP : SUBSIDIARY;
        final arce__Account_has_Analysis__c accHasRel = Arc_Gen_AccHasAnalysis_Data.getAccHasRelation(accHasAId);
        final Map<Id, Arc_Gen_Account_Wrapper> accWrapper = Arc_Gen_Account_Locator.getAccountInfoById(new List<Id>{accHasRel.arce__customer__c});
        final Map<Id,Arc_Gen_Product_Wrapper> pWrap = Arc_Gen_Product_Locator.getProductsFromTypology(tipologia);
        limitRespMap = Arc_Gen_Limits_Service.callLimitsService(typoOfCustomer,accWrapper.get(accHasRel.arce__customer__c).accNumber, 'limits');
        if (limitRespMap.containsKey('ERROR')) {
            throw new QueryException(Label.serviceFailure + ' ' + limitRespMap.get('ERROR').gblCodeResponse);
        }
        if (limitRespMap.containsKey(pWrap.get(prodId).externalId)) {
            prodResp.put('lastApproved',limitRespMap.get(pWrap.get(prodId).externalId).lastApproved);
            prodResp.put('commited',limitRespMap.get(pWrap.get(prodId).externalId).commited);
            prodResp.put('uncommited',limitRespMap.get(pWrap.get(prodId).externalId).uncommited);
            prodResp.put('currentLimit',limitRespMap.get(pWrap.get(prodId).externalId).currentLimit);
            prodResp.put('outstanding',limitRespMap.get(pWrap.get(prodId).externalId).outstanding);
        }
        return prodResp;
    }
}