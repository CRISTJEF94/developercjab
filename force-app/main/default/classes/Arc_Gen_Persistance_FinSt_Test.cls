/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Persistance_FinSt
* @Author   Berth Elena Téllez Lira  eberthaelena.tellez.contractor@bbva.com
* @Date     Created: 24/01/2020
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Test class that covers:  Arc_Gen_Persistance_FinSt_Serice
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
@isTest
public class Arc_Gen_Persistance_FinSt_Test {
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method, create data for dinamic form
    * --------------------------------------------------------------------------------------
    * @Author   berthaelena.tellez.contractor@bbva.com
    * @param void
    * @return void
    * @example setupTest()
    * --------------------------------------------------------------------------------------
    **/
    @testSetup
      static void setupTest(){
        Arc_UtilitysDataTest_tst.setupAcccounts();
        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{'G000001'});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get('G000001');

        final arce__Analysis__c newArce = Arc_UtilitysDataTest_tst.crearArceAnalysis('Arce Analysis', null, groupAccount.accId);
        newArce.arce__Stage__c = '1';
        insert newArce;

        final arce__Account_has_Analysis__c newAnalysis = Arc_UtilitysDataTest_tst.crearAccHasAnalysis(null, newArce.Id, groupAccount.accId, 's-01');
        newAnalysis.arce__InReview__c = true;
        newAnalysis.arce__path__c = 'MAC';
        insert newAnalysis;

        final arce__Data_Collections__c dataColpeer = Arc_UtilitysDataTest_tst.crearDataCollection('Test Prueba', 'Maturity_table', '01');
        insert dataColpeer;

        final arce__Table_Content_per_Analysis__c maturityData = Arc_UtilitysDataTest_tst.crearTableContentAnalysis(newAnalysis.Id, dataColpeer.id, 'Maturity_table', '2019');
        insert maturityData;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Method that is responsible for preparing the list of objects and
    identifiers to execute the class to save basic data.
    * --------------------------------------------------------------------------------------
    * @Author   berthaelena.tellez.contractor@bbva.com
    * @param void
    * @return void
    * @example prepareData()
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Public static void prepareData(){
      final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{'G000001'});
      final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get('G000001');
      arce__account_has_analysis__c ahaTest = Arc_Gen_AccHasAnalysis_Data.getAccForResume(new Set<Id>{groupAccount.accId})[0];
      arce__account_has_analysis__c ahaTestFinal = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<String>{(String)ahaTest.Id})[0];
        Test.startTest();
        String test1 =  Arc_Gen_FinRisk_Pers_service.save(new List<arce__account_has_analysis__c>{ahaTestFinal});
        system.assertEquals(test1,test1, 'Test save persistance');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Method that is responsible for preparing the list of objects and
    identifiers to execute the class to save basic data.
    * --------------------------------------------------------------------------------------
    * @Author   berthaelena.tellez.contractor@bbva.com
    * @param void
    * @return void
    * @example prepareData()
    * --------------------------------------------------------------------------------------
    **/
    @isTest Static void testDataNull() {
      final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{'G000001'});
      final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get('G000001');
      arce__account_has_analysis__c ahaTest = Arc_Gen_AccHasAnalysis_Data.getAccForResume(new Set<Id>{groupAccount.accId})[0];
      arce__account_has_analysis__c ahaTestFinal = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<String>{(String)ahaTest.Id})[0];
      ahaTestFinal.put('arce__ffss_auditor_fullname_name__c','test');
      ahaTestFinal.put('arce__ffss_cnsld_perimeter_desc__c','test');
      ahaTestFinal.put('arce__ffss_auditor_fullname_name__c','test');
      ahaTestFinal.put('arce__debt_maturity_desc__c','test');
      ahaTestFinal.put('arce__number_entity_type__c','test');
      ahaTestFinal.put('arce__debt_maturity_available_type__c','test');
      ahaTestFinal.put('arce__cust_budget_cyr_ind_type__c','test');
      ahaTestFinal.put('arce__cust_budget_incl_ffss_ind_type__c','test');
      ahaTestFinal.put('arce__cust_proj_2yr_3yr_ind_type__c','test');
      ahaTestFinal.put('arce__cust_proj_2yr_3yr_desc__c','test');
      ahaTestFinal.put('arce__property_ownership_type__c','test');
      ahaTestFinal.put('arce__property_ownership_desc__c','test');
      ahaTestFinal.put('arce__var_over_20_balance_desc__c','test');
      ahaTestFinal.put('arce__equity_total_asset_20_type__c','test');
      Test.startTest();
      String result = Arc_Gen_FinRisk_Pers_service.save(new List<sObject>{ahaTestFinal});
      system.assertEquals(result, result, 'The test to the empty constructor was successfully');
      Test.stopTest();
    }

}