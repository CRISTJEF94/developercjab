/*------------------------------------------------------------------
*Author:        Diego Miguel Tamarit - diego.miguel.contractor@bbva.com / dmiguelt@minsait.com
*Project:      	ARCE - BBVA Bancomer
*Description:   Test class for code coverage of:
Arc_Gen_ContentPerAnalysis_controller
Arc_Gen_ContentPerAnalysis_data
Arc_Gen_ContentPerAnalysis_service
*_______________________________________________________________________________________
*Version    Date           Author                               Description
*1.0        11/04/2019     diego.miguel.contractor@bbva.com    	Creación.
*1.1        23/07/2019    luisruben.quinto.munoz@bbva.com    	updates params.
-----------------------------------------------------------------------------------------*/
@isTest
public class Arc_Gen_ContentPerAnalysis_ctr_test {
    @testSetup static void setup() {
        Arc_UtilitysDataTest_tst.setupAcccounts();
        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{'G000001'});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get('G000001');

        arce__Sector__c newSector = arc_UtilitysDataTest_tst.crearSector('s-01', '100', 's-01', null);
        insert newSector;

        arce__Analysis__c newArce = arc_UtilitysDataTest_tst.crearArceAnalysis('Arce Analysis', null, groupAccount.accId);
        insert newArce;

        arce__Account_has_Analysis__c newAnalysis = arc_UtilitysDataTest_tst.crearAccHasAnalysis(newSector.id, newArce.Id, groupAccount.accId, 's-01');
        insert newAnalysis;

        arce__risk_position_summary__c riskPosition = arc_UtilitysDataTest_tst.crearRiskPositionSummary(null, null);
        insert riskPosition;
    }

    @isTest static void testBeforeInsert() {
        arce__Account_has_Analysis__c accHasAnls = [SELECT Id FROM arce__Account_has_Analysis__c limit 1];
        arce__risk_position_summary__c riskPosition = [SELECT Id FROM arce__risk_position_summary__c limit 1];

        Test.startTest();
        Arc_Gen_ContentPerAnalysis_controller controller = new Arc_Gen_ContentPerAnalysis_controller();
        riskPosition = (arce__risk_position_summary__c)controller.beforeInsert(riskPosition, accHasAnls.id);
        system.assertEquals(riskPosition.arce__account_has_analysis_id__c, accHasAnls.id,'testBeforeInsert Id did not match');
        controller.afterInsert(riskPosition.id, accHasAnls.id);
        Test.stopTest();
    }

}