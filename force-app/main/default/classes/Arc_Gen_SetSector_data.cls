/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_SetSector_data
* @Author   ARCE Team
* @Date     Created: 2019-05-07
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Data class for SetSector
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-05-07 diego.miguel.contractor@bbva.com
*             Class creation.
* |2019-08-26 eduardoefrain.hernandez.contractor@bbva.com
*             Update getAnalizedClientsByAnalysis method
* |2019-05-10 mariohumberto.ramirez.contractor@bbva.com
*             Added new query to get the id of the account has analysis group
* -----------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_SetSector_data {
    /**
    *-------------------------------------------------------------------------------
    * @description retrieves a list of arce__Account_has_Analysis__c related to given
    * analysis and client given and updates arce__sector_rt_type__c with the given one
    *--------------------------------------------------------------------------------
    * @date 7/5/2019
    * @author ARCE Team
    * @param String analysisId - id of the arce abject
    * @param String clientId - id of the economic group
    * @param String selctedSector - sector choosen
    * @return void
    * @example getAnalizedClientsByAnalysis(analysisId, clientId, selctedSector)
    * ------------------------------------------------------------------------------
    */

    public static void getAnalizedClientsByAnalysis(String analysisId,String clientId,String selctedSector) {
        List<arce__Account_has_Analysis__c> accHasLts = new List<arce__Account_has_Analysis__c>();
        final List<arce__Analysis__c> arceAnalysis = [SELECT Id, (SELECT Id, arce__sector_rt_type__c, arce__Customer__c, arce__Customer__r.Name, arce__Customer__r.ParentId FROM arce__Account_has_Analysis__r) FROM arce__Analysis__c WHERE Id = :analysisId];
        final Id accHasGroupId = [SELECT Id FROM arce__Account_has_Analysis__c WHERE arce__Customer__c = :clientId AND arce__Analysis__c = :analysisId].Id;
        for (arce__Analysis__c arce: arceAnalysis) {
            for (arce__Account_has_Analysis__c accHasAn: arce.arce__Account_has_Analysis__r) {
                accHasAn.arce__sector_rt_type__c = selctedSector;
                accHasLts.add(accHasAn);
            }
        }
        Arc_Gen_Expandible_Table_Service.verifyTypologiesInserted(accHasGroupId);
        Arc_Gen_PotitionBankTable_Service.verifyDataInserted(accHasGroupId);
        update accHasLts;
    }
}