/**
* --------------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_NAHA_Service_Helper
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2019-10-12
* @Group    ARCE
* --------------------------------------------------------------------------------------------------------
* @Description Helper class for Arc_Gen_NAHACtrl
* --------------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-10-12 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* |2020-01-29 javier.soto.carrascosa@bbva.com
*             Recover boolean saveobject
* |2020-04-02 juanmanuel.perez.ortiz.contractor@bbva.com
*             Add two new parameters in setupPath() and created SetupPathWrapper to avoid 'long parameter lists'
* --------------------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_NAHA_Service_Helper {
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   mariohumberto.ramirez.contractor@bbva.com
    * @Date     2019-10-12
    * @param void
    * @return void
    * @example Arc_Gen_NAHA_Service Service = new Arc_Gen_NAHA_Service()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_NAHA_Service_Helper() {

    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Method that call the path service
    * ----------------------------------------------------------------------------------------------------
    * @Author   mariohumberto.ramirez.contractor@bbva.com
    * @Date     2019-10-12
    * @param idRecord - Id of an account has analysis object
    * @return Arc_Gen_ServiceAndSaveResponse - response of the service
    * @example callPathService(idRecord)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static Arc_Gen_ServiceAndSaveResponse callPathService(Id idRecord) {
        Arc_Gen_getPathDataService_service.SetupPathWrapper pathParameters = new Arc_Gen_getPathDataService_service.SetupPathWrapper();
        final Arc_Gen_getPathDataService_service servicePath = new Arc_Gen_getPathDataService_service();
        final List<String> parameters =  Arc_Gen_getPathDataService_data.getAnalysisAndCustomer(idRecord);
        pathParameters.analysisId = parameters[0];
        pathParameters.customerId = parameters[1];
        pathParameters.clientNumber = parameters[2];
        pathParameters.subsidiary = parameters[3];
        pathParameters.saveobject = false;
        return servicePath.setupPath(pathParameters);
    }
}