/**
  * @File Name          : Arc_Gen_NewAnalysis_Service.cls
  * @Description        : Service Class for newAnalysis
  * @Author             : lUISARTURO.PARRA.CONTACTOR@bbva.com
  * @Group              : ARCE
  * @Last Modified By   : juanmanuel.perez.ortiz.contractor@bbva.com
  * @Last Modified On   : 22/01/2020 16:41:33
  * @Modification Log   :
  *==============================================================================
  * Ver         Date                     Author                 Modification
  *==============================================================================
  * 1.0    30/04/2019           eduardoefrain.hernandez.contractor@bbva.com     Initial Version
  * 1.1    02/04/2019           diego.miguel.contractor@bbva.com                Added logic to redirect to valid ARCE
  * 1.2    03/05/2019           diego.miguel.contractor@bbva.com                Added functions to save ARCE name AND subtype
  * 1.3    09/05/2019           diego.miguel.contractor@bbva.com                Added functions to redirect acording to ARCE satatus AND update status
  * 1.4    14/05/2019           diego.miguel.contractor@bbva.com                Added methods to groups ws support
  * 1.5    27/08/2019           luisruben.quinto.munoz@bbva.com                 deleted comment and reference to arce__parent_analysis_id__c
  * 1.6    27/08/2019           luisarturo.parra.contractor@bbva.com            refactorizacion
  * 1.7    04/12/2019           manuelhugo.castillo.contractor@bbva.com         Modify methods 'getPreviousArce','fillcustomedata' replace Account to AccountWrapper
  * 1.8    13/01/2020           mariohumberto.ramirez.contractor@bbva.com       Added new methods getPreviousArceOnline and setanalysis
  *                                                                             Deleted unused param analysisSubType
  *
  * 1.8    14/01/2020           juanmanuel.perez.ortiz.contractor@bbva.com      Add custom labels in traceability
  * 1.9    22/01/2020           juanmanuel.perez.ortiz.contractor@bbva.com      Add logic to fix customer orphan assigment
  * 1.10   29/01/2020           javier.soto.carrascosa@bbva.com      Fix too many queries Financial Statements
  **/
public without sharing class Arc_Gen_NewAnalysis_Service {
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_NewAnalysis_Service service = new Arc_Gen_NewAnalysis_Service()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_NewAnalysis_Service() {

    }
    /**class type to save return data**/
    public class AnalysisResponse {
        /**
            * @Description: String analysisId
        */
        public String analysisId {get;set;}
        /**
            * @Description: String status
        */
        public String status {get;set;}
        /**
            * @Description: String errorMessage
        */
        public String errorMessage {get;set;}
    }
    /**
    *--------------------------------------------------------------------------------
    * @Description method that gets previous arce
    *--------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-13-01
    * @param    recordId - Id of the client
    * @return   List<String>
    * @example  getPreviousArceOnline(recordId)
    * -------------------------------------------------------------------------------
    */
    public static List<String> getPreviousArceOnline(String recordId, String accountswraper) {
        return Arc_Gen_NewAnalysis_Service_Helper.getPreviousArceOnline(recordId, accountswraper);
    }
    /**
    *--------------------------------------------------------------------------------
    * @Description method that gets previous arce
    *--------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-13-01
    * @param    recordId - Id of the client
    * @param    isorphan - boolean
    * @param    orphanNumber - numbert of the orphan
    * @return   String
    * @example  setanalysis(recordId)
    * -------------------------------------------------------------------------------
    */
    public static Arc_Gen_NewAnalysis_Service.AnalysisResponse setanalysis(String recordId, Boolean isorphan, String orphanNumber, String accounts) {
        return Arc_Gen_NewAnalysis_Service_Helper.setanalysis(recordId, isorphan, orphanNumber, accounts);
    }
    /**
    *-------------------------------------------------------------------------------
    * @description method that updates arce status
    *-------------------------------------------------------------------------------
    * @date 12/09/2019
    * @author luisarturo.parra.contractor@bbva.com
    * @param List<arce__Account_has_Analysis__c> analyzedClientList
    * @return none
    * @example  private void setRatingVariables(List<arce__Account_has_Analysis__c> analyzedClientList)
    */
    public static void updateStatusArce(String arceId, String status) {
        user currentUser = [select id, Name from user where id=:system.UserInfo.getUserId()];
        Arc_Gen_Traceability.saveEvent(arceId, Label.Arc_Gen_Traceability_02, 'approve', Label.Arc_Gen_ExecRepStg + ' : ' + Label.Arc_Gen_Stage_01 + ' | ' +  Label.Arc_Gen_TraceabilityState + ' : ' + Label.Arc_Gen_Traceability_02 + ' | ' + Label.Arc_Gen_TraceabilityUserCode +  ' : ' + currentUser.Name.toUpperCase() + ' | ' + Label.Arc_Gen_TraceabilitySubprocess +  ' : ' + Label.Arc_Gen_TraceabilityNewAnalysis);
        Arc_Gen_NewAnalysis_data.upsertObjects(new List < arce__Analysis__c > {
            new arce__Analysis__c(id = arceId, arce__wf_status_id__c = status)
                });
    }
}