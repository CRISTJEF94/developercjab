/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Buttons_controller
* @Author   BBVA Developers
* @Date     Created: 2019-11-04
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description this class is the controller for the table.
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-10-04  BBVA Developers.
*              Class creation.
* |2019-08-29 mariohumberto.ramirez.contractor@bbva.com
*             Added new param "apiNameObject" and logic in order to build a generic JSON for all
*              tables in dynamic form
* |2019-11-04  BBVA Developers.
*              Modify logic for Related Record adaption
* |2019-12-02 german.sanchez.perez.contractor@bbva.com | franciscojavier.bueno@bbva.com
* API names modified with the correct name on Business Glossary
* |2020-01-13 franciscojavier.bueno@bbva.com
* Shareholding field must not control that it adds up to 100%, or does not exceed 100%
* ------------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_Buttons_controller {
/**
* --------------------------------------------------------------------------------------
* @description class that contains the config of the table_content_per_analysis.
* @param collectionType string to configure labels
* @param recordId of the account
* @return returns a json formated with the configurations of the buttons.
* @example getButtonsJsonComponent(String collectionType, String recordId)
**/
  public static String getButtonsJsonComponent(String collectionType, String recordId, String apiNameObject) {
    String collectionTypeLabel = Schema.getGlobalDescribe().get(apiNameObject).getDescribe().getRecordTypeInfosByDeveloperName().get(collectionType).getName();
    String filterTable = 'RecordType.Developername=\''+collectionType+'\'';
    String relatedField, fieldSeeker, fieldsApiName, modalCss = '';
    String validateClass;
    switch on collectionType {
      when 'Geographies', 'Activities' {
        relatedField = 'arce__Table_Content_per_Analysis__r';
        fieldsApiName = 'arce__table_content_year__c,arce__table_content_percentage__c';
        fieldSeeker = 'arce__Data_Collection_Id__c';
        validateClass = 'Arc_Gen_ValidateContent_service';
        }
      when 'Shareholders' {
        relatedField = 'arce__third_participant_details__r';
        fieldsApiName = 'arce__third_participant_per__c,arce__shrhldr_financial_sponsor_type__c,arce__shareholder_sponsor_year_id__c';
        fieldSeeker = 'arce__Third_Participant_id__c';
        modalCss = 'slds-modal_small';
        validateClass = 'Arc_Gen_ValidateShareholders_service';
        filterTable += 'ORDER BY arce__third_participant_desc__c ASC';
        }
      when 'Main_clients','Main_suppliers' {
        relatedField = 'arce__third_participant_details__r';
        fieldsApiName = 'arce__third_participant_per__c';
        fieldSeeker = 'arce__Third_Participant_id__c';
        validateClass = 'Arc_Gen_ValidateThirdParticipant_service';
        }
      when 'Main_affiliates' {
        relatedField = 'arce__third_participant_details__r';
        fieldsApiName = 'arce__third_participant_per__c,arce__economic_activity_sector_desc__c,arce__consolidation_method_id__c,arce__non_customer_type__c';
        fieldSeeker = 'arce__Third_Participant_id__c';
        modalCss = 'slds-modal_large';
        validateClass = '';
        filterTable += 'ORDER BY arce__third_participant_desc__c ASC';
        }
      when 'Main_Banks' {
        relatedField = 'arce__Main_Banks__r';
        fieldsApiName = 'arce__entity_name__c, arce__entity_quota_share_per__c';
        fieldSeeker = '';
        validateClass = 'Arc_Gen_ValidateMainBanks_service';
      }
      when 'Maturity_table' {
        relatedField = 'arce__Table_Content_per_Analysis__r';
        fieldsApiName = 'arce__table_content_year__c,arce__table_content_value__c';
        fieldSeeker = 'arce__Data_Collection_Id__c';
        validateClass = 'Arc_Gen_ValidateContentMaturity_service';
      }
    }
    String buttons = '{'+
                      '"name": "rrtm:RelatedRecordTableManager",'+
                      '"inModalFromButton": {'+
                      '"labelButton": "' + Label.Arc_Gen_ManageRecords + ' ' + collectionTypeLabel + '",'+
                      '"editMode": true,'+
                      '"variantButton": "neutral",'+
                      '"modalCss": "'+modalCss+'",'+
                      '"iconButton": "",'+
                      '"positionButton": "left",'+
                      '"headerModal": "' + Label.Arc_Gen_ManageRecords + ' ' + collectionTypeLabel + '",'+
                      '"closeButtonModal": true,'+
                      '"closeModalActionEvt":{'+
                      '"name":"cmpw:GBL_ComponentWrapperRefresh_Evt",'+
                      '"attributes": {"uniqueNameEvt":"'+collectionType+'"}'+
                      '}'+
                      '},'+
                      '"attributes": {'+
                      '"fieldsApiName":"'+fieldsApiName+'",'+
                      '"relatedName":"'+relatedField+'",'+
                      '"sObjectType":"'+apiNameObject+'",'+
                      '"recordTypeName":"'+collectionType+'",'+
                      '"filterTable":"'+filterTable+'",'+
                      '"customMetadata":"Arc_Gen_'+collectionType+'",'+
                      '"fieldSeeker":"'+fieldSeeker+'",'+
                      '"readOnly":false,'+
                      '"recordId":"'+recordId+'",'+
                      '"apexClassSave":"'+validateClass+'"}}';
    return buttons;
  }

}