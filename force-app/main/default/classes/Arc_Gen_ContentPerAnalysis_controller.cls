/*------------------------------------------------------------------
* @Description:   test for method class Arc_Gen_ProposeInPreparation_controller.
* @Project:   ARCE - BBVA Bancomer
* @Author: Angel Fuertes Gomez - angel.fuertes2@bbva.com / BBVA DWP
* @Date     Created: 12/03/2019
* @Group              : ARCE
* @Changes :
*_______________________________________________________________________________________
*Version    Date           Author                               Description
*1.0        12/03/2019     Angel Fuertes Gomez                  	Creation.
-----------------------------------------------------------------------------------------*/
global class Arc_Gen_ContentPerAnalysis_controller implements rfwi.GBL_RecordFormWidget_Interface {
/**
*-------------------------------------------------------------------------------
* @description beforeInsert calls an interface method .
*--------------------------------------------------------------------------------
* @author  angel.fuertes2@bbva.com
* @date     12/03/2019
* @Method: associates the record to be created with the mini ARCE
* @param:       newRecord object (arce__Table_Content_per_Analysis__c)
* @param:       recordId id (arce__Account_has_Analysis__c)
* @return String SObject
**/
    global SObject beforeInsert(SObject newRecord,Id recordId){
        Arc_Gen_ContentPerAnalysis_service service = new Arc_Gen_ContentPerAnalysis_service();
        return service.beforeInsertService(newRecord, recordId);
    }
/**
*-------------------------------------------------------------------------------
* @description afterInsert this method is mandatory in an interface class
* https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_classes_interfaces.htm
*--------------------------------------------------------------------------------
* @author  angel.fuertes2@bbva.com
* @date     12/03/2019
* @Method:      afterInsert.
* @param:       newRecord Id (arce__Table_Content_per_Analysis__c)
* @param:       recordId id (arce__Account_has_Analysis__c)
* @Description: This method is not used.
*/
    global void afterInsert(Id newRecord,Id recordId){
    }
}