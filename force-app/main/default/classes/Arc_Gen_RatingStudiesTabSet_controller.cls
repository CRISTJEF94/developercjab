/**
* @File Name          : Arc_Gen_RatingStudiesTabSet_controller.cls
* @Description        : Controller of the rating studies dynamic form component
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE Team
* @Last Modified By   : eduardoefrain.hernandez.contractor@bbva.com
* @Last Modified On   : 18/10/2019 15:18:33
* @Changes   :
* Ver       Date            Author      		    Modification
* 1.0    17/10/2019   eduardoefrain.hernandez.contractor@bbva.com     Initial Version
**/
public class Arc_Gen_RatingStudiesTabSet_controller {
/**
* @Class: dynamicFormParameters
* @Description: Wrapper that contains the parameters of the studies dynamic form
* @author ARCE Team
*/
    public class DynamicFormParameters {
        /**
        * @description: Standard Id of the selected rating record
        **/
        @AuraEnabled public String ratingId {get; set;}
        /**
        * @description: Name of the Dynamic form template
        **/
        @AuraEnabled public String templateName {get; set;}
    }
    /**
    * @description: Empty constructor
    **/
    @TestVisible
    private Arc_Gen_RatingStudiesTabSet_controller() {

    }
/**
*-------------------------------------------------------------------------------
* @description Method that calls the business logic in the service class
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 17/10/2019
* @param String ffssId - Standard Id of the selected Financial statement
* @return DynamicFormParameters - Wrapper of parameters
* @example getAnalyzedClient(String ffssId)
**/
    @AuraEnabled
    public static DynamicFormParameters getAnalyzedClient(String ffssId) {
        DynamicFormParameters parameters = new DynamicFormParameters();
        try {
            final List<String> paramList = Arc_Gen_RatingStudiesTabSet_service.getAnalyzedClientId(ffssId);
            parameters.ratingId = paramList.get(0);
            parameters.templateName = paramList.get(1);
        } catch(ListException e) {
            parameters.ratingId = '';
            parameters.templateName = '';
        }
        Return parameters;
    }
}