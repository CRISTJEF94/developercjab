/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_NewAnalysis_Controller_test
* @Author   diego.miguel.contractor@bbva.com
* @Date     Created: 2019-04-11
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Test class for code coverage of: Arc_Gen_NewAnalysis_Controller,Arc_Gen_NewAnalysis_data
* Arc_Gen_NewAnalysis_Service, Arc_Gen_SetSector_Service, Arc_Gen_SetSector_data
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-04-11 diego.miguel.contractor@bbva.com
*             Class creation.
* |2019-30-04 eduardoefrain.hernandez.contractor@bbva.com
*             updated
* |2019-05-03 diego.miguel.contractor@bbva.com
*             Update to name ARCE
* |2019-05-09 diego.miguel.contractor@bbva.com
*             Update to Arce status
* |2019-05-14 diego.miguel.contractor@bbva.com
*             Added test methods to groups coverage.
* |2019-08-13 mariohumberto.ramirez.contractor@bbva.com
*             Added new test method testSetClientSectorNull in order to increase code coverage
* |2019-05-14 luisruben.quinto.munoz@bbva.com
*             Delete reference to arce__Id__c
* |2019-09-26 javier.soto.carrascosa@bbva.com
*             Remove incorrect test methods
* |2019-11-27 mariohumberto.ramirez.contractor@bbva.com
*             update test class
* |2020-01-09 javier.soto.carrascosa@bbva.com
*             Adapt test classess with account wrapper and setupaccounts
* |2020-01-29 javier.soto.carrascosa@bbva.com
*             Adapt test classess to cover new developments
* |2020-02-08 ricardo.almanza.contractor@bbva.com
*             Added orphan
* -----------------------------------------------------------------------------------------------
*/
@isTest
public class Arc_Gen_NewAnalysis_Controller_test {
    /**
    * @Description: String with external id of test group
    */
    static final string GROUP_ID = 'G000001';
    /**
    * @Description: String with external id of test group
    */
    static final string GROUP_ID2 = 'G000002';
    /**
    * @Description: String with external id of test subsidiary
    */
    static final string SUBSIDIARY_ID = 'C000001';
    /**
    * @Description: Insert new Analysis
    */
    private static arce__Analysis__c setAnalysis(String name) {
        arce__Analysis__c analysis = new arce__Analysis__c(
            Name = name
        );
        Insert analysis;
        Return analysis;
    }

    private static arce__Account_has_Analysis__c setAnalyzedClient(String clientId,String analysisId) {
        arce__Account_has_Analysis__c analyzedClient = new arce__Account_has_Analysis__c(
            arce__Customer__c = clientId,
            arce__Analysis__c = analysisId
        );
        Insert analyzedClient;
        Return analyzedClient;
    }
    private static arce__Account_has_Analysis__c getAnalyzedClient(String accNumber) {
        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{accNumber});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(accNumber);
        arce__Analysis__c analysis = setAnalysis('Analysis ');
        arce__Account_has_Analysis__c accHasAn = setAnalyzedClient(groupAccount.accId,analysis.Id);
        Return accHasAn;
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @testSetup static void setup() {
        Arc_UtilitysDataTest_tst.setupAcccounts();
        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{GROUP_ID,GROUP_ID2});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(GROUP_ID);
        arce__Sector__c newSector = Arc_UtilitysDataTest_tst.crearSector('s-01', '100', 's-01', null);
        insert newSector;
        arce__Analysis__c newArce = Arc_UtilitysDataTest_tst.crearArceAnalysis('Arce Analysis', null, groupAccount.accId);
        insert newArce;
        arce__Account_has_Analysis__c newAnalysis = Arc_UtilitysDataTest_tst.crearAccHasAnalysis(newSector.id, newArce.Id, groupAccount.accId, 's-01');
        newAnalysis.arce__group_asset_header_type__c = '1';
        insert newAnalysis;
        final Arc_Gen_Account_Wrapper groupAccount2 = groupAccWrapper.get(GROUP_ID2);
        List<sObject> iasoCnfList = Arc_UtilitysDataTest_tst.crearIasoUrlsCustomSettings();
        insert iasoCnfList;
        arce__Account_has_Analysis__c accHasAnalysis = new arce__Account_has_Analysis__c(arce__gf_company_economic_actvy_id__c = '234567890', arce__customer__c = groupAccount2.accId,arce__Analysis__c = newArce.Id);
        insert accHasAnalysis;
        SectorAssetBanxico__c sector = new SectorAssetBanxico__c(company_economic_activity_id__c = '234567890', Name = 'sector test', sector_id__c = '0099887766');
        insert sector;
        Sectores_catalog__c sectorPart2 = new Sectores_catalog__c(sector_outlook_type__c = 'type', sector_id__c = '0099887766', Name = 'Sector test');
        insert sectorPart2;
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @isTest static void testlistpa() {
        Test.startTest();
        String response = Arc_Gen_NewAnalysis_Controller.listparticipant('ae2NAUGjEQ1WtclUDaytLg');
        system.assertEquals(response, response, 'contains id');
        Test.stopTest();
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @isTest static void testesconomicp() {
        Test.startTest();
        String response = Arc_Gen_NewAnalysis_Controller.economicarticipants('ae2NAUGjEQ1WtclUDaytLg');
        system.assertEquals(response, response, 'contains id');
        Test.stopTest();
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @isTest static void groupstructure() {
        Test.startTest();
        String listp='{"servicecallerror":"","errormessage":"","error204message":"","customersdata":[{"pRelRelationTypeId":"IG","pRelLevel":"02","participname":null,"participantId":"IV019470","parentEconomicGroupId":null},{"pRelRelationTypeId":"IG","pRelLevel":"03","participname":null,"participantId":"C000001","parentEconomicGroupId":null}]}';
        String economicp = '{"servicecallerror":null,"groupinfo":{"groupname":"GUZMAN FONSECA,SA DE CV","groupid":"G000001","decryptedgroupid":"G000001"},"errormessage":null,"error204message":null,"business":{"bussineslistdocuments":[{"businessDocumentNumber":"USQ921005WWX","businessDocTypeName":null,"businessDocTypeId":null}],"businessLegalName":"DEGUSSA CORMPANY Lochlynn Seanna","businessId":"C000001"}}';
        String response = Arc_Gen_NewAnalysis_Controller.constructgroupstructure(listp,economicp,'C000001',false);
        system.assertEquals(false, String.isEmpty(response), 'The response is not empty');
        Test.stopTest();
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @isTest static void setarceOrphan() {
        Account acc = [SELECT Id FROM Account WHERE AccountNumber = 'C000003' limit 1];
        final List<Arc_Gen_Account_Wrapper> accWrapLts =  new List<Arc_Gen_Account_Wrapper>();
        final Arc_Gen_Account_Wrapper accWrap =  new Arc_Gen_Account_Wrapper();
        accWrap.accId = acc.Id;
        accWrapLts.add(accWrap);
        Test.startTest();
        String response = Arc_Gen_NewAnalysis_Controller.setanalysis((String)acc.Id,true,'C000001', JSON.serialize(accWrapLts));
        system.assertEquals(false, String.isEmpty(response), 'The response is not empty');
        Test.stopTest();
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @isTest static void setarce() {
        Account acc = [SELECT Id FROM Account WHERE AccountNumber = 'C000001' limit 1];
        Account groupAcc = [SELECT Id FROM Account WHERE AccountNumber = 'G000001' limit 1];
        final List<Arc_Gen_Account_Wrapper> accWrapLts =  new List<Arc_Gen_Account_Wrapper>();
        final Arc_Gen_Account_Wrapper accWrap =  new Arc_Gen_Account_Wrapper();
        accWrap.accId = acc.Id;
        accWrapLts.add(accWrap);
        final Arc_Gen_Account_Wrapper accWrapGp =  new Arc_Gen_Account_Wrapper();
        accWrapGp.accId = groupAcc.Id;
        accWrapLts.add(accWrapGp);
        Test.startTest();
        String response = Arc_Gen_NewAnalysis_Controller.setanalysis((String)acc.Id,false,'C000001', JSON.serialize(accWrapLts));
        system.assertEquals(false, String.isEmpty(response), 'The response is not empty');
        Test.stopTest();
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @isTest static void updatearce() {
        arce__Analysis__c arce = [SELECT Id FROM arce__Analysis__c limit 1];
        system.assertEquals(arce, arce, 'contains id');
        Test.startTest();
        Arc_Gen_NewAnalysis_Controller.updateArceToPreparing(arce.Id);
        Test.stopTest();
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @isTest static void testarceprev() {
        Account acc = [SELECT Id FROM Account limit 1];
        system.assertEquals(acc, acc, 'contains id');
        final List<Arc_Gen_Account_Wrapper> accWrapLts =  new List<Arc_Gen_Account_Wrapper>();
        final Arc_Gen_Account_Wrapper accWrap =  new Arc_Gen_Account_Wrapper();
        accWrap.accId = acc.Id;
        accWrapLts.add(accWrap);
        Test.startTest();
        String[] response = Arc_Gen_NewAnalysis_Controller.getPreviousArce(acc.Id, JSON.serialize(accWrapLts));
        Test.stopTest();
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @isTest static void testarceprevNew() {
        Account acc = [SELECT Id FROM Account limit 1];
        delete [SELECT Id FROM arce__Analysis__c LIMIT 100];
        system.assertEquals(acc, acc, 'contains id');
        final List<Arc_Gen_Account_Wrapper> accWrapLts =  new List<Arc_Gen_Account_Wrapper>();
        final Arc_Gen_Account_Wrapper accWrap =  new Arc_Gen_Account_Wrapper();
        accWrap.accId = acc.Id;
        accWrapLts.add(accWrap);
        Test.startTest();
        String[] response = Arc_Gen_NewAnalysis_Controller.getPreviousArce(acc.Id, JSON.serialize(accWrapLts));
        Test.stopTest();
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @isTest static void testsector() {
        Account acc = [SELECT Id FROM Account limit 1];
        arce__Analysis__c arc = [SELECT Id FROM arce__Analysis__c limit 1];
        system.assertEquals(arc, arc, 'contains id');
        Test.startTest();
        Arc_Gen_ArceAnalysis_Data.getGroupId(arc.Id);
        Arc_Gen_NewAnalysis_Controller.SectorResponse response = Arc_Gen_NewAnalysis_Controller.setClientSector(arc.Id,acc.Id,'s-01');
        Test.stopTest();
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @isTest static void testsectorNull() {
        Account acc = [SELECT Id FROM Account limit 1];
        arce__Analysis__c arc = [SELECT Id FROM arce__Analysis__c limit 1];
        system.assertEquals(arc, arc, 'contains id');
        Test.startTest();
        Arc_Gen_NewAnalysis_Controller.SectorResponse response = Arc_Gen_NewAnalysis_Controller.setClientSector(arc.Id,acc.Id,null);
        Test.stopTest();
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @isTest static void testsectorError() {
        Account acc = [SELECT Id FROM Account limit 1];
        arce__Analysis__c arc = [SELECT Id FROM arce__Analysis__c limit 1];
        system.assertEquals(arc, arc, 'contains id');
        Test.startTest();
        Arc_Gen_NewAnalysis_Controller.SectorResponse response = Arc_Gen_NewAnalysis_Controller.setClientSector(acc.Id,arc.Id,'s-01');
        Test.stopTest();
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @isTest static void testpath() {
        arce__Account_has_Analysis__c accHasAnalysis1 = getAnalyzedClient(GROUP_ID);
        system.assertEquals(accHasAnalysis1, accHasAnalysis1, 'contains id');
        Test.startTest();
        Arc_Gen_ServiceAndSaveResponse response = Arc_Gen_NewAnalysis_Controller.callPathService((String)accHasAnalysis1.arce__Analysis__c,(String)accHasAnalysis1.arce__Customer__c,true);
        Test.stopTest();
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @isTest static void testcallPersistence() {
        arce__Analysis__c newArce = [SELECT Id FROM arce__Analysis__c limit 1];
        Test.startTest();
        Arc_Gen_ServiceAndSaveResponse response = Arc_Gen_NewAnalysis_Controller.callPersistence(newArce.Id);
        Arc_Gen_NewAnalysis_Controller.callPersistence(newArce.Id);
        system.assertNotEquals(null, response,'testcallPersistence call');
        Test.stopTest();
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @isTest static void setarc() {
        String acc = [SELECT Id FROM Account limit 1].Id;
        system.assertEquals(acc, acc, 'contains id');
        Test.startTest();
        Arc_Gen_Account_Wrapper response = Arc_Gen_NewAnalysis_Controller.getaccdataforservices(acc);
        Test.stopTest();
    }
    /**
    * @Method:  test method
    * @Description: testing method.
    */
    @isTest static void emtyConstuctor() {
        Test.startTest();
        Arc_Gen_NewAnalysis_Service service = new Arc_Gen_NewAnalysis_Service();
        system.assertEquals(service, service, 'Test Success');
        Test.stopTest();
    }
}