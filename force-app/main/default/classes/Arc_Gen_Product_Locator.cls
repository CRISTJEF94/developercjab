/**
  * @File Name : Arc_Gen_Product_Locator.cls
  * @Description : Locator for products
  * @Author : javier.soto.carrascosa@bbva.com
  * @Group : ARCE - BBVA
  * @Last Modified By : javier.soto.carrascosa@bbva.com
  * @Last Modified On : 26/11/2019 17:59
  * @Modification Log :
  *==============================================================================
  * Ver Date Author          Modification
  *==============================================================================
  * 1.0 26/11/2019 ARCE TEAM Creation file
  **/
public without sharing class Arc_Gen_Product_Locator implements Arc_Gen_Product_Interface {
    /**
    *-------------------------------------------------------------------------------
    * @description  Method that retrieves products based on typology
    --------------------------------------------------------------------------------
    * @author javier.soto.carrascosa@bbva.com
    * @date 2019-11-26
    * @param String Typology
    * @return Map<Id,Arc_Gen_Product_Wrapper>
    * @example public static Map<Id,Arc_Gen_Product_Wrapper> getProductsFromTypology(String typology) {
    **/
    public static Map<Id,Arc_Gen_Product_Wrapper> getProductsFromTypology(String typology) {
        Map<Id,Arc_Gen_Product_Wrapper> lstWrapper = new Map<Id,Arc_Gen_Product_Wrapper>();
        Arc_Gen_Product_Interface classLocatorProd = getProdLocatorClass();
        if (classLocatorProd != null) {
            try {
                lstWrapper = classLocatorProd.getProductsFromTypology(typology);
            } catch(Exception ex) {
                throw new QueryException(ex);
            }
        }
        return lstWrapper;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description  Method that retrieves all active products to be select in positions
    --------------------------------------------------------------------------------
    * @author javier.soto.carrascosa@bbva.com
    * @date 2019-11-26
    * @param String Typology
    * @return Map<Id,Arc_Gen_Product_Wrapper>
    * @example public static Map<Id,Arc_Gen_Product_Wrapper> getProductsActive()
    **/
    public static Map<Id,Arc_Gen_Product_Wrapper> getProductsActive() {
        Map<Id,Arc_Gen_Product_Wrapper> lstWrapper = new Map<Id,Arc_Gen_Product_Wrapper>();
        Arc_Gen_Product_Interface classLocatorProd = getProdLocatorClass();
        if (classLocatorProd != null) {
            try {
                lstWrapper = classLocatorProd.getProductsActive();
            } catch(Exception ex) {
                throw new QueryException(ex);
            }
        }
        return lstWrapper;
    }
    /**
    * ---------------------------------------------------------------------------------------------------
    * @Description - Method "getAccLocatorClass" to find the class with the interface Arc_Gen_Account_Interface
    * ---------------------------------------------------------------------------------------------------
    * @Author   juanignacio.hita.contractor@bbva.com
    * @Date     Created: 2019-11-26
    * @return   Arc_Gen_Account_Interface
    * @example Arc_Gen_Account_Interface accInterface = getAccLocatorClass(nameConfig)
    * ---------------------------------------------------------------------------------------------------
    **/
    public static Arc_Gen_Product_Interface getProdLocatorClass() {
        List<Arce_Config__mdt> lstArceConfig = Arc_Gen_Arceconfigs_locator.getConfigurationInfo('ProductInterface');
        System.Type objType = Type.forName(lstArceConfig.get(0).Value1__c);
        Arc_Gen_Product_Interface classLocatorProd = (Arc_Gen_Product_Interface)objType.newInstance();
        return classLocatorProd;
    }
}