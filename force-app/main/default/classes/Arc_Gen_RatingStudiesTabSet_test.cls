/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_RatingStudiosUpdateTable
* @Author   Eduardo Efraín Hernández Rendón  eduardoefrain.hernandez.contractor@bbva.com
* @Date     Created: 28/10/2019
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Test class that covers: Arc_Gen_RatingStudiesTabSet_controller, Arc_Gen_RatingStudiesTabSet_service
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |28/10/2019 eduardoefrain.hernandez.contractor@bbva.com
*             Class creation.
* |2020-01-09 javier.soto.carrascosa@bbva.com
*             Adapt test classess with account wrapper and setupaccounts
* -----------------------------------------------------------------------------------------------
*/
@isTest
public class Arc_Gen_RatingStudiesTabSet_test {
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param void
    * @return void
    * @example setupTest()
    * --------------------------------------------------------------------------------------
    **/
    @testSetup
    static void setupTest() {
        Arc_UtilitysDataTest_tst.setupAcccounts();
        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{'G000001'});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get('G000001');

        final arce__Analysis__c newArce = Arc_UtilitysDataTest_tst.crearArceAnalysis('Arce Analysis', null, groupAccount.accId);
        newArce.arce__Stage__c = '1';
        insert newArce;

        final arce__Account_has_Analysis__c newAnalysis = Arc_UtilitysDataTest_tst.crearAccHasAnalysis(null, newArce.Id, groupAccount.accId, 's-01');
        newAnalysis.arce__InReview__c = true;
        newAnalysis.arce__path__c = 'MAC';
        insert newAnalysis;

        final arce__rating__c newRating = Arc_UtilitysDataTest_tst.crearRating('Default');
        newRating.arce__rating_id__c = '9999999999';
        insert newRating;

        final arce__Financial_Statements__c newFfss = Arc_UtilitysDataTest_tst.crearFinStatement(groupAccount.accId, newAnalysis.Id, newRating.Id, 'Default');
        insert newFfss;
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param void
    * @return void
    * @example testing()
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    static void testing() {
        final List<arce__Financial_Statements__c> ffssList = [SELECT Id,arce__rating_id__c FROM arce__Financial_Statements__c WHERE arce__rating_id__r.arce__rating_id__c = '9999999999'];
        Test.startTest();
        final Arc_Gen_RatingStudiesTabSet_controller.dynamicFormParameters params = Arc_Gen_RatingStudiesTabSet_controller.getAnalyzedClient(ffssList.get(0).Id);
        Arc_Gen_RatingStudiosUpdateTable tableInterface = new Arc_Gen_RatingStudiosUpdateTable();
        tableInterface.updateSelected(ffssList.get(0).Id, false);
        System.assertEquals(ffssList.get(0).arce__rating_id__c, params.ratingId, 'Get rating data');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param void
    * @return void
    * @example testingListError()
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    static void testingListError() {
        Test.startTest();
        final Arc_Gen_RatingStudiesTabSet_controller.dynamicFormParameters params = Arc_Gen_RatingStudiesTabSet_controller.getAnalyzedClient('0001234567890');
        System.assertEquals('', params.ratingId, 'Error to get rating data');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param void
    * @return void
    * @example testingConstructor()
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    static void testingConstructor() {
        Test.startTest();
        Arc_Gen_RatingStudiesTabSet_service constrController = new Arc_Gen_RatingStudiesTabSet_service();
        Arc_Gen_RatingStudiesTabSet_controller constrService = new Arc_Gen_RatingStudiesTabSet_controller();
        System.assertEquals(constrController, constrController,'Empty constructor of controller class');
        System.assertEquals(constrService, constrService,'Empty constructor of service class');
        Test.stopTest();
    }
}