/*------------------------------------------------------------------
* @File Name: Arc_Gen_ModalFinanStatemController
* @Author:        Diego Miguel Tamarit - diego.miguel.contractor@bbva.com / dmiguelt@minsait.com
* @Group:      	ARCE - BBVA Bancomer
* @Description:   Test class for code coverage of:
* @Changes :
*Arc_Gen_Balance_Tables_service
*Arc_Gen_Balance_Tables_data
*_______________________________________________________________________________________
*Version    Date           Author                               Description
*1.0        11/04/2019     diego.miguel.contractor@bbva.com    	Creación.
*1.1        25/04/2019     diego.miguel.contractor@bbva.com    	Updated functions to cover ratios and EEff WS
*1.2        30/04/2019     diego.miguel.contractor@bbva.com    	Added function to test Arc_Gen_getIASOResponse
*1.3        30/04/2019     diego.miguel.contractor@bbva.com    	Added function to test Arc_Gen_getIASOResponse
*1.4        24/10/2019     mariohumberto.ramirez.contractor@bbva.com  Updated functions to cover updateEEFFtoShow, getFinancialState2Show, updateRecordLts
*1.4.1      02/12/2019     german.sanchez.perez.contractor@bbva.com
*                          franciscojavier.bueno@bbva.com               Api names modified with the correct name on business glossary
*1.5        24/10/2019     mariohumberto.ramirez.contractor@bbva.com  Added new methods (testFsDetailsSuccess, testFsDetailsNull, testFsDetailsFails)
*                                                                     in order to increment test coverage of Arc_Gen_FinStatDetails_Service,
*                                                                     Arc_Gen_FinStatDetails_Service_Helper and Arc_Gen_ModalFinanStatemController
*1.6        07/01/2020     javier.soto.carrascosa@bbva.com          Add support for account wrapper and setupaccounts
-----------------------------------------------------------------------------------------*/
@isTest
public class Arc_Gen_ModalFinanStatemController_test {
    /**
    * --------------------------------------------------------------------------------------
    * @Description setup method
    * --------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_ModalFinanStatemController_test.setup()
    * --------------------------------------------------------------------------------------
    **/
    @testSetup static void setup() {
        Arc_UtilitysDataTest_tst.setupAcccounts();
        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{'G000001'});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get('G000001');

        arce__Sector__c newSector = Arc_UtilitysDataTest_tst.crearSector('s-01', '100', 's-01', null);
        insert newSector;

        arce__Analysis__c newArce = Arc_UtilitysDataTest_tst.crearArceAnalysis('Arce Analysis', null, groupAccount.accId);
        insert newArce;

        arce__Account_has_Analysis__c newAnalysis = Arc_UtilitysDataTest_tst.crearAccHasAnalysis(newSector.id, newArce.Id, groupAccount.accId, 's-01');
        insert newAnalysis;

        arce__rating__c rating = Arc_UtilitysDataTest_tst.crearRating(null);
        insert rating;

        arce__Financial_Statements__c finStatement = Arc_UtilitysDataTest_tst.crearFinStatement(groupAccount.accId, newAnalysis.id, rating.id, null);
        finStatement.arce__financial_statement_end_date__c = Date.today();
        finStatement.arce__financial_statement_id__c = '70202018129';
        finStatement.arce__ffss_valid_type__c = '1';
        insert finStatement;

        arce__Financial_Statements__c finStatement2 = Arc_UtilitysDataTest_tst.crearFinStatement(groupAccount.accId, newAnalysis.id, rating.id, null);
        finStatement2.arce__financial_statement_end_date__c = Date.today();
        finStatement2.arce__financial_statement_id__c = 'dummyFFSS';
        insert finStatement2;

        arce__Data_Collections__c dataCollection = Arc_UtilitysDataTest_tst.crearDataCollection('Cash Flow Analysis', 'Cash_Flow_Analysis', '04');
        dataCollection.arce__collection_code_term__c = 'workingCapital';
        insert dataCollection;

        arce__Table_Content_per_Analysis__c tableCAnalysis = Arc_UtilitysDataTest_tst.crearTableContentAnalysis(newAnalysis.id, dataCollection.id, 'Cash_Flow_Analysis', '2017');
        insert tableCAnalysis;

        List<sObject> iasoCnfList = Arc_UtilitysDataTest_tst.crearIasoUrlsCustomSettings();
        insert iasoCnfList;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description test method
    * --------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_ModalFinanStatemController_test.testSaveAndServiceResp()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testSaveAndServiceResp() {
        Arc_Gen_ServiceAndSaveResponse response = new Arc_Gen_ServiceAndSaveResponse();
        response.serviceCode = '200';
        response.serviceMessage = 'Success';
        response.serviceResponse = '{}';
        response.saveStatus = 'true';
        response.saveMessage = 'Success';
        System.assertEquals(response.serviceCode, '200');
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description test method
    * --------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_ModalFinanStatemController_test.testFetchAccounts()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testFetchAccounts() {
        arce__Account_has_Analysis__c accHasAnalysis = [SELECT arce__financial_statement_id__c FROM arce__Account_has_Analysis__c limit 1];
        arce__Financial_Statements__c finStatement = [SELECT arce__financial_statement_id__c FROM arce__Financial_Statements__c limit 1];
        Test.startTest();
        List <arce__Financial_Statements__c> analysisResponse =
            Arc_Gen_ModalFinanStatemController.fetchFinancialStatements(accHasAnalysis.id);
        system.assertEquals(finStatement.arce__financial_statement_id__c,
            analysisResponse[0].arce__financial_statement_id__c, 'testFetchAccounts Id do not match');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description test method
    * --------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_ModalFinanStatemController_test.testFetchAccountsFail()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testFetchAccountsFail() {
        Test.startTest();
        try {
            Arc_Gen_ModalFinanStatemController.fetchFinancialStatements(null);
            System.assert(false, 'The call to the above method must have thrown an exception.');
        } catch (Exception ex) {
            // Everything is ok.
        }
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description test method
    * --------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_ModalFinanStatemController_test.testCallEngineFinancialState()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testCallEngineFinancialState() {
        arce__Account_has_Analysis__c accHasAnalysis = [SELECT Id FROM arce__Account_has_Analysis__c limit 1];
        final List<String> ids = new List<String> { '70202016120' };
        final List<String> leverageValuesList = new List<String>{'totalRevenues','ebitda','grossFinancialDebt','netFinancialDebt','totalAssets'};
        arce__Data_Collections__c dataCollection = new arce__Data_Collections__c();
        arce__Table_Content_per_Analysis__c tableCAnalysis = new arce__Table_Content_per_Analysis__c();
        final List<arce__Data_Collections__c> dataCollectionList = new List<arce__Data_Collections__c>();
        final List<arce__Table_Content_per_Analysis__c> tableCAnalysisList = new List<arce__Table_Content_per_Analysis__c>();
        for(String levValue : leverageValuesList) {
            dataCollection = Arc_UtilitysDataTest_tst.crearDataCollection('Cash Flow Analysis', 'Cash_Flow_Analysis', '04');
            dataCollection.arce__collection_code_term__c = levValue;
            dataCollectionList.add(dataCollection);
        }
        insert dataCollectionList;
        for(Integer i=0; i<leverageValuesList.size();i++) {
            tableCAnalysis = Arc_UtilitysDataTest_tst.crearTableContentAnalysis(accHasAnalysis.id, dataCollection.id, 'Cash_Flow_Analysis', '2017');
            tableCAnalysis.arce__table_content_parent_code_id__c = '70202016120';
            tableCAnalysis.arce__account_has_analysis_id__c = accHasAnalysis.id;
            tableCAnalysis.arce__Data_Collection_Id__c = dataCollectionList[i].id;
            tableCAnalysisList.add(tableCAnalysis);
        }
        insert tableCAnalysisList;
        Test.startTest();
        Arc_Gen_Balance_Tables_service.ratiosAndRatingResponse serviceResponseList = new Arc_Gen_Balance_Tables_service.ratiosAndRatingResponse();
        serviceResponseList = Arc_Gen_ModalFinanStatemController.callEngineFinancialState(accHasAnalysis.id, ids);
        system.assertEquals('Success',serviceResponseList.ratiosStatus,'testCallEngineFinancialState status do not match');

        final List<arce__Financial_Statements__c> finStatementList = [SELECT id, arce__financial_statement_id__c FROM arce__Financial_Statements__c];
        for (arce__Financial_Statements__c ffss: finStatementList) {
            ffss.arce__ffss_valid_type__c = '2';
        }
        update finStatementList;
        Arc_Gen_Balance_Tables_service.ratiosAndRatingResponse serviceResponseList2 = new Arc_Gen_Balance_Tables_service.ratiosAndRatingResponse();
        serviceResponseList2 = Arc_Gen_ModalFinanStatemController.callEngineFinancialState(accHasAnalysis.id, ids);
        system.assertEquals('Success',serviceResponseList2.ratiosStatus,'testCallEngineFinancialState status do not match');
        List<Map<String,String>> eeffData2Update = new List<Map<String,String>>{new Map<String,String>{'Id' => '70202018129', 'currency' => 'EUR', 'unit' => 'UNITS'},new Map<String,String>{'Id' => '70202018128', 'currency' => 'MXN', 'unit' => 'MILLIONS'},new Map<String,String>{'Id' => '70202018127', 'currency' => 'MXN', 'unit' => 'BILLION'}};
        Arc_Gen_Balance_Tables_service.updateEEFFtoShow(eeffData2Update,accHasAnalysis.id);
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description test method
    * --------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_ModalFinanStatemController_test.testDataGetNewRecordsOnly()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testDataGetNewRecordsOnly() {
        List<arce__Financial_Statements__c> finStatementList = [SELECT id, arce__rating_id__c, arce__account_has_analysis_id__c, arce__financial_statement_id__c FROM arce__Financial_Statements__c];
        String analysisId = finStatementList[0].arce__account_has_analysis_id__c;
        Test.startTest();
        List<arce__Financial_Statements__c> newRecords = Arc_Gen_Balance_Tables_service.getNewRecordsOnly(finStatementList,analysisId);
        system.assertEquals(0,newRecords.size(),'testGetNewRecordsOnly list size do not match.');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description test method
    * --------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_ModalFinanStatemController_test.testDataInsertRecordsFail()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testDataInsertRecordsFail() {
        Test.startTest();
        Arc_Gen_Balance_Tables_data locator = new Arc_Gen_Balance_Tables_data();
        Arc_Gen_Balance_Tables_data.saveResult saveResult = locator.insertRecords(null);
        system.assertEquals('false',saveResult.status,'testDataInsertRecordsFail expected status false.');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description test method
    * --------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_ModalFinanStatemController_test.testServiceProcessError()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testServiceProcessError() {
        HttpResponse httpR = new HttpResponse();
        String jsonStr = '{"data":{"controls":[{"id": "formulaError","description": "Unable to calculate total financial debt over equity","isValid": false},{"id": "formulaError","description": "Unable to calculate total financial debt over equity","isValid": false}]}}';
        httpR.setBody(jsonStr);
        Test.startTest();
        String strResult = Arc_Gen_Balance_Tables_service.processError(httpR);
        system.assertEquals('Unable to calculate total financial debt over equity<br/>Unable to calculate total financial debt over equity',strResult,'testServiceProcessError error msg do not match.');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description test method
    * --------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_ModalFinanStatemController_test.testHttpGetServiceResponse()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testHttpGetServiceResponse() {
        List<String> fakeResponses = new List<String>{'204','400','403','404','409','500'};
        Arc_Gen_getIASOResponse.serviceResponse response = new Arc_Gen_getIASOResponse.serviceResponse();
        Test.startTest();
        for(String fRes : fakeResponses) {
            response = Arc_Gen_getIASOResponse.calloutIASO(fRes,'{"HttpResponse":"FakeResponse"}');
            system.assertEquals(String.valueOf(fRes),response.serviceCode,'testHttpGetServiceResponse response did not match.');
        }
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description test method
    * --------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_ModalFinanStatemController_test.testFsDetailsSuccess()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testFsDetailsSuccess() {
        arce__Account_has_Analysis__c accHasAnalysis = [SELECT Id FROM arce__Account_has_Analysis__c limit 1];
        Test.startTest();
        Arc_Gen_ModalFinanStatemController.FinancialDetailsWrapper response = Arc_Gen_ModalFinanStatemController.consultFSdetails(accHasAnalysis.Id, '70202018129');
        system.assertEquals(true, response.gblSuccessOperation, 'The Number of clients was updated successfully');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description test method
    * --------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_ModalFinanStatemController_test.testFsDetailsNull()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testFsDetailsNull() {
        arce__Account_has_Analysis__c accHasAnalysis = [SELECT Id FROM arce__Account_has_Analysis__c limit 1];
        Test.startTest();
        Arc_Gen_ModalFinanStatemController.FinancialDetailsWrapper response = Arc_Gen_ModalFinanStatemController.consultFSdetails(accHasAnalysis.Id, null);
        system.assertEquals(true, response.gblSuccessOperation, 'The Number of clients was updated successfully');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description test method
    * --------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_ModalFinanStatemController_test.testFsDetailsNull()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testFsDetailsFails() {
        Test.startTest();
        Arc_Gen_ModalFinanStatemController.FinancialDetailsWrapper response = Arc_Gen_ModalFinanStatemController.consultFSdetails(null, null);
        system.assertEquals(false, response.gblSuccessOperation, 'There was an error');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description test method
    * --------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_ModalFinanStatemController_test.testEmptyConstructor()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testEmptyConstructor() {
        Test.startTest();
        Arc_Gen_FinStatDetails_Service service =  new Arc_Gen_FinStatDetails_Service();
        system.assertEquals(service, service, 'The test to the empty constructor was successfully');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description test method
    * --------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_ModalFinanStatemController_test.testEmptyConstructor()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testEmptyConstructor2() {
        Test.startTest();
        Arc_Gen_FinStatDetails_Service_Helper helper =  new Arc_Gen_FinStatDetails_Service_Helper();
        system.assertEquals(helper, helper, 'The test to the empty constructor was successfully');
        Test.stopTest();
    }
}