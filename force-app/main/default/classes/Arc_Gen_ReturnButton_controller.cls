/**
* @File Name          : Arc_Gen_ReturnButton_controller.cls
* @Description        : Controller that return an ARCE to the before status.
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE
* @Last Modified By   : luisruben.quinto.munoz@bbva.com
* @Last Modified On   : 23/7/2019 20:32:19
* @Changes
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    3/5/2019 13:14:27   eduardoefrain.hernandez.contractor@bbva.com     Initial Version
**/
public without sharing class Arc_Gen_ReturnButton_controller {
    /**
    *-------------------------------------------------------------------------------
    * @description Empty priavate constructor
    --------------------------------------------------------------------------------
    * @author eduardoefrain.hernandez.contractor@bbva.com
    * @date 2019-05-13
    * @example private Arc_Gen_ReturnButton_controller ()
    **/
    @TestVisible
    private Arc_Gen_ReturnButton_controller () {
    }

    /**
    *-------------------------------------------------------------------------------
    * @description initDelegation
    *--------------------------------------------------------------------------------
    * @date   15/01/2020
    * @author juanignacio.hita.contractor@bbva.com
    * @param  Id : accHasAnalysisId
    * @return String
    * @example  String res = initDelegation(ambit, accHasId, action)
    */
    @AuraEnabled
    public static String initDelegation(Id accHasAnalysisId) {
      try {
        final arce__Analysis__c arceAnalysis = Arc_Gen_ArceAnalysis_Data.gerArce(accHasAnalysisId);
        final Arc_Gen_User_Wrapper wrpUser = Arc_Gen_User_Locator.getUserInfo(System.UserInfo.getUserId());
        final Arc_Gen_Delegation_Wrapper wrapper = Arc_Gen_Propose_Helper.initDelegation(wrpUser.ambitUser, arceAnalysis.Id, 'RETURN');
        return JSON.serialize(wrapper);
      } catch (Exception e) {
          throw new AuraHandledException(System.Label.Arc_Gen_ApexCallError + e);
      }
    }
    /**
    *-------------------------------------------------------------------------------
    * @description initIdentification
    *--------------------------------------------------------------------------------
    * @date   15/01/2020
    * @author juanignacio.hita.contractor@bbva.com
    * @param  Object : delegation wrapper
    * @return String
    * @example  String res = initIdentification(wrapper)
    */
    @AuraEnabled
    public static String initIdentification(Object wrapper) {
        try {
            final Arc_Gen_Delegation_Wrapper wrapperSerialize = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) wrapper, Arc_Gen_Delegation_Wrapper.class);
            final Arc_Gen_User_Wrapper wrpUser = Arc_Gen_User_Locator.getUserInfo(System.UserInfo.getUserId());
            return Arc_Gen_Propose_Helper.initIdentification(wrpUser.ambitUser, wrapperSerialize.analysisId);
        } catch (Exception e) {
            throw new AuraHandledException(System.Label.Arc_Gen_ApexCallError + e);
        }
    }
    /**
    *-------------------------------------------------------------------------------
    * @description evaluateIdentification
    *--------------------------------------------------------------------------------
    * @date   15/01/2020
    * @author juanignacio.hita.contractor@bbva.com
    * @param  Id : accHasAnalysisId
    * @param  Object : wrapper
    * @param  String : userId
    * @return String : JSON serialize of the delegation wrapper
    * @example  String res = evaluateIdentification(accHasId, wrapper, userId);
    */
    @AuraEnabled
    public static String evaluateIdentification(Id accHasAnalysisId, Object wrapper, String userId, String reason) {
        try {
            final Arc_Gen_Delegation_Wrapper wrapperRet = Arc_Gen_ReturnButton_service.evaluateIdentification(accHasAnalysisId, wrapper, userId, reason);
            return JSON.serialize(wrapperRet);
        } catch (Exception e) {
            throw new AuraHandledException(System.Label.Arc_Gen_ApexCallError + e);
        }
    }
}