/**
* @File Name          : Arc_Gen_RatingVariablesTable_service.cls
* @Description        : Contains the logic of the rating variables table json contruction
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE Group
* @Last Modified By   : eduardoefrain.hernandez.contractor@bbva.com
* @Last Modified On   : 24/7/2019 16:00:00
* @Changes
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    30/4/2019 17:54:42   eduardoefrain.hernandez.contractor@bbva.com     Initial Version
* 1.1    24/7/2019 10:49:00   eduardoefrain.hernandez.contractor@bbva.com     Fix incidences
* 1.1    26/04/2020 17:29:51   javier.soto.carrascosar@bbva.com     Add missing exception
**/
public without sharing class Arc_Gen_RatingVariablesTable_service {
/**
* @Class: VariablesWrapper
* @Description: Wrapper that contain all the level one variables of the rating
* @author BBVA
*/
    public Class VariablesWrapper {
        /**
        * @Description: Description name of the variable
        */
        @AuraEnabled private String description {get;set;}
        /**
        * @Description: Number of the variable score
        */
        @AuraEnabled private String score {get;set;}
        /**
        * @Description: Value of the variable
        */
        @AuraEnabled private String value {get;set;}
        /**
        * @Description: Maximum score number of the variable
        */
        @AuraEnabled private String maxScore {get;set;}
        /**
        * @Description: List of variables level two of this variable
        */
        @AuraEnabled private List<Items> items {get;set;}
    }
/**
* @Class: Items
* @Description: Wrapper that contain all the level two variables of the rating
* @author BBVA
*/
    public Class Items {
        /**
        * @Description: Description name of the variable
        */
        @AuraEnabled private String description {get;set;}
        /**
        * @Description: Number of the variable score
        */
        @AuraEnabled private String score {get;set;}
        /**
        * @Description: Value of the variable
        */
        @AuraEnabled private String value {get;set;}
        /**
        * @Description: Maximum score number of the variable
        */
        @AuraEnabled private String maxScore {get;set;}
    }
/**
* @Class: Items
* @Description: Wrapper that contain all the level two qualitative variables of the rating
* @author BBVA
*/
    public Class QualitativeItems {
        /**
        * @Description: Description name of the variable
        */
        @AuraEnabled private String description {get;set;}
        /**
        * @Description: Qualitative response of this variable
        */
        @AuraEnabled private String answer {get;set;}
    }
/**
*-------------------------------------------------------------------------------
* @description Method that obtains the Json to construct the table
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param List<arce__rating_variables_detail__c> variablesLevelOne
* @param List<arce__rating_variables_detail__c> variablesLevelTwo
* @return String - The json to construct the table
* @example public static String setTableJson(List<arce__rating_variables_detail__c> variablesLevelOne,List<arce__rating_variables_detail__c> variablesLevelTwo)
**/
    public static String setTableJson(List<arce__rating_variables_detail__c> variablesLevelOne,List<arce__rating_variables_detail__c> variablesLevelTwo) {

        Map<String,List<arce__rating_variables_detail__c>> variablesMap = new Map<String,List<arce__rating_variables_detail__c>>();
        for(arce__rating_variables_detail__c varOne : variablesLevelOne) {
            Boolean variableWithChilds = false;
            for(arce__rating_variables_detail__c varTwo : variablesLevelTwo) {
                if(varOne.arce__rating_variable_name__c == varTwo.arce__rating_variable_name__c) {
                    variableWithChilds = true;
                    if(variablesMap.containsKey(varOne.arce__rating_variable_name__c)) {
                        variablesMap.get(varOne.arce__rating_variable_name__c).add(varTwo);
                    }else{
                        variablesMap.put(varOne.arce__rating_variable_name__c, new List<arce__rating_variables_detail__c>{varTwo});
                    }
                }
            }
            if(!variableWithChilds) {
                variablesMap.put(varOne.arce__rating_variable_name__c, new List<arce__rating_variables_detail__c>{});
            }
        }
        List<VariablesWrapper> variablesList = new List<VariablesWrapper>();
        for(arce__rating_variables_detail__c var : variablesLevelOne) {
            VariablesWrapper variableLevelOne = new VariablesWrapper();
            variableLevelOne.description = (String)var.rating_variable_large_id__c;
            variableLevelOne.score = String.valueOf(var.arce__rating_var_score_number__c);
            variableLevelOne.value = String.valueOf(var.arce__rating_var_value_amount__c);
            variableLevelOne.maxScore = String.valueOf(var.arce__rating_var_max_score_number__c);
            List<Items> itemsList = new List<Items>();
            for(arce__rating_variables_detail__c item : variablesMap.get(var.arce__rating_variable_name__c)) {
                Items variableLevelTwo = new Items();
                variableLevelTwo.description = (String)item.rating_variable_large_id__c;
                variableLevelTwo.score = String.valueOf(item.arce__rating_var_score_number__c);
                variableLevelTwo.value = String.valueOf(item.arce__rating_var_value_amount__c);
                variableLevelTwo.maxScore = String.valueOf(item.arce__rating_var_max_score_number__c);
                itemsList.add(variableLevelTwo);
            }
            variableLevelOne.items = itemsList;
            variablesList.add(variableLevelOne);
        }
        Return JSON.serializePretty(variablesList);
    }
/**
*-------------------------------------------------------------------------------
* @description Method that obtains the level one variables to be shown in the table
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String ratingId
* @return List<arce__rating_variables_detail__c>
* @example public static List<arce__rating_variables_detail__c> getVariablesLevelOne(String ratingId)
**/
    public static List<arce__rating_variables_detail__c> getVariablesLevelOne(String ratingId) {
        Arc_Gen_RatingVariablesTable_data locator = new Arc_Gen_RatingVariablesTable_data();
        final List<arce__rating_variables_detail__c> variables = locator.getVariablesByLevel(ratingId,'1');
        Return variables;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that obtains the level two variables to be shown in the table
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String ratingId
* @return List<arce__rating_variables_detail__c>
* @example public static List<arce__rating_variables_detail__c> getVariablesLevelOne(String ratingId)
**/
    public static List<arce__rating_variables_detail__c> getVariablesLevelTwo(String ratingId) {
        Arc_Gen_RatingVariablesTable_data locator = new Arc_Gen_RatingVariablesTable_data();
        final List<arce__rating_variables_detail__c> variables = locator.getVariablesByLevel(ratingId,'2');
        Return variables;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that obtains qualitative variable to be shown in the table
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String ratingId
* @return List<arce__rating_variables_detail__c>
* @example public static String setQualitativeVariable(String ratingId)
**/
    public static String setQualitativeVariable(String ratingId) {
        Arc_Gen_RatingVariablesTable_data locator = new Arc_Gen_RatingVariablesTable_data();
        List<QualitativeItems> variables = new List<QualitativeItems>();
        String qualitativeString;
        List<arce__rating_variables_detail__c> qualitativeVariables = locator.getQualitativeVariables(ratingId);
        if (qualitativeVariables.isEmpty()) {
            throw new QueryException('No rating variables found');
        } else {
            for(arce__rating_variables_detail__c var : qualitativeVariables) {
                QualitativeItems variable = new QualitativeItems();
                variable.description = (String)var.rating_variable_large_id__c;
                variable.answer = '';
                variables.add(variable);
            }
            qualitativeString = JSON.serializePretty(variables);
        }
        Return qualitativeString;
    }
}