/**
  * @File Name          :   public with sharing class Arc_Gen_Remove_Controller.cls
  * @Description        : Controller class for the remove functionality
  * @Author             : luisarturo.parra.contractor@bbva.com
  * @Group              : ARCE
  * @Last Modified By   : luisruben.quinto.munoz@bbva.com
  * @Last Modified On   : 24/7/2019 13:44:32
  * @Modification Log   :
  *==============================================================================
  * Ver         Date                     Author                 Modification
  *==============================================================================
  * 1.0    5/7/2019 12:50:32   luisarturo.parra.contractor@bbva.com     Initial Version
  * 1.1    6/21/2019 12:50:32  ismaelyovani.obregon.contractor@bbva.com     Sanction 1.0 Q3 Added Method that gets value of picklist.
  **/
public with sharing class Arc_Gen_Remove_Controller {
    /**
    *-------------------------------------------------------------------------------
    * @description set new values for arce
    --------------------------------------------------------------------------------
    * @author luisarturo.parra.contractor@bbva.com
    * @date 20/05/2019
    * @param String recordId of the arce to update status
    * @param String reason for the update
    * @return List < arce__Account_has_Analysis__c >
    * @example public static void updateStatusController(String recordId)
    **/
  @AuraEnabled
  public static void updateRemoveInfo(Id recordId,String reasonPick, String reasonDesc, String reasonPickLabel) {
    try {
      Arc_Gen_Remove_Service.updateRemoveInfo(recordId, reasonPick, reasonDesc, reasonPickLabel);
      } catch (Exception e) {
        throw new AuraHandledException(System.Label.Arc_Gen_ApexCallError+e);
    }
  }
    /**
    *-------------------------------------------------------------------------------
    * @description get discard reasons picklist values.
    --------------------------------------------------------------------------------
    * @author ismaelyovani.obregon.contractor@bbva.com
    * @date 21/06/2019
    * @param void
    * @return List<map<String,String>> of pick list values and labels
    * @example List<map<String,String>> getDiscardList()
    **/
  @AuraEnabled
  public static List<map<String,String>> getDiscardList(String varObject,String varField) {
      List<map<String,String>> listReasons = new List<map<String,String>>();
      try{
          listReasons = Arc_Gen_Remove_service.getDiscardReasons();
        } catch (Exception e) {
          throw new AuraHandledException(System.Label.Arc_Gen_ApexCallError+e);
      }
      return listReasons;
  }
}