/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_BasicData_Pers_service
* @Author   Javier Soto Carrascosa
* @Date     Created: 04/042020
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Class that manages save for Basic Data
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |2020-04-04 Javier Soto Carrascosa
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
global class Arc_Gen_BasicData_Pers_service implements dyfr.Save_Interface {//NOSONAR
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that is responsible for invoking the classes to save the
      basic data information.
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param listObject - List of the account has analisys object
    * @return String with the execution message
    * @example save (listObject)
    * -----------------------------------------------------------------------------------------------
    **/
    public static String save(List<sObject> listObject) {
        Map<String, Object> basicInfoMap = new Map<String, Object>();
        Map<String, Object> bussinessRiskMap = new Map<String, Object>();
        final arce__Account_has_Analysis__c ahaData = Arc_Gen_Persistence_Utils.getAhaFromSobject(listObject);
        final arce__Account_has_Analysis__c accHasAnalysis = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<String>{(String)ahaData.Id})[0];
        basicInfoMap = basicInfoJSON(ahaData, accHasAnalysis);
        bussinessRiskMap = businessRiskJSON(ahaData, accHasAnalysis);
        final Map<Id, Arc_Gen_Account_Wrapper> listacc = Arc_Gen_Account_Locator.getAccountInfoById(new List<String>{accHasAnalysis.arce__Customer__c});
        final String participantId = Arc_Gen_CallEncryptService.getEncryptedClient(listacc.get(accHasAnalysis.arce__Customer__c).accNumber);
        final boolean basicInfo = Arc_Gen_OraclePers_service.basicInfoWS(participantId, accHasAnalysis.Name, basicInfoMap);
        final boolean businessRisk = Arc_Gen_OraclePers_service.businessRiskWS(participantId, accHasAnalysis.Name, bussinessRiskMap);
        return JSON.serialize(new Arc_Gen_wrpSave(basicInfo&&businessRisk,'',listObject));
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that is responsible for generating the basic info indicators Object
      basic data information.
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @return List<Map<String, Object>> with WS structure
    * @example addIndicators(ahaData)
    * -----------------------------------------------------------------------------------------------
    **/
    private static List<Map<String, Object>> addIndicators(arce__Account_has_Analysis__c ahaData, arce__Account_has_Analysis__c prevAha) {
        List<Map<String, Object>> listMapIndicators = new List<Map<String, Object>>();
        Map<String, String> mapIndicators = new Map<String,String>{'arce__enterprise_group_type__c'=>'BELONGS_TO_ECONOMIC_GROUP', 'arce__participant_leveraged_type__c'=>'HAS_PREVIOUS_LEVERAGE', 'arce__client_refinanced_type__c'=>'IS_REFINANCED'};
        Map<String, String> indicatorsComments = new Map<String,String>{'arce__enterprise_group_type__c'=>'arce__enterprise_group_desc__c', 'arce__client_refinanced_type__c'=>'arce__client_refinanced_desc__c'};
        for (string element : mapIndicators.keyset()) {
            if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get(element)) || Arc_Gen_ValidateInfo_utils.isFilled((String)prevAha.get(element))) {
                Map<String, Object> indicatorMap = new Map<String, Object>();
                indicatorMap.put('id',mapIndicators.get(element));
                indicatorMap.put('isActive',Arc_Gen_Persistence_Utils.booleanFromYesNo((String)ahaData.get(element)));
                if (Arc_Gen_ValidateInfo_utils.isFilled(indicatorsComments.get(element))) {
                    indicatorMap.put('description',ahaData.get(indicatorsComments.get(element)));
                }
                listMapIndicators.add(indicatorMap);
            }
        }
        return listMapIndicators;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that is responsible for generating the basic info indicators Object
      basic data information.
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param string - key
    * @return Map<String, Object> with WS structure for sector, subsector and activity
    * @example addSector(ahaData, key)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> addSector(arce__Account_has_Analysis__c ahaData, String key, arce__Account_has_Analysis__c prevAha) {
        Map<String, String> assetMap = new Map<String,String>{'sector'=>'arce__sector_desc__c', 'subsector'=>'arce__subsector_desc__c', 'economicActivity' => 'arce__economic_activity_sector_desc__c'};
        Map<String, String> assetTemplate = new Map<String,String>{'economicActivity' => 'arce__rating_econ_sector_tmpl_id__c'};
        Map<String, Object> idMap =  new Map<String, Object>();
        if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get(assetMap.get(key))) || Arc_Gen_ValidateInfo_utils.isFilled((String)prevAha.get(assetMap.get(key)))) {
            idMap.put('id',Arc_Gen_Persistence_Utils.defaultValueList((String)ahaData.get(assetMap.get(key))));
            if (Arc_Gen_ValidateInfo_utils.isFilled(assetTemplate.get(key)) && (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get(assetTemplate.get(key))) || Arc_Gen_ValidateInfo_utils.isFilled((String)prevAha.get(assetTemplate.get(key))))) {
                idMap.put('assetTemplate',ahaData.get(assetTemplate.get(key)));
            }
        }
        return idMap;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that is responsible for adding the default sector if needed
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param string - key
    * @return Map<String, Object> with WS structure for sector, subsector and activity
    * @example addSector(ahaData, key)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> addDefaultSector(Map<String, Object> finalWSMap, Map<String, Object> sector) {
        final Map<String, Object> sector0 = new Map<String, Object>{'id' => '0'};
        final Map<String, Object> defaultSector = new Map<String, Object>{'sector' => sector0};
        if (Arc_Gen_ValidateInfo_utils.hasInfoMapObj(finalWSMap) && !Arc_Gen_ValidateInfo_utils.hasInfoMapObj(sector)) {
            finalWSMap.put('sector', defaultSector);
        }
        return finalWSMap;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that generates JSON for basic-info WS
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param string - key
    * @return Map<String, Object> with WS structure for Basic Info WS
    * @example basicInfoJSON(ahaData)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> basicInfoJSON (arce__Account_has_Analysis__c ahaData, arce__Account_has_Analysis__c prevAha){
        Map<String, Object> finalWSMap = new Map<String, Object>();
        final List<Map<String, Object>> indicators = addIndicators(ahaData, prevAha);
        final Map<String, Object> sector = addSector(ahaData, 'sector', prevAha);
        final Map<String, Object> subsector = addSector(ahaData, 'subsector', prevAha);
        final Map<String, Object> economicActivity = addSector(ahaData, 'economicActivity', prevAha);
        finalWsMap = Arc_Gen_Persistence_Utils.addifFilled(finalWsMap,'economicActivity',economicActivity);
        finalWsMap = Arc_Gen_Persistence_Utils.addifFilled(finalWsMap,'subsector',subsector);
        finalWsMap = Arc_Gen_Persistence_Utils.addifFilledList(finalWsMap,'indicators',indicators);
        finalWsMap = Arc_Gen_Persistence_Utils.addifFilled(finalWsMap,'sector',sector);
        finalWsMap = addDefaultSector(finalWsMap, sector);
        return finalWSMap;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that is responsible for generating the basic info indicators Object
      basic data information.
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param string - key
    * @return Map<String, Object> with WS structure for sector, subsector and activity
    * @example addSector(ahaData, key)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> addManagementSub (arce__Account_has_Analysis__c ahaData, String key) {
        Map<String, String> managementMap = new Map<String,String>{'managementExperience'=>'arce__years_experience_mngmt_type__c', 'riskProfile'=>'arce__mngmt_aggressiveness_type__c', 'managementStyle' => 'arce__management_style_type__c'};
        Map<String, String> commentsMap = new Map<String,String>{'managementExperience' => 'arce__years_experience_mngmt_desc__c','riskProfile' => 'arce__fin_aggressiveness_mngmt_desc__c','managementStyle' => 'arce__management_style_desc__c'};
        Map<String, Object> idMap =  new Map<String, Object>();
        if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get(managementMap.get(key)))) {
            String enumValue = managementEnum((String)ahaData.get(managementMap.get(key)),key);
            idMap.put('id',enumValue);
            if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get(commentsMap.get(key)))) {
                idMap.put('comments',ahaData.get(commentsMap.get(key)));
            }
        }
        return idMap;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that generates JSON for basic-info WS
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param string - key
    * @return Map<String, Object> with WS structure for Basic Info WS
    * @example businessRiskJSON(ahaData)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> businessRiskJSON (arce__Account_has_Analysis__c ahaData, arce__Account_has_Analysis__c prevAha) {
        Map<String, Object> finalManagementMap = new Map<String, Object>();
        Map<String, Object> finalWSMap = new Map<String, Object>();
        final Map<String, Object> managementExperience = addManagementSub(ahaData, 'managementExperience');
        if (Arc_Gen_ValidateInfo_utils.hasInfoMapObj(managementExperience)) {
            final Map<String, Object> riskProfile = addManagementSub(ahaData, 'riskProfile');
            final Map<String, Object> managementStyle = addManagementSub(ahaData, 'managementStyle');
            finalManagementMap = Arc_Gen_Persistence_Utils.addifFilled(finalManagementMap,'managementExperience',managementExperience);
            finalManagementMap = Arc_Gen_Persistence_Utils.addifFilled(finalManagementMap,'riskProfile',riskProfile);
            finalManagementMap = Arc_Gen_Persistence_Utils.addifFilled(finalManagementMap,'managementStyle',managementStyle);
        }
        if (Arc_Gen_ValidateInfo_utils.hasInfoMapObj(finalManagementMap)) {
            finalWSMap.put('management',finalManagementMap);
        }
        return finalWSMap;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that generates JSON for basic-info WS
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param String - value
    * @param string - key
    * @return Map<String, Object> with WS structure for Basic Info WS
    * @example managementEnum(value, key)
    * -----------------------------------------------------------------------------------------------
    **/
    private static String managementEnum (String value, String key) {
        final Map<String, String> experience = new Map<String, String>{'1'=>'FROM_0_TO_5','2'=>'FROM_6_TO_10','3'=>'MORE_THAN_10'};
        final Map<String, String> aggressiveness = new Map<String, String>{'1'=>'AGGRESSIVE','2'=>'MEDIUM','3'=>'CONSERVATIVE'};
        final Map<String, String> profileInvestor = new Map<String, String>{'1'=>'PERSONALIST','2'=>'PROFESSIONAL','3'=>'FAMILIAR'};
        final Map<String, Map<String, String>> enumMaps = new Map<String, Map<String, String>>{'managementStyle'=>profileInvestor,'managementExperience'=>experience,'riskProfile'=>aggressiveness};
        final Map<String, String> mapForKey = enumMaps.get(key);
        return Arc_Gen_Persistence_Utils.getEnumFromMap(value, mapForKey);
    }

}