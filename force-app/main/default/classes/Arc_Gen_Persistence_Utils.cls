/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Persistence_Utils
* @Author   Javier Soto Carrascosa
* @Date     Created: 04/042020
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Class that manages save for Basic Data
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |2020-04-04 Javier Soto Carrascosa
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_Persistence_Utils {
    /*------------------------------------------------------------------------------------------------------
    *@Description Builder Arc_Gen_Persistence_Utils
    * -----------------------------------------------------------------------------------------------------
    * @Author   Javier Soto
    * @Date     2020-04-06
    * @param    null
    * @return   Arc_Gen_Persistence_Utils
    * @example  new Arc_Gen_Persistence_Utils()
    * */
    @TestVisible
    private Arc_Gen_Persistence_Utils() {}
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Custom Exception for Persistence Utils
    * ----------------------------------------------------------------------------------------------------
    * @Author   Javier Soto Carrascosa  javier.soto.carrascosa@bbva.com
    * @Date     Created: 2020-02-24
    * @example throw new PersistenceException('Unexpected Error');
    * ----------------------------------------------------------------------------------------------------
    **/
    public class PersistenceException extends Exception {}
    /**
    *-------------------------------------------------------------------------------
    * @description  Method getAhaFromSobject
    --------------------------------------------------------------------------------
    * @author javier.soto.carrascosa@bbva.com
    * @date 2020-04-04
    * @param List<sObject> listObject
    * @return   arce__Account_has_Analysis__c
    * @example getAhaFromSobject(listObject)
    **/
    public static arce__Account_has_Analysis__c getAhaFromSobject(List<sObject> listObject) {
        arce__Account_has_Analysis__c ahaData = new arce__Account_has_Analysis__c();
        Integer ahaCount = 0;
        for(sObject obj : listObject) {
            switch on obj {
                when arce__Account_has_Analysis__c aha {
                    ahaCount++;
                    ahaData = aha;
                }
            }
        }
        if (ahaCount>1 || ahaCount==0) {
            throw new PersistenceException('Unexpected persistence error');
        }
        return ahaData;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that fields Map and Key if Map is filled
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param Map<String, Object> finalWSMap
    * @param string - key
    * @param Map<String,Object>  - keyValue
    * @return Map<String, Object> with WS structure for objects that are filled
    * @example addifFilled(finalWSMap,key,keyValue)
    * -----------------------------------------------------------------------------------------------
    **/
    public static Map<String, Object> addifFilled(Map<String, Object> finalWSMap, String key, Map<String,Object> keyValue) {
        if (Arc_Gen_ValidateInfo_utils.hasInfoMapObj(keyValue)) {
            finalWSMap.put(key,keyValue);
        }
        return finalWSMap;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that fields Map and Key if List Map is filled
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param List<Map<String, Object>> finalWSMap
    * @param string - key
    * @param Map<String,Object>  - keyValue
    * @return Map<String, Object> with WS structure for objects that are filled
    * @example addifFilled(finalWSMap,key,keyValue)
    * -----------------------------------------------------------------------------------------------
    **/
    public static Map<String, Object> addifFilledList(Map<String, Object> finalWSMap, String key, List<Map<String,Object>> keyValue) {
        if (Arc_Gen_ValidateInfo_utils.hasInfoListMapObj(keyValue)) {
            finalWSMap.put(key,keyValue);
        }
        return finalWSMap;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that is responsible for business risk info callout
      basic data information.
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param String - iasoWS
    * @param String - jsonWS
    * @return boolean - callout executed OK / KO
    * @example addIndicators(ahaData)
    * -----------------------------------------------------------------------------------------------
    **/
    public static Boolean executePersistence(String iasoWS, String jsonWS) {
        boolean res;
        Arc_Gen_ServiceAndSaveResponse serviceAndSaveResp = new Arc_Gen_ServiceAndSaveResponse();
        Arc_Gen_getIASOResponse.serviceResponse iasoResponse = new Arc_Gen_getIASOResponse.serviceResponse();
        iasoResponse = Arc_Gen_getIASOResponse.calloutIASO(iasoWS,jsonWS);
        serviceAndSaveResp.serviceCode = iasoResponse.serviceCode;
        serviceAndSaveResp.serviceMessage = iasoResponse.serviceMessage;
        if(serviceAndSaveResp.serviceCode == '200' || serviceAndSaveResp.serviceCode == '201' || serviceAndSaveResp.serviceCode == '204') {
            res = true;
        } else {
            PersistenceException excep = new PersistenceException();
            excep.setMessage('Unexpected persistence error');
            throw excep;
        }
        return res;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method to retrieve ENUM from map an code
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param string - key
    * @param Map<String,String> - map of enums
    * @return String
    * @example getEnumFromMap(code,originMap)
    * -----------------------------------------------------------------------------------------------
    **/
    public static String getEnumFromMap(String code, Map<String,String> originMap) {
        String finalEnum = '';
        if (originMap.containsKey(code)) {
            finalEnum = originMap.get(code);
        } else {
            throw new PersistenceException('Error Mapping ENUM, code does not exist.');
        }
        return finalEnum;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method to retrieve default value for list selecto
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param string - key
    * @return String
    * @example defaultValueList(code)
    * -----------------------------------------------------------------------------------------------
    **/
    public static String defaultValueList(String code) {
        String returnCode;
        if (Arc_Gen_ValidateInfo_utils.isFilled(code)) {
            returnCode = code;
        } else {
            returnCode = '0';
        }
        return returnCode;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method to retrieve default value for list selecto
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param string - key
    * @return String
    * @example defaultValueList(code)
    * -----------------------------------------------------------------------------------------------
    **/
    public static Boolean booleanFromYesNo(String value) {
        Boolean returnBool;
        if (value==null || value == '2') {
            returnBool = false;
        } else {
            returnBool = true;
        }
        return returnBool;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method to retrieve default value for list selecto
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param string - key
    * @return String
    * @example defaultValueList(code)
    * -----------------------------------------------------------------------------------------------
    **/
/**
*-------------------------------------------------------------------------------
* @description  Get Configuration Information
*--------------------------------------------------------------------------------
* @author javier.soto.carrascosa@bbva.com
* @date 10/04/2020
* @param configurationName - String with the configuration name
* @return Service_persistence__mdt
* @example public Service_persistence__mdt getConfigurationInfo(String configurationName)
*--------------------------------------------------------------------------------
**/
public static Map<String, Service_persistence__mdt> getConfigurationInfo(List<String> configurationNames) {
    Map<String, Service_persistence__mdt> mapListConfigs = new Map<String, Service_persistence__mdt>();
    List<Service_persistence__mdt> mdtConfigList = new List<Service_persistence__mdt>([SELECT DeveloperName, Id, JSON_input__c, api_field_Name__c, iasoVariables__c FROM Service_persistence__mdt where DeveloperName=:configurationNames]);
    if (mdtConfigList.size() == configurationNames.size()) {
        for (Service_persistence__mdt configElement : mdtConfigList) {
            mapListConfigs.put(configElement.DeveloperName, configElement);
        }
    } else {
        throw new QueryException('Error, missing Persistence Configuration'+configurationNames);
    }
    return mapListConfigs;
}
}