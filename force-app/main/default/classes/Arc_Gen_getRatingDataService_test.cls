/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_RatingStudiosUpdateTable
* @Author   Eduardo Efraín Hernández Rendón  eduardoefrain.hernandez.contractor@bbva.com
* @Date     Created: 28/10/2019
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Tests the classes getRatingDataService and RatingVariablesTable
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |28/10/2019 eduardoefrain.hernandez.contractor@bbva.com
*             Class creation.
* |23/7/2019 eduardoefrain.hernandez.contractor@bbva.com
*             Added comments, localism and updates test coverage
* |26/9/2019 javier.soto.carrascosa@bbva.com
*             Remove incorrect test method
* |21/10/2019 eduardoefrain.hernandez.contractor@bbva.com
*             Update test coverage for studies and overlay
* |28/10/2019 eduardoefrain.hernandez.contractor@bbva.com
*             Add static service parameters
* |2019-12-02 german.sanchez.perez.contractor@bbva.com | franciscojavier.bueno@bbva.com
*             Api names modified with the correct name on business glossary
* |2020-01-08 javier.soto.carrascosa@bbva.com
*             Adapt test classess with account wrapper and setupaccounts
* |24/01/2020 juanmanuel.perez.ortiz.contractor@bbva.com
*             Remove logic static parameters to ASO services
* -----------------------------------------------------------------------------------------------
*/
@isTest
public class Arc_Gen_getRatingDataService_test {
    /**
    * @Description: String with external id of test group
    */
    static final string GROUP_ID = 'G000001';
    /**
    * --------------------------------------------------------------------------------------
    * @Description setup test
    * --------------------------------------------------------------------------------------
    * @Author   javier.soto.carrascosa@bbva.com
    * @Date     Created: 2020-01-08
    * @param void
    * @return void
    * @example setup()
    * --------------------------------------------------------------------------------------
    **/
    @testSetup static void setup() {
    Arc_UtilitysDataTest_tst.setupAcccounts();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param String name - Name of the arce
    * @return  arce__Analysis__c
    * @example setAnalysis(String name)
    * --------------------------------------------------------------------------------------
    **/
    public static arce__Analysis__c setAnalysis(String name) {
        arce__Analysis__c analysis = new arce__Analysis__c(
            Name = name
        );
        Insert analysis;
        Return analysis;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param String clientId - Id of the account
    * @param String analysisId - Id of the arce
    * @param String validFs - Id of valid FS
    * @return arce__Account_has_Analysis__c
    * @example setAnalyzedClient(String clientId,String analysisId,String validFs)
    * --------------------------------------------------------------------------------------
    **/
    public static arce__Account_has_Analysis__c setAnalyzedClient(String clientId,String analysisId,String validFs) {
        arce__Account_has_Analysis__c analyzedClient = new arce__Account_has_Analysis__c(
            arce__Customer__c = clientId,
            arce__Analysis__c = analysisId,
            arce__ffss_for_rating_id__c = validFs,
            arce__branch_id__c = '0002'
        );
        Insert analyzedClient;
        Return analyzedClient;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param String accName - Name of the account
    * @param String accNumber - Number of the account
    * @param String analysisName - Name of the arce
    * @param String validFs - Id of valid FS
    * @return arce__Account_has_Analysis__c
    * @example getAnalyzedClient(String accName,String accNumber,String analysisName,String validFs)
    * --------------------------------------------------------------------------------------
    **/
    public static arce__Account_has_Analysis__c getAnalyzedClient(String accNumber,String analysisName,String validFs) {
        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{accNumber});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(accNumber);
        arce__Analysis__c analysis = setAnalysis(analysisName);
        arce__Account_has_Analysis__c accHasAn = setAnalyzedClient(groupAccount.accId,analysis.Id,validFs);
        Return accHasAn;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param String ratingId - Id of the rating
    * @param String validInd - Valid indicator
    * @return arce__Financial_Statements__c
    * @example setFFSS(String ratingId, String validInd)
    * --------------------------------------------------------------------------------------
    **/
    public static arce__Financial_Statements__c setFFSS(String ratingId, String validInd) {
        arce__Financial_Statements__c ffss = new arce__Financial_Statements__c(
            arce__ffss_valid_type__c = validInd,
            arce__financial_statement_id__c = '70252018129',
            arce__rating_id__c = ratingId
        );
        Insert ffss;
        Return ffss;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param void
    * @return arce__rating__c
    * @example setRating()
    * --------------------------------------------------------------------------------------
    **/
    public static arce__rating__c setRating() {
        arce__rating__c rating = new arce__rating__c();
        Insert rating;
        Return rating;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param String analysisId - arce id
    * @param String ratingId - id of the rating
    * @return arce__rating_variables_detail__c
    * @example setRatingVariable(String analysisId,String ratingId)
    * --------------------------------------------------------------------------------------
    **/
    public static arce__rating_variables_detail__c setRatingVariable(String analysisId,String ratingId) {
        arce__rating_variables_detail__c ratingVariable = new arce__rating_variables_detail__c(
            arce__account_has_analysis_id__c = analysisId,
            arce__rating_id__c = ratingId
        );
        Insert ratingVariable;
        Return ratingVariable;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param void
    * @return void
    * @example getCustomerDataTest()
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    static void getCustomerDataTest() {
        arce__Account_has_Analysis__c accHasAn = getAnalyzedClient(GROUP_ID,'Analysis Test',null);
        test.startTest();
        Arc_Gen_getRatingDataService_controller.clientData customerData = Arc_Gen_getRatingDataService_controller.getCustomerData(accHasAn.Id);
        System.assertEquals(GROUP_ID, customerData.customerId, 'This obtains the externalid of the client');
        test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param void
    * @return void
    * @example testingFiancialAlertRating()
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    static void testingFiancialAlertRating() {
        Arc_Gen_ServiceAndSaveResponse response = new Arc_Gen_ServiceAndSaveResponse();
        final User userAdmin = Arc_UtilitysDataTest_tst.crearUsuario('UserAdmin',System.Label.Cls_arce_ProfileSystemAdministrator,'');
        userAdmin.federationIdentifier = 'XME0514';
        Insert userAdmin;
        final List<sObject> iasoCnfList = Arc_UtilitysDataTest_tst.crearIasoUrlsCustomSettings();
        insert iasoCnfList;
        final arce__rating__c rating = setRating();
        arce__Financial_Statements__c ffss = setFFSS(rating.Id, '1');
        arce__Account_has_Analysis__c accHasAn = getAnalyzedClient(GROUP_ID,'Analysis Test',ffss.Id);
        arce__rating_variables_detail__c ratingVariable = setRatingVariable(accHasAn.Id,rating.Id);
        test.startTest();
        System.runAs(userAdmin) {
            response = Arc_Gen_getRatingDataService_controller.setupRating(accHasAn.Id, accHasAn.arce__Customer__c, null);
            Arc_Gen_RatingVariablesTable_controller.getDataTable(rating.Id);
            Arc_Gen_RatingVariablesTable_controller.getQualitativeTable(rating.Id);
        }
        System.assertEquals('true', response.saveStatus, 'The DML status were successful, the response is true');
        test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param void
    * @return void
    * @example testingStageRating()
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    static void testingStageRating() {
        Arc_Gen_ServiceAndSaveResponse response = new Arc_Gen_ServiceAndSaveResponse();
        final User userAdmin = Arc_UtilitysDataTest_tst.crearUsuario('UserAdmin',System.Label.Cls_arce_ProfileSystemAdministrator,'');
        userAdmin.federationIdentifier = 'XME0514';
        Insert userAdmin;
        final List<sObject> iasoCnfList = Arc_UtilitysDataTest_tst.crearIasoUrlsCustomSettings();
        insert iasoCnfList;
        final arce__rating__c rating = setRating();
        final arce__Financial_Statements__c ffss = setFFSS(rating.Id, '1');
        final arce__Account_has_Analysis__c accHasAn = getAnalyzedClient(GROUP_ID,'Analysis Test',ffss.Id);
        setRatingVariable(accHasAn.Id,rating.Id);
        test.startTest();
        System.runAs(userAdmin) {
            response = Arc_Gen_getRatingDataService_controller.setupRating(accHasAn.Id, accHasAn.arce__Customer__c, 'ratingEngineStageMock');
            Arc_Gen_RatingVariablesTable_controller.getDataTable(rating.Id);
            Arc_Gen_RatingVariablesTable_controller.getQualitativeTable(rating.Id);
        }
        System.assertEquals('true', response.saveStatus, 'The DML status were successful, the response is true');
        test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param void
    * @return void
    * @example testingRatingNoValid()
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    static void testingRatingNoValid() {
        Arc_Gen_ServiceAndSaveResponse response = new Arc_Gen_ServiceAndSaveResponse();
        final User userAdmin = Arc_UtilitysDataTest_tst.crearUsuario('UserAdmin',System.Label.Cls_arce_ProfileSystemAdministrator,'');
        userAdmin.federationIdentifier = 'XME0514';
        Insert userAdmin;
        final List<sObject> iasoCnfList = Arc_UtilitysDataTest_tst.crearIasoUrlsCustomSettings();
        insert iasoCnfList;
        final arce__rating__c rating = setRating();
        arce__Financial_Statements__c ffss = setFFSS(rating.Id, '2');
        final arce__Account_has_Analysis__c accHasAn = getAnalyzedClient(GROUP_ID,'Analysis Test',ffss.Id);
        setRatingVariable(accHasAn.Id,rating.Id);
        test.startTest();
        System.runAs(userAdmin) {
            response = Arc_Gen_getRatingDataService_controller.setupRating(accHasAn.Id, accHasAn.arce__Customer__c, 'ratingEngineStageMock');
            Arc_Gen_RatingVariablesTable_controller.getDataTable(rating.Id);
            Arc_Gen_RatingVariablesTable_controller.getQualitativeTable(rating.Id);
        }
        System.assertEquals('false', response.saveStatus, 'The DML status were unsuccessful, the response is false');
        test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param void
    * @return void
    * @example testError400()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testError400() {
        Arc_Gen_ServiceAndSaveResponse response = new Arc_Gen_ServiceAndSaveResponse();
        final List<sObject> iasoCnfList = Arc_UtilitysDataTest_tst.crearIasoUrlsCustomSettings();
        insert iasoCnfList;
        final arce__rating__c rating = setRating();
        arce__Financial_Statements__c ffss = setFFSS(rating.Id, '1');
        final arce__Account_has_Analysis__c accHasAn = getAnalyzedClient(GROUP_ID,'Analysis Test',ffss.Id);
        setRatingVariable(accHasAn.Id,rating.Id);
        test.startTest();
        response = Arc_Gen_getRatingDataService_service.setupRating(accHasAn.Id, accHasAn.arce__Customer__c, '400', 'ratingEngineStageMock');
        Arc_Gen_RatingVariablesTable_controller.getDataTable('0');
        Arc_Gen_RatingVariablesTable_controller.getQualitativeTable('0');
        System.assertEquals('false', response.saveStatus, 'The DML status were successful, the response is true');
        test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param void
    * @return void
    * @example testingCalloutError()
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    static void testingCalloutError() {
        final arce__Account_has_Analysis__c accHasAn = getAnalyzedClient(GROUP_ID,null,null);
        test.startTest();
        final Arc_Gen_ServiceAndSaveResponse response = Arc_Gen_getRatingDataService_controller.setupRating(accHasAn.Id,accHasAn.arce__Customer__c,'Error');
        System.assertEquals('false', response.saveStatus, 'The response of the save record');
        test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param void
    * @return void
    * @example testingDML()
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    static void testingDML() {
        Arc_Gen_getRatingDataService_data locator = new Arc_Gen_getRatingDataService_data();
        Arc_Gen_getRatingDataService_data.saveResult result = new Arc_Gen_getRatingDataService_data.saveResult();
        final String longName = '12345678901234567890121234567890123456789012123456789012345678901212345678901234567890121234567890123456789012';
        List<arce__analysis__c> arceIns = new List<arce__analysis__c>{new arce__analysis__c(Name = longName)};
        arce__analysis__c arce1 = new arce__analysis__c(Name='Test1');
        insert arce1;
        arce__analysis__c arce2 = new arce__analysis__c(Name='Test2');
        insert arce2;
        arce1.Name = '';
        arce2.Name = '';
        List<arce__analysis__c> arceUp1 = new List<arce__analysis__c>{arce1};
        test.startTest();
        result = locator.insertRecords(arceIns);
        locator.updateRecords(arceUp1);
        locator.updateRecord(arce2);
        System.assertEquals('false', result.status, 'The DML status is wrong because of null parameter, the response is false');
        test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test setup method
    * --------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @param void
    * @return void
    * @example testingConstructor()
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    static void testingConstructor() {
        Test.startTest();
        Arc_Gen_Rating_data constructor = new Arc_Gen_Rating_data();
        final Arc_Gen_getRatingDataService_helper constHelper = new Arc_Gen_getRatingDataService_helper();
        System.assertEquals(constHelper, constHelper,'Empty helper constructor');
        System.assertEquals(constructor, constructor,'Empty constructor');
        Test.stopTest();
    }
}