/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_NewGroups_Service_Helper
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2020-13-01
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Helper class for Arc_Gen_NewGroups_service.
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2020-13-01 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* ------------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_NewGroups_Service_Helper {
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-13-01
    * @param void
    * @return void
    * @example Arc_Gen_NewGroups_Service_Helper helper = new Arc_Gen_NewGroups_Service_Helper()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_NewGroups_Service_Helper() {

    }
    /**
    *-------------------------------------------------------------------------------
    * @Description get a list of decrypted clients
    *-------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-13-01
    * @param listparticipants - list participant data
    * @return List of account number of decripted clients
    * @example getdecrytedClientsId(listparticipants)
    * -----------------------------------------------------------------------------
    */
    public static List<String> getdecrytedClientsId(Arc_Gen_CallListParticipant.Innertoreturnlistp listparticipants) {
        final List<String> decryptedClients =  new List<String>();
        final List<Arc_Gen_CallListParticipant.participantobj> participants = listparticipants.customersdata;/**get encrypted data of customers from service response**/
        for(Arc_Gen_CallListParticipant.participantobj participant : participants){
            decryptedClients.add(participant.participantId);
        }
        return decryptedClients;
    }
    /**
    *-------------------------------------------------------------------------------
    * @Description - Method to create group accounts
    *-------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-13-01
    * @param economicparticipants - economic participant data
    * @return Map<String, Arc_Gen_Account_Wrapper> - map with group info
    * @example getdecrytedClientsId(economicparticipants)
    * -----------------------------------------------------------------------------
    */
    public static Map<String, Arc_Gen_Account_Wrapper> createGroupAcc(Arc_Gen_CallEconomicParticipants.Innertoreturn economicparticipants) {
        Map<String, Arc_Gen_Account_Wrapper> groupWrap = new Map<String, Arc_Gen_Account_Wrapper>();
        Arc_Gen_Account_Wrapper accWrap = new Arc_Gen_Account_Wrapper();
        Arc_Gen_ServiceAndSaveResponse createGrp = new Arc_Gen_ServiceAndSaveResponse();
        Map<String, String> grpAcc = new Map<String, String>();/**if group is not finded the group is created**/
        grpAcc.put('Name', economicparticipants.groupinfo.groupname);
        grpAcc.put('AccountNumber', economicparticipants.groupinfo.groupid);
        createGrp = Arc_Gen_Account_Locator.createGroup(grpAcc);/**the group is inserted**/
		System.debug('createGrp:'+createGrp);
        accWrap.accId = createGrp.createdRsr[0];
        accWrap.name = economicparticipants.groupinfo.groupname;
        accWrap.participantType = 'Group';
        groupWrap.put(economicparticipants.groupinfo.groupid, accWrap);
        return groupWrap;
    }
    /**
    *-------------------------------------------------------------------------------
    * @Description - Method to create group accounts
    *-------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-13-01
    * @param mapAccWrapByAccNum - map with accounts info
    * @param groupInSF - map with group info
    * @return Map<String, Arc_Gen_Account_Wrapper> - map with accounts info updated
    * @example updateAccounts(mapAccWrapByAccNum, groupInSF)
    * -----------------------------------------------------------------------------
    */
    public static Map<String, Arc_Gen_Account_Wrapper> updateAccounts(Map<String, Arc_Gen_Account_Wrapper> mapAccWrapByAccNum, Arc_Gen_Account_Wrapper groupInSF) {
        Map<String, Arc_Gen_Account_Wrapper> accWrapByAccNum = mapAccWrapByAccNum;
        Map<Id, Map<String,String>>  mapAccsToUp = new Map<Id, Map<String,String>> ();
        for (String accNumber: mapAccWrapByAccNum.keySet()) {
            Map<String, String> accAttribute = new Map<String, String>();
            Arc_Gen_Account_Wrapper accWrap = new Arc_Gen_Account_Wrapper();
            accAttribute.put('ParentId', groupInSF.accId);
            mapAccsToUp.put(mapAccWrapByAccNum.get(accNumber).accId,accAttribute);
            accWrap = mapAccWrapByAccNum.get(accNumber);
            accWrap.accParentId = groupInSF.accId;
            accWrap.participantType = 'Client';
            accWrapByAccNum.put(accNumber, accWrap);
        }
        Arc_Gen_Account_Locator.accountUpdate(mapAccsToUp);
        return accWrapByAccNum;
    }
    /**
    *-------------------------------------------------------------------------------
    * @Description - Method to create group accounts
    *-------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-13-01
    * @param decryptedClients - list of decrypted clients
    * @param economicPartId - decrypted group
    * @param accountNumber - account number
    * @param groupInSF - map with group info
    * @return Arc_Gen_NewGroups_service.Returnstructure wrapper
    * @example getStructureFinal(decryptedClients, economicPartId, accountNumber, groupInSF)
    * -----------------------------------------------------------------------------
    */
    public static Arc_Gen_NewGroups_service.Returnstructure getStructureFinal(List<String> decryptedClients, String economicPartId, String accountNumber, Map<String, Arc_Gen_Account_Wrapper> groupInSF) {
        final Arc_Gen_NewGroups_service.Returnstructure structure = new Arc_Gen_NewGroups_service.Returnstructure();
        final List<Arc_Gen_Account_Wrapper> lstParticipantsWrap = new List<Arc_Gen_Account_Wrapper>();
        final List<Account> accountsInSF = new List<Account>();
        final Boolean updateStructure = Arc_Gen_GenericUtilities.getUpdateStructure();
        Map<String, Arc_Gen_Account_Wrapper> mapAccWrapByAccNum = Arc_Gen_Account_Locator.getAccountByAccNumber(decryptedclients);
        if (updateStructure) {
            mapAccWrapByAccNum = updateAccounts(mapAccWrapByAccNum, groupInSF.get(economicPartId));
        }
        for (String accNumber : mapAccWrapByAccNum.keySet()) {
            Arc_Gen_Account_Wrapper accWrapParticipant = new Arc_Gen_Account_Wrapper();
            Account accParticipant = new Account();
            accWrapParticipant = mapAccWrapByAccNum.get(accNumber);
            accParticipant.Id = accWrapParticipant.accId;
            accParticipant.Name = accWrapParticipant.name;
            accParticipant.AccountNumber = accWrapParticipant.accNumber;
            accParticipant.ParentId = groupInSF.get(economicPartId).accId;
            accountsInSF.add(accParticipant);
            lstParticipantsWrap.add(accWrapParticipant);
        }
        lstParticipantsWrap.add(groupInSF.get(economicPartId));
        structure.participantsinSF = accountsInSF;
        structure.participantsOnline = lstParticipantsWrap;
        structure.groupID = groupInSF.get(economicPartId) == null ? accountNumber :groupInSF.get(economicPartId).accId;
        structure.noGroupsInSf = false;
        return structure;
    }
}