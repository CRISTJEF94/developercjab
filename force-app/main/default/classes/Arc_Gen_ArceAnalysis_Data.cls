/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_ArceAnalysis_Data
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 29/07/2019
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Data class for object arce__Analysis__c
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |2019-07-29 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* |2019-08-27 luisruben.quinto.munoz@bbva.com
*             Deletes reference to arce__id__c.
* |2019-09-25 mariohumberto.ramirez.contractor@bbva.com
*             Added new methods gerArce, getArceForTraceability getIdsOfChildAccount
* |2019-09-30 mariohumberto.ramirez.contractor@bbva.com
*             Added new method getIdsOfChildAccount
* |2019-10-29 eduardoefrain.hernandez.contractor@bbva.com
*             Added new wrapper for global workflow users
* |2019-11-20 javier.soto.carrascosa@bbva.com
*             Added new fields to gerArce
* |2019-12-06 luisarturo.parar.contractor@bbva.com
*             Added field arce__wf_status_id__c
* |2019-12-10 juanmanuel.perez.ortiz.contractor@bbva.com
*             Modified getArceAnalysisData and getWFUsers methods
* |2019-12-18 juanmanuel.perez.ortiz.contractor@bbva.com
*             Add new field Temporal_status__c in getArceAnalysisData and changeStatusArce methods
* |2020-17-01 mariohumberto.ramirez.contractor@bbva.com
*             Change arce__Customer__r.ParentId for arce__group_asset_header_type__c in queries
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_ArceAnalysis_Data {
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 29/07/2019
    * @param void
    * @return void
    * @example Arc_Gen_ArceAnalysis_Data data = new Arc_Gen_ArceAnalysis_Data()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_ArceAnalysis_Data() {

    }
/**
*-------------------------------------------------------------------------------
* @description WorkflowUsers get info about workflow users for the analysis
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 2019-10-29
* @example WorkflowUsers wfu = new WorkflowUsers();
**/
    public class WorkflowUsers {
        /**
        * @Description: User id that has the ownership of the current workflow status
        */
        public string assignedUser {get; set;}
        /**
        * @Description: User id that has the ownership of the previous workflow status
        */
        public string previousUser {get; set;}
        /**
        * @Description: User id of the arce analysis owner or administrator
        */
        public string arceOwner  {get; set;}
        /**
        * @Description: Indicates if the current user is active for this workflow step
        */
        public Boolean activeUser  {get; set;}
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that return a list of all fields in arce__Analysis__c object
    * -----------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 29/07/2019
    * @param arceLtsId - List of ids of the arce__Account_has_Analysis__c object to consult
    * @return List<arce__Analysis__c> - List of all fields in arce__Analysis__c object
    * @example getArceAnalysisData(arceLtsId)
    * -----------------------------------------------------------------------------------------------
    **/
    public static List<arce__Analysis__c> getArceAnalysisData(List<Id> arceLtsId) {
        return [SELECT Name,OwnerId,arce__anlys_wkfl_prev_user_name__c,Temporal_status__c,arce__parent_analysis_id__c,arce__analysis_customer_relation_type__c,arce__anlys_wkfl_rturn_br_level_type__c,CurrencyIsoCode,arce__Group__c,arce__bbva_committees_type__c,arce__Rating__c,arce__recordTypeName__c,arce__Stage__c,arce__stage_collective_type__c,arce__wf_status_id__c,arce__anlys_wkfl_edit_br_level_type__c,arce__anlys_wkfl_user_edit_list_desc__c,arce__anlys_wkfl_committee_name__c,arce__anlys_wkfl_der_cmtee_list_desc__c,arce__anlys_wkfl_discard_reason_desc__c,arce__anlys_wkfl_discard_reason_id__c,arce__anlys_wkfl_der_br_level_type__c,arce__anlys_wkfl_return_reason_desc__c,arce__anlys_wkfl_sanction_rslt_type__c,arce__anlys_wkfl_sanction_rsn_desc__c,arce__anlys_wkfl_snctn_br_level_type__c,arce__anlys_wkfl_stage_type__c,arce__anlys_wkfl_status_stage_type__c,arce__anlys_wkfl_sub_process_type__c,arce__anlys_wkfl_edit_user_name__c
            FROM arce__Analysis__c
            WHERE id = :arceLtsId];
    }
/**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that return a list of all fields in arce__Analysis__c object
    * -----------------------------------------------------------------------------------------------
    * @Author   eduardoefrain.hernandez.contractor@bbva.com
    * @Date     Created: 29/07/2019
    * @param Id arceId - Id of the analyzed client
    * @return WorkflowUsers - Wrapper of the workflow involved users
    * @example getWFUsers(arceId)
    * -----------------------------------------------------------------------------------------------
    **/
    public static WorkflowUsers getWFUsers(Id arceId) {
        WorkflowUsers wfu = new WorkflowUsers();
        arce__Analysis__c arce = getArceAnalysisData(new List<Id>{arceId}).get(0);
        wfu.assignedUser = arce.OwnerId; //Implement locally
        wfu.previousUser = arce.arce__anlys_wkfl_prev_user_name__c; //Implement locally
        wfu.arceOwner = ''; //Implement locally
        wfu.activeUser = false; //Implement locally
        Return wfu;
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description Method that change the status of an Arce Analysis
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 29/07/2019
    * @param arceAccHasAn - arce__Account_has_Analysis__c object
    * @param analysis - arce__Analysis__c object
    * @param status - string with the desirable status
    * @return void
    * @example changeStatusArce(arceAccHasAn, analysis, status)
    * --------------------------------------------------------------------------------------
    **/
    public static void changeStatusArce(arce__Account_has_Analysis__c arceAccHasAn, arce__Analysis__c analysis, String status) {
        arceAccHasAn.arce_ctmr_flag__c = false;
        arceAccHasAn.arce__anlys_wkfl_sbanlys_status_type__c = '1';
        analysis.arce__wf_status_id__c = status;
        analysis.Temporal_status__c = false;
        upsert analysis;
        upsert arceAccHasAn;
    }
    /**
    * ----------------------------------------------------------------------------------------------
    * @Description return the ids of child accounts
    * ----------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 31/07/2019
    * @param groupId - Id of a group account
    * @param arceId - Id of the current arce analysis
    * @return a List<arce__Analysis__c> that contains the child ids of a parent account
    * @example getIdsOfChildAccount(groupId, arceId)
    * ----------------------------------------------------------------------------------------------
    */
    public static List<arce__Analysis__c> getIdsOfChildAccount(Id groupId, Id arceId) {
        return [SELECT Id, (SELECT Id, arce__Customer__c, arce__Customer__r.Name FROM arce__Account_has_Analysis__r WHERE arce__InReview__c = true AND arce__group_asset_header_type__c = '2') FROM arce__Analysis__c WHERE arce__Group__c = :groupId AND Id = :arceId];
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Method that search the customer Id with arce Id
    * --------------------------------------------------------------------------------------
    * @Author   Angel Fuertes Gomez  angel.fuertes2@bbva.com
    * @Date     Created: 29/07/2019
    * @param analysis - arce__Analysis__c object id
    * @return object type arce__Analysis__c
    * @example  public static void changeStatusArce(String recordId)
    * --------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static arce__Analysis__c getGroupId(String recordId) {
        return [select Id,arce__Group__c,arce__wf_status_id__c from arce__Analysis__c where Id=: recordId limit 1];
    }
    /**
    * ---------------------------------------------------------------------------------------------------
    * @Description - Method to get arce
    * ---------------------------------------------------------------------------------------------------
    * @Author   Angel Fuertes Gomez - angel.fuertes2@bbva.com / BBVA DWP
    * @Date     Created: 2019-05-04
    * @param recordId - id of the arce
    * @return arce__Analysis__c object
    * @example gerArce(inputTerm)
    * ---------------------------------------------------------------------------------------------------
    **/
    public static arce__Analysis__c gerArce(String recordId){
        return [SELECT Id,Name,ownerId,arce__wf_status_id__c,CreatedById,arce__bbva_committees_type__c,arce__anlys_wkfl_edit_br_level_type__c FROM arce__Analysis__c WHERE id in (SELECT arce__Analysis__c FROM arce__Account_has_Analysis__c WHERE Id=:recordId)];
    }
    /**
    * ---------------------------------------------------------------------------------------------------
    * @Description - Method to get arce
    * ---------------------------------------------------------------------------------------------------
    * @Author   Angel Fuertes Gomez - angel.fuertes2@bbva.com / BBVA DWP
    * @Date     Created: 2019-05-04
    * @param recordId - id of the arce
    * @return arce__Analysis__c object
    * @example gerArce(inputTerm)
    * ---------------------------------------------------------------------------------------------------
    **/
    public static arce__Analysis__c getArceForTraceability(String recordId){
        return [SELECT Id,toLabel(arce__Stage__c), toLabel(arce__wf_status_id__c),toLabel(arce__anlys_wkfl_status_stage_type__c), toLabel(arce__anlys_wkfl_edit_br_level_type__c) FROM arce__Analysis__c WHERE id in (SELECT arce__Analysis__c FROM arce__Account_has_Analysis__c WHERE Id=:recordId)];
    }
    /**
    * ---------------------------------------------------------------------------------------------------
    * @Description - Method to updates arce_analysis__c
    * ---------------------------------------------------------------------------------------------------
    * @Author   javier.soto.carrascosa@bbva.com
    * @Date     Created: 2019-10-23
    * @param listArce - List<arce__Analysis__c>
    * @return arce__Analysis__c object
    * @example gerArce(inputTerm)
    * ---------------------------------------------------------------------------------------------------
    **/
    public static void updateArce (List<arce__Analysis__c> listArce) {
        if ( !listArce.isEmpty() ) {
            update listArce;
        }
    }
    /**
    * ---------------------------------------------------------------------------------------------------
    * @Description - Method to updates arce_analysis__c
    * ---------------------------------------------------------------------------------------------------
    * @Author   javier.soto.carrascosa@bbva.com
    * @Date     Created: 2019-10-23
    * @param listArce - List<arce__Analysis__c>
    * @return arce__Analysis__c object
    * @example gerArce(inputTerm)
    * ---------------------------------------------------------------------------------------------------
    **/
    public static List<arce__Analysis__c> insertArce (List<arce__Analysis__c> listArce) {
        if ( !listArce.isEmpty() ) {
            insert listArce;
        }
        return listArce;
    }
    /**
    * ---------------------------------------------------------------------------------------------------
    * @Description - Method to get Arces
    * ---------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-13-01
    * @param AccountsIds - List<Id>
    * @return List<arce__Analysis__c> object
    * @example getArcesFromAccounts(AccountsIds)
    * ---------------------------------------------------------------------------------------------------
    **/
    public static List<arce__Analysis__c> getArcesFromAccounts(List<Id> accountsIds) {
        return [SELECT Name,OwnerId,arce__anlys_wkfl_prev_user_name__c,Temporal_status__c,arce__parent_analysis_id__c,arce__analysis_customer_relation_type__c,arce__anlys_wkfl_rturn_br_level_type__c,CurrencyIsoCode,arce__Group__c,arce__bbva_committees_type__c,arce__Rating__c,arce__recordTypeName__c,arce__Stage__c,arce__stage_collective_type__c,arce__wf_status_id__c,arce__anlys_wkfl_edit_br_level_type__c,arce__anlys_wkfl_user_edit_list_desc__c,arce__anlys_wkfl_committee_name__c,arce__anlys_wkfl_der_cmtee_list_desc__c,arce__anlys_wkfl_discard_reason_desc__c,arce__anlys_wkfl_discard_reason_id__c,arce__anlys_wkfl_der_br_level_type__c,arce__anlys_wkfl_return_reason_desc__c,arce__anlys_wkfl_sanction_rslt_type__c,arce__anlys_wkfl_sanction_rsn_desc__c,arce__anlys_wkfl_snctn_br_level_type__c,arce__anlys_wkfl_stage_type__c,arce__anlys_wkfl_status_stage_type__c,arce__anlys_wkfl_sub_process_type__c,arce__anlys_wkfl_edit_user_name__c
                FROM arce__Analysis__c
                WHERE arce__Group__c = :accountsIds AND arce__Stage__c != '3'
                ORDER BY CreatedDate DESC LIMIT 1];
    }
    /**
    *-------------------------------------------------------------------------------
    * @description editAnalysisFields
    *--------------------------------------------------------------------------------
    * @date		09/01/2020
    * @author	juanignacio.hita.contractor@bbva.com
    * @param	analysisId : analysis arce id
    * @param    fieldValueMap : map of key value with the name of the field and value
    * @return	void
    * @example	Arc_Gen_Workflow_Data.updateAnalysisSctnLevel();
    */
    public static void editAnalysisFields(Id analysisId, Map<String, Object> fieldValueMap) {
        arce__Analysis__c analysis = [SELECT Id FROM arce__Analysis__c WHERE Id =: analysisId];
        for (String fieldName : fieldValueMap.keySet()) {
            analysis.put(fieldName, fieldValueMap.get(fieldName));
        }
        update analysis;
    }
}