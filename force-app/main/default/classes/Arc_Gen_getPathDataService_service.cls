/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_getPathDataService_service.cls
* @Author   eduardoefrain.hernandez.contractor@bbva.com
* @Date     Created: 2019-07-25
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Service Class for Path Service
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-05-28 eduardoefrain.hernandez.contractor@bbva.com
*             Initial Version.
* |2019-07-23 eduardoefrain.hernandez.contractor@bbva.com
*             Added Comments.
* |2019-09-25 franciscojavier.bueno@bbva.com
*             Change Salesforce ID by Analysis Number.
* |2019-12-05 jhovanny.delacruz.cruz@bbva.com
*             Enable de encryption method
* |2020-02-04 juanmanuel.perez.ortiz.contractor@bbva.com
*             Add two new parameters in setupPath() and created SetupPathWrapper to avoid 'long parameter lists'
* |2020-01-29 javier.soto.carrascosa@bbva.com
*             Add fixes
* -----------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_getPathDataService_service {
    public class SetupPathWrapper {
        /**
        * @Description: String analysisId
        */
        public String analysisId {get; set;}
        /**
        * @Description: String customerId
        */
        public String customerId {get; set;}
        /**
        * @Description: String clientNumber
        */
        public String clientNumber {get; set;}
        /**
        * @Description: String subsidiary
        */
        public String subsidiary {get; set;}
        /**
        * @Description: String saveObject
        */
        public Boolean saveobject {get; set;}
    }
/**
*-------------------------------------------------------------------------------
* @description Method that sets the variables for the path service
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 28/05/2019
* @param String analysisId - Analysis Id
* @param String customerId - Customer Id
* @param String customerNumber - Customer Number
* @param String subsidiary - Indicates if the miniARCE is a Group or a Subsidiary
* @return Arc_Gen_ServiceAndSaveResponse - Wrapper with the Service response
* @example public Arc_Gen_ServiceAndSaveResponse setupPath(Arc_Gen_getPathDataService_service.SetupPathWrapper pathParameters)
**/
    public Arc_Gen_ServiceAndSaveResponse setupPath(Arc_Gen_getPathDataService_service.SetupPathWrapper pathParameters) {
        String selectedPath,customerNumber;
        Arc_Gen_ServiceAndSaveResponse serviceAndSaveResp = new Arc_Gen_ServiceAndSaveResponse();
        Arc_Gen_getPathDataService_data locator = new Arc_Gen_getPathDataService_data();
        Arc_Gen_getIASOResponse.serviceResponse response = new Arc_Gen_getIASOResponse.serviceResponse();
        Arc_Gen_CustomServiceMessages serviceMessage = new Arc_Gen_CustomServiceMessages();
        List<arce__Account_has_Analysis__c> lista = locator.getAccountHasAnalysis(pathParameters.analysisId,pathParameters.customerId);
        customerNumber = Arc_Gen_CallEncryptService.getEncryptedClient(pathParameters.clientNumber);
        response = locator.callPathService(lista[0].Name,customerNumber,pathParameters.subsidiary);
        serviceAndSaveResp.serviceCode = response.serviceCode;
        serviceAndSaveResp.serviceMessage = response.serviceMessage;
        serviceAndSaveResp.updatefields = new Map<String,Object>();
        if(response.serviceCode == String.valueOf(serviceMessage.CODE_200)) {
            Map<String, Object> responseMap = response.serviceResponse;
            Object listServiceResp = (Object)responseMap.get('data');
            Map<String, Object> data = (Map<String, Object>)listServiceResp;
            Object operationData = (Object)data.get('operationData');
            Map<String, Object> riskPaths = (Map<String, Object>)operationData;
            List<Object> paths = (List<Object>)riskPaths.get('riskPaths');
            Map<String, Object> assessment = (Map<String, Object>)paths[0];
            Map<String, Object> assessmentPath = (Map<String, Object>)assessment.get('riskPathType');
            selectedPath = (String)assessmentPath.get('id');
            Arc_Gen_ServiceAndSaveResponse save = setPath(pathParameters.analysisId,pathParameters.customerId,selectedPath,pathParameters.saveobject);
            serviceAndSaveResp.saveStatus = save.saveStatus;
            serviceAndSaveResp.saveMessage = save.saveMessage;
            serviceAndSaveResp.updatefields = save.updatefields;
        }
        Return serviceAndSaveResp;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that sets the path in the ARCE
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 28/05/2019
* @param String analysisId - Analysis Id
* @param String customerId - Customer Id
* @param String selectedPath - Selected Path
* @return Arc_Gen_getPathDataService_data.saveResult - Wrapper that contains the result of a DML process
* @example public static Arc_Gen_getPathDataService_data.saveResult setPath(String analysisId,String customerId,String selectedPath)
**/
    public static Arc_Gen_ServiceAndSaveResponse setPath(String analysisId,String customerId,String selectedPath, Boolean saveobject) {
        Arc_Gen_ServiceAndSaveResponse serviceAndSaveResp = new Arc_Gen_ServiceAndSaveResponse();
        Arc_Gen_getPathDataService_data locator = new Arc_Gen_getPathDataService_data();
        List<arce__Account_has_Analysis__c> analysis = locator.getAccountHasAnalysis(analysisId,customerId);
        if(!analysis.isEmpty()) {
            analysis[0].arce__path__c = selectedPath;
        }
        if(saveobject) {
            serviceAndSaveResp = locator.updateRecords(analysis);
            serviceAndSaveResp.saveStatus = 'true';
        } else {
            serviceAndSaveResp.updatefields = new Map<String,Object>();
            serviceAndSaveResp.updatefields.put('arce__path__c',analysis[0].arce__path__c);
        }
        Return serviceAndSaveResp;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that obtains the customer number
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 28/05/2019
* @param String analysisId - Analysis Id
* @param String customerId - Customer Id
* @return String - Customer Number
* @example public static String getCustomerNumber(String analysisId,String customerId)
**/
    public static String getCustomerNumber(String analysisId,String customerId) {
        return Arc_Gen_AccHasAnalysis_Data.getAccountHasAnalysis(analysisId,customerId)[0].accWrapperObj.accNumber;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that obtains the customer data
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 28/05/2019
* @param String accHasAnalysisId - Account Has Analysis Id
* @return String - Customer Data
* @example public static String getCustomerData(String accHasAnalysisId)
**/
    public static List<String> getCustomerData(String accHasAnalysisId) {
        Return Arc_Gen_getPathDataService_data.getAnalysisAndCustomer(accHasAnalysisId);
    }
}