/**
  * ------------------------------------------------------------------------------------------------
  * @Name     Arc_Gen_NewGroups_service
  * @Author   luisarturo.parra.contractor@bbva.com
  * @Date     Created: 2019-04-30
  * @Group    ARCE
  *=======================================================================================================================
  * Ver                  Date                         Author                       Modification
  * 1.0              12/09/2019    luisarturo.parra.contractor@bbva.com         Initial version
  * 1.1              05/12/2019    manuelhugo.castillo.contractor@bbva.com      Modify method 'handlecall','accountstoremove' replace Account to AccountWrapper
  * 1.2              13/01/2020    mariohumberto.ramirez.contractor@bbva.com    Added new methods handleGroupStructureOnline
  * 1.3              17/01/2020    javier.soto.carrascosa@bbva.com              Add support for new account interfaces
  * 1.4              22/01/2020    juanmanuel.perez.ortiz.contractor@bbva.com   Add line to return orphan structure
  * 1.4              08/02/2020    ricardo.almanza.contractor@bbva.com          Removed class orphan and and sent as parameter

  *=======================================================================================================================
  * -----------------------------------------------------------------------------------------------
  */
public virtual class Arc_Gen_NewGroups_service extends Arc_Gen_NewGroups_Data{
    /**
    *-------------------------------------------------------------------------------
    * @description method that sets rating
    *-------------------------------------------------------------------------------
    * @date 12/09/2019
    * @author luisarturo.parra.contractor@bbva.com
    * @param List<arce__Account_has_Analysis__c> analyzedClientList
    * @return none
    * @example  private void setRatingVariables(List<arce__Account_has_Analysis__c> analyzedClientList)
    */
    public class Returnstructure {
        /**
            * @Description: String participantsinSF
        */
        public  List<Account> participantsinSF {get; set;}
        /**
            * @Description: String participantsnotinSF
        */
        public  List<String> participantsnotinSF {get; set;}
        /**
            * @Description: String participantsremoved
        */
        public  List<Account> participantsremoved {get; set;}
        /**
            * @Description: String groupID
        */
        public  String groupID {get; set;}
        /**
            * @Description: String isorphan
        */
        public  Boolean isorphan {get; set;}
        /**
            * @Description: String orphanNumber
        */
        public  String orphanNumber {get; set;}
        /**
            * @Description: String orphanId
        */
        public  String orphanId {get; set;}
        /**
            * @Description: String participantsOnline
        */
        public  List<Arc_Gen_Account_Wrapper> participantsOnline {get; set;}
        /**
            * @Description: String noGroupsInSf
        */
        public  Boolean noGroupsInSf {get; set;}
        /**
            * @Description: String updateStructure
        */
        public  Boolean updateStructure {get; set;}
    }
    /**
    *-------------------------------------------------------------------------------
    * @Description method that handle groups structure Online
    *-------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-13-01
    * @param List<arce__Account_has_Analysis__c> analyzedClientList
    * @return Returnstructure - wrapper with the group structure data
    * @example handleGroupStructureOnline(economicparticipants,listparticipants,accountNumber)
    * -----------------------------------------------------------------------------
    */
    public virtual Arc_Gen_NewGroups_service.Returnstructure handleGroupStructureOnline(Arc_Gen_CallEconomicParticipants.Innertoreturn economicparticipants, Arc_Gen_CallListParticipant.Innertoreturnlistp listparticipants, String accountNumber, Boolean isOrphan ) {
        Arc_Gen_NewGroups_service newGroupServ = new Arc_Gen_NewGroups_service();
        Arc_Gen_NewGroups_service.Returnstructure structure = new Arc_Gen_NewGroups_service.Returnstructure();
        final List<Arc_Gen_Account_Wrapper> lstParticipantsWrap = new List<Arc_Gen_Account_Wrapper>();
        final Boolean updateStructure = Arc_Gen_GenericUtilities.getUpdateStructure();
        structure.noGroupsInSf = false;
        final List<String> decryptedClients = isOrphan ? new List<String>() : Arc_Gen_NewGroups_Service_Helper.getdecrytedClientsId(listparticipants);
        Map<String, Arc_Gen_Account_Wrapper> groupInSF = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{economicparticipants.groupinfo.groupid, accountNumber});
        System.debug('groupInSF:'+groupInSF);
		if ((groupInSF.get(economicparticipants.groupinfo.groupid) == null || groupInSF.get(accountNumber) == null) && isOrphan == false) {
            if (updateStructure) {
                groupInSF = Arc_Gen_NewGroups_Service_Helper.createGroupAcc(economicparticipants);
            } else {
                structure.noGroupsInSf = true;
            }
        }
        if (isOrphan && structure.noGroupsInSf == false) {
            final Map<String, Arc_Gen_Account_Wrapper> mapAccWrapByAccNum = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{accountNumber});
            lstParticipantsWrap.add(mapAccWrapByAccNum.get(accountNumber));
            structure.isorphan = true;
            structure.orphanId = mapAccWrapByAccNum.get(accountNumber).accId;
            structure.orphanNumber = accountNumber;
            structure.participantsOnline = lstParticipantsWrap;
            structure.noGroupsInSf = false;
            structure.groupID = mapAccWrapByAccNum.get(accountNumber).accId;
        } else if (structure.noGroupsInSf == false) {
            structure = Arc_Gen_NewGroups_Service_Helper.getStructureFinal(decryptedclients, economicparticipants.groupinfo.groupid, accountNumber, groupInSF);
        }
        structure.updateStructure = updateStructure;
        return structure;
    }
    /**
    *-------------------------------------------------------------------------------
    * @Description Update the name of the Group
    *-------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-13-01
    * @param    updateStructure - Boolean to know if the group structure is active
    * @param    economicGroupName - Name of the group
    * @param    economicGroupAccNum - Account number of the group
    * @param    groupInSF - Arc_Gen_Account_Wrapper with the group info
    * @return   void
    * @example  updateGroupName(updateStructure,economicGroupName,economicGroupAccNum,groupInSF)
    * -----------------------------------------------------------------------------
    */
    public void updateGroupName(Boolean updateStructure, String economicGroupName, String economicGroupAccNum, Map<String, Arc_Gen_Account_Wrapper> groupInSF) {
        if (updateStructure) {
            Map<String, String> accAttr = new Map<String, String>();
            Map<Id, Map<String,String>>  mapAccsToUp = new Map<Id, Map<String,String>> ();
            accAttr.put('Name', economicGroupName);
            mapAccsToUp.put(groupInSF.get(economicGroupAccNum).accId,accAttr);
            Arc_Gen_Account_Locator.accountUpdate(mapAccsToUp);
        }
    }
}