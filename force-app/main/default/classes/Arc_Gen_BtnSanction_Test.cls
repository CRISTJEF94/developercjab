/*------------------------------------------------------------------
*Author:        Angel Fuertes Gomez - angel.fuertes2@bbva.com / BBVA DWP
*Project:      	ARCE - BBVA Bancomer
*Description:   test for method class Arc_Gen_BtnSanction_controller.
*_______________________________________________________________________________________
*Version    Date           Author                               Description
*1.0        02/05/2019     Angel Fuertes Gomez                  	Creation.
*1.1        20/11/2019     Javier Soto Carrascosa                  	Update setup.
*1.2        31/12/2019     Juan Manuel Perez Ortiz                 	Update setup.
*1.3        03/01/2020     Mario H. Ramirez Lio						Update class
-----------------------------------------------------------------------------------------*/
@isTest
public class Arc_Gen_BtnSanction_Test {
    /**
    * @Method:      method that creates the data to use in the test.
    * @Description: testing method.
    */
    @testSetup static void setup() {
        User userTest = Arc_UtilitysDataTest_tst.crearUsuario('UserTest',System.Label.Cls_arce_ProfileSystemAdministrator,'');
        insert userTest;
        Account accTest = Arc_UtilitysDataTest_tst.crearCuentaGrupo('accTest', '', '123');

        arce__Analysis__c newAnalysis = new arce__Analysis__c();
        newAnalysis.Name = 'Analysis';
        newAnalysis.arce__bbva_committees_type__c = '1';
        newAnalysis.arce__anlys_wkfl_edit_br_level_type__c = '1';
        newAnalysis.OwnerId = userTest.Id;
        insert newAnalysis;

        arce__Account_has_Analysis__c children = new arce__Account_has_Analysis__c();
        children.arce__Analysis__c = newAnalysis.Id;
        children.arce__Stage__c = '3';
        children.arce__Rating__c = 'AA';
        children.arce__InReview__c = true;
        children.arce__Customer__c = accTest.Id;
        insert children;
    }
    /**
    * @Method:      test for method initDelegation
    * @Description: testing method.
    */
    @isTest
    public static void testInitDelegation() {
        arce__Account_has_Analysis__c aha = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
        Test.startTest();
        final String ret = Arc_Gen_BtnSanction_controller.initDelegation(aha.Id);
        final Arc_Gen_Delegation_Wrapper wrapperSerialize = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) ret, Arc_Gen_Delegation_Wrapper.class);
        System.assertEquals(wrapperSerialize.ambit, '1', 'init delegation test');
        try {
            Arc_Gen_BtnSanction_controller.initDelegation(null);
        } catch(Exception ex) {
            System.assert(ex.getMessage().contains('Script'), 'Script-thrown exception');
        }
        Test.stopTest();
    }
    /**
    * @Method:      test for method saveScopeOfSanction
    * @Description: testing method.
    */
    @isTest
    public static void testSaveScopeOfSanction() {
        Test.startTest();
        final arce__Account_has_Analysis__c miniArce = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
        final Arc_Gen_Delegation_Wrapper wrapper = Arc_UtilitysDataTest_tst.crearDelegationWrapper();
        final Map<String, String> data123 = Arc_UtilitysDataTest_tst.crearMapaSaveScopeSanction(miniArce.Id, '1');
        final Map<String, String> data46 = Arc_UtilitysDataTest_tst.crearMapaSaveScopeSanction(miniArce.Id, '4');
        final Map<String, String> data5 = Arc_UtilitysDataTest_tst.crearMapaSaveScopeSanction(miniArce.Id, '5');
        final String retSave1 = Arc_Gen_BtnSanction_controller.saveScopeOfSanction(JSON.serialize(wrapper), data123);
        final String retSave2 = Arc_Gen_BtnSanction_controller.saveScopeOfSanction(JSON.serialize(wrapper), data46);
        final String retSave3 = Arc_Gen_BtnSanction_controller.saveScopeOfSanction(JSON.serialize(wrapper), data5);
        final Arc_Gen_Delegation_Wrapper wrapperSerialize1 = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) retSave1, Arc_Gen_Delegation_Wrapper.class);
        System.assertEquals(wrapperSerialize1.codStatus, 500, 'save scope of sanction');
        final Arc_Gen_Delegation_Wrapper wrapperSerialize2 = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) retSave2, Arc_Gen_Delegation_Wrapper.class);
        System.assertEquals(wrapperSerialize2.codStatus, 500, 'save scope of sanction');
        final Arc_Gen_Delegation_Wrapper wrapperSerialize3 = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) retSave3, Arc_Gen_Delegation_Wrapper.class);
        System.assertEquals(wrapperSerialize3.codStatus, 500, 'save scope of sanction');
        Test.stopTest();
    }
    /**
    * @Method:      test for method searchUser
    * @Description: testing method.
    */
    @isTest
    public static void testSearchUser() {
        Test.startTest();
        final List<Map<String, String>> lstMap = Arc_Gen_BtnSanction_controller.searchUser('UserTest');
        System.assertEquals(lstMap[0].get('nameUser'), 'UserTest', 'Method search user test');
        Test.stopTest();
    }
    /**
    * @Method:      test for method updateAHABtnSanction
    * @Description: testing method.
    */
    @isTest
    public static void testUpdateAHAB() {
        Test.startTest();
        final Id recordId = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1].Id;
        final Map<String, Object> mapFields = new Map<String, Object>();
        final Map<String, Object> mapRet1 = Arc_Gen_BtnSanction_service.updateAHABtnSanction(1, (String) recordId, mapFields);
        Arc_Gen_BtnSanction_service.updateAHABtnSanction(4, (String) recordId, mapFields);
        Arc_Gen_BtnSanction_service.updateAHABtnSanction(5, (String) recordId, mapFields);
        System.assertEquals(mapRet1.get('arce__Stage__c'), '3', 'test update AHAB');
        Test.stopTest();
    }
}