/**
* @File Name          : Arc_Gen_RatingStudiesTabSet_controller.cls
* @Description        : Business logic of the rating studies dynamic form component
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE Team
* @Last Modified By   : eduardoefrain.hernandez.contractor@bbva.com
* @Last Modified On   : 21/10/2019 12:54:33
* @Changes   :
* Ver       Date            Author      		    Modification
* 1.0    17/10/2019   eduardoefrain.hernandez.contractor@bbva.com     Initial Version
**/
public class Arc_Gen_RatingStudiesTabSet_service {
    /**
    * @description: Instance of the data access class
    **/
    private static Arc_Gen_Balance_Tables_data dataAccess = new Arc_Gen_Balance_Tables_data();
    /**
    * @description: Empty constructor
    **/
    @TestVisible
    private Arc_Gen_RatingStudiesTabSet_service() {

    }
/**
*-------------------------------------------------------------------------------
* @description Method that calls the business logic in the service class
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 17/10/2019
* @param String ffssId - Standard Id of the selected Financial statement
* @return List<String> - List that contains the ratingId and the templateName
* @example getAnalyzedClientId(String ffssId)
**/
    public static List<String> getAnalyzedClientId(String ffssId) {
        final List<arce__Financial_Statements__c> ffssList = dataAccess.getFinancialStatements(new List<String>{ffssId});
        final List<arce__Account_has_Analysis__c> analyzedClient = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<String>{ffssList.get(0).arce__account_has_analysis_id__c});
        final String template = analyzedClient.get(0).arce__rating_scale_type__c == '1' ? 'r-01-100' : 'r-02-100';
        Return new List<String>{ffssList.get(0).arce__rating_id__c, template};
    }
}