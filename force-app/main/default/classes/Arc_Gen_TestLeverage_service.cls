/**
* @File Name          : Arc_Gen_TestLeverage_service.cls
* @Description        : Contains the logic of the test leveraged
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE Team
* @Last Modified By   : javier.soto.carrascosa@bbva.com
* @Last Modified On   : 1/10/2019 15:31:56
* @Changes
*=======================================================================================================================
* Ver         Date                     Author      		      Modification
*=======================================================================================================================
* 1.0    30/4/2019 18:00:36   eduardoefrain.hernandez.contractor@bbva.com     Initial Version
* 1.1    19/7/2019 18:00:56   luisruben.quinto.munoz@bbva.com    Change documentations
* 1.2    1/10/2019 15:31:56   javier.soto.carrascosa@bbva.com    Update sme
* 1.2.1  2/12/2019 13:28:50   german.sanchez.perez.contractor@bbva.com
*                             franciscojavier.bueno@bbva.com               Api names modified with the correct name on business glossary
**/
public with sharing class Arc_Gen_TestLeverage_service {
/**
*-------------------------------------------------------------------------------
* @description wrapper for the test leveraged variables
*--------------------------------------------------------------------------------
* @author  eduardoefrain.hernandez.contractor@bbva.com
* @date    30/4/2019
* @Method: LeverageVariables
* @param:
* @return:
**/
    public class LeverageVariables {
        /**
        * @Description: Risk amount
        */
        Double risk;
        /**
        * @Description: Rating value
        */
        String rating;
        /**
        * @Description: Financial sponsor indicator
        */
        Boolean financialSponsor;
        /**
        * @Description: Small or Medium Enterprise Indicator
        */
        Boolean smeIndicator;
        /**
        * @Description: Debt amount
        */
        Double debt;
        /**
        * @Description: Ebitda amount
        */
        Double ebitda;
        /**
        * @Description: Debt over ebitda ratio
        */
        Double debtOverEbitda;
        /**
        * @Description: Type of calculation
        */
        String calculateType;
    }
/**
*-------------------------------------------------------------------------------
* @description wrapper for the LeverageTestResponse
*--------------------------------------------------------------------------------
* @author  eduardoefrain.hernandez.contractor@bbva.com
* @date    30/4/2019
* @Method: LeverageTestResponse
* @param:
* @return:
**/
    public class LeverageTestResponse {
        /**
        * @Description: Status of the process
        */
        @AuraEnabled public String status {get;set;}
        /**
        * @Description: Message of the leverage
        */
        @AuraEnabled public String message {get;set;}
        /**
        * @Description: List of the variables used in the leveraged
        */
        @AuraEnabled public List<String> leveragedVariables {get;set;}
    }
/**
*-------------------------------------------------------------------------------
* @description Method that creates the response of the leveraged
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId of type arce__Account_has_Analysis__c
* @return LeverageTestResponse - A wrapper that contains the information of the test leveraged
* @example public static LeverageTestResponse setupLeverage(String analysisId)
**/
    public static LeverageTestResponse setupLeverage(String analysisId) {
        Arc_Gen_TestLeverage_data locator = new Arc_Gen_TestLeverage_data();
        LeverageTestResponse leveragedResponse = new LeverageTestResponse();
        Arc_Gen_TestLeverage_data.saveResult saveResult = new Arc_Gen_TestLeverage_data.saveResult();
        arce__Account_has_Analysis__c analyzedClient = locator.getAccountHasAnalysis(analysisId);
        LeverageVariables leverageBefore = setupLeverageVariables(analyzedClient,'before');
        LeverageVariables leverageAfter = setupLeverageVariables(analyzedClient,'after');
        if(leverageBefore.risk != 0.0 && leverageBefore.rating != Null) {
            List<String> leveragedResults = new List<String>();
            analyzedClient = calculateLeverage(leverageBefore,analyzedClient);
            analyzedClient = calculateLeverage(leverageAfter,analyzedClient);
            saveResult = locator.updateRecord(analyzedClient);
            arce__Account_has_Analysis__c analyzedClientResult = locator.getLeveragedResults(analyzedClient.Id);
            leveragedResponse.status = saveResult.status;
            leveragedResponse.message = saveResult.message;
            leveragedResults.add(analyzedClientResult.arce__ll_before_adj_ind_type__c);
            leveragedResults.add(analyzedClientResult.arce__ll_before_adj_clsfn_type__c != null ? analyzedClientResult.arce__ll_before_adj_clsfn_type__c : '');
            leveragedResults.add(analyzedClientResult.arce__ll_after_adj_ind_type__c);
            leveragedResults.add(analyzedClientResult.arce__ll_after_adj_clsfn_type__c != null ? analyzedClientResult.arce__ll_after_adj_clsfn_type__c : '');
            leveragedResponse.leveragedVariables = leveragedResults;
        } else {
            leveragedResponse.status = 'false';
            leveragedResponse.message = System.Label.Cls_arce_LeverageErrorBecauseNoData;
            analyzedClient.arce__anlys_wkfl_sbanlys_status_type__c = '2';
            locator.updateRecord(analyzedClient);
        }
        Return leveragedResponse;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that sets the variables that will be use for the test leveraged
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param arce__Account_has_Analysis__c analyzedClient for the test leveraged
* @param String calculateType - "Before" or "After" Adjustments
* @return LeverageVariables - A wrapper that contains the different variables that will be used in test leveraged
* @example public static LeverageVariables setupLeverageVariables(arce__Account_has_Analysis__c analyzedClient,String calculateType)
**/
    public static LeverageVariables setupLeverageVariables(arce__Account_has_Analysis__c analyzedClient,String calculateType) {
        LeverageVariables leverage = new LeverageVariables();
        Arc_Gen_TestLeverage_data locator = new Arc_Gen_TestLeverage_data();
        leverage.risk = (Double)analyzedClient.arce__current_proposed_amount__c;
        leverage.rating = (String)analyzedClient.arce__ffss_for_rating_id__r.arce__rating_final__c;
        leverage.financialSponsor = analyzedClient.arce__Customer__r.controlled_by_sponsor_type__c == '1' ? TRUE : FALSE;
        leverage.smeIndicator = analyzedClient.arce__smes_eur_comuty_defn_type__c == '1' ? TRUE : FALSE;
        leverage.debt = (calculateType == 'before' ? analyzedClient.arce__gross_financial_debt_amount__c : analyzedClient.arce__ll_adj_debt_amount__c);
        leverage.ebitda = (calculateType == 'before' ? analyzedClient.arce__ebitda_interest_number__c : analyzedClient.arce__ll_adj_ebitda_amount__c);
        leverage.debtOverEbitda = leverage.ebitda != 0 ? leverage.debt / leverage.ebitda : 0;
        leverage.calculateType = calculateType;
        Return leverage;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that calculates the test leveraged
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param LeverageVariables leverage - A wrapper that contains the different variables that will be used in test leveraged
* @param arce__Account_has_Analysis__c analyzedClients for the test leveraged
* @return arce__Account_has_Analysis__c - current record on test leveraged
* @example public static arce__Account_has_Analysis__c calculateLeverage(LeverageVariables leverage,arce__Account_has_Analysis__c analyzedClient)
**/
    public static arce__Account_has_Analysis__c calculateLeverage(LeverageVariables leverage,arce__Account_has_Analysis__c analyzedClients) {
        Arc_Gen_TestLeverage_data locator = new Arc_Gen_TestLeverage_data();
        arce__Account_has_Analysis__c analyzedClient = analyzedClients;
        final Double currencyRate = getCurrencyRate(System.Label.Cls_arce_CurrencyTypeSelected);
        final Double fiveMillions = 5;
        final Integer four = 4;
        final Integer six = 6;
        final Integer zero = 0;
        if(leverage.risk >= fiveMillions) {
            if(getValidRatings().contains(leverage.rating)) {
                if(leverage.financialSponsor) {
                    analyzedClient = leverageTestTrue(analyzedClient,leverage.calculateType,'NI');
                } else {
                    analyzedClient = leverageTestFalse(analyzedClient,leverage.calculateType);
                }
            } else {
                if(leverage.financialSponsor) {
                    analyzedClient = leverageTestTrue(analyzedClient,leverage.calculateType,'FS');
                } else {
                    if(leverage.smeIndicator) {
                        analyzedClient = leverageTestFalse(analyzedClient,leverage.calculateType);
                    } else {
                        if(leverage.debtOverEbitda > four || leverage.debtOverEbitda < zero || (leverage.debt > zero && leverage.ebitda == zero)) {
                            if(leverage.debtOverEbitda > six || leverage.debtOverEbitda < zero || (leverage.debt > zero && leverage.ebitda == zero)) {
                                analyzedClient = leverageTestTrue(analyzedClient,leverage.calculateType,'R6');
                            } else {
                                analyzedClient = leverageTestTrue(analyzedClient,leverage.calculateType,'R4');
                            }
                        } else {
                            analyzedClient = leverageTestFalse(analyzedClient,leverage.calculateType);
                        }
                    }
                }
            }
        } else {
            analyzedClient = leverageTestFalse(analyzedClient,leverage.calculateType);
        }
        analyzedClient.arce__anlys_wkfl_sbanlys_status_type__c = '3';
        Return analyzedClient;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that change the test leveraged indcator field to YES
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param arce__Account_has_Analysis__c analyzedClient
* @param String calculateType - "Before" or "After"
* @param String leverageType - Classification type of the test leveraged
* @return arce__Account_has_Analysis__c record with arce__ll_before_adj_ind_type__c, arce__ll_before_adj_clsfn_type__c data
* @example public static arce__Account_has_Analysis__c leverageTestTrue(arce__Account_has_Analysis__c analyzedClient,String calculateType,String leverageType)
**/
    public static arce__Account_has_Analysis__c leverageTestTrue(arce__Account_has_Analysis__c analyzedClient,String calculateType,String leverageType) {
        switch on calculateType {
            when 'before' {
                analyzedClient.arce__ll_before_adj_ind_type__c = '1';
                analyzedClient.arce__ll_before_adj_clsfn_type__c = leverageType;
            }
            when 'after' {
                analyzedClient.arce__ll_after_adj_ind_type__c = '1';
                analyzedClient.arce__ll_after_adj_clsfn_type__c = leverageType;
            }
        }
        Return analyzedClient;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that change the test leveraged indcator field to NO
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param arce__Account_has_Analysis__c analyzedClient variable to update
* @param String calculateType - "Before" or "After"
* @return arce__Account_has_Analysis__c object with preset variables
* @example leverageTestFalse(arce__Account_has_Analysis__c analyzedClient,String calculateType) {
**/
    public static arce__Account_has_Analysis__c leverageTestFalse(arce__Account_has_Analysis__c analyzedClient,String calculateType) {
        switch on calculateType {
            when 'before' {
                analyzedClient.arce__ll_before_adj_ind_type__c = '2';
                analyzedClient.arce__ll_before_adj_clsfn_type__c = null;
            }
            when 'after' {
                analyzedClient.arce__ll_after_adj_ind_type__c = '2';
                analyzedClient.arce__ll_after_adj_clsfn_type__c = null;
            }
        }
        Return analyzedClient;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that return a set of all rating values
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param void
* @return Set<String>
* @example  getValidRatings() {
**/
    public static Set<String> getValidRatings() {
        Set<String> validRatings = new Set<String>{'BBB-','BBB','BBB+','A-','A','A+','AA-','AA','AA+','AAA'};
        Return validRatings;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that return the currency rate of the organization
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String currencyIsoCode for the test leveraged
* @return Double
* @example getCurrencyRate(String currencyIsoCode) {
**/
    public static Double getCurrencyRate(String currencyIsoCode) {
        Arc_Gen_TestLeverage_data locator = new Arc_Gen_TestLeverage_data();
        Double currencyRate = Double.valueOf(locator.getCurrency(currencyIsoCode).get('ConversionRate'));
        Return currencyRate;
    }
}