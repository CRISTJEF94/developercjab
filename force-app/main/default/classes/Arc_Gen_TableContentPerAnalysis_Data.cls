/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_TableContentPerAnalysis_Data
* @Author   Ricardo Almanza Angeles  ricardo.almanza.contractor@bbva.com
* @Date     Created: 2019-06-20
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Data Service for arce__Table_Content_per_Analysis__c object
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-06-20 ricardo.almanza.contractor@bbva.com
*             Class creation.
* |2019-08-26 eduardoefrain.hernandez.contractor@bbva.com
*             Update execRepGetTblsRelatedArce method
* |2019-11-04 javier.soto.carrascosa@@bbva.com
*             Add new methods
* |2020-01-30 juanmanuel.perez.ortiz.contractor@bbva.com
*             Add missing custom labels
* -----------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_TableContentPerAnalysis_Data {
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description Wrapper that contain an error message
    * -----------------------------------------------------------------------------------------------
    * @param - void
    * @return - String with an error message
    * @example responseWrapperMnBanks wrapper = new responseWrapperMnBanks()
    * -----------------------------------------------------------------------------------------------
    **/
    public class ResponseWrapperCntntAnaly {
        /**
        * @Description: String with an error message
        */
        @AuraEnabled public String gblErrorResponse {get;set;}
    }
    /*------------------------------------------------------------------------------------------------------
    *@Description Insert arce__Table_Content_per_Analysis__c
    * -----------------------------------------------------------------------------------------------------
    * @Author   Ricardo Almanza
    * @Date     2019-06-20
    * @param    toIns array of objects to insert
    * @return   ResponseWrapperCntntAnaly object with string in case of error
    * @example  insertTableContent(toIns)
    * */
    public static ResponseWrapperCntntAnaly insertTableContent(arce__Table_Content_per_Analysis__c[] toIns) {
        final ResponseWrapperCntntAnaly wrapper = new ResponseWrapperCntntAnaly();
        try{
            if(toIns.isEmpty()) {
                throw new DMLException(System.Label.Arc_Gen_NotElements);
            }
            Insert toIns;
        } catch (DmlException exep) {
            wrapper.gblErrorResponse = exep.getMessage();
        }
        return wrapper;
    }
    /*------------------------------------------------------------------------------------------------------
    *@Description Update arce__Table_Content_per_Analysis__c
    * -----------------------------------------------------------------------------------------------------
    * @Author   Ricardo Almanza
    * @Date     2019-06-20
    * @param    toUpd array of objects to insert
    * @return   ResponseWrapperCntntAnaly object with string in case of error
    * @example  updateTableContent(toUpd)
    * */
    public static ResponseWrapperCntntAnaly updateTableContent(arce__Table_Content_per_Analysis__c[] toUpd) {
        final ResponseWrapperCntntAnaly wrapper = new ResponseWrapperCntntAnaly();
        try{
            if(toUpd.isEmpty()) {
                throw new DMLException(System.Label.Arc_Gen_NotElements);
            }
            Update toUpd;
        } catch (DmlException exep) {
            wrapper.gblErrorResponse = exep.getMessage();
        }
        return wrapper;
    }
    /*------------------------------------------------------------------------------------------------------
    *@Description Delte arce__Table_Content_per_Analysis__c
    * -----------------------------------------------------------------------------------------------------
    * @Author   Ricardo Almanza
    * @Date     2019-06-20
    * @param    toDel array of objects to insert
    * @return   ResponseWrapperCntntAnaly object with string in case of error
    * @example  deleteTableContent(toDel)
    * */
    public static ResponseWrapperCntntAnaly deleteTableContent(arce__Table_Content_per_Analysis__c[] toDel) {
        final ResponseWrapperCntntAnaly wrapper = new ResponseWrapperCntntAnaly();
        try{
            if(toDel.isEmpty()) {
                throw new DMLException(System.Label.Arc_Gen_NotElements);
            }
            Delete toDel;
        } catch (DmlException exep) {
            wrapper.gblErrorResponse = exep.getMessage();
        }
        return wrapper;
    }
    /*------------------------------------------------------------------------------------------------------
    *@Description Get arce__Table_Content_per_Analysis__c related to arce__account_has_analysis_id__c and grouped
    * -----------------------------------------------------------------------------------------------------
    * @Author   Ricardo Almanza
    * @Date     2019-06-20
    * @param    rid id related to arce__account_has_analysis_id__c
    * @return   AggregateResult[] array with arce__Table_Content_per_Analysis__c grouped related
    * @example  execRepGetFnHighlights(rid)
    * */
    public static AggregateResult[] execRepGetFnHighlights(Id rid) {
        List<String> tblscpa = Label.Arc_Gen_ExecRepDtlCllHigh.split(',');
        return [Select arce__table_content_year__c, arce__collection_order_type__c, MIN(arce__Data_collection_name__c) data, SUM(arce__table_content_percentage__c) val1, SUM(arce__table_content_value__c) val2 from arce__Table_Content_per_Analysis__c where arce__account_has_analysis_id__c = :rid AND arce__collection_order_type__c in :tblscpa group by arce__table_content_year__c, arce__collection_order_type__c order by arce__collection_order_type__c asc];
    }
    /*------------------------------------------------------------------------------------------------------
    *@Description Get arce__Table_Content_per_Analysis__c related to arce__account_has_analysis_id__c and grouped with a custom where
    * -----------------------------------------------------------------------------------------------------
    * @Author   Ricardo Almanza
    * @Date     2019-07-17
    * @param    rid id related to arce__account_has_analysis_id__c
    * @param    typ to get diferent werys where greatert than arce__arce_collection_order_number__c
    * lesser than arce__arce_collection_order_number__c or without comparision with arce__arce_collection_order_number__c
    * @param	arcColltbl value to compare to arce__collection_table__c
    * @param	arcCollNumb value to compare to arce__arce_collection_order_number__c
    * @return   AggregateResult[] array with arce__Table_Content_per_Analysis__c grouped related
    * @example  execRepGetFnHighlights(rid)
    * */
    public static AggregateResult[] execRepGetTblsRelatedArce(Id rid,String typ,String arcColltbl,Decimal arcCollNumb) {
        AggregateResult[] ret;
        switch on typ{
            when 'gtt'{
                ret = [Select arce__table_content_year__c, arce__collection_order_type__c, MIN(arce__Data_collection_name__c) data, SUM(arce__table_content_percentage__c) val1, SUM(arce__table_content_value__c) val2 from arce__Table_Content_per_Analysis__c where arce__account_has_analysis_id__c = :rid AND arce__collection_table__c = :arcColltbl and arce__arce_collection_order_number__c > :arcCollNumb group by arce__table_content_year__c, arce__collection_order_type__c order by arce__collection_order_type__c asc];
            }
            when 'ltt'{
                ret = [Select arce__table_content_year__c, arce__collection_order_type__c, MIN(arce__Data_collection_name__c) data, SUM(arce__table_content_percentage__c) val1, SUM(arce__table_content_value__c) val2 from arce__Table_Content_per_Analysis__c where arce__account_has_analysis_id__c = :rid AND arce__collection_table__c = :arcColltbl and arce__arce_collection_order_number__c < :arcCollNumb group by arce__table_content_year__c, arce__collection_order_type__c order by arce__collection_order_type__c asc];
            }
            when 'non'{
                ret = [Select arce__table_content_year__c, arce__collection_order_type__c, MIN(arce__Data_collection_name__c) data, SUM(arce__table_content_percentage__c) val1, SUM(arce__table_content_value__c) val2 from arce__Table_Content_per_Analysis__c where arce__account_has_analysis_id__c = :rid AND arce__collection_table__c = :arcColltbl group by arce__table_content_year__c, arce__collection_order_type__c order by arce__collection_order_type__c asc];
            }
            when 'mat'{
                ret = [Select arce__table_content_year__c, arce__Data_Collection_Id__r.Name, MIN(arce__Data_collection_name__c) data, SUM(arce__table_content_percentage__c) val1, SUM(arce__table_content_value__c) val2 from arce__Table_Content_per_Analysis__c where arce__account_has_analysis_id__c = :rid AND arce__collection_table__c = :arcColltbl group by arce__table_content_year__c, arce__Data_Collection_Id__r.Name order by arce__Data_Collection_Id__r.Name  asc];
            }
            when else{
                ret = new List<AggregateResult>();
            }
        }
        return ret;
    }
/**
*-------------------------------------------------------------------------------
* @description getRecordsYears gets the info to build charts in the front.
*--------------------------------------------------------------------------------
* @author  luisruben.quinto.munoz@bbva.com
* @date     11/04/2019
* @param collectionType type of collection.
* @param  recordId Id of the record.
* @return List<arce__Table_Content_per_Analysis__c>
**/
  public List<arce__Table_Content_per_Analysis__c> getRecordsYears(String collectionType, String recordId) {
    Id idRT = Arc_Gen_GenericUtilities.getRecType('arce__Table_Content_per_Analysis__c', collectionType);
    return [SELECT arce__table_content_year__c From arce__Table_Content_per_Analysis__c WHERE arce__account_has_analysis_id__c = :recordId and recordTypeId =: idRT order by arce__table_content_year__c DESC ];
  }
/**
*-------------------------------------------------------------------------------
* @description getRecordsYears gets the info to build charts in the front.
*--------------------------------------------------------------------------------
* @author  ricardo.almanza.contractor@bbva.com
* @date     21/01/2020
* @param collectionType type of collection.
* @param  recordId Id of the record.
* @return List<arce__Table_Content_per_Analysis__c>
**/
  public static List<arce__Table_Content_per_Analysis__c> getRecordsbyArcAcc(String collectionType,List<String> recordId) {
    Id idRT = Arc_Gen_GenericUtilities.getRecType('arce__Table_Content_per_Analysis__c', collectionType);
    return [SELECT Id,arce__table_content_year__c,arce__table_content_percentage__c,arce__Data_Collection_Id__r.Name From arce__Table_Content_per_Analysis__c WHERE arce__account_has_analysis_id__c = :recordId and recordTypeId =: idRT order by arce__table_content_year__c DESC ];
  }

  /**
  *-------------------------------------------------------------------------------
  * @description getRecordsCollectionTable gets the collection Table to build charts in the front.
  *--------------------------------------------------------------------------------
  * @author manuelhugo.castillo.contractor@bbva.com
  * @date   17/10/2019
  * @param  Collection of collection Table
  * @param  recordId Id of the record.
  * @return List<arce__Table_Content_per_Analysis__c>
  **/
  public List<arce__Table_Content_per_Analysis__c> getRecordsCollectionTable(String collectionType, String recordId) {
    Id idRT = Arc_Gen_GenericUtilities.getRecType('arce__Table_Content_per_Analysis__c', collectionType);
    return [SELECT arce__collection_table__c From arce__Table_Content_per_Analysis__c WHERE arce__account_has_analysis_id__c = :recordId and recordTypeId =: idRT order by arce__table_content_year__c DESC];
  }
}