/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Expandible_Table_Controller
* @Author   BBVA
* @Date     Created: 2019-06-20
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Controller class for Arc_Gen_Expandible_Table_cmp
* ------------------------------------------------------------------------------------------------
* @Changes
* |2019-06-20 mariohumberto.ramirez.contractor@bbva.com
*             Deleted some global wrapper classes
*             Added new wrappers deleted in the global wrapper classes
*             - Arc_Gen_DataTable, Arc_Gen_TableColumns and Arc_Gen_TableRow
*             Change to SOC
* |2019-08-14 mariohumberto.ramirez.contractor@bbva.com
*             Deleted call to the method verifyTypologiesInserted in Arc_Gen_Expandible_Table_Service
* |2019-09-30 mariohumberto.ramirez.contractor@bbva.com
*             Added new method sumTypologies
* |2019-10-11 mariohumberto.ramirez.contractor@bbva.com
*             Added new method getHeaderDate
* |2020-01-28 mariohumberto.ramirez.contractor@bbva.com
*             modify method getproducts and added new method changeServiceFlag
* |2020-01-30 javier.soto.carrascosa@bbva.com
*             Add HU 787 missing functionality
* |2020-04-30 joseluis.garcia4.contractor@bbva.com
*             Update limit values in insertProducts method.
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_Expandible_Table_Controller {
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @author mariohumberto.ramirez.contractor@bbva.com
    * @date 2019-06-20
    * @param void
    * @return void
    * @example Arc_Gen_Expandible_Table_Controller controller = new Arc_Gen_Expandible_Table_Controller()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_Expandible_Table_Controller() {
    }

    /**
    * -----------------------------------------------------------------------------------------------
    * @Description Wrapper that contain all the information to create the policie table
    * -----------------------------------------------------------------------------------------------
    * @author mariohumberto.ramirez.contractor@bbva.com
    * @date 2019-06-20
    * @param void
    * @return all the information to create the table
    * @example ResponseWrapper wrapper = new ResponseWrapper()
    * -----------------------------------------------------------------------------------------------
    **/
    public class ResponseWrapper {
        /**
        * @Description: Boolean that represent a succesfull call
        */
        @AuraEnabled public Boolean successResponse {get;set;}
        /**
        * @Description: Map with some info in the data
        */
        @AuraEnabled public Arc_Gen_DataTable jsonResponse {get;set;}
        /**
        * @Description: String with an error response
        */
        @AuraEnabled public String errorResponse {get;set;}
        /**
        * @Description: Map with some the info of the delete response
        */
        @AuraEnabled public Map<String,String> deleteResponse {get;set;}
        /**
        * @Description: Map with some the info of the delete response
        */
        @AuraEnabled public List<String> productResponse {get;set;}
        /**
        * @Description: Map with some the info of the delete response
        */
        @AuraEnabled public List<Map<String,String>> gblProductResp {get;set;}
        /**
        * @Description: Map with values to insert products
        */
        @AuraEnabled public Map<String,Double> values2Insert {get;set;}

    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Method that return a JSON with the data to construct the policie table
    * --------------------------------------------------------------------------------------
    * @author mariohumberto.ramirez.contractor@bbva.com
    * @date 2019-06-20
    * @param recordId - Id of the account_has_analysis.
    * @param inputClass - String with whe name of the controller class
    * @return response - Wrapper with the data to construct the policie tablew
    * @example dataResponse(recordId, inputClass)
    * --------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static ResponseWrapper dataResponse(Id recordId, String inputClass) {
        ResponseWrapper response = new ResponseWrapper();
        response.successResponse = true;
        final String def = 'default';
        Arc_Gen_DataTable dataJson = new Arc_Gen_DataTable();
        if (inputClass == def) {
            final arce__analysis__c arce = Arc_Gen_ArceAnalysis_Data.gerArce((String)recordId);
            final string sanctionResult = Arc_Gen_ArceAnalysis_Data.getArceAnalysisData(New List<Id>{arce.Id})[0].arce__anlys_wkfl_sanction_rslt_type__c;
            dataJson.columns = Arc_Gen_Expandible_Table_Service.getColumns(sanctionResult);
            dataJson.data = Arc_Gen_Expandible_Table_Service.buildNestedData(recordId);
            response.jsonResponse = dataJson;
        } else {
            System.Type objType = Type.forName(inputClass);
            final Arc_Gen_Expandible_Table_Interface interfaceClass = (Arc_Gen_Expandible_Table_Interface)objType.newInstance();
            response.jsonResponse = interfaceClass.getData(recordId);
        }
        return response;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description get date of sanction to show in politics header table
    * --------------------------------------------------------------------------------------
    * @author mariohumberto.ramirez.contractor@bbva.com
    * @date 2019-10-11
    * @param recordId - id of the account_has_analysis.
    * @return String - date of sanction
    * @example getHeaderDate(recordId)
    * --------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static String getHeaderDate(Id recordId) {
        return Arc_Gen_Expandible_Table_Service.getHeaderDate(recordId);
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description Delete records
    * --------------------------------------------------------------------------------------
    * @author mariohumberto.ramirez.contractor@bbva.com
    * @date 2019-06-20
    * @param recordId - id of the account_has_analysis.
    * @return response. map of data for policies deletition
    * @example deleteRecords(recordId)
    * --------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static Map<String,String> deleteRecords(Id recordId) {
        final ResponseWrapper response = new ResponseWrapper();
        response.deleteResponse = Arc_Gen_Expandible_Table_Service.deleteRecord(recordId);
        return response.deleteResponse;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Method that return's the record Id of a record type
    * --------------------------------------------------------------------------------------
    * @author mariohumberto.ramirez.contractor@bbva.com
    * @date 2019-06-20
    * @param recordTypeName - name of the record type
    * @return recordIdResponse - String with the Id of the record type
    * @example getRecordId(recordTypeName)
    * --------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static String getRecordId(String recordTypeName) {
        final String recordIdResponse = Arc_Gen_Expandible_Table_Service.getRecordTypeId(recordTypeName);
        return recordIdResponse;
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description get a list of products
    * --------------------------------------------------------------------------------------
    * @author mariohumberto.ramirez.contractor@bbva.com
    * @date 2019-06-20
    * @param tipologia - name of the typology
    * @return lista. list of typologys
    * @example getProducts(tipologia)
    * --------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static ResponseWrapper getProducts(String tipologia) {
        ResponseWrapper response = new ResponseWrapper();
        try {
            response.successResponse = true;
            response.gblProductResp = Arc_Gen_Expandible_Table_Service.getProductsService(tipologia);
        } catch (Exception e) {
            response.successResponse = false;
            response.errorResponse = e.getMessage() + ' : ' + e.getLineNumber();
        }
        return response;
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description change the status of the validate client to false
    * --------------------------------------------------------------------------------------
    * @author mariohumberto.ramirez.contractor@bbva.com
    * @date 2019-06-20
    * @param recordId - id of the account has analysis object
    * @param desactivate - string to desactivate the flag
    * @return ResponseWrapper wrapper for policies to validate status
    * @example desactivateValidFlag()
    * --------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static ResponseWrapper desactivateValidFlag(string recordId, string desactivate) {
        final ResponseWrapper wrapper = new ResponseWrapper();
        try {
            Arc_Gen_Validate_Customer_Data.activeCustomerFlag(recordId, desactivate);
            wrapper.successResponse = Arc_Gen_TabSet_service.changeArceState(recordId);
            wrapper.errorResponse = System.Label.Arc_Gen_Success_Parrilla_Message;
        } catch (Exception e) {
            wrapper.errorResponse = System.Label.Arc_Gen_ApexCallError + ' : ' + e.getMessage() + ' : ' + e.getLineNumber();
        }
        return wrapper;
    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that sum 2 typologies
    * ----------------------------------------------------------------------------------------------------
    * @Author mariohumberto.ramirez.contractor@bbva.com
    * @Date 2019-09-30
    * @param recordId - Id of the account has analysis object
    * @return void
    * @example sumTypologies(recordId)
    * ----------------------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static void sumTypologies(Id recordId) {
    Arc_Gen_Expandible_Table_Service.sumTypologies(recordId);
    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that sum 2 typologies
    * ----------------------------------------------------------------------------------------------------
    * @Author mariohumberto.ramirez.contractor@bbva.com
    * @Date 2020-01-28
    * @param recordId - Id of the account has analysis object
    * @return ResponseWrapper
    * @example changeServiceFlag(recordId)
    * ----------------------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static ResponseWrapper changeServiceFlag(Id recordId) {
        ResponseWrapper wrapper = new ResponseWrapper();
        try {
            wrapper.successResponse = true;
            Arc_Gen_Auto_ExpTable_Service.changeServiceFlag(recordId);
        } catch (Exception e) {
            wrapper.successResponse = false;
            wrapper.errorResponse = System.Label.Arc_Gen_ApexCallError + ' : ' + e.getMessage() + ' : ' + e.getLineNumber();
        }
        return wrapper;
    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that return the amount of the selected product
    * ----------------------------------------------------------------------------------------------------
    * @Author mariohumberto.ramirez.contractor@bbva.com
    * @Date 2020-01-28
    * @param accHasAId - Id of the account has analysis object
    * @param tipologia - developer name of the typology
    * @param prodId - Id of the selected product
    * @return ResponseWrapper
    * @example insertProducts(recordId)
    * ----------------------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static ResponseWrapper insertProducts(String accHasAId, String tipologia, String prodId, String recordId) {
        ResponseWrapper wrapper = new ResponseWrapper();
        try {
            wrapper.successResponse = true;
            System.Type objType = Type.forName('Arc_Gen_Auto_ExpTable_Service');
            final Arc_Gen_Expandible_Table_Interface interfaceClass = (Arc_Gen_Expandible_Table_Interface)objType.newInstance();
            wrapper.values2Insert = interfaceClass.insertProducts(accHasAId, tipologia, prodId);

            // Update limits retrieved by service in arce__limits_exposures__c record.
            arce__limits_exposures__c limitsRecord = new arce__limits_exposures__c(
                Id = recordId,
                arce__last_approved_amount__c = wrapper.values2Insert.get('lastApproved'),
                arce__curr_approved_commited_amount__c = wrapper.values2Insert.get('commited'),
                arce__curr_apprv_uncommited_amount__c = wrapper.values2Insert.get('uncommited'),
                arce__current_formalized_amount__c =wrapper.values2Insert.get('currentLimit'),
                arce__outstanding_amount__c = wrapper.values2Insert.get('outstanding'),
                arce__current_approved_amount__c = 0
            );

            Arc_Gen_LimitsExposures_Data.updateExposureData(new List<arce__limits_exposures__c> { limitsRecord });
        } catch (Exception e) {
            wrapper.successResponse = false;
            wrapper.errorResponse = System.Label.Arc_Gen_ApexCallError + ' : ' + e.getMessage() + ' : ' + e.getLineNumber();
        }
        return wrapper;
    }
}