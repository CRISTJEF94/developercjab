/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Expandible_Table_Service
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2019-06-20
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Service class for Arc_Gen_Policies_Controller
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-06-20 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* |2019-06-20 mariohumberto.ramirez.contractor@bbva.com
*             Add new attribute to the JSON response in getColumns method.
* |2019-08-09 mariohumberto.ramirez.contractor@bbva.com
*             Add new line that call the method "deleteDuplicatesValues" the method evals if the
*             typology is duplicated and delete the duplicated record
* |2019-08-14 mariohumberto.ramirez.contractor@bbva.com
*             Deleted call to the method deleteDuplicatesValues
* |2019-09-30 mariohumberto.ramirez.contractor@bbva.com
*             Added new method sumTypologies
* |2019-10-11 mariohumberto.ramirez.contractor@bbva.com
*             Added new method getHeaderDate
* |2020-01-21 juanmanuel.perez,ortiz.contractor@bbva.com
*             Remove column in expandible table
* |2020-01-06 mariohumberto.ramirez.contractor@bbva.com
*             modify method getproductService
* |2020-01-30 javier.soto.carrascosa@bbva.com
*             Add HU 787 missing functionality
* -----------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_Expandible_Table_Service {

    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_Expandible_Table_Service service = new Arc_Gen_Expandible_Table_Service()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_Expandible_Table_Service() {

    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that verify if there are arce__limits_exposures__c records inserted and insert
    * records if there are not records inserted
    * ----------------------------------------------------------------------------------------------------
    * @param recordId - Id of the account has analysis object
    * @return void
    * @example verifyTypologiesInserted()
    * ----------------------------------------------------------------------------------------------------
    **/
    public static void verifyTypologiesInserted(Id recordId) {
        final List<arce__limits_exposures__c> exposureData = Arc_Gen_LimitsExposures_Data.getExposureData(new List<Id>{recordId});
        if (exposureData.isEmpty()) {
            Arc_Gen_ExpTable_Service_Helper.insertTypologies(recordId);
        }
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description get date of sanction to show in politics header table
    * --------------------------------------------------------------------------------------
    * @author mariohumberto.ramirez.contractor@bbva.com
    * @date 2019-10-11
    * @param recordId - id of the account_has_analysis.
    * @return String  date of sanction
    * @example getHeaderDate(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static String getHeaderDate(Id recordId) {
        return String.valueOf(Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<String>{(String)recordId})[0].arce__Analysis__r.arce__analysis_risk_sanction_date__c);
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return the column information of the policy table
    * --------------------------------------------------------------------------------------
    * @param void
    * @return columns information of the policy table
    * @example getColumns()
    * --------------------------------------------------------------------------------------
    **/
    public static List<Arc_Gen_TableColumns> getColumns(String arceStage) {
        Map<String,String> attributes = new Map<String,String>();
        attributes.put('alignment','center');
        Arc_Gen_TableColumns tipology = new Arc_Gen_TableColumns();
        tipology.type = 'text';
        tipology.fieldName = 'tipology';
        tipology.label = System.Label.Arc_Gen_Tipology;
        tipology.initialWidth = 400;
        tipology.cellAttributes = attributes;
        Arc_Gen_TableColumns lastApproved = new Arc_Gen_TableColumns();
        lastApproved.type = 'number';
        lastApproved.fieldName = 'lastApproved';
        lastApproved.label = System.Label.Arc_Gen_Last_Approved;
        lastApproved.cellAttributes = attributes;
        Arc_Gen_TableColumns commited = new Arc_Gen_TableColumns();
        commited.type = 'number';
        commited.fieldName = 'commited';
        commited.label = System.Label.Arc_Gen_Committed;
        commited.cellAttributes = attributes;
        Arc_Gen_TableColumns uncommited = new Arc_Gen_TableColumns();
        uncommited.type = 'number';
        uncommited.fieldName = 'uncommited';
        uncommited.label = System.Label.Arc_Gen_Non_committed;
        uncommited.cellAttributes = attributes;
        Arc_Gen_TableColumns currentLimit = new Arc_Gen_TableColumns();
        currentLimit.type = 'number';
        currentLimit.fieldName = 'currentLimit';
        currentLimit.label = System.Label.Arc_Gen_Current_limit;
        currentLimit.cellAttributes = attributes;
        Arc_Gen_TableColumns outstanding = new Arc_Gen_TableColumns();
        outstanding.type = 'number';
        outstanding.fieldName = 'outstanding';
        outstanding.label = System.Label.Arc_Gen_Outstanding;
        outstanding.cellAttributes = attributes;
        Arc_Gen_TableColumns limitProposed = new Arc_Gen_TableColumns();
        limitProposed.type = 'number';
        limitProposed.fieldName = 'limitProposed';
        limitProposed.label =  arceStage == '1' || arceStage == '2'  ?  System.Label.Arc_Gen_Proposed_Approved : System.Label.Arc_Gen_Proposed;
        limitProposed.cellAttributes = attributes;
        List<Arc_Gen_TableColumns> columns = new List<Arc_Gen_TableColumns>{tipology,lastApproved,commited,uncommited,currentLimit,outstanding,limitProposed};
        return columns;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return the data needed to create the policy table
    * --------------------------------------------------------------------------------------
    * @param recordId - id of the account_has_analysis.
    * @return ordenatedList a  List<Arc_Gen_TableRow> that contain the data to create the
    * policy table.
    * @example buildNestedData(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static List<Arc_Gen_TableRow> buildNestedData (Id recordId) {
        return Arc_Gen_ExpTable_Service_Helper.getNestedData(recordId);
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Delete records
    * --------------------------------------------------------------------------------------
    * @param recordId - id of the account_has_analysis.
    * @return response.
    * @example deleteRecord(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static Map<String,String> deleteRecord(Id recordId) {
        return Arc_Gen_ExpTable_Service_Helper.deleteRecordTable(recordId);
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Method that return's the record Id of a record type
    * --------------------------------------------------------------------------------------
    * @param recordTypeName - name of the record type
    * @return recordIdResp - String with the Id of the record type
    * @example getRecordTypeId(recordTypeName)
    * --------------------------------------------------------------------------------------
    **/
    public static String getRecordTypeId(String recordTypeName) {
        String recordIdResp = '';
        for(RecordType recordId : Arc_Gen_GenericUtilities.getRecordTypeData(recordTypeName)) {
            recordIdResp = recordId.Id;
        }
        return recordIdResp;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description get a list of products
    * --------------------------------------------------------------------------------------
    * @param tipologia - name of the typology
    * @return lista.
    * @example getProductsService(tipologia)
    * --------------------------------------------------------------------------------------
    **/
    public static List<Map<string,string>> getProductsService(String tipologia) {
        final List<Map<string,string>> listaProd = new List<Map<string,string>>();
        Map<Id,Arc_Gen_Product_Wrapper> pWrap = Arc_Gen_Product_Locator.getProductsFromTypology(tipologia);
        if (pWrap.isEmpty()) {
            throw new QueryException(Label.Arc_Gen_VoidList);
        }
        Arc_Gen_Product_Wrapper prod = new Arc_Gen_Product_Wrapper();
        for (id prodId: pWrap.keySet()) {
            prod = pWrap.get(prodId);
            listaProd.add(new map<String,String>{'value' => prodId , 'label' => prod.productName});
        }
        return listaProd;
    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that sum typologies
    * ----------------------------------------------------------------------------------------------------
    * @Author mariohumberto.ramirez.contractor@bbva.com
    * @Date 2019-09-30
    * @param recordId - Id of the account has analysis object
    * @return void
    * @example sumTypologies(recordId)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static void sumTypologies(Id recordId) {
        Arc_Gen_ExpTable_Service_Helper.sumTypologies(recordId);
    }
}