/**
* @File Name          : Arc_Gen_CallListParticipant.cls
* @Description        : Contain logic to process participants ASO WS (getEconomiclistpart)
* @Author             : Luis Arturo Parra Rosas / luisarturo.parra.contractor@bbva.com
* @Group              : ARCE Team
* @Data Class         : Arc_Gen_Groups_data
* @Main ctr Class     : Arc_Gen_Groups_Service
* @Test Class         : Arc_Gen_Groups_controller_test
* @Last Modified By   : luisarturo.parra.contractor@bbva.com
* @Last Modified On   : 30/08/2019
* @Modification Log   :
*==========================================================================================
* Ver         Date                     Author                            Modification
*==========================================================================================
* 1.0         30/08/2019                luisarturo.parra.contractor@bbva.com     Initial Version
* 1.1         26/02/2020                javier.soto.carrascosa@bbva.com     	 Add support to filter groups in listparticipants
* 1.2         27/02/2020                juanignacio.hita.contractor@bbva.com     Add pagination  service
****************************************************************************************************************/
@SuppressWarnings('PMD.ExcessivePublicCount')
public without sharing class Arc_Gen_CallListParticipant {
    /**
    * @Description : Wrapper participant object
    */
    public class Participantobj {
        public String participantId {get; set;}
        public String  participname{get; set;}
        public String  pRelLevel {get; set;}
        public String  pRelRelationTypeId {get; set;}
        public String  parentEconomicGroupId {get; set;}
    }
    /**
    * @Description : Wrapper used in the error managed
    */
    public class Innertoreturnlistp{
        public String error204message {get; set;}
        public String errormessage {get; set;}
        public String servicecallerror {get; set;}
        public List<Participantobj> customersdata {get; set;}

        /**
        * @Description : Constructor of the wrapper Innertoreturnlistp
        */
        void constructor() {
            error204message = '';
            errormessage = '';
            servicecallerror = '';
            customersdata = new List<Participantobj>();
        }
    }
    /**
    * @Description : Wrapper of links
    */
    public class WrapperLinks {
        /**
        * @Description: Links
        */
        public LinkUnit links {get;set;}
    }
    /**
    * @Description : Wrapper of links  (first and next of the key service)
    */
    public class LinkUnit {
        /**
        * @Description : First link
        */
        public String first {get;set;}
        /**
        * @Description : Next link
        */
        public String next {get;set;}
    }
    /**
    * @Description : return data init
    */
    public static Innertoreturnlistp returndata = new Innertoreturnlistp();
    /**
    *-------------------------------------------------------------------------------
    * @description calls getAnalysis and return data needed to visualizate current stage:
    * serviceStatus - OK/KO
    * message - empty / message to be displayed
    * retToPreparing - OK/KO
    *--------------------------------------------------------------------------------
    * @date     20/06/2019
    * @author   diego.miguel.contractor@bbva.com
    * @param    recordId - account_has_analysis Id
    * @return   Arc_Gen_RefreshClass_service.refreshMessagesResponse
    * @example  public static Arc_Gen_CallListParticipant.Innertoreturnlistp callListParticipants(String encryptedGroup) {
    */
    public static Arc_Gen_CallListParticipant.Innertoreturnlistp callListParticipants(String encryptedGroup) {
        Integer maxCalls = Integer.valueOf(verifyPaginationService());
        List<Arc_Gen_getIASOResponse.serviceResponse> lstResponses = new List<Arc_Gen_getIASOResponse.serviceResponse>();
        List<String[]> customersdata;
        returndata.constructor();

        if (Test.isRunningTest()) {
            maxCalls = 1;
        }

        try {
            lstResponses = callWithPagination(encryptedGroup, maxCalls);
            if (!lstResponses.isEmpty()) {
                for (Arc_Gen_getIASOResponse.serviceResponse response : lstResponses) {
                    final List<Participantobj> participantObj = processDataListParticipants(response.serviceResponse);
                    returndata.customersdata.addAll(participantObj);
                }
            }

            if (returndata.errormessage != '') {
                throw new AuraHandledException(returndata.errormessage);//NOSONAR
            } else {
                return returndata;
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());//NOSONAR
        }
    }
    /**
    *-------------------------------------------------------------------------------
    * @description call to list participant service with pagination and return an array of response
    *--------------------------------------------------------------------------------
    * @date     10/02/2020
    * @author   juanignacio.hita.contractor@bbva.com
    * @param    encryptedGroup
    * @param    maxCalls
    * @return   List<Arc_Gen_getIASOResponse.serviceResponse>
    * @example  callWithPagination(encryptedGroup, maxCalls);
    */
    public static List<Arc_Gen_getIASOResponse.serviceResponse> callWithPagination(String encryptedGroup, Integer maxCalls) {
        List<Arc_Gen_getIASOResponse.serviceResponse> lstResponses = new List<Arc_Gen_getIASOResponse.serviceResponse>();
        final Arc_Gen_CustomServiceMessages serviceMessages = new Arc_Gen_CustomServiceMessages();
        String linkNext = '';
        Integer currentCalls = 1;
        String paramsReq = '';
        Boolean errorService = false;
        do {
            paramsReq = '{"groupId":"' + encryptedGroup + '", "paginationKey":"' + linkNext + '"}';
            final Arc_Gen_getIASOResponse.serviceResponse sResponse = callListParticipant(paramsReq);
            if (sResponse.serviceCode == String.valueOf(serviceMessages.CODE_200)) {
                lstResponses.add(sResponse);
                linkNext = getNextLinkParticipants(sResponse.serviceResponse);
                currentCalls++;
            } else if (sResponse.serviceCode == String.valueOf(serviceMessages.CODE_204) && sResponse.serviceMessage == serviceMessages.SUCCESSFUL_204) {
                returndata.error204message = System.Label.Lc_arce_GRP_noExstGrp;
                lstResponses = new List<Arc_Gen_getIASOResponse.serviceResponse>();
                errorService = true;
            } else {
                returndata.errormessage= System.Label.Cls_arce_GRP_servError + ': ' + sResponse.serviceCode + ' / ' + sResponse.serviceMessage;
                lstResponses = new List<Arc_Gen_getIASOResponse.serviceResponse>();
                errorService = true;
            }
        } while (currentCalls <= maxCalls && linkNext != '' && !errorService);
        return lstResponses;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description return true if the pagination service configuration is active
    *--------------------------------------------------------------------------------
    * @date     10/02/2020
    * @author   juanignacio.hita.contractor@bbva.com
    * @param    nameConfig - configuration name of the metadata
    * @return   Boolean
    * @example  verifyPaginationService('PagintationParticipants');
    */
    public static Integer verifyPaginationService() {
        Integer valueMax = 1;
        List<Arce_Config__mdt> lstArceConfig = Arc_Gen_Arceconfigs_locator.getConfigurationInfo('PaginationParticipants');
        if (!lstArceConfig.isEmpty()) {
            valueMax = Integer.valueOf(lstArceConfig[0].Value1__c);
            valueMax = valueMax < 0 ? 1 : valueMax;
        }
        return valueMax;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Process the response, to get the following next link to service call
    *--------------------------------------------------------------------------------
    * @date     10/02/2020
    * @author   juanignacio.hita.contractor@bbva.com
    * @param    serviceResponse - Map<String, Object>
    * @return   String
    * @example  getNextLinkParticipants(serviceResponse);
    */
    public static String getNextLinkParticipants(Map<String, Object> serviceResponse) {
        String nextLink = '';
        final Map<String, Object> mapPagination = getMapFromJson(serviceResponse, 'pagination');
        Map<String, String> mapParams = new Map<String, String>();
        if (mapPagination != null && mapPagination.size() > 0) {
            final String strPagination = JSON.serialize(serviceResponse.get('pagination'));
            final WrapperLinks wrpLink = (WrapperLinks) System.JSON.deserialize(strPagination, WrapperLinks.class);

            if (wrpLink.links.next != null) {
                final String subUrl = wrpLink.links.next.subString(wrpLink.links.next.indexOf('?') + 1);
                final List<String> subUrlAmp = subUrl.split('&');
                for (String strUrl : subUrlAmp) {
                    final List<String> paramsUrl = strUrl.split('=');
                    mapParams.put(paramsUrl[0], paramsUrl[1]);
                }
                nextLink = mapParams.get('paginationKey');
            }
        }
        return nextLink;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description calls getAnalysis and return data needed to visualizate current stage:
    * serviceStatus - OK/KO
    * message - empty / message to be displayed
    * retToPreparing - OK/KO
    *--------------------------------------------------------------------------------
    * @date     20/06/2019
    * @author   diego.miguel.contractor@bbva.com
    * @param    recordId - account_has_analysis Id
    * @return   Arc_Gen_RefreshClass_service.refreshMessagesResponse
    * @example  private static List<Participantobj> processDataListParticipants(Map<String, Object> serviceResponse) {
    */
    private static List<Participantobj> processDataListParticipants(Map<String, Object> serviceResponse) {
        List<Participantobj> participantreturlist = new List<Participantobj>();
        for(Map<String, Object> participant : getListFromJson(serviceResponse, 'data')) {
            Participantobj participants = new Participantobj();
            participants.participantId = (String) participant.get('id');
            Map<String, Object> participantRelation = getMapFromJson(participant, 'relation');
            participants.pRelLevel = (String) participantRelation.get('level');
            Map<String, Object> pRelRelationType = getMapFromJson(participant, 'participantType');
            participants.pRelRelationTypeId = (String) pRelRelationType.get('id');
            Map<String, Object> pRelParentEconomicGroup = getMapFromJson(participantRelation, 'parentEconomicGroup');
            String pRelParentEconomicGroupId = (String)pRelParentEconomicGroup.get('id');
            if(! String.isEmpty(pRelParentEconomicGroupId)) {// do not recover group from list participants
                participantreturlist.add(participants);
            }
        }
        return participantreturlist;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description calls getAnalysis and return data needed to visualizate current stage:
    * serviceStatus - OK/KO
    * message - empty / message to be displayed
    * retToPreparing - OK/KO
    *--------------------------------------------------------------------------------
    * @date     20/06/2019
    * @author   diego.miguel.contractor@bbva.com
    * @param    recordId - account_has_analysis Id
    * @return   Arc_Gen_RefreshClass_service.refreshMessagesResponse
    * @example  private static List<Map<String, Object>> getListFromJson(Map<String, Object> prevMap, String keyToRetrieve) {
    */
    private static List<Map<String, Object>> getListFromJson(Map<String, Object> prevMap, String keyToRetrieve) {
        List<Map<String, Object>> listOfMap = new List<Map<String, Object>>();
        List<Object> objectList = (List<Object>)prevMap.get(keyToRetrieve);
        for(Object element : objectList) {
            listOfMap.add((Map<String, Object>)element);
        }
        return listOfMap;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description calls getAnalysis and return data needed to visualizate current stage:
    * serviceStatus - OK/KO
    * message - empty / message to be displayed
    * retToPreparing - OK/KO
    *--------------------------------------------------------------------------------
    * @date     20/06/2019
    * @author   diego.miguel.contractor@bbva.com
    * @param    recordId - account_has_analysis Id
    * @return   Arc_Gen_RefreshClass_service.refreshMessagesResponse
    * @example  private static Map<String, Object> getMapFromJson(Map<String, Object> prevMap, String keyToRetrieve) {
    */
    private static Map<String, Object> getMapFromJson(Map<String, Object> prevMap, String keyToRetrieve) {
        Map<String, Object> data = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(prevMap.get(keyToRetrieve)));
        if (data == null) {
            data = new Map<String, Object>();
        }
        return data;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description calls getAnalysis and return data needed to visualizate current stage:
    * serviceStatus - OK/KO
    * message - empty / message to be displayed
    * retToPreparing - OK/KO
    *--------------------------------------------------------------------------------
    * @date     20/06/2019
    * @author   diego.miguel.contractor@bbva.com
    * @param    recordId - account_has_analysis Id
    * @return   Arc_Gen_RefreshClass_service.refreshMessagesResponse
    * @example  public static Arc_Gen_getIASOResponse.serviceResponse callListParticipant(String params) {
    */
    private static Arc_Gen_getIASOResponse.serviceResponse callListParticipant(String params) {
        return Arc_Gen_getIASOResponse.calloutIASO('listParticipants', params);
    }
}