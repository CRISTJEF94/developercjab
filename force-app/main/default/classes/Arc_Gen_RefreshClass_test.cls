/*------------------------------------------------------------------
*Author:        Diego Miguel Tamarit - diego.miguel.contractor@bbva.com / dmiguelt@minsait.com
*Project:       ARCE - BBVA Bancomer
*Description:   Test class for code coverage of:
*Arc_Gen_ProposeInPreparation_controller
*Arc_Gen_RefreshClass_service
*Arc_Gen_RefreshClass_data
*_______________________________________________________________________________________
*Version    Date           Author                               Description
*1.0        28/05/2019     diego.miguel.contractor@bbva.com     Creación.
*1.1        21/11/2019     eduardoefrain.hernandez.contractor@bbva.com Update test methods because of propose refactor
*1.2        07/01/2020     javier.soto.carrascosa@bbva.com Add support for account wrapper and setupaccounts
*1.2        28/02/2020     juanignacio.hita.contractor@bbva.com Add new test method
-----------------------------------------------------------------------------------------*/
@isTest
public class Arc_Gen_RefreshClass_test {
    /**
    * @Description: String with external id of test group
    */
    static final string GROUP_ID = 'G000001';
    /**
    * @Description: String with external id of test group
    */
    static final string SUBSIDIARY_ID1 = 'C000001';
    /**
    * @Description: String with external id of test subsidiary
    */
    static final string SUBSIDIARY_ID2 = 'C000002';
    /**
    * @Method:      test for method Arc_Gen_RefreshClass_test.
    * @Description: test setup method.
    */
    @testSetup static void setup() {
        Arc_UtilitysDataTest_tst.setupAcccounts();
        List<sObject> iasoCnfList = Arc_UtilitysDataTest_tst.crearIasoUrlsCustomSettings();
        insert iasoCnfList;

        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{GROUP_ID,SUBSIDIARY_ID1,SUBSIDIARY_ID2});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(GROUP_ID);
        final Arc_Gen_Account_Wrapper childAccount1 = groupAccWrapper.get(SUBSIDIARY_ID1);
        final Arc_Gen_Account_Wrapper childAccount2 = groupAccWrapper.get(SUBSIDIARY_ID2);

        arce__Sector__c newSector = arc_UtilitysDataTest_tst.crearSector('Generic', '100;300;400', 's-01', null);
        insert newSector;

        arce__Analysis__c newArce = Arc_UtilitysDataTest_tst.crearArceAnalysis('Arce Analysis', null, groupAccount.accId);
        newArce.arce__Stage__c = '1';
        insert newArce;

        arce__Analysis__c newArce2 = Arc_UtilitysDataTest_tst.crearArceAnalysis('Arce Analysis11', null, groupAccount.accId);
        newArce2.arce__Stage__c = '1';
        insert newArce2;

        arce__Account_has_Analysis__c acc1 = Arc_UtilitysDataTest_tst.crearAccHasAnalysis(newSector.id, newArce.Id, groupAccount.accId, 's-01');
        acc1.arce__InReview__c = true;
        acc1.arce__path__c = 'MAC';
        acc1.arce__group_asset_header_type__c = '1';
        insert acc1;

        arce__Account_has_Analysis__c acc2 = Arc_UtilitysDataTest_tst.crearAccHasAnalysis(newSector.id, newArce.Id, childAccount1.accId, 's-01');
        acc2.arce__InReview__c = true;
        acc2.arce__path__c = 'MACAHA2';
        acc2.arce__group_asset_header_type__c = '2';
        insert acc2;

        arce__Account_has_Analysis__c acc3 = Arc_UtilitysDataTest_tst.crearAccHasAnalysis(newSector.id, newArce.Id, childAccount2.accId, 's-01');
        acc3.arce__InReview__c = true;
        acc3.arce__path__c = 'MAC';
        insert acc3;

        arce__rating__c rating = Arc_UtilitysDataTest_tst.crearRating(null);
        insert rating;

        arce__Financial_Statements__c finStatement = Arc_UtilitysDataTest_tst.crearFinStatement(childAccount1.accId, acc2.id, rating.id, null);
        finStatement.arce__financial_statement_end_date__c = Date.today();
        finStatement.arce__ffss_valid_type__c = '1';
        insert finStatement;

        acc2.arce__ffss_for_rating_id__c = finStatement.Id;
        Update acc2;

        Arc_UtilitysDataTest_tst.createAssessmentData();
        Arc_UtilitysDataTest_tst.createBankingRelationshipDepData();
        Arc_UtilitysDataTest_tst.createBasicDataData();
        Arc_UtilitysDataTest_tst.createFinancialRiskData();
        Arc_UtilitysDataTest_tst.createBusinessRiskData();
        Arc_UtilitysDataTest_tst.createIndustryAnalysisData();
        Arc_UtilitysDataTest_tst.createPolicies();
    }
    /**
    * @Method:  Test for method constructor
    * @Description: Testing method.
    */
    @isTest
    static void testConstructor() {
        Test.startTest();
        final Arc_Gen_Refresh_controller contConstructor = new Arc_Gen_Refresh_controller();
        System.assertEquals(contConstructor, contConstructor, 'Empty constructor');
        Test.stopTest();
    }
    /**
    * @Method:      Test with success for method getAllAnalysis (recordId)
    * @Description: testing method.
    */
    @isTest static void testGetAllAnalysisOK() {
        final Id recordId = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1].Id;
        Test.startTest();
        final List<Arc_Gen_Account_Has_Analysis_Wrapper> lst = Arc_Gen_Refresh_controller.getAHARefresh(recordId);
        System.assertEquals(lst[0].ahaObj.Id, recordId, 'Get all analysis test');
        Test.stopTest();
    }
    /**
    * @Method:      Test with fail for method getAllAnalysis (recordId)
    * @Description: testing method.
    */
    @isTest static void testGetAllAnalysisKO() {
        try {
            final Id recordId = [SELECT Id FROM arce__Analysis__c LIMIT 1].Id;
            Test.startTest();
            Arc_Gen_Refresh_controller.getAHARefresh(recordId);
            Test.stopTest();
        } catch (Exception ex) {
            System.assert(ex.getMessage().contains('Script'), 'Script-thrown exception');
        }
    }
    /**
    * @Method:      Test with success for method callListCustomers
    * @Description: testing method.
    */
    @isTest static void testCallListCustomersOK() {
        final List<arce__Account_has_Analysis__c> newMiniArces = [SELECT Id FROM arce__Account_has_Analysis__c WHERE arce__path__c = 'MACAHA2'];
        Test.startTest();
        Arc_Gen_RefreshClass_service.refreshMessagesResponse response = Arc_Gen_Refresh_controller.callListCustomers(newMiniArces[0].Id);
        System.assertEquals('OK', response.serviceStatus, 'testCallListCustomFromRefresh expected OK response');
        Test.stopTest();
    }
    /**
    * @Method:      Test with fail for method callListCustomers
    * @Description: testing method.
    */
    @isTest static void testCallListCustomersKO() {
        try {
            final List<arce__Analysis__c> newMiniArces = [SELECT Id FROM arce__Analysis__c LIMIT 1];
            Test.startTest();
            Arc_Gen_Refresh_controller.callListCustomers(newMiniArces[0].Id);
            Test.stopTest();
        } catch (Exception ex) {
            System.assert(ex.getMessage().contains('Script'), 'Script-thrown exception');
        }
    }
    /**
    * @Method:      Test with success for method getListParticipants
    * @Description: testing method.
    */
    @isTest static void testGetListParticipants() {
        try {
            Test.startTest();
            final String[] withoutgroup = new String[0];
            final String[] groupnumber = new String[0];
            for (Account acc : [SELECT Id, AccountNumber FROM Account LIMIT 3]) {
                withoutgroup.add(acc.Id);
                groupnumber.add(acc.AccountNumber);
            }

            final List<arce__Account_has_Analysis__c> newMiniArces = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
            Arc_Gen_Refresh_controller.getListParticipants('sfYZ9luP93g9U9AqdpF1Iw', withoutgroup, groupnumber, newMiniArces[0].Id);
            Test.stopTest();
        } catch (Exception ex) {
            System.assert(ex.getMessage().contains('Script'), 'Script-thrown exception');
        }
    }
    /**
    * @Method:      Test with success for method createAHAParticipant
    * @Description: testing method.
    */
    @isTest static void testCreateAHAParticipantOK() {
        Test.startTest();
            List<String> partNoExist = new List<String>();
            final List<Account> lstAccount = [SELECT Id, AccountNumber FROM Account LIMIT 3];
            final Id analysisId = [SELECT Id FROM arce__Analysis__c LIMIT 1].Id;
            for (Account acc : lstAccount) {
                partNoExist.add(acc.AccountNumber);
            }
            final List<arce__Account_has_Analysis__c> lst = Arc_Gen_RefreshClass_data.createAHAParticipants(partNoExist, analysisId);
            System.assertEquals(lst[0].arce__Analysis__c, analysisId, 'Create AHA participant test');
        Test.stopTest();
    }
}