/**
* @File Name          : Arc_Gen_ValidateRating_service.cls
* @Description        : Class that contains the logic of the rating validation
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE Team
* @Last Modified By   : juanmanuel.perez.ortiz.contractor@bbva.com
* @Last Modified On   : 30/01/2020 16:52:30
* @Changes
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    13/5/2019 18:00:36   eduardoefrain.hernandez.contractor@bbva.com     Initial Version
* 1.1    26/9/2019 09:20:36   javier.soto.carrascosa@bbva.com     Remove mock
* 1.2    19/12/2019 16:53:27  manuelhugo.castillo.contractor@bbva.com         Modify methods 'getRatingData,changeRatingStatus,setupValidateRating' replace
*                                                                             arce__Account_has_Analysis__c to Arc_Gen_Account_Has_Analysis_Wrapper
* 1.3    24/01/2020 12:06:11  juanmanuel.perez.ortiz.contractor@bbva.com      Remove logic static parameters to ASO services
* 1.4    30/01/2020 16:52:30   juanmanuel.perez.ortiz.contractor@bbva.com     Add missing custom labels
**/
public without sharing class Arc_Gen_ValidateRating_service {
    /**
    * @Description: Name of the EncryptionFlag custom metadata
    */
    private final static String ENCRYPTION_FLAG = 'EncryptionFlag';
    /**
    * @Description: Name of the bankId custom setting
    */
    private final static String BANK_ID = '0001';
    /**
    * @Class: Adjustments
    * @Description: Wrapper that contain all the rating adjustments
    * @author BBVA
    */
    public class Adjustments {
        /**
        * @Description: Rating score
        */
        Decimal score{get;set;}
        /**
        * @Description: Rating expressed in short scale
        */
        String shortScaleValue{get;set;}
        /**
        * @Description: Rating expressed in long scale
        */
        String longScaleValue{get;set;}
        /**
        * @Description: Rating default probability perentage
        */
        String defaultProbability{get;set;}
    }
/**
*-------------------------------------------------------------------------------
* @description Method that gets the rating data
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 2019-05-13
* @param String analyzedClientId id of account for the analisis
* @return List<String> - A list of client rating data. Position[0] = ratingId, Position[1] = rating final, Position[2] = rating score
* @example public static List<String> getRatingData(String analyzedClientId)
**/
    public static List<String> getRatingData(String analyzedClientId) {
        List<String> ratingData = new List<String>();
        Arc_Gen_ValidateRating_data locator = new Arc_Gen_ValidateRating_data();
        Arc_Gen_Account_Has_Analysis_Wrapper analysis = locator.getAccountHasAnalysis(analyzedClientId);
        arce__rating__c rating = locator.getRatingData(analysis.ahaObj.arce__ffss_for_rating_id__r.arce__rating_id__c);
        ratingData.add(rating.Id);
        ratingData.add(rating.arce__rating_long_value_type__c);
        ratingData.add(String.valueOf(rating.arce__total_rating_score_number__c));
        Return ratingData;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that inicializes the response of the validate rating
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 2019-05-13
* @param String analyzedClientId id of account for the analisis
* @return Arc_Gen_ServiceAndSaveResponse - A wrapper with the DML save results and the response of the service
* @example public static Arc_Gen_ServiceAndSaveResponse getRatingData(String analyzedClientId)
**/
    public static Arc_Gen_ServiceAndSaveResponse setupValidateRating(String analyzedClientId, String ratingId, String mockCode) {
        Arc_Gen_ValidateRating_data locator = new Arc_Gen_ValidateRating_data();
        Arc_Gen_getRatingDataService_data locatorRating = new Arc_Gen_getRatingDataService_data();
        Arc_Gen_Account_Has_Analysis_Wrapper analyzedClient = locator.getAccountHasAnalysis(analyzedClientId);
        Arc_Gen_ServiceAndSaveResponse response = new Arc_Gen_ServiceAndSaveResponse();
        String customerNumber = analyzedClient.accWrapperObj.accNumber;
        if(Boolean.valueOf(Arc_Gen_Arceconfigs_locator.getConfigurationInfo(ENCRYPTION_FLAG)[0].Value1__c)) {
            customerNumber = Arc_Gen_CallEncryptService.getEncryptedClient(customerNumber);
        }
        String parametersJson = Arc_Gen_getRatingDataService_helper.setServiceParameters(analyzedClient.ahaObj, customerNumber, System.Label.Cls_arce_RatingMethodValidated, BANK_ID);
        arce__rating__c rating = locator.getRatingData(ratingId);
        List<arce__rating_variables_detail__c> variablesList = locator.getRatingVariables(ratingId);
        List<Adjustments> adjustments = setAdjustments(variablesList);
        List<Adjustments> finalRating = setFinalRating(rating);
        String jsonInput = generatejsonInput(rating, adjustments, finalRating);
        String serviceName = 'ratingEngine';
        if(String.isNotBlank(mockCode)) {
            serviceName = mockCode;
            parametersJson = '{"messages":[{"code":"connectionError","message":"Unable to call Passive Products service","parameters": []}]}';
        }
        try {
            Arc_Gen_getIASOResponse.serviceResponse serviceResp = locatorRating.callRatingService(parametersJson, serviceName);
            locator.callService(jsonInput);
            response = processResponse(serviceResp, analyzedClient.ahaObj);
        } catch(CalloutException e) {
            response.saveStatus = 'false';
            response.serviceMessage = e.getMessage();
        }
        Return response;
    }
/*-------------------------------------------------------------------------------
* @description Method that process the service response codes
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param Arc_Gen_getIASOResponse.serviceResponse response - Wrapper that contains the service response info
* @param arce__Account_has_Analysis__c analysis - Analized Client record
* @param List<arce__Financial_Statements__c> - Valid FFSS that will be used by the rating
* @return Arc_Gen_ServiceAndSaveResponse - Wrapper that contains the process information
* @example public static Arc_Gen_ServiceAndSaveResponse processResponse(Arc_Gen_getIASOResponse.serviceResponse response, arce__Account_has_Analysis__c analysis, List<arce__Financial_Statements__c> validFinancialSt)
**/
    public static Arc_Gen_ServiceAndSaveResponse processResponse(Arc_Gen_getIASOResponse.serviceResponse response, arce__Account_has_Analysis__c analyzedClient) {
        Arc_Gen_ServiceAndSaveResponse serviceAndSaveResp = new Arc_Gen_ServiceAndSaveResponse();
        Arc_Gen_CustomServiceMessages serviceMessage = new Arc_Gen_CustomServiceMessages();
        serviceAndSaveResp.serviceCode = response.serviceCode;
        serviceAndSaveResp.serviceMessage = response.serviceMessage;
        if(response.serviceCode == String.valueOf(serviceMessage.CODE_200)) {
            List<String> changeStatus = changeRatingStatus(analyzedClient.Id);
            serviceAndSaveResp.saveStatus = changeStatus[0];
            serviceAndSaveResp.saveMessage = changeStatus[1];
        } else if(response.serviceCode == String.valueOf(serviceMessage.CODE_400)){
            serviceAndSaveResp.saveStatus = 'false';
            serviceAndSaveResp.saveMessage = Arc_Gen_getRatingDataService_helper.processErrorData(response.serviceResponse);
        }
        Return serviceAndSaveResp;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that changes the status of a Rating from Calculated to Validated
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 2019-05-13
* @param String analyzedClientId id of account for the analisis
* @return List<String> A list of update results. Position[0] = status(true/false), Position[1] = message
* @example public static List<String> changeRatingStatus(String analyzedClientId)
**/
    public static List<String> changeRatingStatus(String analyzedClientId) {
        List<String> changeStatusData = new List<String>();
        Arc_Gen_ValidateRating_data locator = new Arc_Gen_ValidateRating_data();
        Arc_Gen_Account_Has_Analysis_Wrapper analysis = locator.getAccountHasAnalysis(analyzedClientId);
        arce__rating__c rating = locator.getRatingData(analysis.ahaObj.arce__ffss_for_rating_id__r.arce__rating_id__c);
        rating.arce__status_type__c = '3';
        Arc_Gen_ValidateRating_data.saveResult result = locator.updateRecord(rating);
        changeStatusData.add(result.status);
        changeStatusData.add(result.message);
        Return changeStatusData;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that sets the a wrapper with the ratng adjustments
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 2019-05-13
* @param List<arce__rating_variables_detail__c> variablesList - List of rating variables records
* @return List<Adjustments> - A wrapper that contains a rating data
* @example public static List<Adjustments> setAdjustments(List<arce__rating_variables_detail__c> variablesList)
**/
    public static List<Adjustments> setAdjustments(List<arce__rating_variables_detail__c> variablesList) {
        List<Adjustments> adjList = new List<Adjustments>();
        for(arce__rating_variables_detail__c var : variablesList) {
            if(var.arce__adj_long_rating_value_type__c != null) {
                Adjustments adj = new Adjustments();
                adj.score = var.arce__adj_total_rating_score_number__c;
                adj.shortScaleValue = var.arce__adj_short_rating_value_type__c;
                adj.longScaleValue = var.arce__adj_long_rating_value_type__c;
                adj.defaultProbability = String.valueOf(var.arce__PD_per__c);
                adjList.add(adj);
            }
        }
        Return adjList;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that changes sets the final rating data
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 2019-05-13
* @param arce__rating__c rating - record
* @return List<Adjustments> - A wrapper that contains a rating data
* @example public static List<Adjustments> setFinalRating(arce__rating__c rating)
**/
    public static List<Adjustments> setFinalRating(arce__rating__c rating) {
        List<Adjustments> finalList = new List<Adjustments>();
        Adjustments adj = new Adjustments();
        adj.score = rating.arce__total_rating_score_number__c;
        adj.shortScaleValue = rating.arce__rating_short_value_type__c;
        adj.longScaleValue = rating.arce__rating_long_value_type__c;
        adj.defaultProbability = String.valueOf(rating.arce__PD_per__c);
        finalList.add(adj);
        Return finalList;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that generates the service entry json
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 2019-05-13
* @param arce__rating__c rating - record
* @param List<Adjustments> adjustments - Wrapper with the adjustments data
* @param List<Adjustments> finalRating - Wrapper with the final rating data
* @return String - The entry json
* @example public static String generatejsonInput(arce__rating__c rating, List<Adjustments> adjustments, List<Adjustments> finalRating)
**/
    public static String generatejsonInput(arce__rating__c rating, List<Adjustments> adjustments, List<Adjustments> finalRating) {
        JSONGenerator gen = JSON.createGenerator(false);
        gen.writeStartObject();
        gen.writeFieldName('data');
        gen.writeStartObject();
        gen.writeFieldName('rating');
        gen.writeStartObject();
        gen.writeStringField('id', rating.arce__rating_id__c);
        gen.writeDateField('evaluationDate', System.now().date());
        gen.writeFieldName('status');
        gen.writeStartObject();
        gen.writeStringField('id', 'VALIDATED');
        gen.writeEndObject();
        gen.writeNumberField('score', 90);
        gen.writeStringField('defaultProbability', '0.01');
        gen.writeStringField('shortScaleValue', rating.arce__short_rating_value_type__c);
        gen.writeStringField('longScaleValue', rating.arce__long_rating_value_type__c);
        gen.writeObjectField('adjustments', adjustments);
        gen.writeObjectField('final', finalRating);
        gen.writeEndObject();
        gen.writeEndObject();
        gen.writeEndObject();
        String jsonInput = gen.getAsString();
        String jsonEscaped = jsonInput.replace('"', '\\"');
        Return jsonEscaped;
    }
}