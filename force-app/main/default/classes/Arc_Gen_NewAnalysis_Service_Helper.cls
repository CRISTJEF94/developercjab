/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_NewAnalysis_Service_Helper
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2020-13-01
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Helper class for Arc_Gen_NewGroups_service.
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2020-13-01 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* |2020-08-02 ricardo.almanza.contractor@bbva.com
*             Added Orphan
* ------------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_NewAnalysis_Service_Helper {
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-13-01
    * @param void
    * @return void
    * @example Arc_Gen_NewAnalysis_Service_Helper helper = new Arc_Gen_NewAnalysis_Service_Helper()
    * ----------------------------------------------------------------------------------------------------
    **/
    private Arc_Gen_NewAnalysis_Service_Helper() {

    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Custom Exception for New Analysis process
    * ----------------------------------------------------------------------------------------------------
    * @Author   Javier Soto Carrascosa  javier.soto.carrascosa@bbva.com
    * @Date     Created: 2020-02-24
    * @example throw new NewAnalysisException('Unexpected Error');
    * ----------------------------------------------------------------------------------------------------
    **/
    public class NewAnalysisException extends Exception {}
    /**
    *--------------------------------------------------------------------------------
    * @Description method that gets previous arce
    *--------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-13-01
    * @param    recordId - Id of the client
    * @return   List<String>
    * @example  getPreviousArceOnline(recordId)
    * -------------------------------------------------------------------------------
    */
    public static List<String> getPreviousArceOnline(String recordId, String accountswraper) {
        final List<String> arceDataList =  new List<String>();
        final List<Id> accIds = new List<Id>();
        Arc_Gen_Account_Wrapper currentAcc = new Arc_Gen_Account_Wrapper();
        final List<Arc_Gen_Account_Wrapper> accountsWrapLts = (List<Arc_Gen_Account_Wrapper>) JSON.deserialize(accountswraper, List<Arc_Gen_Account_Wrapper>.Class);
        final Arc_Gen_Account_Wrapper groupWrapper = accountsWrapLts[accountsWrapLts.size() - 1];
        for (Arc_Gen_Account_Wrapper accWrapper: accountsWrapLts) {
            if (accWrapper.accId == recordId) {
                currentAcc = accWrapper;
            }
            accIds.add(accWrapper.accId);
        }
        final List<arce__Analysis__c> arceLts = Arc_Gen_ArceAnalysis_Data.getArcesFromAccounts(accIds);
        if (arceLts.isEmpty()) {
            arceDataList.add('');
            arceDataList.add('');
            arceDataList.add(currentAcc.accNumber );
            arceDataList.add(currentAcc.bankId);
            arceDataList.add(groupWrapper.accId == recordId ? groupWrapper.accId : currentAcc.accId);
            arceDataList.add(groupWrapper.accId == recordId ? 'Grupo' : 'Customer');
        } else {
            arceDataList.add(arceLts[0].Id);
            arceDataList.add(arceLts[0].arce__wf_status_id__c);
            arceDataList.add(currentAcc.accNumber );
            arceDataList.add(currentAcc.bankId);
            arceDataList.add(groupWrapper.accId == Id.valueOf(recordId) ? groupWrapper.accId : currentAcc.accId);
            arceDataList.add(groupWrapper.accId == Id.valueOf(recordId) ? 'Grupo' : 'Customer');
        }
        return arceDataList;
    }
    /**
    *--------------------------------------------------------------------------------
    * @Description method that gets previous arce
    *--------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-13-01
    * @param    recordId - Id of the client
    * @param    isorphan - boolean
    * @param    orphanNumber - numbert of the orphan
    * @return   String
    * @example  setanalysis(recordId)
    * -------------------------------------------------------------------------------
    */
    public static Arc_Gen_NewAnalysis_Service.AnalysisResponse setanalysis(String recordId, Boolean isorphan, String orphanNumber, String accounts) {
        Boolean boolOrphan = isorphan == null ? false : isorphan;
        Arc_Gen_NewAnalysis_Service.AnalysisResponse response =  new Arc_Gen_NewAnalysis_Service.AnalysisResponse();
        List<arce__Analysis__c> newArceAnalysis =  new List<arce__Analysis__c>();
        final List<Id> accIds = new List<Id>();
        final List<arce__Account_has_Analysis__c> accHas2CreateLts = new List<arce__Account_has_Analysis__c>();
        final List < arce__rating_variables_detail__c > ratingVariablesLts = new List <arce__rating_variables_detail__c> ();
        if (boolOrphan) {
            final Map<String, Arc_Gen_Account_Wrapper> orphan = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{orphanNumber});
            final Id analisysRecordTypeId = Arc_Gen_NewAnalysis_data.getRecordTypes('arce__Analysis__c');
            final List<arce__Analysis__c> arceLts = Arc_Gen_ArceAnalysis_Data.getArcesFromAccounts(new List<Id>{orphan.get(orphanNumber).accId});
            if (arceLts.isEmpty()) {
                newArceAnalysis = createArceAnalysis(orphan.get(orphanNumber), analisysRecordTypeId, boolOrphan);
                List<arce__Financial_Statements__c> emptyFFSS = Arc_Gen_FinancialStatements_locator.getEmptyFFSS();
                accHas2CreateLts.add(buildAccHasAnalysis(newArceAnalysis[0].Id, orphan.get(orphanNumber), emptyFFSS, 'Orphan'));
                Arc_Gen_AccHasAnalysis_Data.upsertObjects(accHas2CreateLts);
                ratingVariablesLts.add(Arc_Gen_Rating_Var_detail_Data.setEmptyRatingVariable(accHas2CreateLts[0].Id));
                Arc_Gen_Rating_Var_detail_Data.insertRatingVariables(ratingVariablesLts);
                response.status = 'NUEVO';
                response.analysisId = newArceAnalysis[0].Id;
                Arc_Gen_NewAnalysis_data.updateArceName(newArceAnalysis[0].Id);
            }
        } else {
            final List<Arc_Gen_Account_Wrapper> accountsWrapLts = (List<Arc_Gen_Account_Wrapper>) JSON.deserialize(accounts, List<Arc_Gen_Account_Wrapper>.Class);
            final Arc_Gen_Account_Wrapper groupWrapper = accountsWrapLts[accountsWrapLts.size() - 1];
            final Id analisysRecordTypeId = Arc_Gen_NewAnalysis_data.getRecordTypes('arce__Analysis__c');
            for (integer i = 0; i < accountsWrapLts.size() - 1; i++) {
                accIds.add(accountsWrapLts[i].accId);
            }
            final List<arce__Analysis__c> arceLts = Arc_Gen_ArceAnalysis_Data.getArcesFromAccounts(accIds);
            if (arceLts.isEmpty()) {
                newArceAnalysis = createArceAnalysis(groupWrapper, analisysRecordTypeId, boolOrphan);
                List<arce__Financial_Statements__c> emptyFFSS = Arc_Gen_FinancialStatements_locator.getEmptyFFSS();
                for (integer i = 0; i < accountsWrapLts.size() - 1; i++) {
                    accHas2CreateLts.add(buildAccHasAnalysis(newArceAnalysis[0].Id, accountsWrapLts[i], emptyFFSS, 'subsidiary'));
                }
                accHas2CreateLts.add(buildAccHasAnalysis(newArceAnalysis[0].Id, groupWrapper, emptyFFSS,'Group'));
                Arc_Gen_AccHasAnalysis_Data.upsertObjects(accHas2CreateLts);
                for (arce__Account_has_Analysis__c accHas: accHas2CreateLts) {
                    ratingVariablesLts.add(Arc_Gen_Rating_Var_detail_Data.setEmptyRatingVariable(accHas.Id));
                }
                Arc_Gen_Rating_Var_detail_Data.insertRatingVariables(ratingVariablesLts);
                response.status = 'NUEVO';
                response.analysisId = newArceAnalysis[0].Id;
                Arc_Gen_NewAnalysis_data.updateArceName(newArceAnalysis[0].Id);
                user currentUser = Arc_Gen_NewAnalysis_data.currentuser();
                Arc_Gen_Traceability.saveEvent(response.analysisId, Label.Arc_Gen_Traceability_01, 'approve', Label.Arc_Gen_ExecRepStg + ' : ' + Label.Arc_Gen_Stage_01 + ' | ' + Label.Arc_Gen_TraceabilityState + ' : ' + Label.Arc_Gen_Traceability_01 + ' | ' + Label.Arc_Gen_TraceabilityUserCode + ' : ' + currentUser.Name.toUpperCase() + ' | ' + Label.Arc_Gen_TraceabilitySubprocess + ' : ' + Label.Arc_Gen_TraceabilityNewAnalysis);
            }
        }
        return response;
    }
    /**
    *-------------------------------------------------------------------------------
    * @Description method that organize the customer data in wrapper class
    *-------------------------------------------------------------------------------
    * @Date 12/09/2019
    * @Author luisarturo.parra.contractor@bbva.com
    * @param Account firstaccountanalized
    * @param client type group or customer
    * @return List<arce__Analysis__c> type class
    * @example private SaveResponse createarcefromcustomer(accData, analisysRecordTypeId, isorphan)
    * ------------------------------------------------------------------------------
    */
    public static List<arce__Analysis__c> createArceAnalysis(Arc_Gen_Account_Wrapper accData, Id analisysRecordTypeId, Boolean isorphan) {
        final boolean noPermissionCreate = !customerAllocation(accData.accId);
        if (noPermissionCreate) {
            NewAnalysisException excp = new NewAnalysisException();
            excp.setMessage(System.Label.Customer_Allocation_Error_1);
            throw excp;
        }
        arce__Analysis__c newAnalysis = new arce__Analysis__c();
        newAnalysis.Name = accData.name;
        newAnalysis.arce__Group__c = accData.accId;
        newAnalysis.arce__Stage__c = '1';
        newAnalysis.arce__wf_status_id__c = '01';
        newAnalysis.arce__analysis_customer_relation_type__c = isorphan == true ? '02' : '01';
        newAnalysis.arce__anlys_wkfl_sub_process_type__c = '2';
        newAnalysis.RecordTypeId = analisysRecordTypeId;
        newAnalysis.OwnerId = userinfo.getuserId();
        return Arc_Gen_ArceAnalysis_Data.insertArce(new List<arce__Analysis__c>{newAnalysis});
    }
    /**
    *-------------------------------------------------------------------------------
    * @description method that organize the customer data in wrapper class
    *-------------------------------------------------------------------------------
    * @Date 12/09/2019
    * @Author luisarturo.parra.contractor@bbva.com
    * @param Account firstaccountanalized
    * @param client type group or customer
    * @return arce__Account_has_Analysis__c type class
    * @example buildAccHasAnalysis()
    * ------------------------------------------------------------------------------
    */
    public static arce__Account_has_Analysis__c buildAccHasAnalysis(Id analysisId, Arc_Gen_Account_Wrapper accData, List<arce__Financial_Statements__c> emptyFFSS, String clientType) {
        final arce__Account_has_Analysis__c newAccHasAn = new arce__Account_has_Analysis__c();
        newAccHasAn.arce__Analysis__c = analysisId;
        newAccHasAn.arce__Customer__c = accData.accId;
        newAccHasAn.arce__InReview__c = clientType == 'Group' || clientType == 'Orphan' ? true : false;
        newAccHasAn.arce__anlys_wkfl_sbanlys_status_type__c = '1';
        newAccHasAn.arce__sector_rt_type__c = 's-01 ';
        newAccHasAn.arce__group_asset_header_type__c = clientType == 'Group' ? '1' : '2';
        newAccHasAn.arce__ffss_for_rating_id__c = emptyFFSS.isEmpty() ? Arc_Gen_FinancialStatements_locator.setFFSS().Id : emptyFFSS[0].Id;
        return newAccHasAn;

    }
    /**
    *-------------------------------------------------------------------------------
    * @description Method that calls customer allocation service and validates the permission
    *-------------------------------------------------------------------------------
    * @date 09/09/2019
    * @author antonio.munoz.perez.contractor@bbva.com
    * @param String customerId - Customer Id
    * @return Boolean - result of the customer allocation validations
    * @example public static Boolean customerAllocation(Id customerId)
    */
    public static Boolean customerAllocation(Id customerId) {
        final List<ID> records = new List<ID>();
        records.add(customerId);
        final Type inter = Type.forName('arcealloc.Allocation_Service');
        Return String.isNotBlank(String.valueOf(inter)) ? ((Map<Id,boolean>) ((Callable) inter.newInstance()).call('checkBulkCreationPrivilegesFromAcc', new Map<String, Object> {'accIdsLst' => records})).get(customerId) : true ;
    }
}