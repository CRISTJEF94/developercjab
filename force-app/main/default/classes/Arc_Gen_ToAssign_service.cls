/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_ToAssign_service
* @Author   Angel Fuertes Gomez - angel.fuertes2@bbva.com / BBVA DWP
* @Date     Created: 2019-05-04
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Class used for the button to Assign in the stage of Sanction.
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-05-04 angel.fuertes2@bbva.com
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_ToAssign_service {
    /**
        * @Description: String with the dev name of the recordtype
    */
    static final string TASK_DEV_NAME = 'Arc_Gen_ARCE_Task';
    /**
    *-------------------------------------------------------------------------------
    * @description Empty priavate constructor
    --------------------------------------------------------------------------------
    * @author eduardoefrain.hernandez.contractor@bbva.com
    * @date 2019-09-25
    * @example private Arc_Gen_ToAssign_service ()
    **/
    @TestVisible
    private Arc_Gen_ToAssign_service () {
    }
    /**
    * ---------------------------------------------------------------------------------------------------
    * @Description - Method to find users
    * ---------------------------------------------------------------------------------------------------
    * @Author   Angel Fuertes Gomez - angel.fuertes2@bbva.com / BBVA DWP
    * @Date     Created: 2019-05-04
    * @param inputTerm - String to find users
    * @return a list of map with the users
    * @example searchUser(inputTerm)
    * ---------------------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static List<map<String,String>> searchUser(String inputTerm){
        List<map<String,String>> ltsUsers = new list<map<String,String>>();
        List<Arc_Gen_User_Wrapper> lstWrap = Arc_Gen_User_Locator.getUsers(inputTerm);
        for(Arc_Gen_User_Wrapper wrap : lstWrap) {
            ltsUsers.add(new map<String,String>{'nameUser' => wrap.userBasicInfo.Name, 'perfilUser' => wrap.profileName, 'idUser' => wrap.userBasicInfo.Id});
        }
        return ltsUsers;
    }
    /**
    * ---------------------------------------------------------------------------------------------------
    * @Description - Method to assign the arce
    * ---------------------------------------------------------------------------------------------------
    * @Author   Angel Fuertes Gomez - angel.fuertes2@bbva.com / BBVA DWP
    * @Date     Created: 2019-05-04
    * @param userId - id of the user selected
    * @param recordId - id of the arce
    * @return a string
    * @example toAssign(userId, recordId)
    * ---------------------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static String toAssign(Id userId, Id recordId) {
        Arc_Gen_User_Wrapper wrpUser = Arc_Gen_User_Locator.getUserInfo(System.UserInfo.getUserId());
        arce__Analysis__c arceForTraceability = Arc_Gen_ArceAnalysis_Data.getArceForTraceability(recordId);
        arce__Analysis__c arce = Arc_Gen_ArceAnalysis_Data.gerArce(recordId);
        /*
            LOCAL CODE HERE
        */
        /*
            END OF THE LOCAL CODE
        */
        Arc_Gen_AccHasAnalysis_Data.upsertObjects(new List<arce__Analysis__c>{(arce__Analysis__c)arce});
        insert Arc_Gen_GenericUtilities.createTask(userId, arce.Id, System.Label.Arc_Gen_ArcePropose, TASK_DEV_NAME);
        Arc_Gen_Traceability.saveEvent(arce.Id, arceForTraceability.arce__wf_status_id__c, System.Label.Arc_Gen_TraceabilitySendBack, System.Label.Arc_Gen_ExecRepStg + ' : ' + arceForTraceability.arce__Stage__c + ' | ' + System.Label.Arc_Gen_TraceabilityState + ' : ' + arceForTraceability.arce__anlys_wkfl_status_stage_type__c + ' | ' + System.Label.Arc_Gen_TraceabilityUserCode + ' : ' + system.UserInfo.getName() + ' | ' + System.Label.Arc_Gen_TraceabilityUserProfile + ' : ' + wrpUser.profileName + ' | ' + System.Label.Arc_Gen_TraceabilityAmbit + ' : ' + arceForTraceability.arce__anlys_wkfl_edit_br_level_type__c + System.Label.Arc_Gen_TraceabilityOffice + ' : ' + wrpUser.branchText);
        return System.Label.Arc_Gen_AssignedARCESuccess;
    }
}