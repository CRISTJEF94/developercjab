/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_BusinessRisk_Pers_service
* @Author   Javier Soto Carrascosa
* @Date     Created: 04/042020
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Class that manages save for Basic Data
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |2020-04-04 Javier Soto Carrascosa
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
@SuppressWarnings('sf:TooManyMethods')
global class Arc_Gen_BusinessRisk_Pers_service implements dyfr.Save_Interface {
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that is responsible for invoking the classes to save the
      basic data information.
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param listObject - List of the account has analisys object
    * @return String with the execution message
    * @example save (listObject)
    * -----------------------------------------------------------------------------------------------
    **/
    public static String save(List<sObject> listObject) {
        final arce__Account_has_Analysis__c ahaData = Arc_Gen_Persistence_Utils.getAhaFromSobject(listObject);
        Map<String, Object> finalBusinessRiskMap = new Map<String, Object>();
        final arce__Account_has_Analysis__c accHasAnalysis = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<String>{(String)ahaData.Id})[0];
        final Map<String, Object> businessRiskInfoMap = businessRiskJSON(ahaData, accHasAnalysis);
        finalBusinessRiskMap = Arc_Gen_Persistence_Utils.addifFilled(finalBusinessRiskMap,'businessInformation',businessRiskInfoMap);
        final Map<Id, Arc_Gen_Account_Wrapper> listacc = Arc_Gen_Account_Locator.getAccountInfoById(new List<String>{accHasAnalysis.arce__Customer__c});
        final String participantId = Arc_Gen_CallEncryptService.getEncryptedClient(listacc.get(accHasAnalysis.arce__Customer__c).accNumber);
        final boolean businessRisk = Arc_Gen_OraclePers_service.businessRiskWS(participantId, accHasAnalysis.Name, finalBusinessRiskMap);
        return JSON.serialize(new Arc_Gen_wrpSave(businessRisk,'',listObject));
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that generates JSON for basic-info WS
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param string - key
    * @return Map<String, Object> with WS structure for Basic Info WS
    * @example basicInfoJSON(ahaData)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> businessRiskJSON (arce__Account_has_Analysis__c ahaData, arce__Account_has_Analysis__c prevAha) {
        Map<String, Object> wsObject = new Map<String, Object>();
        Map<String, String> singleMapAPIName = new Map<String, String>{'geographicScopeType'=>'arce__conc_geography_activity_type__c','exportSalesPercentage'=>'arce__export_range_year_type__c','importSalesPercentage'=>'arce__import_range_year_type__c'};
        wsObject = generateSingleMap(ahaData, singleMapAPIName, wsObject);
        wsObject = mapGeoActBool(ahaData, 'geography', wsObject);
        wsObject = mapGeoActBool(ahaData, 'activity', wsObject);
        final Map<String, Object> ecoActivity = ecoActivity(ahaData);
        wsObject = Arc_Gen_Persistence_Utils.addifFilled(wsObject, 'economicActivity', ecoActivity);
        //final Map<String, Object> marketShare = marketShare(ahaData, prevAha);
        //wsObject = Arc_Gen_Persistence_Utils.addifFilled(wsObject, 'marketShare', marketShare);
        final Map<String, Object> diversification = diversifExchange(ahaData, prevAha, 'diversification');
        wsObject = Arc_Gen_Persistence_Utils.addifFilled(wsObject, 'diversification', diversification);
        final Map<String, Object> exchangeRate = diversifExchange(ahaData, prevAha, 'exchangeRateVulnerability');
        wsObject = Arc_Gen_Persistence_Utils.addifFilled(wsObject, 'exchangeRateVulnerability', exchangeRate);
        final Map<String, Object> relevantFacts = relevantFacts(ahaData);
        wsObject = Arc_Gen_Persistence_Utils.addifFilled(wsObject, 'relevantFacts', relevantFacts);
        final List<Map<String, Object>> thirdParties = thirdParties(ahaData, prevAha);
        wsObject = Arc_Gen_Persistence_Utils.addifFilledList(wsObject, 'thirdPartiesDependencies', thirdParties);
        return wsObject;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that generates single Map or default value if emput
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param sMap<String, String> - singleMapAPIName
    * @param Map<String, Object> - wsObject
    * @return Map<String, Object> with WS structure for Basic Info WS
    * @example addSingleMap(ahaData,singleMapAPIName,wsObject)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> generateSingleMap (arce__Account_has_Analysis__c ahaData,Map<String, String> singleMapAPIName, Map<String, Object> wsObject) {
        for (string element : singleMapAPIName.keySet()) {
            wsObject.put(element, Arc_Gen_Persistence_Utils.defaultValueList((String)ahaData.get(singleMapAPIName.get(element))));
        }
        return wsObject;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that generates single Map or default value if emput
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param string - geoOrAct
    * @param Map<String, Object> - wsObject
    * @return Map<String, Object> with WS structure
    * @example mapGeoActBool(ahaData,geoOrAct,wsObject)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String,Object> mapGeoActBool (arce__Account_has_Analysis__c ahaData, String geoOrAct, Map<String, Object> wsObject) {
        Map<String,String> geoMap = new Map<String,String>{'isGlobalBusiness'=>'arce__customer_multi_country_type__c','comments'=>'arce__breakdown_geography_desc__c'};
        Map<String,String> actMap = new Map<String,String>{'hasMultiLineBusiness'=>'arce__cust_multi_business_line_type__c','comments'=>'arce__breakdown_activity_desc__c'};
        Map<String,String> currentMap = geoOrAct == 'geography' ? geoMap : actMap;
        Map<String,Object> insideMap = new Map<String,Object>();
        for (string element : currentMap.keySet()) {
            if (element == 'comments') {
                insideMap.put(element,ahaData.get(currentMap.get(element)));
            } else {
                insideMap.put(element,Arc_Gen_Persistence_Utils.booleanFromYesNo((String)ahaData.get(currentMap.get(element))));
            }
        }
        wsObject.put(geoOrAct,(Object)insideMap);
        return wsObject;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that generates map for economicactivity
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param Map<String, Object> - wsObject
    * @return Map<String, Object> with WS structure
    * @example ecoActivity(ahaData,wsObject)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> ecoActivity (arce__Account_has_Analysis__c ahaData) {
        Map<String, Object> ecoMap = new Map<String, Object>();
        if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get('arce__economic_activity_sector_desc__c'))) {
            ecoMap = new Map<String,Object>{'description'=>ahaData.get('arce__economic_activity_sector_desc__c')};
        }
        return ecoMap;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that generates map for market share
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param Map<String, Object> - wsObject
    * @return Map<String, Object> with WS structure
    * @example ecoActivity(ahaData,wsObject)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> marketShare (arce__Account_has_Analysis__c ahaData, arce__Account_has_Analysis__c prevAha) {
        Map<String, Object> shareMap = new Map<String, Object>();
        if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get('arce__market_share_type__c')) || Arc_Gen_ValidateInfo_utils.isFilled((String)prevAha.get('arce__market_share_type__c'))) {
            shareMap = new Map<String,Object>{'id'=>Arc_Gen_Persistence_Utils.defaultValueList((String)ahaData.get('arce__market_share_type__c')),'comments'=>ahaData.get('arce__gf_market_share_desc__c')};
        }
        return shareMap;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that generates map diversification and exchange rating vulnerability
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param Map<String, Object> - wsObject
    * @param String - key
    * @return Map<String, Object> with WS structure
    * @example ecoActivity(ahaData,wsObject,key)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> diversifExchange (arce__Account_has_Analysis__c ahaData, arce__Account_has_Analysis__c prevAha, String key) {
        Map<String,String> diversification = new Map<String,String>{'diversificationType'=>'arce__dvrsfn_product_service_type__c','description'=>'arce__dvrsfn_product_service_desc__c'};
        Map<String,String> exchangeRate = new Map<String,String>{'vulnerabilityType'=>'arce__exch_rate_vulnerability_type__c','description'=>'arce__exch_rate_vulnerability_desc__c'};
        Map<String,String> currentMap = key == 'diversification' ? diversification : exchangeRate;
        Map<String,Object> insideMap = new Map<String,Object>();
        boolean generateMap = false;
        for (string element : currentMap.keySet()) {
            if (element == 'description') {
                insideMap.put(element,ahaData.get(currentMap.get(element)));
            } else if(Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get(currentMap.get(element))) || Arc_Gen_ValidateInfo_utils.isFilled((String)prevAha.get(currentMap.get(element)))) {
                insideMap.put(element,Arc_Gen_Persistence_Utils.defaultValueList((String)ahaData.get(currentMap.get(element))));
                generateMap = true;
            }
        }
        return generateMap == true ? insideMap : new Map<String, Object>();
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that generates map for relevant facs
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param Map<String, Object> - wsObject
    * @return Map<String, Object> with WS structure
    * @example relevantFacts(ahaData,wsObject)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> relevantFacts (arce__Account_has_Analysis__c ahaData) {
        Map<String, Object> relevantFactsMap = new Map<String, Object>();
        if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get('arce__new_business_venture_type__c')) && Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get('arce__new_business_venture_desc__c'))) {
            relevantFactsMap = new Map<String,Object>{'hasSignificant'=>(Object)Arc_Gen_Persistence_Utils.booleanFromYesNo((String)ahaData.get('arce__new_business_venture_type__c')), 'description' => ahaData.get('arce__new_business_venture_desc__c')};
        }
        return relevantFactsMap;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that generates map for relevant facs
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param Map<String, Object> - wsObject
    * @return Map<String, Object> with WS structure
    * @example relevantFacts(ahaData,wsObject)
    * -----------------------------------------------------------------------------------------------
    **/
    private static List<Map<String,Object>> thirdParties (arce__Account_has_Analysis__c ahaData, arce__Account_has_Analysis__c prevAha) {
        List<Map<String,Object>> listThirdParties = new List<Map<String,Object>>();
        List<arce__Third_Participant_Details__c> listadoSupAndClients = new List<arce__Third_Participant_Details__c>();
        if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get('arce__concentration_suppliers_type__c')) || Arc_Gen_ValidateInfo_utils.isFilled((String)prevAha.get('arce__concentration_suppliers_type__c'))) {
            listadoSupAndClients = Arc_Gen_ThirdParticipantDetails_Data.getRecordsbyArcAcc('Main_suppliers', new List<String>{(String)ahaData.Id});
            listThirdParties.addAll(processThirdParties(listadoSupAndClients,'SUPPLIER',ahaData));
        }
        if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get('arce__cust_conc_clients_type__c')) || Arc_Gen_ValidateInfo_utils.isFilled((String)prevAha.get('arce__cust_conc_clients_type__c'))) {
            listadoSupAndClients = Arc_Gen_ThirdParticipantDetails_Data.getRecordsbyArcAcc('Main_clients', new List<String>{(String)ahaData.Id});
            listThirdParties.addAll(processThirdParties(listadoSupAndClients,'CUSTOMER',ahaData));
        }
        return listThirdParties;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that generates map for relevant facs
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param Map<String, Object> - wsObject
    * @return Map<String, Object> with WS structure
    * @example relevantFacts(ahaData,wsObject)
    * -----------------------------------------------------------------------------------------------
    **/
    private static List<Map<String,Object>> processThirdParties (List<arce__Third_Participant_Details__c> listThird, String key, arce__Account_has_Analysis__c ahaData) {
        List<Map<String,Object>> listThirdParties = new List<Map<String,Object>>();
        final String value = key == 'SUPPLIER' ? (String)ahaData.get('arce__concentration_suppliers_type__c') : (String)ahaData.get('arce__cust_conc_clients_type__c');
        if (listThird.isEmpty()) {
            final Map<String,Object> fakeTpart = generateFakeThird(key, value, ahaData);
            listThirdParties.add(fakeTpart);
        } else {
            for(arce__Third_Participant_Details__c thirdPart : listThird) {
                Map<String,Object> tPart = new Map<String,Object>();
                tpart.put('id',thirdPart.get('Id'));
                tpart.put('thirdPartyDependencyType',key);
                tpart.put('dependencyDegree',value);
                tpart.put('name',thirdPart.get('arce__third_participant_desc__c'));
                tpart.put('percentage',thirdPart.get('arce__third_participant_per__c'));
                listThirdParties.add(tPart);
            }
        }
        return listThirdParties;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that generates map for relevant facs
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param Map<String, Object> - wsObject
    * @return Map<String, Object> with WS structure
    * @example relevantFacts(ahaData,wsObject)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> generateFakeThird(String key, String value, arce__Account_has_Analysis__c ahaData) {
        Map<String,Object> tPart = new Map<String,Object>();
        tpart.put('id', ahaData.GET('Id'));
        tpart.put('thirdPartyDependencyType',(Object)key);
        tpart.put('dependencyDegree',(Object)value);
        tpart.put('name',(Object)'PLACEHOLDER NAME');
        tpart.put('percentage',(Object)0);
        return tPart;
    }
}