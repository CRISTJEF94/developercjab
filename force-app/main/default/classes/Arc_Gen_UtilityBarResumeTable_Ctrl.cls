/**
* @File Name          : Arc_Gen_UtilityBarResumeTable_Ctrl.cls
* @Description        : table for customer politics resume
* @Author             :luisarturo.parra.contractor@bbva.com
* @Group              : ARCE
* @Last Modified By   : luisarturo.parra.contractor@bbva.com
* @Last Modified On   :  23/12/2019 05:17
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author                 Modification
*==============================================================================
* 1.0    23/12/2019          luisarturo.parra.contractor@bbva.com     Initial Version
**/
public class Arc_Gen_UtilityBarResumeTable_Ctrl {
    /**private constructor for sonar**/
    private Arc_Gen_UtilityBarResumeTable_Ctrl(){}
/**
*-------------------------------------------------------------------------------
* @description get typologies for the table
*-------------------------------------------------------------------------------
* @date 23/12/2019
* @author luisarturo.parra.contractor@bbva.com
* @param none
* @return   List<arce__limits_typology__c>
* @example public static List<arce__limits_typology__c> getDataTypologies() {
*/
    @AuraEnabled
    public static List<arce__limits_typology__c> getDataTypologies() {
        return Arc_Gen_LimitsTypologies_Data.getTypologiesData();
    }
/**
*-------------------------------------------------------------------------------
* @description get data for the table
*-------------------------------------------------------------------------------
* @date 23/12/2019
* @author luisarturo.parra.contractor@bbva.com
* @param recordId analysisId of the account has analysis
* @return   List<String>
* @example public static List<String> getTableData(Id recordId) {
*/
    @AuraEnabled
    public static List<List<String>> getTableData(Id recordId) {
      return Arc_Gen_UtilityBarResumeTable_Service.getTableData(recordId);
    }
}