/**
* @File Name          : Arc_Gen_RatingVariablesTable_controller.cls
* @Description        : Controller that obtains the response of the rating variables table from the service
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE Group
* @Last Modified By   : eduardoefrain.hernandez.contractor@bbva.com
* @Last Modified On   : 30/4/2019 18:18:26
* @Changes
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    30/4/2019 17:29:51   eduardoefrain.hernandez.contractor@bbva.com     Initial Version
* 1.1    26/04/2020 17:29:51   javier.soto.carrascosar@bbva.com     Add missing response
**/
public without sharing class Arc_Gen_RatingVariablesTable_controller {
    /**
    * @Description: Error Literal
    */
    private final static String ERROR_TEXT = 'Error';
    /**
    * @Description: Error Literal
    */
    private final static String SUCCESS_TEXT = 'Success';
/**
* @Class: TableResponse
* @Description: Wrapper that contains the response of the table logic
* @author BBVA
*/
    public Class TableResponse {
        /**
        * @Description: Status of the process
        */
        @AuraEnabled public String status {get;set;}
        /**
        * @Description: Json to configure the table
        */
        @AuraEnabled public String tableJson {get;set;}
        /**
        * @Description: Json to configure the table
        */
        @AuraEnabled public String errorMessage {get;set;}
    }
/**
*-------------------------------------------------------------------------------
* @description Method that obtains the data of the rating variables table
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId
* @return String
* @example public static String getDataTable(String analysisId)
**/
    @AuraEnabled
    public static TableResponse getDataTable(String ratingId) {
        TableResponse response = new TableResponse();
        try {
            List<arce__rating_variables_detail__c> variablesLevelOne = Arc_Gen_RatingVariablesTable_service.getVariablesLevelOne(ratingId);
            List<arce__rating_variables_detail__c> variablesLevelTwo = Arc_Gen_RatingVariablesTable_service.getVariablesLevelTwo(ratingId);
            response.tableJson = Arc_Gen_RatingVariablesTable_service.setTableJson(variablesLevelOne,variablesLevelTwo);
            response.status = SUCCESS_TEXT;
        } catch(QueryException e) {
            response.status = ERROR_TEXT;
            response.errorMessage = e.getMessage();
        }
        Return response;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that obtains the data of the rating qualitative variable table
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId
* @return String
* @example public static String getQualitativeTable(String analysisId)
**/
    @AuraEnabled
    public static TableResponse getQualitativeTable(String ratingId) {
        TableResponse response = new TableResponse();
        try {
            String dataTable = Arc_Gen_RatingVariablesTable_service.setQualitativeVariable(ratingId);
            response.tableJson = dataTable;
            response.status = SUCCESS_TEXT;
        } catch(QueryException e) {
            response.status = ERROR_TEXT;
            response.errorMessage = e.getMessage();
        }
        Return response;
    }
}