/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Workflow_Service
* @Author   Juan Ignacio Hita Manso juanignacio.hita.contractor@bbva.com
* @Date     Created: 2019-07-01
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Controller APEX
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-12-03 juanignacio.hita.contractor@bbva.com
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
public class Arc_Gen_Workflow_Service {
    /**
    *-------------------------------------------------------------------------------
    * @description function that find a class that implements the Arc_Gen_Workflow interface with the "WorkflowConfig" configuration metadata
    *--------------------------------------------------------------------------------
    * @date		07/01/2019
    * @author	juanignacio.hita.contractor@bbva.com
    * @return   Arc_Gen_Workflow_Interface
    * @example	Arc_Gen_Workflow_Services.workflowClass();
    */
    public static Arc_Gen_Workflow_Interface workflowClass() {
        List<Arce_Config__mdt> configMetadata = Arc_Gen_Arceconfigs_locator.getConfigurationInfo('WorkflowConfig');
        Arc_Gen_Workflow_Interface workFlowClass;
        if (!configMetadata.isEmpty()) {
            final System.Type objType = Type.forName(configMetadata.get(0).Value1__c);
            try {
                workFlowClass = (Arc_Gen_Workflow_Interface) objType.newInstance();
            } catch (Exception ex) {
                throw new QueryException(ex);
            }
        }
        return workFlowClass;
    }

}