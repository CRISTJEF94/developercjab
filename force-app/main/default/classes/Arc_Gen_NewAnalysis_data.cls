/**
* @File Name          : Arc_Gen_NewAnalysis_data.cls
* @Description        : Data class to NewAnalysis
* @Author             : luisarturo.parra.contractor@bbva.com
* @Group              : ARCE
* @Last Modified By   : luisarturo.parra.contractor@bbva.com
* @Last Modified On   : 4/7/2019 11:35:48
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author                 Modification
*==============================================================================
* 1.0    30/04/2019           eduardoefrain.hernandez.contractor@bbva.com     Initial Version
* 1.1    02/04/2019           diego.miguel.contractor@bbva.com                Added logic to redirect to valid ARCE
* 1.2    03/05/2019           diego.miguel.contractor@bbva.com                Added functions to save ARCE name AND subtype
* 1.3    09/05/2019           diego.miguel.contractor@bbva.com                Added functions to redirect acording to ARCE satatus AND update status
* 1.4    14/05/2019           diego.miguel.contractor@bbva.com                Added methods to groups ws support
* 1.5    04/11/2019           manuelhugo.castillo.contractor@bbva.com         Fix query stage ARCE
* 1.6    14/11/2019           mariohumberto.ramirez.contractor@bbva.com       Fix analysis type assigment
* 1.7    02/12/2019           luisarturo.parra.contractor@bbva.com            Refactorizacion
* 1.8    04/12/2019           manuelhugo.castillo.contractor@bbva.com         Modify methods 'getUniqueCustomerDatacl','getClientsByGroup','checkifhasgroup' replace Account to AccountWrapper
**/
public without sharing class Arc_Gen_NewAnalysis_data {
  /**
  *-------------------------------------------------------------------------------
  * @description Principal method that gets customer data
  *-------------------------------------------------------------------------------
  * @date 12/09/2019
  * @author luisarturo.parra.contractor@bbva.com
  * @param recordId- customer id
  * @return Arc_Gen_Account_Wrapper
  * @example public static  Account getUniqueCustomerDatacl (String recordId)
  */
  public static Arc_Gen_Account_Wrapper getUniqueCustomerDatacl(String recordId) {
    List<Id> lstIds = new List<Id>{Id.valueOf(recordId)};
    Map<Id, Arc_Gen_Account_Wrapper> accWrap = Arc_Gen_Account_Locator.getAccountInfoById(lstIds);
    return accWrap.get(Id.valueOf(recordId));
  }
  /**
  *-------------------------------------------------------------------------------
  * @description methot to get recordtype info
  *-------------------------------------------------------------------------------
  * @date 12/09/2019
  * @author luisarturo.parra.contractor@bbva.com
  * @param sObjectType
  * @return AnalysisResponse type class
  * @example  public static  Id getRecordTypes(String sObjectType)
  */
  public static Id getRecordTypes(String sObjectType) {
    Id analysisType = [SELECT Id, Name FROM RecordType WHERE sObjectType = : sObjectType limit 1].Id;
    Return analysisType;
  }
  /**
  *-------------------------------------------------------------------------------
  * @description method that updates arce name
  *-------------------------------------------------------------------------------
  * @date 12/09/2019
  * @author luisarturo.parra.contractor@bbva.com
  * @param recordId- customer id
  * @return void
  * @example  public static  void updateArceName(String arceId)
  */
  public static void updateArceName(String arceId) {
    String groupAnalysisId = '';
    arce__Analysis__c newAnalysis = new arce__Analysis__c();
    newAnalysis = [SELECT Id, name, arce__parent_analysis_id__c, toLabel(arce__anlys_wkfl_sub_process_type__c) FROM arce__Analysis__c WHERE id = : arceId];
    if (newAnalysis != null) {
      String maxGroupName = newAnalysis.name.length() > 44 ? newAnalysis.name.substring(0, 43) : newAnalysis.name;
      newAnalysis.name = newAnalysis.arce__anlys_wkfl_sub_process_type__c + ' - ' + maxGroupName + ' - ' + newAnalysis.arce__parent_analysis_id__c;
      update newAnalysis;
    }
  }
  /**
  *-------------------------------------------------------------------------------
  * @description currentuser method
  *-------------------------------------------------------------------------------
  * @date 12/09/2019
  * @author luisarturo.parra.contractor@bbva.com
  * @return currentuser
  * @example public static  user  currentuser()
  */
  public static user currentuser() {
    user currentUser = [select id, Name from user where id = : system.UserInfo.getUserId()];
    return currentUser;
  }
  /**
  *-------------------------------------------------------------------------------
  * @description set empty rating
  *-------------------------------------------------------------------------------
  * @date 12/09/2019
  * @author luisarturo.parra.contractor@bbva.com
  * @param account_has_analysis_id
  * @example public static  arce__rating_variables_detail__c setEmptyRatingVariable(String analysisId)
  */
  public static void upsertObjects(List < sObject > ltsObjects) {
    if (!ltsObjects.isEmpty()) {
      upsert ltsObjects;
    }
  }
}