/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Validate_ContentMaturity_service
* @Author   Javier Soto Carrascosa
* @Date     Created: 2019-11-03
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Class for related records table manager validations
* ------------------------------------------------------------------------------------------------
* |2019-11-03 javier.soto.carrascosa@bbva.com
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
global class Arc_Gen_ValidateContentMaturity_service implements rrtm.RelatedManager_Interface {
    /**
    * --------------------------------------------------------------------------------------
    * @Description Wrapper class
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hita.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * --------------------------------------------------------------------------------------
    **/
    class Wrapper extends rrtm.RelatedRecord_WrapperValidation {}
    /**
    *-------------------------------------------------------------------------------
    * @description Method that validates completeness of the info
    --------------------------------------------------------------------------------
    * @author javier.soto.carrascosa@bbva.com
    * @date 2019-11-13
    * @param Map mapObj with the record information, Wrapper with validation result and messages, Integer ite with line number
    * @return Wrapper with validation result and messages updated
    * @example public static Wrapper validateInfo(Map<String, Object> mapObj, Wrapper strucWrapper, Integer ite)
    **/
    private static Wrapper validateInfo(Map<String, Object> mapObj, Wrapper strucWrapper, Integer ite) {
        String amount = String.valueOf(mapObj.get('arce__table_content_value__c'));
        String year = (String) mapObj.get('arce__table_content_year__c');
        String seeker = String.valueOf(mapObj.get('Seeker'));
        if(!Arc_Gen_ValidateInfo_utils.isFilled(amount)) {
            strucWrapper.msgInfo = strucWrapper.msgInfo + string.format(Label.Arc_Gen_CompleteField_Amount, new List<String>{String.valueOf(ite)});
            strucWrapper.validation = false;
        }
        if(!Arc_Gen_ValidateInfo_utils.isFilled(year)) {
            strucWrapper.msgInfo = strucWrapper.msgInfo + string.format(Label.Arc_Gen_CompleteField_Year, new List<String>{String.valueOf(ite)});
            strucWrapper.validation = false;
        }
        if(!Arc_Gen_ValidateInfo_utils.isFilled(seeker)) {
            strucWrapper.msgInfo = strucWrapper.msgInfo + string.format(Label.Arc_Gen_SeekerMandatory, new List<String>{String.valueOf(ite)});
            strucWrapper.validation = false;
        }
        return strucWrapper;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description description Method that validates the info coming from maturity table
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hita.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param    List<Object> - lstRecords with records to validate
    * @return   JSON String with validation result and messages updated
    * @example static String beforeSaveData(List<Object> lstRecords)
    * --------------------------------------------------------------------------------------
    **/
    public static String beforeSaveData(List<Object> lstRecords) {
        Wrapper wrapper = new Wrapper();
        wrapper.validation = true;
        wrapper.msgInfo = '';

        if(!lstRecords.isEmpty()) {
            Integer ite = 0;
            for (Object obj : lstRecords) {
                String strJson = JSON.serialize(obj);
                Map<String, Object> mapObj = (Map<String, Object>)JSON.deserializeUntyped(strJson);
                ite++;
                wrapper = validateInfo(mapObj,wrapper,ite);
            }
        }
        return JSON.serialize(wrapper);
    }
}