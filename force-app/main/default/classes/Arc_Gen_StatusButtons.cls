/**
* @File Name          : Arc_Gen_StatusButtons.cls
* @Description        : Used to retrieve buttons status
* @Author             : ARCE Team
* @Group              : ARCE
* @Last Modified By   : luisruben.quinto.munoz@bbva.com
* @Last Modified On   : 12/7/2019 19:04:18
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    7/5/2019 23:57:32   diego.miguel.contractor@bbva.com     Added comments
* 1.1    27/01/2020 16:42:00   javier.soto.carrascosa@bbva.com     Add support for arce allocation
* 1.2    29/04/2020 08:42:00   javier.soto.carrascosa@bbva.com     Fix arce allocation parameter
**/
global class Arc_Gen_StatusButtons implements dwp_dbpn.DynamicButtonsStatusInterface {
    /**
    *-------------------------------------------------------------------------------
    * @description Controls the availability of the given button
    * with the selectedSector
    *--------------------------------------------------------------------------------
    * @date 7/5/2019
    * @author ARCE Team
    * @param String recordId
    * @param String sObjectName
    * @return Set<String>
    */
    global Set<String> getDynamicButtonStatus(String recordId, String sObjectName) {
        Set<String> buttonsStatusSet = new Set<String>();
        final List<Id> lstIds = new List<Id>();
        lstIds.add(recordId);
        final Type inter = Type.forName('arcealloc.Allocation_Service');
        final boolean custAlloc = String.isNotBlank(String.valueOf(inter)) ? ((Map<Id,boolean>) ((Callable) inter.newInstance()).call('checkBulkPrivileges', new Map<String, Object> {'accHasAnlysIdsLst' => lstIds})).get(recordId) : true ;
        if (custAlloc) {
            final Map<String,Set<String>> actionsPerOppIdMap = dwp_dace.DynamicActionsEngine_helper.getAvailableActions(lstIds,sObjectName,'Arc_Gen_Buttons');
            if ( actionsPerOppIdMap.get(recordId) != null) {
            buttonsStatusSet.addAll(actionsPerOppIdMap.get(recordId));
            }
        }
        return buttonsStatusSet;
    }
}