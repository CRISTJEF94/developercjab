/**
* @File Name          : Arc_Gen_CallEconomicParticipants.cls
* @Description        : Contain logic to process Groups ASO WS (getEconomicParticipations)
* @Author             : Luis Arturo Parra Rosas / luisarturo.parra.contractor@bbva.com
* @Group              : ARCE Team
* @Data Class         : Arc_Gen_Groups_data
* @Main ctr Class     : Arc_Gen_Groups_Service
* @Test Class         : Arc_Gen_Groups_controller_test
* @Last Modified By   : luisarturo.parra.contractor@bbva.com
* @Last Modified On   : 30/08/2019
* @Modification Log   :
*==========================================================================================
* Ver         Date                     Author                            Modification
*==========================================================================================
* 1.0         30/08/2019                luisarturo.parra.contractor@bbva.com     Initial Version
* 1.0         08/02/2019                ricardo.almanza.contractor@bbva.com     added config for 204 for orphan test
****************************************************************************************************************/
@SuppressWarnings('PMD.ExcessivePublicCount')
public without sharing class Arc_Gen_CallEconomicParticipants {
    public class Businessdataobj{
        public String businessDocumentNumber{get; set;}
        public String businessDocTypeId {get; set;}
        public String businessDocTypeName {get; set;}
    }
    public class Groupdata{
        public String groupname {get; set;}
        public String groupid {get; set;}
        public String decryptedgroupid {get; set;}
    }
    public class Businessdatacontainer{
        public String businessId{get; set;}
        public String businessLegalName {get; set;}
        public List<Businessdataobj> bussineslistdocuments {get; set;}
    }
    public class Innertoreturn{
        public String error204message {get; set;}
        public String errormessage {get; set;}
        public String servicecallerror {get; set;}
        public Businessdatacontainer business {get; set;}
        public Groupdata groupinfo {get; set;}
        public Boolean isorphan {get; set;}
    }
    /**
    *-------------------------------------------------------------------------------
    * @description calls getAnalysis and return data needed to visualizate current stage:
    * serviceStatus - OK/KO
    * message - empty / message to be displayed
    * retToPreparing - OK/KO
    *--------------------------------------------------------------------------------
    * @date     20/06/2019
    * @author   diego.miguel.contractor@bbva.com
    * @param    recordId - account_has_analysis Id
    * @return   Arc_Gen_RefreshClass_service.refreshMessagesResponse
    * @example  public static Arc_Gen_CallListParticipant.Innertoreturnlistp callListParticipants(String encryptedGroup) {
    */
    public static Arc_Gen_CallEconomicParticipants.Innertoreturn callEconomicParticipations(String encryptedClient) {
        Innertoreturn returndata = new Innertoreturn();
        final Arc_Gen_CustomServiceMessages serviceMessages = new Arc_Gen_CustomServiceMessages();
        String servicecallerror ,responsewarningdescription204 ,errormessage;
        String params = '{"customerId":"'+encryptedClient+'"}';// params to economic participant service
        String[] businessdata, groupdata, s204, ecodes;
        final Boolean getOrphanAnsStructure = Arc_Gen_GenericUtilities.getOrphanAnsStructure();
        Arc_Gen_getIASOResponse.serviceResponse sResponse = new Arc_Gen_getIASOResponse.serviceResponse();
        try {
            sResponse = callEconomicParticipation(params);//iaso call
            system.debug('sResponse economicp'+sResponse);
            if(getOrphanAnsStructure) {
                sResponse.serviceMessage = serviceMessages.SUCCESSFUL_204;
            }
        } catch (Exception e) {
            returndata.servicecallerror =  System.Label.Cls_arce_GRP_servError + '. ' + e.getMessage();
            return returndata;
        }
        if(sResponse.serviceMessage == serviceMessages.SUCCESSFUL_200) {
            Map<String, Object> data = getMapFromJson(sResponse.serviceResponse, 'data');
            Map<String, Object> business = getMapFromJson(data, 'business');
            returndata.business = getbusinnessdatafromListPart(business);
            List<Map<String, Object>> groupsList = getListFromJson(data, 'groups');
            returndata.groupinfo = getgroupIDandNamefromListPart(groupsList);
        } else if(sResponse.serviceMessage == serviceMessages.SUCCESSFUL_204) {
            responsewarningdescription204 = sResponse.serviceHeaders.get('responsewarningdescription');
            returndata.error204message = responsewarningdescription204;
            Groupdata groupinformation = NEW Groupdata();
            groupinformation.groupid = encryptedClient;
            returndata.groupinfo = groupinformation;
            returndata.isorphan = true;
            // We cannot pass through this lines on test because Mock always return 200 code.
        } else {
            // Devolver mensaje de ERROR HTTP
            // We cannot pass through this lines on test because Mock always return 200 code.
            returndata.errormessage =  System.Label.Cls_arce_GRP_servError + ': ' + sResponse.serviceCode + ' ' + sResponse.serviceMessage;
        }
        return returndata;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description calls getAnalysis and return data needed to visualizate current stage:
    * serviceStatus - OK/KO
    * message - empty / message to be displayed
    * retToPreparing - OK/KO
    *--------------------------------------------------------------------------------
    * @date     20/06/2019
    * @author   diego.miguel.contractor@bbva.com
    * @param    recordId - account_has_analysis Id
    * @return   Arc_Gen_RefreshClass_service.refreshMessagesResponse
    * @example  public static Arc_Gen_CallListParticipant.Innertoreturnlistp callListParticipants(String encryptedGroup) {
    */
    private static Businessdatacontainer getbusinnessdatafromListPart (Map<String, Object> business){
        Businessdatacontainer businessreturn =  new Businessdatacontainer() ;
        businessreturn.businessId = (String)business.get('id');
        businessreturn.businessLegalName = (String)business.get('legalName');
        List<Map<String, Object>> businessDocumentsList = getListFromJson(business, 'businessDocuments');
        businessreturn.bussineslistdocuments = new List<Businessdataobj>();
        for (Map<String, Object> businessDoc : businessDocumentsList) {
            Businessdataobj docs = new Businessdataobj();
            docs.businessDocumentNumber = (String)businessDoc.get('documentNumber');
            Map<String, Object> businessDocumentType = getMapFromJson(businessDoc, 'businessDocumentType');
            docs.businessDocTypeId = (String)businessDoc.get('id');
            docs.businessDocTypeName = (String)businessDoc.get('name');
            businessreturn.bussineslistdocuments.add(docs);
        }
        return businessreturn;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description calls getAnalysis and return data needed to visualizate current stage:
    * serviceStatus - OK/KO
    * message - empty / message to be displayed
    * retToPreparing - OK/KO
    *--------------------------------------------------------------------------------
    * @date     20/06/2019
    * @author   diego.miguel.contractor@bbva.com
    * @param    recordId - account_has_analysis Id
    * @return   Arc_Gen_RefreshClass_service.refreshMessagesResponse
    * @example  public static Arc_Gen_CallListParticipant.Innertoreturnlistp callListParticipants(String encryptedGroup) {
    */
    private static Groupdata getgroupIDandNamefromListPart (List<Map<String, Object>> groupsList){
        Groupdata groupinformation = NEW Groupdata();
        groupinformation.groupid = (String)groupsList[0].get('id');
        groupinformation.groupname = (String)groupsList[0].get('name'); // NAME OF THE GROUP RETRIEVED BY ECONOMIC PARTICIPANTS "GROUP TO WHICH IT BELONGS"
        return groupinformation;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description calls getAnalysis and return data needed to visualizate current stage:
    * serviceStatus - OK/KO
    * message - empty / message to be displayed
    * retToPreparing - OK/KO
    *--------------------------------------------------------------------------------
    * @date     20/06/2019
    * @author   diego.miguel.contractor@bbva.com
    * @param    recordId - account_has_analysis Id
    * @return   Arc_Gen_RefreshClass_service.refreshMessagesResponse
    * @example  public static Arc_Gen_CallListParticipant.Innertoreturnlistp callListParticipants(String encryptedGroup) {
    */
    private static Map<String, Object> getMapFromJson(Map<String, Object> prevMap, String keyToRetrieve) {
        Map<String, Object> data = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(prevMap.get(keyToRetrieve)));
        if (data == null) {
            data = new Map<String, Object>();
        }
        return data;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description calls getAnalysis and return data needed to visualizate current stage:
    * serviceStatus - OK/KO
    * message - empty / message to be displayed
    * retToPreparing - OK/KO
    *--------------------------------------------------------------------------------
    * @date     20/06/2019
    * @author   diego.miguel.contractor@bbva.com
    * @param    recordId - account_has_analysis Id
    * @return   Arc_Gen_RefreshClass_service.refreshMessagesResponse
    * @example  public static Arc_Gen_CallListParticipant.Innertoreturnlistp callListParticipants(String encryptedGroup) {
    */
    private static List<Map<String, Object>> getListFromJson(Map<String, Object> prevMap, String keyToRetrieve) {
        List<Map<String, Object>> listOfMap = new List<Map<String, Object>>();
        List<Object> objectList = (List<Object>)prevMap.get(keyToRetrieve);
        for(Object element : objectList) {
            listOfMap.add((Map<String, Object>)element);
        }
        return listOfMap;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description calls getAnalysis and return data needed to visualizate current stage:
    * serviceStatus - OK/KO
    * message - empty / message to be displayed
    * retToPreparing - OK/KO
    *--------------------------------------------------------------------------------
    * @date     20/06/2019
    * @author   diego.miguel.contractor@bbva.com
    * @param    recordId - account_has_analysis Id
    * @return   Arc_Gen_RefreshClass_service.refreshMessagesResponse
    * @example  public static Arc_Gen_CallListParticipant.Innertoreturnlistp callListParticipants(String encryptedGroup) {
    */
    private static Arc_Gen_getIASOResponse.serviceResponse callEconomicParticipation(String params) {
        // ratios service name (CMT). Mock must be set to 'retrieve mock = true' in order to pass test clases
        String serviceName = 'economicParticipations';
        return callEngine(serviceName, params);
    }
    /**
    *-------------------------------------------------------------------------------
    * @description calls getAnalysis and return data needed to visualizate current stage:
    * serviceStatus - OK/KO
    * message - empty / message to be displayed
    * retToPreparing - OK/KO
    *--------------------------------------------------------------------------------
    * @date     20/06/2019
    * @author   diego.miguel.contractor@bbva.com
    * @param    recordId - account_has_analysis Id
    * @return   Arc_Gen_RefreshClass_service.refreshMessagesResponse
    * @example  public static Arc_Gen_CallListParticipant.Innertoreturnlistp callListParticipants(String encryptedGroup) {
    */
    private static Arc_Gen_getIASOResponse.serviceResponse callEngine(String serviceName, String params) {
        return Arc_Gen_getIASOResponse.calloutIASO(serviceName,params);
    }
}