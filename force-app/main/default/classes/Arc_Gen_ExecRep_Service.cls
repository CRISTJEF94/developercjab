/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_ExecRep_Service
* @Author   Ricardo Almanza Angeles  ricardo.almanza.contractor@bbva.com
* @Date     Created: 2019-06-20
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Service for Executive Summary visualforce
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-06-20 ricardo.almanza.contractor@bbva.com
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_ExecRep_Service {
    /*------------------------------------------------------------------------------------------------------
    *@Description Builder Arc_Gen_ExecRep_Service
    * -----------------------------------------------------------------------------------------------------
    * @Author   Ricardo Almanza
    * @Date     2019-07-03
    * @param    null
    * @return   Arc_Gen_ExecRep_Service
    * @example  new Arc_Gen_ExecRep_Service()
    * */
    @TestVisible
    private Arc_Gen_ExecRep_Service() {}
    /*------------------------------------------------------------------------------------------------------
    *@Description Builder Page that control to build Executive Summary
    * -----------------------------------------------------------------------------------------------------
    * @Author   Ricardo Almanza
    * @Date     2019-06-20
    * @param    Arc_Gen_ExecRepCtrl.WrapPDF wrappObj Objeto wrapper para generar pdf
    * @return    Arc_Gen_ExecRepCtrl.WrapPDF wrappObj Objeto wrapper para generar pdf
    * @example  Arc_Gen_ExecRepCtrl.WrapPDF GenPDF(Arc_Gen_ExecRepCtrl.WrapPDF wrappObj)
    * */
    public static Arc_Gen_ExecRepCtrl.WrapPDF genPDF(Arc_Gen_ExecRepCtrl.WrapPDF wrappObj) {
        final Id rid= wrappObj.rid;
        Arc_Gen_ExecRepCtrl.WrapPDF newWrappObj = wrappObj;
        newWrappObj = Arc_Gen_ExecRep_Service_Helper.simpleQrys(rid,newWrappObj);
        newWrappObj = Arc_Gen_ExecRep_Service_Helper.createDataTipology(rid,newWrappObj);
        newWrappObj = Arc_Gen_ExecRep_Service_Helper.createFnHighlights(rid,newWrappObj);
        newWrappObj = Arc_Gen_ExecRep_Service_Helper.createFFSS(rid,newWrappObj);
        return newWrappObj;
    }
}