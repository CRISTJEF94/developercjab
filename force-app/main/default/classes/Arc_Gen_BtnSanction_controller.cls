/*-------------------------------------------------------------------------------------------------------------------------------------------------
*Author:        Angel Fuertes Gomez - angel.fuertes2@bbva.com / BBVA DWP
*Project:      	ARCE - BBVA Bancomer
*Description:   Class used for the button to Sanction.
*__________________________________________________________________________________________________________________________________________________
*Version    Date           Author                               Description
*1.0        30/04/2019     Angel Fuertes Gomez                  Creation.
*1.1        03/01/2020     Mario H. Ramirez Lio                 Fix comments, added new param (typeMessage) to the wrapper WrapperBtnSanction
---------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class Arc_Gen_BtnSanction_controller {
    /**
    *-------------------------------------------------------------------------------
    * @description initDelegation
    *--------------------------------------------------------------------------------
    * @date   09/01/2020
    * @author juanignacio.hita.contractor@bbva.com
    * @param  String : ambit
    * @param  Id : accHasId
    * @param  String : action
    * @return String
    * @example  String res = initDelegation(ambit, accHasId, action)
    */
    @AuraEnabled
    public static String initDelegation(Id accHasAnalysisId) {
      try {
        final arce__Analysis__c arce = Arc_Gen_ArceAnalysis_Data.gerArce(accHasAnalysisId);
        final Arc_Gen_User_Wrapper wrpUser = Arc_Gen_User_Locator.getUserInfo(System.UserInfo.getUserId());
        final Arc_Gen_Delegation_Wrapper wrapper = Arc_Gen_Propose_Helper.initDelegation(wrpUser.ambitUser, arce.Id, 'SANCTION');
        return JSON.serialize(wrapper);
      } catch (Exception e) {
        throw new AuraHandledException(System.Label.Arc_Gen_ApexCallError + e);
      }
    }
    /**
    * ------------------------------------------------------------------------------
    * @Description: Save the reason for the sanction
    * ------------------------------------------------------------------------------
    * @param:       Id record id (arce__Analysis__c)
    * @return:      WrapperBtnSanction
    * @example getValuesScopeOfSanction(recordId)
    * ------------------------------------------------------------------------------
    */
    @AuraEnabled
    public static String saveScopeOfSanction(Object wrapper, Map<string,string> data) {
        final Arc_Gen_Delegation_Wrapper wrapperSerialize = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) wrapper, Arc_Gen_Delegation_Wrapper.class);
        final Arc_Gen_Delegation_Wrapper wrapperRet = Arc_Gen_BtnSanction_service.saveScopeOfSanction(wrapperSerialize, data);
        return JSON.serialize(wrapperRet);
    }
    /**
    *-------------------------------------------------------------------------------
    * @Description: Method to search users
    *-------------------------------------------------------------------------------
    * @param:  inputTerm string to search
    * returns: List<map<String,String>>
    * @example searchUser(inputTerm)
    * ------------------------------------------------------------------------------
    */
    @AuraEnabled
    public static List<map<String,String>> searchUser(String inputTerm){
        return Arc_Gen_BtnSanction_service.searchUser(inputTerm);
    }
}