/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Auto_Position_Service
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2020-01-28
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Helper class for Arc_Gen_PotitionBankTable_Ctlr and Arc_Gen_Auto_Position_Service
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2020-01-28 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* |2020-03-31 mariohumberto.ramirez.contractor@bbva.com
*             fix dml operation call before limit service call
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_Auto_Position_Service_Helper {
    /**
        * @Description: External Id of TOTAL CREDIT RISK
    */
    static final String TCR_EXT_ID = Arc_Gen_LimitsTypologies_Data.getTypologiesByDevName('TP_0006').arce__risk_typo_ext_id__c;
    /**
        * @Description: Api Name for Client
    */
    static final String CLIENT = '2';
    /**
        * @Description: Name of the table last row
    */
    static final String TOTAL = 'TOTAL';
    /**
        * @Description: Name of the table last row
    */
    static final String UNIT = 'UNIT';
    /**
        * @Description: param to call limits service
    */
    static final string S_GROUP = 'GROUP';
    /**
        * @Description: param to call limits service
    */
    static final string SUBSIDIARY = 'SUBSIDIARY';
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-01-28
    * @param void
    * @return void
    * @example Arc_Gen_Auto_Position_Service_Helper helper = new Arc_Gen_Auto_Position_Service_Helper()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_Auto_Position_Service_Helper() {

    }
    public class WrapperAux {
        /**
        * @Description: List of Arc_Gen_TableRow wrapper
        */
        List<Arc_Gen_TableRow> tableRowLts {get;set;}
        /**
        * @Description: List of arce__risk_position_summary__c object
        */
        List<arce__risk_position_summary__c> summaryDataGroup {get;set;}
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return the row information of the table
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-01-28
    * @param recordId -  Id of the account has analysis object
    * @return row information of the table
    * @example getColumns()
    * --------------------------------------------------------------------------------------
    **/
    public static List<Arc_Gen_TableRow> getRows(Id recordId) {
        final String typoOfCustomer = Arc_Gen_GenericUtilities.getTypeOfCustomer(recordId) == 'Group' ? S_GROUP : SUBSIDIARY;
        return typoOfCustomer == 'GROUP' ? getRowsGroup(recordId, typoOfCustomer, 'limits') : getRowsFilial(recordId, typoOfCustomer, 'limits');
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return the row information of the table
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-24
    * @param recordId -  Id of the account has analysis object
    * @return row information of the table
    * @example getRowsFilial()
    * --------------------------------------------------------------------------------------
    **/
    public static List<Arc_Gen_TableRow> getRowsFilial(Id recordId, String typoOfCustomer, String serviceName) {
        List<Arc_Gen_TableRow> rowList = new List<Arc_Gen_TableRow>();
        final Id accId = Arc_Gen_AccHasAnalysis_Data.getAccHasRelation(recordId).arce__customer__c;
        final Map<Id, Arc_Gen_Account_Wrapper> accWrapper = Arc_Gen_Account_Locator.getAccountInfoById(new List<Id>{accId});
        final Map<String,Arc_Gen_Limits_Service.LimitsResponse> limitRespMap = Arc_Gen_Limits_Service.callLimitsService(typoOfCustomer,accWrapper.get(accId).accNumber, serviceName);
        if (limitRespMap.containsKey('ERROR')) {
            throw new QueryException(Label.serviceFailure + ' ' + limitRespMap.get('ERROR').gblCodeResponse);
        }
        final Map<Id, Arc_Gen_Product_Wrapper> prodWrapper = Arc_Gen_Product_Locator.getProductsActive();
        final List<String> extProdIdLts = new List<String>(limitRespMap.keySet());
        final List<arce__risk_position_summary__c> riskPosSumAllDataLts = Arc_Gen_Risk_Position_summary_Data.getPositionSummaryData(new List<Id>{recordId});
        if (Arc_Gen_Risk_Position_summary_Data.getPositionSummaryByProd(new List<Id>{recordId}, extProdIdLts).isEmpty()) {
            rowList = insertRecordsFilial(prodWrapper, limitRespMap, recordId, typoOfCustomer);
        } else {
            rowList = getRecordsFilial(riskPosSumAllDataLts, typoOfCustomer, limitRespMap);
        }
        return rowList;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return the row information of the table
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-01-28
    * @param recordId -  Id of the account has analysis object
    * @return row information of the table
    * @example getRowsGroup(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static List<Arc_Gen_TableRow> getRecordsFilial(List<arce__risk_position_summary__c> riskPosSumAllDataLts, String typoOfCustomer, Map<String,Arc_Gen_Limits_Service.LimitsResponse> limitRespMap) {
        final List<Arc_Gen_TableRow> rowList = new List<Arc_Gen_TableRow>();
        final Arc_Gen_TableRow rowGroup = new Arc_Gen_TableRow();
        for (arce__risk_position_summary__c riskPosSumData: riskPosSumAllDataLts) {
            if (riskPosSumData.arce__Product_id__c == null) {
                riskPosSumData.arce__banrel_current_limit_name__c = limitRespMap.get(TCR_EXT_ID).currentLimit;
                riskPosSumData.arce__banrel_commitment_name__c = limitRespMap.get(TCR_EXT_ID).commited;
                riskPosSumData.arce__banrel_uncommitment_name__c = limitRespMap.get(TCR_EXT_ID).uncommited;
                riskPosSumData.arce__banrel_outstanding_name__c = limitRespMap.get(TCR_EXT_ID).outstanding;
                Map<String,String> info = new Map<String,String>();
                info.put('clientType', typoOfCustomer);
                info.put('automatic', 'true');
                rowGroup.name = riskPosSumData.Id;
                rowGroup.recordTypeId = riskPosSumData.recordTypeId;
                rowGroup.product = TOTAL;
                rowGroup.currentLimit = limitRespMap.get(TCR_EXT_ID).currentLimit;
                rowGroup.commited = limitRespMap.get(TCR_EXT_ID).commited;
                rowGroup.uncommited = limitRespMap.get(TCR_EXT_ID).uncommited;
                rowGroup.outstanding = limitRespMap.get(TCR_EXT_ID).outstanding;
                rowGroup.comments = riskPosSumData.arce__banrel_comments_desc__c;
                rowGroup.currencyType = limitRespMap.get(TCR_EXT_ID).currencyType;
                rowGroup.unit = UNIT;
                rowGroup.info = info;
            } else {
                Map<String,String> info = new Map<String,String>();
                Arc_Gen_TableRow row = new Arc_Gen_TableRow();
                info.put('clientType', typoOfCustomer);
                info.put('automatic', 'true');
                riskPosSumData.arce__banrel_current_limit_name__c = limitRespMap.get(riskPosSumData.arce__Product_id__r.ExternalId).currentLimit;
                riskPosSumData.arce__banrel_commitment_name__c = limitRespMap.get(riskPosSumData.arce__Product_id__r.ExternalId).commited;
                riskPosSumData.arce__banrel_uncommitment_name__c = limitRespMap.get(riskPosSumData.arce__Product_id__r.ExternalId).uncommited;
                riskPosSumData.arce__banrel_outstanding_name__c = limitRespMap.get(riskPosSumData.arce__Product_id__r.ExternalId).outstanding;
                row.name = riskPosSumData.Id;
                row.recordTypeId = riskPosSumData.recordTypeId;
                row.product = riskPosSumData.arce__Product_id__r.Name;
                row.currentLimit = limitRespMap.get(riskPosSumData.arce__Product_id__r.ExternalId).currentLimit;
                row.commited = limitRespMap.get(riskPosSumData.arce__Product_id__r.ExternalId).commited;
                row.uncommited = limitRespMap.get(riskPosSumData.arce__Product_id__r.ExternalId).uncommited;
                row.outstanding = limitRespMap.get(riskPosSumData.arce__Product_id__r.ExternalId).outstanding;
                row.comments = riskPosSumData.arce__banrel_comments_desc__c;
                row.currencyType = limitRespMap.get(riskPosSumData.arce__Product_id__r.ExternalId).currencyType;
                row.unit = UNIT;
                row.info = info;
                rowList.add(row);
            }
        }
        rowList.add(rowGroup);
        Arc_Gen_Risk_Position_summary_Data.updateRecords(riskPosSumAllDataLts);
        return rowList;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return the row information of the table
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-01-28
    * @param recordId -  Id of the account has analysis object
    * @param typoOfCustomer -  group/subsidiary
    * @param serviceName -  name of the service to consult
    * @return row information of the table
    * @example getRowsGroup(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static List<Arc_Gen_TableRow> getRowsGroup(Id recordId, String typoOfCustomer, String serviceName) {
        WrapperAux wrapperAux = new WrapperAux();
        final List<Id> accIds = new List<Id>();
        final Id groupId;
        List<Arc_Gen_TableRow> rowLts =  new List<Arc_Gen_TableRow>();
        integer flag = 0;
        final List<arce__risk_position_summary__c> riskPositionsummaryDataGroup = new List<arce__risk_position_summary__c>();
        final Id recordTypeId = Arc_Gen_Risk_Position_summary_Data.getRecordTypeRiskPositionSum('Comments');
        final arce__Account_has_Analysis__c accHasRel = Arc_Gen_AccHasAnalysis_Data.getAccHasRelation(recordId);
        final List<arce__Account_has_Analysis__c> accHasLts = Arc_Gen_AccHasAnalysis_Data.getAccHasAnFromArce(accHasRel.arce__Analysis__c);
        for (arce__Account_has_Analysis__c accHas: accHasLts) {
            if (accHas.arce__group_asset_header_type__c == CLIENT) {
                accIds.add(accHas.arce__customer__c);
            } else {
                groupId = accHas.arce__customer__c;
            }
        }
        final Map<Id, Arc_Gen_Account_Wrapper> mapWrapper = Arc_Gen_Account_Locator.getAccountInfoById(accIds);
        final Map<Id, Arc_Gen_Account_Wrapper> mapGroupWrapper = Arc_Gen_Account_Locator.getAccountInfoById(new List<Id>{groupId});
        final List<arce__risk_position_summary__c> riskPositionDataLts = Arc_Gen_Risk_Position_summary_Data.getPositionSummaryByAccount(new List<Id>{recordId}, accIds);
        final List<arce__risk_position_summary__c> riskPositionGroup = Arc_Gen_Risk_Position_summary_Data.getPositionSummaryByAccount(new List<Id>{recordId}, new List<Id>{groupId});
        if (riskPositionDataLts.isEmpty()) {
            wrapperAux = insertNewRecords(mapWrapper, recordTypeId, recordId, typoOfCustomer);
            rowLts = wrapperAux.tableRowLts;
        } else {
            flag = 1;
            for (arce__risk_position_summary__c riskSumData: riskPositionDataLts) {
                Map<String,String> info = new Map<String,String>();
                info.put('clientType', typoOfCustomer);
                info.put('automatic', 'true');
                Arc_Gen_TableRow row = new Arc_Gen_TableRow();
                riskSumData.arce__banrel_current_limit_name__c = mapWrapper.get(riskSumData.arce__account_Id__c).currentLimit;
                riskSumData.arce__banrel_commitment_name__c = mapWrapper.get(riskSumData.arce__account_Id__c).commited;
                riskSumData.arce__banrel_uncommitment_name__c = mapWrapper.get(riskSumData.arce__account_Id__c).unCommited;
                riskSumData.arce__banrel_outstanding_name__c = mapWrapper.get(riskSumData.arce__account_Id__c).outstanding;
                riskPositionsummaryDataGroup.add(riskSumData);
                row.name = riskSumData.Id;
                row.recordTypeId = riskSumData.recordTypeId;
                row.product = riskSumData.arce__account_Id__r.Name;
                row.currentLimit = mapWrapper.get(riskSumData.arce__account_Id__c).currentLimit;
                row.commited = mapWrapper.get(riskSumData.arce__account_Id__c).commited;
                row.uncommited = mapWrapper.get(riskSumData.arce__account_Id__c).unCommited;
                row.outstanding = mapWrapper.get(riskSumData.arce__account_Id__c).outstanding;
                row.comments = riskSumData.arce__banrel_comments_desc__c;
                row.currencyType = mapWrapper.get(riskSumData.arce__account_Id__c).currencyType;
                row.unit = mapWrapper.get(riskSumData.arce__account_Id__c).unit;
                row.info = info;
                rowLts.add(row);
            }
        }
        rowLts.add(getRowTotalInfo(new List<String>{typoOfCustomer, serviceName}, mapGroupWrapper, groupId, riskPositionGroup));
        if (flag == 1) {
            Arc_Gen_AccHasAnalysis_Data.upsertObjects(riskPositionsummaryDataGroup);
        } else {
            Arc_Gen_AccHasAnalysis_Data.upsertObjects(wrapperAux.summaryDataGroup);
        }
        return rowLts;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return the row information for position table
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-01-28
    * @param params -  service name and typeOfCustomer values
    * @param mapGroupWrapper -  Account wrapper
    * @param groupId -  Id group
    * @param riskPositionGroup -  position summary data related to group
    * @return row information of the table
    * @example getRowTotalInfo(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static Arc_Gen_TableRow getRowTotalInfo(List<String> params, Map<Id, Arc_Gen_Account_Wrapper> mapGroupWrapper, Id groupId, List<arce__risk_position_summary__c> riskPositionGroup) {
        final Arc_Gen_TableRow row = new Arc_Gen_TableRow();
        final Map<String,Arc_Gen_Limits_Service.LimitsResponse> limitRespMap = Arc_Gen_Limits_Service.callLimitsService(params[0],mapGroupWrapper.get(groupId).accNumber, params[1]);
        if (limitRespMap.containsKey('ERROR')) {
            throw new QueryException(Label.serviceFailure + ' ' + limitRespMap.get('ERROR').gblCodeResponse);
        }
        for (String limitId: limitRespMap.keySet()) {
            if (limitId == TCR_EXT_ID) {
                Map<String,String> info = new Map<String,String>();
                info.put('clientType', params[0]);
                info.put('automatic', 'true');
                riskPositionGroup[0].arce__banrel_current_limit_name__c = limitRespMap.get(limitId).currentLimit;
                riskPositionGroup[0].arce__banrel_commitment_name__c = limitRespMap.get(limitId).commited;
                riskPositionGroup[0].arce__banrel_uncommitment_name__c = limitRespMap.get(limitId).uncommited;
                riskPositionGroup[0].arce__banrel_outstanding_name__c = limitRespMap.get(limitId).outstanding;
                row.name = riskPositionGroup[0].Id;
                row.recordTypeId = riskPositionGroup[0].recordTypeId;
                row.product = TOTAL;
                row.currentLimit = limitRespMap.get(limitId).currentLimit;
                row.commited = limitRespMap.get(limitId).commited;
                row.uncommited = limitRespMap.get(limitId).uncommited;
                row.outstanding = limitRespMap.get(limitId).outstanding;
                row.comments = riskPositionGroup[0].arce__banrel_comments_desc__c;
                row.currencyType = limitRespMap.get(limitId).currencyType;
                row.unit = UNIT;
                row.info = info;
            }
        }
        Arc_Gen_AccHasAnalysis_Data.upsertObjects(riskPositionGroup);
        return row;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return the row information of the table
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-01-28
    * @param mapWrapper -  map of account wrapper
    * @param recordTypeId -  Id of the record type to associate
    * @param recordId -  Id of the account has analysis object
    * @param typoOfCustomer - Group/Client
    * @return WrapperAux - wrapper with the info to update and show in the position table
    * @example insertNewRecords(mapWrapper, recordTypeId, recordId, typoOfCustomer);
    * --------------------------------------------------------------------------------------
    **/
    private static WrapperAux insertNewRecords(Map<Id, Arc_Gen_Account_Wrapper> mapWrapper, Id recordTypeId, Id recordId, String typoOfCustomer) {
        WrapperAux wrapperRet = new WrapperAux();
        final List<arce__risk_position_summary__c> riskPositionsummaryDataGroup = new List<arce__risk_position_summary__c>();
        final List<Arc_Gen_TableRow> rowLts =  new List<Arc_Gen_TableRow>();
        for (Id accountId: mapWrapper.keySet()) {
            arce__risk_position_summary__c summaryData = new arce__risk_position_summary__c();
            summaryData.recordTypeId = recordTypeId;
            summaryData.arce__account_Id__c = accountId;
            summaryData.arce__banrel_current_limit_name__c = mapWrapper.get(accountId).currentLimit;
            summaryData.arce__banrel_commitment_name__c = mapWrapper.get(accountId).commited;
            summaryData.arce__banrel_uncommitment_name__c = mapWrapper.get(accountId).unCommited;
            summaryData.arce__banrel_outstanding_name__c = mapWrapper.get(accountId).outstanding;
            summaryData.arce__account_has_analysis_id__c = recordId;
            riskPositionsummaryDataGroup.add(summaryData);
        }
        wrapperRet.summaryDataGroup = riskPositionsummaryDataGroup;
        for (arce__risk_position_summary__c summaryData: riskPositionsummaryDataGroup) {
            Map<String,String> info = new Map<String,String>();
            info.put('clientType', typoOfCustomer);
            info.put('automatic', 'true');
            Arc_Gen_TableRow row = new Arc_Gen_TableRow();
            row.name = summaryData.Id;
            row.recordTypeId = summaryData.recordTypeId;
            row.product = mapWrapper.get(summaryData.arce__account_Id__c).name;
            row.currentLimit = summaryData.arce__banrel_current_limit_name__c;
            row.commited = summaryData.arce__banrel_commitment_name__c;
            row.uncommited = summaryData.arce__banrel_uncommitment_name__c;
            row.outstanding = summaryData.arce__banrel_outstanding_name__c;
            row.comments = summaryData.arce__banrel_comments_desc__c;
            row.info = info;
            row.unit = UNIT;
            rowLts.add(row);
        }
        wrapperRet.tableRowLts = rowLts;
        return wrapperRet;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return the row information of the table
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-01-28
    * @param mapWrapper -  map of account wrapper
    * @param limitRespMap -  Service response
    * @param recordId -  Id of the account has analysis object
    * @param typoOfCustomer - Group/Client
    * @return List<Arc_Gen_TableRow> - row information of the table
    * @example insertNewRecords(mapWrapper, recordTypeId, recordId, typoOfCustomer);
    * --------------------------------------------------------------------------------------
    **/
    private static List<Arc_Gen_TableRow> insertRecordsFilial(Map<Id, Arc_Gen_Product_Wrapper> prodWrapper, Map<String,Arc_Gen_Limits_Service.LimitsResponse> limitRespMap, Id recordId, String typoOfCustomer) {
        List<arce__risk_position_summary__c> riskPositionsummaryAuxLts = new List<arce__risk_position_summary__c>();
        final Id recordTypeId = Arc_Gen_Risk_Position_summary_Data.getRecordTypeRiskPositionSum('Comments');
        final Id accountId = Arc_Gen_AccHasAnalysis_Data.getAccHasRelation(recordId).arce__customer__c;
        for (id prodId: prodWrapper.keySet()) {
            if (limitRespMap.keySet().contains(prodWrapper.get(prodId).externalId)) {
                arce__risk_position_summary__c summaryProduct = new arce__risk_position_summary__c();
                summaryProduct.recordTypeId = recordTypeId;
                summaryProduct.arce__account_Id__c = accountId;
                summaryProduct.arce__banrel_current_limit_name__c = limitRespMap.get(prodWrapper.get(prodId).externalId).currentLimit;
                summaryProduct.arce__banrel_commitment_name__c = limitRespMap.get(prodWrapper.get(prodId).externalId).commited;
                summaryProduct.arce__banrel_uncommitment_name__c = limitRespMap.get(prodWrapper.get(prodId).externalId).uncommited;
                summaryProduct.arce__banrel_outstanding_name__c = limitRespMap.get(prodWrapper.get(prodId).externalId).outstanding;
                summaryProduct.arce__account_has_analysis_id__c = recordId;
                summaryProduct.arce__Product_id__c = prodId;
                riskPositionsummaryAuxLts.add(summaryProduct);
            }
        }
        Arc_Gen_Risk_Position_summary_Data.insertRecords(riskPositionsummaryAuxLts);
        return getRowInfo(recordId, limitRespMap, typoOfCustomer);
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return the row information of the table
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-01-28
    * @param recordId -  Id of the account has analysis object
    * @param limitRespMap -  Service response
    * @param typoOfCustomer - Group/Client
    * @return List<Arc_Gen_TableRow> - row information of the table
    * @example getRowInfo(recordId);
    * --------------------------------------------------------------------------------------
    **/
    public static List<Arc_Gen_TableRow> getRowInfo(Id recordId, Map<String,Arc_Gen_Limits_Service.LimitsResponse> limitRespMap, String typoOfCustomer) {
        final List<Arc_Gen_TableRow> rowList = new List<Arc_Gen_TableRow>();
        final Arc_Gen_TableRow rowGroup = new Arc_Gen_TableRow();
        final List<arce__risk_position_summary__c> riskPosSumAllDataLts = Arc_Gen_Risk_Position_summary_Data.getPositionSummaryData(new List<Id>{recordId});
        for (arce__risk_position_summary__c riskPosSum: riskPosSumAllDataLts) {
            if (riskPosSum.arce__Product_id__c == null) {
                riskPosSum.arce__banrel_current_limit_name__c = limitRespMap.get(TCR_EXT_ID).currentLimit;
                riskPosSum.arce__banrel_commitment_name__c = limitRespMap.get(TCR_EXT_ID).commited;
                riskPosSum.arce__banrel_uncommitment_name__c = limitRespMap.get(TCR_EXT_ID).uncommited;
                riskPosSum.arce__banrel_outstanding_name__c = limitRespMap.get(TCR_EXT_ID).outstanding;
                Map<String,String> info = new Map<String,String>();
                info.put('clientType', typoOfCustomer);
                info.put('automatic', 'true');
                rowGroup.name = riskPosSum.Id;
                rowGroup.recordTypeId = riskPosSum.recordTypeId;
                rowGroup.product = TOTAL;
                rowGroup.currentLimit = limitRespMap.get(TCR_EXT_ID).currentLimit;
                rowGroup.commited = limitRespMap.get(TCR_EXT_ID).commited;
                rowGroup.uncommited = limitRespMap.get(TCR_EXT_ID).uncommited;
                rowGroup.outstanding = limitRespMap.get(TCR_EXT_ID).outstanding;
                rowGroup.comments = riskPosSum.arce__banrel_comments_desc__c;
                rowGroup.currencyType = limitRespMap.get(TCR_EXT_ID).currencyType;
                rowGroup.unit = UNIT;
                rowGroup.info = info;
            } else {
                Map<String,String> info = new Map<String,String>();
                info.put('clientType', typoOfCustomer);
                info.put('automatic', 'true');
                Arc_Gen_TableRow row = new Arc_Gen_TableRow();
                row.name = riskPosSum.Id;
                row.recordTypeId = riskPosSum.recordTypeId;
                row.product = riskPosSum.arce__Product_id__r.Name;
                row.currentLimit = riskPosSum.arce__banrel_current_limit_name__c;
                row.commited = riskPosSum.arce__banrel_commitment_name__c;
                row.uncommited = riskPosSum.arce__banrel_uncommitment_name__c;
                row.outstanding = riskPosSum.arce__banrel_outstanding_name__c;
                row.comments = riskPosSum.arce__banrel_comments_desc__c;
                row.unit = UNIT;
                row.info = info;
                rowList.add(row);
            }
        }
        rowList.add(rowGroup);
        Arc_Gen_Risk_Position_summary_Data.updateRecords(riskPosSumAllDataLts);
        return rowList;
    }
}