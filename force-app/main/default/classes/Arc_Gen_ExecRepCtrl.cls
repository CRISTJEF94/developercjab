/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_ExecRepCtrl
* @Author   Ricardo Almanza Angeles  ricardo.almanza.contractor@bbva.com
* @Date     Created: 2019-06-10
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Class to obtain info in order to print exec Rep
* ------------------------------------------------------------------------------------------------
* @Changes
* |2020-01-24 luisruben.quinto.munoz@bbva.com
*             Added space end line
* -----------------------------------------------------------------------------------------------
*/
@SuppressWarnings('PMD.ExcessivePublicCount')
public with sharing class Arc_Gen_ExecRepCtrl {
    /**
    *   wrapper Financial Statements
    * @author Ricardo Almanza
    */
    public WrapPDF wrapIns {get;set;}

    /**
    * ------------------------------------------------------------------------------------------------
    * @Name     WrapPDF
    * @Author   Ricardo Almanza Angeles  ricardo.almanza.contractor@bbva.com
    * @Date     Created: 2019-06-10
    * @Group    ARCE
    * ------------------------------------------------------------------------------------------------
    * @Description Class to Wrap results between SOC
    * ------------------------------------------------------------------------------------------------
    * @Changes
    * -----------------------------------------------------------------------------------------------
    */
    @SuppressWarnings('PMD.ExcessivePublicCount')
    public class WrapPDF {
        /**
        *   record Id
        * @author Ricardo Almanza
        */
        Public Id rid {get;set;}
        /**
        *   Third Participant details
        * @author Ricardo Almanza
        */
        Public arce__Third_Participant_Details__c[] thrdPrtDtl {get;set;}
        /**
        *   main banks data
        * @author Ricardo Almanza
        */
        Public arce__main_banks__c[] mnBnks {get;set;}
        /**
        *   proposed amount
        * @author Ricardo Almanza
        */
        Public String crrentPropAmmt {get;set;}
        /**
        *   approved amount
        * @author Ricardo Almanza
        */
        Public String crrentApprvAmmt {get;set;}
        /**
        *   header table typologies
        * @author Ricardo Almanza
        */
        Public String[] headTipology {get;set;}
        /**
        *   header table modalities
        * @author Ricardo Almanza
        */
        Public String[] headMod {get;set;}
        /**
        *   header table financial highlights
        * @author Ricardo Almanza
        */
        Public String[] yearsFnHighlight {get;set;}
        /**
        *   data table typologies
        * @author Ricardo Almanza
        */
        Public List<List<String>> dataTipology {get;set;}
        /**
        *   data table Financial Highlights
        * @author Ricardo Almanza
        */
        Public List<List<String>> fnHighlights {get;set;}
        /**
        *   data table Modalities
        * @author Ricardo Almanza
        */
        Public List<List<String>> dataMod {get;set;}
        /**
        *   Object Financial Statements
        * @author Ricardo Almanza
        */
        Public WrappFFSS ffssObj {get;set;}
        /**
        * ------------------------------------------------------------------------------------------------
        * @Description Constructor of WrapPDF to set recordId of Account has analysis
        * ------------------------------------------------------------------------------------------------
        * @param Id recordId of Account has analysis
        * @return Constructed Obj WrapPDF
        * @example new WrapPDF(ApexPages.currentPage().getParameters().get('Id'))
        * ------------------------------------------------------------------------------------------------
        **/
        public WrapPDF(Id accHasId) {
            rid = accHasId;
        }
    }
    /**
    * ------------------------------------------------------------------------------------------------
    * @Name     WrappFFSS
    * @Author   Ricardo Almanza Angeles  ricardo.almanza.contractor@bbva.com
    * @Date     Created: 2019-07-15
    * @Group    ARCE
    * ------------------------------------------------------------------------------------------------
    * @Description Class to Wrap results for Financial Statements between SOC
    * ------------------------------------------------------------------------------------------------
    * @Changes
    * -----------------------------------------------------------------------------------------------
    */
    public class WrappFFSS {
        /**
        *   header table Balance 1
        * @author Ricardo Almanza
        */
        Public String[] yearsBalSh1 {get;set;}
        /**
        *   data table Balance 1
        * @author Ricardo Almanza
        */
        Public List<List<String>> balSh1 {get;set;}
        /**
        *   header table Balance 2
        * @author Ricardo Almanza
        */
        Public String[] yearsBalSh2 {get;set;}
        /**
        *   data table Balance 2
        * @author Ricardo Almanza
        */
        Public List<List<String>> balSh2 {get;set;}
        /**
        *   header table Income statement
        * @author Ricardo Almanza
        */
        Public String[] yearsIncmSt {get;set;}
        /**
        *   data table Income statement
        * @author Ricardo Almanza
        */
        Public List<List<String>> incmSt {get;set;}
        /**
        *   header table cash analysis
        * @author Ricardo Almanza
        */
        Public String[] yearsCshAn {get;set;}
        /**
        *   data table cash analysis
        * @author Ricardo Almanza
        */
        Public List<List<String>> cshAn {get;set;}
        /**
        *   header table ratios
        * @author Ricardo Almanza
        */
        Public String[] yearsRtios {get;set;}
        /**
        *   data table ratios
        * @author Ricardo Almanza
        */
        Public List<List<String>> rtios {get;set;}
        /**
        *   header table maturity
        * @author Ricardo Almanza
        */
        Public String[] yearsMatur {get;set;}
        /**
        *   data table maturity
        * @author Ricardo Almanza
        */
        Public List<List<String>> matur {get;set;}
    }
    /**
    * ------------------------------------------------------------------------------------------------
    * @Description Constructor of Arc_Gen_ExecRepCtrl to obtain data in order to print PDF
    * ------------------------------------------------------------------------------------------------
    * @param ApexPages.StandardController controller Control of Apex page to print VF
    * @return Constructed Obj Arc_Gen_ExecRepCtrl
    * @example Arc_Gen_ExecRepCtrl(ApexPages.StandardController controller)
    * ------------------------------------------------------------------------------------------------
    **/
    public Arc_Gen_ExecRepCtrl(ApexPages.StandardController controller) {
        WrapPDF wrappObj = new WrapPDF(Id.valueOf(controller.getRecord().Id));
        wrappObj = Arc_Gen_ExecRep_Service.genPDF(wrappObj);
        relateWrapperVars(wrappObj);
    }
    private void relateWrapperVars(WrapPDF wrappObj) {
        wrapIns = wrappObj;
    }
}