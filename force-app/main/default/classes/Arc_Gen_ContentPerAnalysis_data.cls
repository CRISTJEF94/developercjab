/*------------------------------------------------------------------
* @Project:   ARCE - BBVA Bancomer
* @Author: Angel Fuertes Gomez - angel.fuertes2@bbva.com / BBVA DWP
* @Date     Created: 12/03/2019
* @Group              : ARCE
* @Description:  data class for de group .Arc_Gen_ContentPerAnalysis
* @Changes :
* @Description:   test for method class Arc_Gen_ProposeInPreparation_controller.
*_______________________________________________________________________________________
*Version    Date           Author                               Description
*1.0        12/03/2019     Angel Fuertes Gomez                  	Creation.
-----------------------------------------------------------------------------------------*/
public without sharing class Arc_Gen_ContentPerAnalysis_data {
/**
*-------------------------------------------------------------------------------
* @description associateIdToTable asociate anid to the table
*--------------------------------------------------------------------------------
* @author  angel.fuertes2@bbva.com
* @date     12/03/2019
* @Method:      associates the record to be created with the mini ARCE
* @param:       newRecord object (arce__Table_Content_per_Analysis__c)
* @param:       recordId id (arce__Account_has_Analysis__c)
* @return:       SObject newRecord to update
* @example associateIdToTable(SObject newRecord, Id recordId)
* -----------------------------------------------------------------------------------------------
*/
    public SObject associateIdToTable(SObject newRecord, Id recordId) {
        newRecord.put('arce__account_has_analysis_id__c',recordId);
        return newRecord;
    }
}