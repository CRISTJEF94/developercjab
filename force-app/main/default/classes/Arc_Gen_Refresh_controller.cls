/*------------------------------------------------------------------
* @Author:       Luisarturo.parra.contractor@bbva.com
* @Project:      ARCE - BBVA Bancomer
* @Description:  Class controller for refresh component lightning
* @Class:        Arc_Gen_RefreshClass_controlelr
*_______________________________________________________________________________________
* Version    Date           Author                                       Description
* 1.0        11/04/2019      Luisarturo.parra.contractor@bbva.com           REFACTORIZACION.
* 1.2        13/01/2020      mariohumberto.ramirez.contractor@bbva.com      Modify method constructgroupstructure and added new comments
* 1.3        08/02/2020      ricardo.almanza.contractor@bbva.com            added for orphan
* 1.4        10/02/2020      juanignacio.hita.contractor@bbva.com           REFACTORIZACION
-----------------------------------------------------------------------------------------*/
public class Arc_Gen_Refresh_controller {
    /**
    *-------------------------------------------------------------------------------
    * @description Empty private constructor
    --------------------------------------------------------------------------------
    * @author juanignacio.hita.contractor@bbva.com
    * @date 2020-02-21
    * @example private Arc_Gen_Refresh_controller ()
    **/
    @TestVisible
    private Arc_Gen_Refresh_controller () {
    }
    /**
    *--------------------------------------------------------------------------------
    * @description getAHARefresh
    *--------------------------------------------------------------------------------
    * @date     06/02/2020
    * @author   juanignacio.hita.contractor@bbva.com
    * @param    recordId
    * @return   List<Arc_Gen_Account_Has_Analysis_Wrapper>
    * @example  Arc_Gen_Account_Has_Analysis_Wrapper wrapp = Arc_Gen_Refresh_controller.getAHARefresh(recordId);
    * -------------------------------------------------------------------------------
    */
    @AuraEnabled
    public static List<Arc_Gen_Account_Has_Analysis_Wrapper> getAHARefresh(String recordId) {
        try {
            return Arc_Gen_RefreshClass_service.getAllAnalysis(recordId);
        } catch (Exception e) {
            throw new AuraHandledException(System.Label.Arc_Gen_ApexCallError + e);
        }
    }
    /**
    *--------------------------------------------------------------------------------
    * @description Call list participant service.
    *--------------------------------------------------------------------------------
    * @date     06/02/2020
    * @author   juanignacio.hita.contractor@bbva.com
    * @param    encryptedgroup - String
    * @param    ahaswithoutgroup - String []
    * @param    ahaswithoutgroupnumber - String []
    * @param    recordId - String
    * @return   String
    * @example  String ret = Arc_Gen_Refresh_controller.getListParticipants(encryptedgroup, ahaswithoutgroup, ahaswithoutgroupnumber, recordId);
    * -------------------------------------------------------------------------------
    */
    @AuraEnabled
    public static String getListParticipants(String encryptedgroup, String[] ahaswithoutgroup, String[] ahaswithoutgroupnumber, String recordId) {
        try {
            final Arc_Gen_CallListParticipant.Innertoreturnlistp wrapper = Arc_Gen_CallListParticipant.callListParticipants(encryptedgroup);
            if (wrapper.customersdata.size() > 0 && wrapper.servicecallerror == '') {
                Arc_Gen_RefreshClass_service.evaluateListParticipants(new List<Object>{wrapper, ahaswithoutgroup, ahaswithoutgroupnumber, recordId});
                }
            return JSON.serialize(wrapper);
        } catch (Exception e) {
            throw new AuraHandledException(System.Label.Arc_Gen_ApexCallError + e);
        }
    }
    /**
    *-------------------------------------------------------------------------------
    * @description function that validate if the customer has a valid rating.
    *--------------------------------------------------------------------------------
    * @date     06/02/2020
    * @author   juanignacio.hita.contractor@bbva.com
    * @param    recordId - account_has_analysis Id
    * @return   Arc_Gen_RefreshClass_service.refreshMessagesResponse
    * @example  Arc_Gen_RefreshClass_service.refreshMessagesResponse wrap = Arc_Gen_Refresh_controller.callListCustomers(recordId);
    */
    @AuraEnabled
    public static Arc_Gen_RefreshClass_service.refreshMessagesResponse callListCustomers(String recordId) {
        try {
            return Arc_Gen_RefreshClass_service.callListCustomers(recordId);
        } catch (Exception e) {
            throw new AuraHandledException(System.Label.Arc_Gen_ApexCallError + e);
        }
    }
}