/**
* @File Name          : Arc_Gen_SetSector_Locator.cls
* @Description        :
* @Author             : luisruben.quinto.munoz@bbva.com
* @Group              :
* @Last Modified By   : luisruben.quinto.munoz@bbva.com
* @Last Modified On   : 4/7/2019 12:37:08
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    4/7/2019 11:35:48   luisruben.quinto.munoz@bbva.com     Initial Version
**/
public with sharing class Arc_Gen_SetSector_Locator {
    public static List<arce__Sector__c> getSectors() {
        List<arce__Sector__c> sectors = [SELECT Id, Name FROM arce__Sector__c];
        Return sectors;
    }
}