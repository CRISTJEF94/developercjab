/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Auto_ExpTable_Service_Helper
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2020-01-28
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Service class for policie table
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2020-01-28 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_Auto_ExpTable_Service_Helper {
    /**
        * @Description: param to call limits service
    */
    static final string S_GROUP = 'GROUP';
    /**
        * @Description: param to call limits service
    */
    static final string SUBSIDIARY = 'SUBSIDIARY';
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-01-28
    * @param void
    * @return void
    * @example Arc_Gen_Auto_ExpTable_Service_Helper helper = new Arc_Gen_Auto_ExpTable_Service_Helper()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_Auto_ExpTable_Service_Helper() {

    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Update policie table records with the data of the limit service
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-01-28
    * @param recordId - id of the acc has analysis object
    * @return void
    * @example fillServiceData(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static void fillServiceData(Id recordId) {
        final String typoOfCustomer = Arc_Gen_GenericUtilities.getTypeOfCustomer(recordId) == 'Group' ? S_GROUP : SUBSIDIARY;
        final arce__Account_has_Analysis__c accHasRel = Arc_Gen_AccHasAnalysis_Data.getAccHasRelation(recordId);
        final Map<Id, Arc_Gen_Account_Wrapper> accWrapper = Arc_Gen_Account_Locator.getAccountInfoById(new List<Id>{accHasRel.arce__customer__c});
        final Map<String,Arc_Gen_Limits_Service.LimitsResponse> limitRespMap = Arc_Gen_Limits_Service.callLimitsService(typoOfCustomer,accWrapper.get(accHasRel.arce__customer__c).accNumber, 'limits');
        final List<arce__limits_exposures__c> limitExposureDataLts = Arc_Gen_LimitsExposures_Data.getExposureData(new List<Id>{recordId});
        for (arce__limits_exposures__c limitExpData: limitExposureDataLts) {
            if (limitRespMap.keySet().contains(limitExpData.arce__limits_typology_id__r.arce__risk_typo_ext_id__c)) {
                limitExpData.arce__last_approved_amount__c = limitRespMap.get(limitExpData.arce__limits_typology_id__r.arce__risk_typo_ext_id__c).lastApproved;
                limitExpData.arce__curr_approved_commited_amount__c = limitRespMap.get(limitExpData.arce__limits_typology_id__r.arce__risk_typo_ext_id__c).commited;
                limitExpData.arce__curr_apprv_uncommited_amount__c = limitRespMap.get(limitExpData.arce__limits_typology_id__r.arce__risk_typo_ext_id__c).uncommited;
                limitExpData.arce__current_formalized_amount__c = limitRespMap.get(limitExpData.arce__limits_typology_id__r.arce__risk_typo_ext_id__c).currentLimit;
                limitExpData.arce__outstanding_amount__c = limitRespMap.get(limitExpData.arce__limits_typology_id__r.arce__risk_typo_ext_id__c).outstanding;
            }
        }
        List<arce__Account_has_Analysis__c> acchasAn = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<String>{(String)recordId});
        acchasAn[0].arce__call_limit_service__c = true;
        acchasAn[0].arce__magnitude_unit_type__c = '1';
        acchasAn[0].arce__prev_magnitude_unit_type__c = '1';
        acchasAn[0].arce__last_update_policie__c = System.now();
        Arc_Gen_AccHasAnalysis_Data.updateAccHasAnalysis(acchasAn);
        Arc_Gen_LimitsExposures_Data.updateExposureData(limitExposureDataLts);
    }
}