/*------------------------------------------------------------------
*Author:        Angel Fuertes Gomez - angel.fuertes2@bbva.com / BBVA DWP
*Project:       ARCE - BBVA Bancomer
*Description:   Class used for the button to Sanction.
*_______________________________________________________________________________________
*Version    Date           Author                               Description
*1.0        30/04/2019     Angel Fuertes Gomez                      Creation.
*1.1        12/11/2019     LUIS ARTURO PARRA ROSAS             SWITCH FOR CHANGING STAGE AND STATE
*1.2        20/11/2019     Javier Soto Carrascosa              Remove data access, fix traceability syle, use generic utilities
*1.3        06/12/2019     LUIS ARTURO PARRA ROSAS             Switch modification
*1.4        26/12/2019     Juan Manuel Perez Ortiz             Add logic to send notifications
*1.5        02/01/2019     Mario H. Ramirez Lio                Added new validation used in BtnSanction_Service_Helper
*1.5        05/02/2019     Juan Ignacio Hita Manso             Added new methods and refactorization
-----------------------------------------------------------------------------------------*/
public with sharing class Arc_Gen_BtnSanction_service {
    /*
        @Description: String with the api name of the option FINALIZED in the picklist arce__Stage__c
    */
    static final String FINALIZED = '3';
    /*
        @Description: String with the api name of the option Finalized Analysis in the picklist arce__wf_status_id__c
    */
    static final String FINALIZED_ANALYSIS = '10';
    /*
        @Description: String with the api name of the option Contrasting Analysis in the picklist arce__wf_status_id__c
    */
    static final String CONTRASTING_ANALYSIS = '04';
    /*
        @Description: String with the control value GROUP
    */
    static final String GROUP_S = '1';
    /*
        @Description: String with the control value SUCCESS
    */
    static final String SUCCESS = 'SUCCESS';
    /*
        @Description: String with the control value ERROR
    */
    static final String ERROR = 'ERROR';
    /*
        @Description: String with the api value of the picklist arce__prop_int_mod_rec_type__c
    */
    static final String YES = '1';
    /**
    *-------------------------------------------------------------------------------
    * @Description: class to store necessary data in the component.
    *-------------------------------------------------------------------------------
    * @param:       void
    * @return       void
    * @example WrapperBtnSanction wrapper = new WrapperBtnSanction()
    * ------------------------------------------------------------------------------
    */
    public class WrapperBtnSanction {
        /*
            @Description: Boolean with a control value true/false
        */
        public boolean  gblResultResponse {get;set;}
        /*
            @Description: String with a message to show in the front
        */
        public String gblDescriptionResponse {get;set;}
        /*
            @Description: String with a control value
        */
        public String typeMessage {get;set;}
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description Method that saves the scope of sanction
    * -----------------------------------------------------------------------------------------------
    * @author  angel.fuertes2@bbva.com
    * @date    2019-06-20
    * @Method: Save the reason for the sanction
    * @param:  Arc_Gen_Delegation_Wrapper delegation wrapper
    * @param:  Map<String, String> data
    * @return an instance of Arc_Gen_Delegation_Wrapper
    * @example validateIndField(myFieldValue)
    * -----------------------------------------------------------------------------------------------
    **/
    public static Arc_Gen_Delegation_Wrapper saveScopeOfSanction(Arc_Gen_Delegation_Wrapper wrapper, Map<string,string> data) {
        Arc_Gen_Delegation_Wrapper wrapperRet = wrapper;
        try {
            Map<String, Object> fieldValueMap = new Map<String, Object>();
            fieldValueMap.put('arce__anlys_wkfl_sanction_rslt_type__c', data.get('sanction'));
            fieldValueMap.put('arce__anlys_wkfl_sanction_rsn_desc__c', data.get('descText'));
            fieldValueMap.put('arce__gf_committee_user_id__c', data.get('idUserSelected'));
            fieldValueMap.put('arce__analysis_risk_sanction_date__c', Date.valueOf(data.get('sancDate')));

            if (date.valueOf(data.get('sancdateCometee')) >= Date.today()) {
                fieldValueMap.put('arce__analysis_risk_expiry_date__c', Date.valueOf(data.get('sancdateCometee')));
            } else {
                throw new DmlException(Label.Arc_Gen_Generic_ErrorComiteeDate);
            }

            if (data.get('ambit') != '') {
                fieldValueMap.put('arce__anlys_wkfl_edit_br_level_type__c', data.get('ambit'));
            }
            final WrapperBtnSanction wrapperBtnSanction = validateFields(Integer.valueOf(data.get('sanction')), data.get('recordId'));
            if (wrapperBtnSanction.typeMessage == SUCCESS) {
                fieldValueMap = Arc_Gen_BtnSanction_service.updateAHABtnSanction(Integer.valueOf(data.get('sanction')), wrapper.analysisId, fieldValueMap);
                Arc_Gen_ArceAnalysis_Data.editAnalysisFields(wrapper.analysisId, fieldValueMap);
                wrapperRet.codStatus = 200;
            } else if (wrapperBtnSanction.typeMessage == ERROR) {
                wrapperRet.codStatus = 500;
                wrapperRet.msgInfo = wrapperBtnSanction.gblDescriptionResponse;
            }
        } catch(Exception ex) {
            wrapperRet.codStatus = 500;
            wrapperRet.msgInfo = ex.getMessage() + ' : ' + ex.getLineNumber();
        }
        return wrapperRet;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description Method that validates some fields in the analysis
    * -----------------------------------------------------------------------------------------------
    * @Author  mariohumberto.ramirez.contractor@bbva.com
    * @Date 2020-01-02
    * @param picklistValue -  value of the selected option in the combo box
    * @return an instance of Arc_Gen_BtnSanction_controller.WrapperBtnSanction
    * @example validateFields(picklistValue)
    * -----------------------------------------------------------------------------------------------
    **/
    public static WrapperBtnSanction validateFields(Integer picklistValue, String recordId) {
        WrapperBtnSanction wrp = new WrapperBtnSanction();
        final List<String> accHasIdLts = new List<String>();
        final List<arce__Account_has_Analysis__c> accHasLts = Arc_Gen_AccHasAnalysis_Data.getAccHasAnFromArce(Arc_Gen_AccHasAnalysis_Data.getAccHasRelation(recordId).arce__Analysis__c);
        for (arce__Account_has_Analysis__c accHasId: accHasLts) {
            if (accHasId.arce__group_asset_header_type__c != GROUP_S) {
                accHasIdLts.add((String)accHasId.Id);
            }
        }
        final  List<arce__Account_has_Analysis__c> accHasDataLts = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(accHasIdLts);
        switch on picklistValue {
            when 1 {
                wrp.typeMessage = SUCCESS;
                wrp.gblDescriptionResponse = Label.Arc_Gen_SanctionSuccessMessage;
            }
            when 2,4 {
                for (arce__Account_has_Analysis__c accHas: accHasDataLts) {
                    if (accHas.arce__prop_int_mod_rec_type__c == YES) {
                        wrp.typeMessage = SUCCESS;
                        wrp.gblDescriptionResponse = picklistValue == 2 ? Label.Arc_Gen_SanctionSuccessMessage : Label.Arc_Gen_Sanction;
                        break;
                    } else {
                        wrp.typeMessage = ERROR;
                        wrp.gblDescriptionResponse = Label.Arc_Gen_SanctionErrorMessage;
                    }
                }
            }
            when else {
                wrp.typeMessage = SUCCESS;
                wrp.gblDescriptionResponse = Label.Arc_Gen_Sanction;
            }
        }
        return wrp;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description Method logic in the account has analysis of the sanction button
    * -----------------------------------------------------------------------------------------------
    * @Author  juanignacio.hita.contractor@bbva.com
    * @Date 2020-02-05
    * @param picklistValue -  value of the selected option in the combo box
    * @param recordId -  record id analysis
    * @param map<string, object> map of fields
    * @return an instance of Arc_Gen_BtnSanction_controller.WrapperBtnSanction
    * @example validateFields(picklistValue)
    * -----------------------------------------------------------------------------------------------
    **/
    public static Map<String, Object> updateAHABtnSanction(Integer picklistValue, String recordId, Map<String, Object> mapFields) {
        Map<String, Object> mapFieldsRet = mapFields;
        switch on picklistValue {
            when 1,2,3 {
                mapFieldsRet.put('arce__wf_status_id__c', FINALIZED_ANALYSIS);
                mapFieldsRet.put('arce__Stage__c', '3');
            }
            when 4,6 {
                final String idUser = Arc_Gen_Propose_Helper.initIdentification(String.valueOf(mapFields.get('arce__anlys_wkfl_edit_br_level_type__c')), recordId);
                mapFieldsRet.put('arce__wf_status_id__c', CONTRASTING_ANALYSIS);
                mapFieldsRet.put('OwnerId', idUser);
            }
            when 5 {
                final Arc_Gen_User_Wrapper wrpUser = Arc_Gen_User_Locator.getUserInfo(System.UserInfo.getUserId());
                mapFieldsRet.put('arce__wf_status_id__c', CONTRASTING_ANALYSIS);
                mapFieldsRet.put('OwnerId', Arc_Gen_Propose_Helper.initIdentification(wrpUser.ambitUser, recordId));
            }
        }
        return mapFieldsRet;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description Method that returns list of users
    * -----------------------------------------------------------------------------------------------
    * @author  luisruben.quinto.munoz@bbva.com
    * @date 2019-10-21
    * @Method:      searchUser
    * @param:       String inputTerm for search
    * @return an instance of List<map<String,String>>
    * @example validateIndField(myFieldValue)
    * -----------------------------------------------------------------------------------------------
    **/
    public static List<map<String,String>> searchUser(String inputTerm) {
        List<map<String,String>> ltsUsers = new list<map<String,String>>();
        final List<Arc_Gen_User_Wrapper> lstWrap = Arc_Gen_User_Locator.getUsers(inputTerm);
        for (Arc_Gen_User_Wrapper wrap : lstWrap) {
            ltsUsers.add(new map<String,String>{'nameUser' => wrap.userBasicInfo.Name, 'perfilUser' => wrap.profileName, 'idUser' => wrap.userBasicInfo.Id});
        }
        return ltsUsers;
    }
}