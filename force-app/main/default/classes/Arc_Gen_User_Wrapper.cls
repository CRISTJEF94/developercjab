/**
*-------------------------------------------------------------------------------
* @description Arc_Gen_User_Wrapper getUserInfo that retrieves full user information
--------------------------------------------------------------------------------
* @author javier.soto.carrascosa@bbva.com
* @date 2019-07-15
* @return wrapper user data
* @example Arc_Gen_User_Wrapper wu = new Arc_Gen_User_Wrapper();
**/
public class Arc_Gen_User_Wrapper {
    /**
    *
    * @Description : User object
    */
    public User userBasicInfo {get; set;}
    /**
    *
    * @Description : string with User branch id
    */
    public String branchId {get; set;}
    /**
    *
    * @Description : String Branch name
    */
    public String branchText {get; set;}
    /**
    *
    * @Description : String branch level id
    */
    public String branchlevel {get; set;}
    /**
    *
    * @Description : String user id
    */
    public String businessAgentId {get; set;}
    /**
    *
    * @Description : String user profile name
    */
    public String profileName {get; set;}
    /**
    *
    * @Description : String ambit user
    */
    public string ambitUser {get; set;}
}