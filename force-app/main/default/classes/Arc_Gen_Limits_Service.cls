/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Limits_Service
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2020-01-28
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Class to manage the service call of getLimits
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2020-01-28 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_Limits_Service {
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-01-28
    * @param void
    * @return void
    * @example Arc_Gen_Limits_Service service = new Arc_Gen_Limits_Service()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_Limits_Service() {

    }
    /**
        * @Description : wrapper for limits service
    */
    public class LimitsResponse {
        /**
        * @Description : Id of the typology/modality from limits service
        */
        public String typoModId {get;set;}
        /**
        * @Description : name of the typology/modality from limits service
        */
        public String name {get;set;}
        /**
        * @Description : currentLimit amount of the typology/modality from limits service
        */
        public Double currentLimit {get;set;}
        /**
        * @Description : outstanding amount of the typology/modality from limits service
        */
        public Double outstanding {get;set;}
        /**
        * @Description : commited amount of the typology/modality from limits service
        */
        public Double commited {get;set;}
        /**
        * @Description : uncommited amount of the typology/modality from limits service
        */
        public Double uncommited {get;set;}
        /**
        * @Description : lastApproved amount of the typology/modality from limits service
        */
        public Double  lastApproved {get;set;}
        /**
        * @Description : currencyType of the typology/modality from limits service
        */
        public String currencyType {get;set;}
        /**
        * @Description : unit of the typology/modality from limits service
        */
        public String unit {get;set;}
        /**
        * @Description : gblCodeResponse error of the limits service
        */
        public String gblCodeResponse {get;set;}
        /**
        * @Description : gblSuccess response of the limits service
        */
        public Boolean gblSuccess {get;set;}
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Method that sets the parameters to the list customer service
    --------------------------------------------------------------------------------
    * @author mariohumberto.ramirez.contractor@bbva.com
    * @date 2020-01-28
    * @param String typeOfClient
    * @param String client number
    * @param String serviceName
    * @return Map<String,LimitsResponse> - Map that contains the response of the called service
    * @example callLimitsService(typeOfClient,accNumber,serviceName)
    **/
    public static Map<String,LimitsResponse> callLimitsService(String typeOfClient, String accNumber, String serviceName) {
        Arc_Gen_getIASOResponse.serviceResponse iasoResponse = new Arc_Gen_getIASOResponse.serviceResponse();
        final Arc_Gen_CustomServiceMessages serviceMessage = new Arc_Gen_CustomServiceMessages();
        LimitsResponse limRespWrap = new LimitsResponse();
        Map<String,LimitsResponse> limitsResponseMap = new Map<String,LimitsResponse>();
        final String customerNumber = Arc_Gen_CallEncryptService.getEncryptedClient(accNumber);
        final String params = '{"ownerType":"' + typeOfClient + '","ownerId":"' + customerNumber + '"}';
        iasoResponse = Arc_Gen_getIASOResponse.calloutIASO(serviceName, params);
        if (iasoResponse.serviceCode == String.valueOf(serviceMessage.CODE_200)) {
            limitsResponseMap = Arc_Gen_Limits_Service_Helper.getServiceData(iasoResponse.serviceResponse);
        } else {
            limRespWrap.gblCodeResponse = iasoResponse.serviceCode;
            limitsResponseMap.put('ERROR', limRespWrap);
        }
        return limitsResponseMap;
    }
}