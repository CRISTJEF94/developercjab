/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_TabsetService_Helper
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 24/06/2019
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Helper class that manages the tabs to be shown in the ARCE.
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |2019-06-24 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* |2019-07-16 mariohumberto.ramirez.contractor@bbva.com
*             Add new method hideDynFormSection.
* |2019-07-23 mariohumberto.ramirez.contractor@bbva.com
*             Add new JSON param DevTemplate
* |2019-07-29 mariohumberto.ramirez.contractor@bbva.com
*             Add new method changeArceStateHelper.
*             Add new constants PREPARING_ANALYSIS, ARCE_PREPARED, CONTRASTING_ANALYSIS, IN_PREPARATION
*             IN_SANCTION
* |2019-08-07 mariohumberto.ramirez.contractor@bbva.com
*             Deleted method generateWrapperTabSet.
*             Change method generateListWrapperTab in order to use the completeness calculation
*             class that has by default the dynamic form component.
*             Add new constant "ONE"
* |2019-08-23 mariohumberto.ramirez.contractor@bbva.com
*             Deleted method hideDynFormSection.
* |2019-10-03 mariohumberto.ramirez.contractor@bbva.com
*             Fixed param to call getRatingStatus method from Arc_Gen_AccHasAnalysis_Data class
* |2019-10-25 mariohumberto.ramirez.contractor@bbva.com
*             Added new method update2zerohidenTypologies
* |2019-12-18 juanmanuel.perez.ortiz.contractor@bbva.com
*             Add new static variables and modified changeArceStateHelper method
* |2020-01-20 juanmanuel.perez.ortiz.contractor@bbva.com
*             Modified logic in policies tab visibility in method called generateListWrapperTabget
* |2020-01-20 mariohumberto.ramirez.contractor@bbva.com
*             Added new method OutstandingFromTabClient()
* -----------------------------------------------------------------------------------------------
*/
public class Arc_Gen_TabsetService_Helper {
    /**
        * @Description: string with the value of preparing analysis status
    */
    static final string PREPARING_ANALYSIS = '02';
    /**
        * @Description: string with the value of preparing analysis status
    */
    static final string PENDING_SANCTION = '08';
    /**
        * @Description: string with the value of preparing analysis status
    */
    static final string ARCE_PREPARED = '03';
    /**
        * @Description: string with value of contrasting analysis status
    */
    static final string CONTRASTING_ANALYSIS = '04';
    /**
        * @Description: string with value of contrasting analysis status
    */
    static final string IN_PREPARATION = '1';
    /**
        * @Description: string with value of contrasting analysis status
    */
    static final string IN_SANCTION = '2';
    /**
        * @Description: integer with value 1
    */
    static final integer ONE = 1;
    /**
        * @Description: List of String with the dev name of the typologies to depecrate
    */
    static final List<String> TYPO_DEPRECATED = new List<String>{'TP_0001', 'TP_0004', 'TP_0006', 'TP_0009', 'TP_0013','TP_0015', 'TP_0016'};
    /**
        * @Description: List of string with the name of some fields in arce__limits_exposure__c object
    */
    static final List<string> IMPORTS_FIELDS = new List<string>{'arce__current_approved_amount__c','arce__curr_apprv_uncommited_amount__c',
            'arce__curr_approved_commited_amount__c','arce__current_formalized_amount__c','arce__outstanding_amount__c',
            'arce__current_proposed_amount__c','arce__last_approved_amount__c'};

    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 24/06/2019
    * @param void
    * @return void
    * @example Arc_Gen_TabsetService_Helper serviceHelper = new Arc_Gen_TabsetService_Helper()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_TabsetService_Helper() {

    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Wrapper that contain a map with the result of each tab
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 07/08/2019
    * @param void
    * @return void
    * @example CompletionResult completion = new CompletionResult()
    * ----------------------------------------------------------------------------------------------------
    **/
    public class CompletionResult {
        /**
        * @Description: Map with the id of the tab and value the wrapper "TabInfoCompletion"
        */
        final private Map<String, TabCompletionInfo> resultCompletion = new Map<String, TabCompletionInfo>();
    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Wrapper that contain the percent information of the tab
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 07/08/2019
    * @param void
    * @return void
    * @example TabCompletionInfo completion = new TabCompletionInfo()
    * ----------------------------------------------------------------------------------------------------
    **/
    public class TabCompletionInfo {
        /**
        * @Description: Integer percentage of the complete fields
        */
        private Integer percentComplete {get; set;}
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that return a list of wrappertabset with the info of each tab
    * -----------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 24/06/2019
    * @param recordId - Id of the current account has analisys object
    * @param sector - object with the information of the sector
    * @return a list of wrappertabset with the info of each tab
    * @example generateListWrapperTab(recordId, sector)
    * -----------------------------------------------------------------------------------------------
    **/
    public static List<Arc_Gen_TabSet_controller.WrapperTabSet> generateListWrapperTab(Id recordId, arce__Sector__c sector) {
        final List<Arc_Gen_TabSet_controller.WrapperTabSet> listWrapper = new List<Arc_Gen_TabSet_controller.WrapperTabSet>();
        integer totalPercent = 0;
        final System.Type objType = Type.forName('dyfr.Dynamic_Form_Completion');
        final dyfr.Form_Completion_Interface formClass = (dyfr.Form_Completion_Interface)objType.newInstance();
        final arce__Account_has_Analysis__c accHasRelation = Arc_Gen_AccHasAnalysis_Data.getAccHasRelation(recordId);
        final List<arce__Account_has_Analysis__c> ratingStatus = Arc_Gen_AccHasAnalysis_Data.getRatingStatus(new List<Id>{recordId});
        for( Schema.PicklistEntry value : Arc_Gen_Sector_Data.getPicklistEntryFroAnalysisSectionType()) {
            Arc_Gen_TabSet_controller.WrapperTabSet wrapperTabSet = new Arc_Gen_TabSet_controller.WrapperTabSet();
            switch on value.getValue() {
                when '100' {
                    wrapperTabSet.nameTemplate = sector.arce__developer_name__c + '-' + value.getValue();
                    wrapperTabSet.nameSection = value.getLabel();
                    wrapperTabSet.isVisible = true;
                    wrapperTabSet.percent = getPercentCompletion(recordId, sector.arce__developer_name__c + '-' + value.getValue(), formClass);
                    wrapperTabSet.devTemplate = value.getValue();
                    totalPercent += wrapperTabSet.percent;
                    listWrapper.add(wrapperTabSet);
                }
                when '300' {
                    wrapperTabSet.nameTemplate = sector.arce__developer_name__c + '-' + value.getValue();
                    wrapperTabSet.nameSection = value.getLabel();
                    wrapperTabSet.isVisible = true;
                    wrapperTabSet.percent = getPercentCompletion(recordId, sector.arce__developer_name__c + '-' + value.getValue(), formClass);
                    wrapperTabSet.devTemplate = value.getValue();
                    totalPercent += wrapperTabSet.percent;
                    listWrapper.add(wrapperTabSet);
                }
                when '400' {
                    wrapperTabSet.nameTemplate = sector.arce__developer_name__c + '-' + value.getValue();
                    wrapperTabSet.nameSection = value.getLabel();
                    wrapperTabSet.isVisible = true;
                    wrapperTabSet.percent = getPercentCompletion(recordId, sector.arce__developer_name__c + '-' + value.getValue(), formClass);
                    wrapperTabSet.devTemplate = value.getValue();
                    totalPercent += wrapperTabSet.percent;
                    listWrapper.add(wrapperTabSet);
                }
                when '500' {
                    wrapperTabSet.nameTemplate = sector.arce__developer_name__c + '-' + value.getValue();
                    wrapperTabSet.nameSection = value.getLabel();
                    wrapperTabSet.isVisible = (totalPercent == 600 && (ratingStatus[0].arce__ffss_for_rating_id__r.arce__rating_id__r.arce__status_type__c == '2' || ratingStatus[0].arce__ffss_for_rating_id__r.arce__rating_id__r.arce__status_type__c == '3')) || accHasRelation.arce__group_asset_header_type__c == '1' ? true : false;
                    wrapperTabSet.percent = 0;
                    wrapperTabSet.devTemplate = value.getValue();
                    listWrapper.add(wrapperTabSet);
                }
                when else {
                    wrapperTabSet.nameTemplate = sector.arce__developer_name__c + '-' + value.getValue();
                    wrapperTabSet.nameSection = value.getLabel();
                    wrapperTabSet.isVisible = true;
                    wrapperTabSet.percent = 0;
                    wrapperTabSet.devTemplate = value.getValue();
                    listWrapper.add(wrapperTabSet);
                }
            }
        }
        return listWrapper;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that return an Integer with the percent value of a tab or the percent sum
    * of subtabs
    * -----------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 07/08/2019
    * @param recordId - Id of the current account has analisys object
    * @param templateDevName - developer name of the template
    * @param formClass - dyfr completion class
    * @return an integer with the completeness info
    * @example getPercentCompletion(recordId, templateDevName, formClass)
    * -----------------------------------------------------------------------------------------------
    **/
    public static Integer getPercentCompletion(Id recordId, String templateDevName, dyfr.Form_Completion_Interface formClass) {
        final String retJSON = formClass.getInfoCompletion(recordId, 'dyfr__Important__c', templateDevName, null, null);
        CompletionResult completionInfo = new CompletionResult();
        completionInfo = (CompletionResult) JSON.deserialize(retJSON, CompletionResult.class);
        final Set<String> setTabIds = completionInfo.resultCompletion.keySet();
        final List<String> tabIds = new List<String>(setTabIds);
        Integer percent = 0;
        if (tabIds.size() == ONE) {
            percent = completionInfo.resultCompletion.get(tabIds[0]).percentComplete;
        } else if (tabIds.size() > ONE) {
            integer sumPercent = 0;
            for (String tabId: tabIds) {
                sumPercent += completionInfo.resultCompletion.get(tabId).percentComplete;
            }
            percent = sumPercent;
        }
        return percent;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that change the status of an Arce Analysis
    * -----------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 29/07/2019
    * @param recordId - Id of the current account has analisys object
    * @return true/false
    * @example changeArceStateHelper(recordId)
    * -----------------------------------------------------------------------------------------------
    **/
    public static Boolean changeArceStateHelper(Id recordId) {
        Boolean statusChange = false;
        final arce__Account_has_Analysis__c relationData = Arc_Gen_AccHasAnalysis_Data.getAccHasRelation(recordId);
        final List<arce__Analysis__c> arceAnalysisData = Arc_Gen_ArceAnalysis_Data.getArceAnalysisData(new List<Id>{relationData.arce__Analysis__c});
        if (arceAnalysisData[0].arce__wf_status_id__c == ARCE_PREPARED && arceAnalysisData[0].arce__Stage__c == IN_PREPARATION) {
            Arc_Gen_ArceAnalysis_Data.changeStatusArce(relationData, arceAnalysisData[0], PREPARING_ANALYSIS);
            statusChange = true;
        }
        if (arceAnalysisData[0].arce__wf_status_id__c == ARCE_PREPARED && arceAnalysisData[0].arce__Stage__c == IN_SANCTION) {
            Arc_Gen_ArceAnalysis_Data.changeStatusArce(relationData, arceAnalysisData[0], CONTRASTING_ANALYSIS);
            statusChange = true;
        }
        if (arceAnalysisData[0].arce__wf_status_id__c == PENDING_SANCTION && arceAnalysisData[0].arce__Stage__c == IN_SANCTION) {
            Arc_Gen_ArceAnalysis_Data.changeStatusArce(relationData, arceAnalysisData[0], PENDING_SANCTION);
            statusChange = true;
        }

        return statusChange;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that update to zero the hiden typologies if is necesary
    * -----------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 25/10/2019
    * @param recordId - Id of the current account has analisys object
    * @return void
    * @example update2zerohidenTypologies(recordId)
    * -----------------------------------------------------------------------------------------------
    **/
    public static void update2zerohidenTypologies(Id recordId) {
        integer aux = 0;
        List<arce__limits_exposures__c> policiesTableData = Arc_Gen_LimitsExposures_Data.getExposureByDevName(new List<Id>{recordId}, TYPO_DEPRECATED);
        for (arce__limits_exposures__c policieTable: policiesTableData) {
            for (integer i = 0; i < IMPORTS_FIELDS.size(); i++) {
                if (Double.valueOf(policieTable.get(IMPORTS_FIELDS[i])) > 0) {
                    aux++;
                }
            }
        }
        if (aux > 0) {
            for (arce__limits_exposures__c policieTable: policiesTableData) {
                policieTable.arce__current_approved_amount__c = 0;
                policieTable.arce__curr_apprv_uncommited_amount__c = 0;
                policieTable.arce__curr_approved_commited_amount__c = 0;
                policieTable.arce__current_formalized_amount__c = 0;
                policieTable.arce__outstanding_amount__c = 0;
                policieTable.arce__current_proposed_amount__c = 0;
                policieTable.arce__last_approved_amount__c = 0;
            }
            Arc_Gen_LimitsExposures_Data.updateExposureData(policiesTableData);
        }
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - get outstanding amount to show in the carrousel
    * -----------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 28/01/2020
    * @param recordId - Id of the current account has analisys object
    * @return String with the message to show in the carrousel
    * @example getOutstandingFromTabClient(recordId)
    * -----------------------------------------------------------------------------------------------
    **/
    public static String getOutstandingFromTabClient(Id recordId) {
        final arce__Account_has_Analysis__c accHasRel = Arc_Gen_AccHasAnalysis_Data.getAccHasRelation(recordId);
        final Map<Id, Arc_Gen_Account_Wrapper> mapWrapper = Arc_Gen_Account_Locator.getAccountInfoById(new List<Id>{accHasRel.arce__customer__c});
        return String.valueOf(mapWrapper.get(accHasRel.arce__customer__c).outstanding) + ' ' + mapWrapper.get(accHasRel.arce__customer__c).currencyType;
    }
}