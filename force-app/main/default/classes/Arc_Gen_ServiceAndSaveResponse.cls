/**
* @File Name          : Arc_Gen_ServiceAndSaveResponse.cls
* @Description        : Use as wraper by ASO services response
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE
* @Last Modified By   : luisruben.quinto.munoz@bbva.com
* @Last Modified On   : 23/7/2019 19:35:30
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    30/4/2019 17:55:51   eduardoefrain.hernandez.contractor@bbva.com     Initial Version
* 1.1    17/1/2020 12:55:51   javier.soto.carrascosa@bbva.com     Add createdRsr
* 1.2    02/04/2020 12:55:51   javier.soto.carrascosa@bbva.com     Add serviceHeaders
**/
public class Arc_Gen_ServiceAndSaveResponse {
    @AuraEnabled public String serviceCode {get;set;}
    @AuraEnabled public String serviceMessage {get;set;}
    @AuraEnabled public String serviceResponse {get;set;}
    @AuraEnabled public String saveStatus {get;set;}
    @AuraEnabled public String saveMessage {get;set;}
    public Map<String,Object> updatefields {get;set;}
    /**
    *
    * @Description : Retrieves List of Salesforce Ids with created resources when needed
    */
    public List<Id> createdRsr {get;set;}
    /**
    *
    * @Description : Retrieves serviceHeaders when needed
    */
    public Map<String, String> serviceHeaders {get;set;}
}