/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Sector_Data
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 24/06/2019
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Data class for object arce__Sector__c
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |2019-06-24 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_Sector_Data {
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_Sector_Data sector = new Arc_Gen_Sector_Data()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_Sector_Data() {

    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that return a list of all fields in arce__Sector__c object
    * -----------------------------------------------------------------------------------------------
    * @param developerName - List of developer name of the arce__Sector__c object to consult
    * @return List<arce__Sector__c> - List of all fields in arce__Account_has_Analysis__c object
    * @example getSectorByDeveloperName(developerName)
    * -----------------------------------------------------------------------------------------------
    **/
    public static List<arce__Sector__c> getSectorByDeveloperName(List<String> developerName) {
        return [SELECT CreatedById,CurrencyIsoCode,LastModifiedById,OwnerId,RecordTypeId,arce__developer_name__c,Name,arce__analysis_section_type__c
                FROM arce__Sector__c
                WHERE arce__developer_name__c = :developerName];
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that return a list of pick list values from arce__Sector__c object
    * -----------------------------------------------------------------------------------------------
    * @param void
    * @return List<Schema.PicklistEntry> - List of PicklistValues
    * @example getPicklistEntryFroAnalysisSectionType()
    * -----------------------------------------------------------------------------------------------
    **/
    public static List<Schema.PicklistEntry> getPicklistEntryFroAnalysisSectionType() {
        Schema.DescribeFieldResult field = arce__Sector__c.arce__analysis_section_type__c.getDescribe();
        return field.getPicklistValues();
    }

}