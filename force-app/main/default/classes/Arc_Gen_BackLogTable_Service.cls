/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_BackLogTable_Service
* @Author   Mario Humberto Ramirez Lio mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2019-07-15
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Service class for Arc_Gen_BackLogTable_Ctrl
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-07-15 mariohumberto.ramirez.contractor@bbva.com
*             Class Creation
* |2019-12-02 german.sanchez.perez.contractor@bbva.com | franciscojavier.bueno@bbva.com
*             API names modified with the correct name on Business Glossary
* -----------------------------------------------------------------------------------------------
*/
public class Arc_Gen_BackLogTable_Service {
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-15
    * @param void
    * @return void
    * @example Arc_Gen_BackLogTable_Service service = new Arc_Gen_BackLogTable_Service()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_BackLogTable_Service() {

    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that return's the data needed to build the backLog Table
    * ----------------------------------------------------------------------------------------------------
    * @Author   mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-15
    * @param recordId - Id of the account has analysis object
    * @return Map<string,double> - map with the values to build the backLog Table
    * @example dataResp(recordId)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static Map<string,double> dataResp(Id recordId) {
        Map<string,double> mapResponse =  new Map<string,double>();
        List<arce__Account_has_Analysis__c> accHasData = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<Id>{recordId});
        mapResponse.put('importX', accHasData[0].arce__gf_backlog_pending_cyr_amount__c);
        mapResponse.put('importY', accHasData[0].arce__gf_backlog_pending_pyr_amount__c);
        mapResponse.put('salesX', accHasData[0].arce__gf_backlog_sales_cyr_number__c);
        mapResponse.put('salesY', accHasData[0].arce__gf_backlog_sales_pyr_number__c);
        return mapResponse;
    }
}