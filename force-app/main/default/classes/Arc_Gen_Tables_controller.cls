/**
* @File Name          : Arc_Gen_Tables_controller.cls
* @Description        : Return formated JSON String to be used on table component
* @Author             : ARCE Team
* @Group              : ARCE
* @Last Modified By   : luisruben.quinto.munoz@bbva.com
* @Last Modified On   : 23/7/2019 19:11:55
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author                   Modification
*==============================================================================
* 1.0    8/5/2019 0:08:52   diego.miguel.contractor@bbva.com     Added comments
**/
public with sharing class Arc_Gen_Tables_controller {
  /**
  *-------------------------------------------------------------------------------
  * @description Retuns formated JSON String with the given collectionType and recordId
  *--------------------------------------------------------------------------------
  * @date 7/5/2019
  * @author ARCE Team
  * @param String collectionType data collection type for tables
  * @param String record Id from account has analisis
  * @return String
  */
  public String getTableJsonComponent(String collectionType, String recordId) {
    String table = '{"name":"dwp_dv:Configurable_Table_Display_cmp","attributes":{"recordId":"'+ recordId +'","configurationId":"Arc_Gen_'+collectionType+'_MA","tableId":"'+collectionType+'","refreshOnRefreshViewEvent":true,"showTitle":true}},';
    return table;
  }
}