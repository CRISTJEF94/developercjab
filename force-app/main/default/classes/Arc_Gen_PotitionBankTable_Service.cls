/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_PotitionBankTable_Service
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2019-07-24
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Service class for Arc_Gen_PotitionBankTable_Ctlr.
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-07-24 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* |2019-11-27 mariohumberto.ramirez.contractor@bbva.com
*             Change the object limits exposure to risk position summary
* -----------------------------------------------------------------------------------------------
*/
global class Arc_Gen_PotitionBankTable_Service implements Arc_Gen_PotitionTable_Interface {
    /**
    * --------------------------------------------------------------------------------------
    * @Description insert TOTAL if is missing
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-24
    * @param recordId - id of the acc has analysis object
    * @return void
    * @example verifyDataInserted(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static Arc_Gen_DataTable getData(Id recordId) {
        Arc_Gen_DataTable dataJson = new Arc_Gen_DataTable();
        dataJson.columns = Arc_Gen_PotitionBankTable_Service_Helper.getColumns(recordId);
        dataJson.data = Arc_Gen_PotitionBankTable_Service_Helper.getRowsHelper(recordId);
        return dataJson;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description insert TOTAL if is missing
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-24
    * @param recordId - id of the acc has analysis object
    * @return void
    * @example verifyDataInserted(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static void verifyDataInserted(Id recordId) {
        if (Arc_Gen_Risk_Position_summary_Data.getPositionSummaryData(new List<Id>{recordId}).isEmpty()) {
            Arc_Gen_PotitionBankTable_Service_Helper.insertTotal(recordId);
        }
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Return an Id of an recordType
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-24
    * @param recordTypeDevName - developer name of the record type
    * @return Id of a recordType
    * @example getRecordTypeId(recordTypeDevName)
    * --------------------------------------------------------------------------------------
    **/
    public static String getRecordTypeId(String recordTypeDevName){
        return (String)Arc_Gen_Risk_Position_summary_Data.getRecordTypeRiskPositionSum(recordTypeDevName);
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Delete a record
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-11-27
    * @param recordId - id of the record to delete
    * @return void
    * @example deleteRecord(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static void deleteRecord(String recordId) {
        Arc_Gen_Risk_Position_summary_Data.deleteRecord(new List<String>{recordId});
    }
}