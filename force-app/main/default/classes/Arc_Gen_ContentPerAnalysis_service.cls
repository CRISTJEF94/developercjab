/*------------------------------------------------------------------
*Author:        Angel Fuertes Gomez - angel.fuertes2@bbva.com / BBVA DWP
*Project:      	ARCE - BBVA Bancomer
*Description:   test for method class Arc_Gen_ProposeInPreparation_controller.
*_______________________________________________________________________________________
*Version    Date           Author                               Description
*1.0        12/03/2019     Angel Fuertes Gomez                  Creation.
*1.1        23/07/2019     Ruben Quinto                          adds with sharing
-----------------------------------------------------------------------------------------*/
public with sharing class Arc_Gen_ContentPerAnalysis_service {
/**
*-------------------------------------------------------------------------------
* @description associateIdToTable asociate anid to the table
*--------------------------------------------------------------------------------
* @author  angel.fuertes2@bbva.com
* @date     12/03/2019
* @Method:      associates the record to be created with the mini ARCE.
* @param:       newRecord object (arce__Table_Content_per_Analysis__c)
* @param:       recordId id (arce__Account_has_Analysis__c)
* @return:       SObject newRecord to update
* @example beforeInsertService(SObject newRecord, Id recordId) {
* -----------------------------------------------------------------------------------------------
*/
    public SObject beforeInsertService(SObject newRecord, Id recordId) {
        Arc_Gen_ContentPerAnalysis_data dataAccess = new Arc_Gen_ContentPerAnalysis_data();
        return dataAccess.associateIdToTable(newRecord, recordId);
    }
}