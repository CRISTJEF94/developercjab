/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_SetSector_Service
* @Author   ARCE Team
* @Date     Created: 2019-05-07
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Service class for SetSector
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-05-07 diego.miguel.contractor@bbva.com
*             Class creation.
* |2019-07-04 ricardo.almanza.contractor@bbva.com
*             Error Management when null setClientSector
* |2019-08-13 mariohumberto.ramirez.contractor@bbva.com
*             clientId param deleted
* |2020-01-30 juanmanuel.perez.ortiz.contractor@bbva.com
*             Add missing custom labels
* -----------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_SetSector_Service {
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Wraper class for sectors
    * -----------------------------------------------------------------------------------------------
    * @Author ARCE Team
    * @Date 2019-05-07
    * @param void
    * @return void
    * @example SectorResponse wrapper = new SectorResponse()
    * -----------------------------------------------------------------------------------------------
    **/
    public class SectorResponse {
        public boolean sectorResultResponse {get;set;}
        public String sectorDescriptionResponse {get;set;}
    }

    /**
    *--------------------------------------------------------------------------------
    * @Description Calls data to update the arce__Account_has_Analysis__c related to
    * the client and analysis given with the selectedSector
    *--------------------------------------------------------------------------------
    * @Date 7/5/2019
    * @Author ARCE Team
    * @param String analysisId id for the analisis
    * @param String selectedSector sector selected
    * @return SectorResponse wrapper instance
    * @example setClientSector(analysisId, selectedSector)
    * -------------------------------------------------------------------------------
    */
    public static SectorResponse setClientSector(String analysisId, String clientId, String selectedSector) {
        SectorResponse response = new SectorResponse();
        try{
            response.sectorResultResponse = true;
            if(selectedSector == null) {
                throw new DMLException (System.Label.Arc_Gen_SectorNull);
            }
            Arc_Gen_SetSector_data.getAnalizedClientsByAnalysis(analysisId,clientId,selectedSector);
        } catch (Exception ex) {
            response.sectorResultResponse = false;
            response.sectorDescriptionResponse = ex.getMessage();
        }
        Return response;
    }
}