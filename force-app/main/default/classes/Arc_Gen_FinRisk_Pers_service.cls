/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_FinRisk_Pers_service
* @Author   Javier Soto Carrascosa
* @Date     Created: 08/04/2020
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Class that manages save for Basic Data
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |2020-04-08 Javier Soto Carrascosa
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
global class Arc_Gen_FinRisk_Pers_service implements dyfr.Save_Interface {//NOSONAR
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that is responsible for invoking the classes to save the
      business riks information
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 08/04/2020
    * @param listObject - List of the account has analisys object
    * @return String with the execution message
    * @example save (listObject)
    * -----------------------------------------------------------------------------------------------
    **/
    public static String save(List<sObject> listObject) {
        Map<String, Object> financialRiskMap = new Map<String, Object>();
        final arce__Account_has_Analysis__c ahaData = Arc_Gen_Persistence_Utils.getAhaFromSobject(listObject);
        final arce__Account_has_Analysis__c accHasAnalysis = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<String>{(String)ahaData.Id})[0];
        financialRiskMap = financialInfoJSON(ahaData, accHasAnalysis);
        final Map<Id, Arc_Gen_Account_Wrapper> listacc = Arc_Gen_Account_Locator.getAccountInfoById(new List<String>{accHasAnalysis.arce__Customer__c});
        final String participantId = Arc_Gen_CallEncryptService.getEncryptedClient(listacc.get(accHasAnalysis.arce__Customer__c).accNumber);
        final boolean financialInfo = Arc_Gen_OraclePers_service.financialRiskWS(participantId, accHasAnalysis.Name, financialRiskMap);
        return JSON.serialize(new Arc_Gen_wrpSave(financialInfo,'',listObject));
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that generates JSON for financial risk WS
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 08/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param arce__Account_has_Analysis__c - prevaha Object
    * @return Map<String, Object> with WS structure for Financial Risk WS
    * @example financialInfoJSON(ahaData, prevAha)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> financialInfoJSON (arce__Account_has_Analysis__c ahaData, arce__Account_has_Analysis__c prevAha){
        Map<String, Service_persistence__mdt> configsMap = Arc_Gen_Persistence_Utils.getConfigurationInfo(new List<String>{'finRiskIndicatorsConfig', 'finRiskVariablesConfig', 'finRiskVariationsConfig'});
        Map<String, Object> finalWSMap = new Map<String, Object>();
        final List<Map<String, Object>> indicators = addIndicators(ahaData, prevAha, configsMap);
        final List<Map<String, Object>> variables = addVariables(ahaData, prevAha, configsMap);
        final List<Map<String, Object>> variations = addVariations(ahaData, prevAha, configsMap);
        final Map<String, Object> audit = addAudit(ahaData);
        final String consolidation = addConsolidation(ahaData);
        final Map<String, Object> maturityTable = addMaturity(ahaData, prevAha);
        final Map<String, Object> budget = generateBudget(ahaData, prevAha);
        finalWsMap = Arc_Gen_Persistence_Utils.addifFilledList(finalWsMap,'variables',variables);
        finalWsMap = Arc_Gen_Persistence_Utils.addifFilledList(finalWsMap,'variations',variations);
        finalWsMap = Arc_Gen_Persistence_Utils.addifFilledList(finalWsMap,'indicators',indicators);
        finalWsMap = Arc_Gen_Persistence_Utils.addifFilled(finalWsMap,'budget',budget);
        finalWsMap.put('maturityTable', maturityTable);
        finalWsMap.put('perimeter', (Object)consolidation);
        finalWsMap.put('audit', audit);
        return finalWSMap;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that is responsible for generating audit Object
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param string - key
    * @return Map<String, Object> with WS structure for sector, subsector and activity
    * @example addSector(ahaData, key)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> addAudit (arce__Account_has_Analysis__c ahaData) {
        Map<String, Object> audit =  new Map<String, Object>();
        Map<String, Object> auditor =  new Map<String, Object>();
        final String opinion = Arc_Gen_Persistence_Utils.defaultValueList((String)ahaData.get('arce__ffss_auditor_opinion_type__c'));
        final String auditorName = Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get('arce__ffss_auditor_fullname_name__c')) ? (String)ahaData.get('arce__ffss_auditor_fullname_name__c') : 'null';
        auditor.put('fullName', (Object)auditorName);
        auditor.put('opinion', (Object)opinion);
        audit.put('businessAgentComments', ahaData.get('arce__ffss_auditor_opinion_desc__c'));
        audit.put('auditor', auditor);
        return audit;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that is responsible for generating audit Object
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param string - key
    * @return Map<String, Object> with WS structure for sector, subsector and activity
    * @example addSector(ahaData, key)
    * -----------------------------------------------------------------------------------------------
    **/
    private static String addConsolidation (arce__Account_has_Analysis__c ahaData) {
        return Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get('arce__ffss_cnsld_perimeter_desc__c')) ? (String)ahaData.get('arce__ffss_cnsld_perimeter_desc__c') : 'null';
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that is responsible for generating audit Object
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param arce__Account_has_Analysis__c - prevAha Object
    * @param string - key
    * @return Map<String, Object> with WS structure for sector, subsector and activity
    * @example addSector(ahaData, key)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> addMaturity (arce__Account_has_Analysis__c ahaData, arce__Account_has_Analysis__c prevAha) {
        Map<String, Object> maturityMap =  new Map<String, Object>();
        if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get('arce__debt_maturity_desc__c')) || Arc_Gen_ValidateInfo_utils.isFilled((String)prevAha.get('arce__debt_maturity_desc__c'))) {
            maturityMap.put('comments', ahaData.get('arce__debt_maturity_desc__c'));
        }
        if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get('arce__number_entity_type__c')) || Arc_Gen_ValidateInfo_utils.isFilled((String)prevAha.get('arce__number_entity_type__c'))) {
            maturityMap.put('banksNumberRange', Arc_Gen_Persistence_Utils.defaultValueList((String)ahaData.get('arce__number_entity_type__c')));
        }
        maturityMap.put('hasMaturityTable', Arc_Gen_Persistence_Utils.booleanFromYesNo((String)ahaData.get('arce__debt_maturity_available_type__c')));
        return maturityMap;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that is responsible for generating budget Object
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 9/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param arce__Account_has_Analysis__c - prevAha Object
    * @param string - key
    * @return Map<String, Object> with WS structure for budget
    * @example generateBudget(ahaData, prevAha)
    * -----------------------------------------------------------------------------------------------
    **/
    private static Map<String, Object> generateBudget (arce__Account_has_Analysis__c ahaData, arce__Account_has_Analysis__c prevAha) {
        Map<String, Object> budgetMap =  new Map<String, Object>();
        if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get('arce__cust_budget_cyr_ind_type__c')) || Arc_Gen_ValidateInfo_utils.isFilled((String)prevAha.get('arce__cust_budget_cyr_ind_type__c'))) {
            if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get('arce__cust_budget_incl_ffss_ind_type__c'))) {
                budgetMap.put('comments',ahaData.get('arce__cust_budget_incl_ffss_desc__c'));
                budgetMap.put('isIncluded', Arc_Gen_Persistence_Utils.booleanFromYesNo((String)ahaData.get('arce__cust_budget_incl_ffss_ind_type__c')));
            }
            budgetMap.put('hasBudget', Arc_Gen_Persistence_Utils.booleanFromYesNo((String)ahaData.get('arce__cust_budget_cyr_ind_type__c')));

        }
        return budgetMap;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that is responsible for generating the basic info indicators Object
      basic data information.
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 04/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param arce__Account_has_Analysis__c - prevAha Object
    * @return List<Map<String, Object>> with WS structure
    * @example addIndicators(ahaData, prevAha)
    * -----------------------------------------------------------------------------------------------
    **/
    private static List<Map<String, Object>> addIndicators(arce__Account_has_Analysis__c ahaData, arce__Account_has_Analysis__c prevAha, Map<String, Service_persistence__mdt> configsMap) {
        List<Map<String, Object>> listMapIndicators = new List<Map<String, Object>>();
        Map<String, String> mapIndicators = (Map<String, String>)JSON.deserialize(configsMap.get('finRiskIndicatorsConfig').api_field_Name__c, Map<String, String>.class);
        Map<String, String> indicatorsComments = (Map<String, String>)JSON.deserialize(configsMap.get('finRiskIndicatorsConfig').iasoVariables__c, Map<String, String>.class);
        for (string element : mapIndicators.keyset()) {
            if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get(element)) || Arc_Gen_ValidateInfo_utils.isFilled((String)prevAha.get(element))) {
                Map<String, Object> indicatorMap = new Map<String, Object>();
                if (Arc_Gen_ValidateInfo_utils.isFilled(indicatorsComments.get(element))) {
                    indicatorMap.put('comments',ahaData.get(indicatorsComments.get(element)));
                }
                indicatorMap.put('isActive',Arc_Gen_Persistence_Utils.booleanFromYesNo((String)ahaData.get(element)));
                indicatorMap.put('id',mapIndicators.get(element));
                listMapIndicators.add(indicatorMap);
            }
        }
        return listMapIndicators;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that is responsible for generating the basic info variables Object
      basic data information.
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 09/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param arce__Account_has_Analysis__c - prevAha Object
    * @return List<Map<String, Object>> with WS structure
    * @example addIndicators(ahaData, prevAha)
    * -----------------------------------------------------------------------------------------------
    **/
    private static List<Map<String, Object>> addVariables (arce__Account_has_Analysis__c ahaData, arce__Account_has_Analysis__c prevAha, Map<String, Service_persistence__mdt> configsMap) {
        List<Map<String, Object>> listMapVariables = new List<Map<String, Object>>();
        Map<String, String> mapVariables = (Map<String, String>)JSON.deserialize(configsMap.get('finRiskVariablesConfig').api_field_Name__c, Map<String, String>.class);
        Map<String, String> variablesComments = (Map<String, String>)JSON.deserialize(configsMap.get('finRiskVariablesConfig').iasoVariables__c, Map<String, String>.class);
        for (string element : mapVariables.keyset()) {
            if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get(element)) || Arc_Gen_ValidateInfo_utils.isFilled((String)prevAha.get(element))) {
                Map<String, Object> indicatorMap = new Map<String, Object>();
                if (Arc_Gen_ValidateInfo_utils.isFilled(variablesComments.get(element))) {
                    indicatorMap.put('comments',ahaData.get(variablesComments.get(element)));
                }
                indicatorMap.put('variableType',Arc_Gen_Persistence_Utils.defaultValueList((String)ahaData.get(element)));
                indicatorMap.put('id',mapVariables.get(element));
                listMapVariables.add(indicatorMap);
            }
        }
        return listMapVariables;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that is responsible for generating the basic info variations Object
      basic data information.
    * -----------------------------------------------------------------------------------------------
    * @Author  Javier Soto Carrascosa
    * @Date     Created: 09/04/2020
    * @param arce__Account_has_Analysis__c - aha Object
    * @param arce__Account_has_Analysis__c - prevAha Object
    * @return List<Map<String, Object>> with WS structure
    * @example addIndicators(ahaData, prevAha)
    * -----------------------------------------------------------------------------------------------
    **/
    private static List<Map<String, Object>> addVariations (arce__Account_has_Analysis__c ahaData, arce__Account_has_Analysis__c prevAha, Map<String, Service_persistence__mdt> configsMap) {
        List<Map<String, Object>> listMapVariations = new List<Map<String, Object>>();
        Map<String, String> mapVariations = (Map<String, String>)JSON.deserialize(configsMap.get('finRiskVariationsConfig').iasoVariables__c, Map<String, String>.class);
        for (string element : mapVariations.keyset()) {
            if (Arc_Gen_ValidateInfo_utils.isFilled((String)ahaData.get(element)) || Arc_Gen_ValidateInfo_utils.isFilled((String)prevAha.get(element))) {
                Map<String, Object> indicatorMap = new Map<String, Object>();
                indicatorMap.put('comments', ahaData.get(element));
                indicatorMap.put('id',mapVariations.get(element));
                listMapVariations.add(indicatorMap);
            }
        }
        return listMapVariations;
    }
}