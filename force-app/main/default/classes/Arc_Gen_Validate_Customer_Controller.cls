/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Validate_Customer_Controller
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2019-05-22
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Controller class for Arc_Gen_Validate_Customer_Service and
* Arc_Gen_Validate_Customer_Data.
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-05-23 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* |2019-10-03 mariohumberto.ramirez.contractor@bbva.com
*             Added new param 'ratingStatus' to ResponseWrapper.
* -----------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_Validate_Customer_Controller {
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_Validate_Customer_Controller controller = new Arc_Gen_Validate_Customer_Controller()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_Validate_Customer_Controller() {

    }

    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Wrapper that contains the response of the validateCustomer method
    * ----------------------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example ResponseWrapper wrapper = new ResponseWrapper()
    * ----------------------------------------------------------------------------------------------------
    **/
    public class ResponseWrapper {
        @AuraEnabled public List<Map<string,string>> gblResponse {get;set;}
        @AuraEnabled public String responseError {get;set;}
        @AuraEnabled public Boolean success {get;set;}
        @AuraEnabled public List<string> ratingStatus {get;set;}
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Returns a List<Map<string,string>> that contain the error message of the
    * Policies table validations
    * --------------------------------------------------------------------------------------
    * @param accHasAId of the object arce__Account_has_Analysis__c
    * @return a List<Map<string,string>> that contain the error message of the
    * Policies table validations
    * @example validateCustomer(accHasAId)
    * --------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static ResponseWrapper validateCustomer(string accHasAId) {
        ResponseWrapper wrapper = new ResponseWrapper();
        try {
            wrapper = Arc_Gen_Validate_Customer_Service.validateTable(accHasAId);
            wrapper.success = true;
        } catch (Exception exep) {
            wrapper.success = false;
            wrapper.responseError = exep.getTypeName() + ' : ' + exep.getMessage() + ' : ' + exep.getStackTraceString();
        }
        return wrapper;
    }
}