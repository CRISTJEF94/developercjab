/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Auto_Position_Service
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2020-01-28
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Service class for Arc_Gen_PotitionBankTable_Ctlr
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2020-01-28 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_Auto_Position_Service implements Arc_Gen_PotitionTable_Interface {
    /**
    * --------------------------------------------------------------------------------------
    * @Description get data to build table
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-01-28
    * @param recordId - id of the acc has analysis object
    * @return Arc_Gen_DataTable - wrapper with the info to build the table
    * @example getData(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static Arc_Gen_DataTable getData(Id recordId) {
        Arc_Gen_DataTable dataJson = new Arc_Gen_DataTable();
        dataJson.columns = Arc_Gen_PotitionBankTable_Service_Helper.getColumns(recordId);
        dataJson.data = Arc_Gen_Auto_Position_Service_Helper.getRows(recordId);
        List<arce__Account_has_Analysis__c> acchasAn = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<String>{(String)recordId});
        acchasAn[0].arce__last_update_position__c = System.now();
        Arc_Gen_AccHasAnalysis_Data.updateAccHasAnalysis(acchasAn);
        return dataJson;
    }
}