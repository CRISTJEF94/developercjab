/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_TabSet_service
* @Author   Angel Fuertes Gomez
* @Date     Created: 27/03/2019
* @Group    ARCE
* ------------------------------------------------------------------------------------------------------
* @Description Class that manages the tabs to be shown in the ARCE and the edition permits.
* ------------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-05-23 Angel Fuertes Gomez
*             Class creation.
* |2019-06-21 mariohumberto.ramirez.contractor@bbva.com
*             Modification of the methods getTemplatesByAccHasAnalysisJson,
*             generateListWrapperTab and generateWrapperTabSet.
* |2019-06-24 mariohumberto.ramirez.contractor@bbva.com
*             Change the class to SOC
* |2019-07-16 mariohumberto.ramirez.contractor@bbva.com
*             Add new line in getTemplatesByAccHasAnalysisJson method that call the method
*             hideDynFormSection in Arc_Gen_TabsetService_Helper class
* |2019-07-29 mariohumberto.ramirez.contractor@bbva.com
*             Add new method changeArceState
* |2019-08-23 mariohumberto.ramirez.contractor@bbva.com
*             Deleted line in getTemplatesByAccHasAnalysisJson method that was calling the method
*             hideDynFormSection in Arc_Gen_TabsetService_Helper class
* |2019-10-25 mariohumberto.ramirez.contractor@bbva.com
*             Update method getTemplatesByAccHasAnalysisJson
* |2019-10-28 mariohumberto.ramirez.contractor@bbva.com
*             Added new method getUnitChangeResponse()
* |2019-12-02 mariohumberto.ramirez.contractor@bbva.com
*             Add new method getColumnReduction()
* |2020-01-27 javier.soto.carrascosa@bbva.com
*             Add support for arce allocation
* |2019-01-28 mariohumberto.ramirez.contractor@bbva.com
*             Added logic to get outstanding amount from account wrapper
* -------------------------------------------------------------------------------------------------------
*/
public class Arc_Gen_TabSet_service {
    /**
        * @Description: internal code to know if the unit have been change
    */
    static final string SAME_UNIT = 'SameUnit';
    /**
        * @Description: List of string with the name of some fields in arce__limits_exposure__c object
    */
    static final List<string> FIELD_API_NAMES = new List<string>{'arce__current_approved_amount__c','arce__curr_apprv_uncommited_amount__c',
            'arce__curr_approved_commited_amount__c','arce__current_formalized_amount__c','arce__outstanding_amount__c',
            'arce__current_proposed_amount__c','arce__last_approved_amount__c'};
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 16/07/2019
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_service service = new Arc_Gen_TabSet_service()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_TabSet_service() {

    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that return a JSON with the information to create the Tabset component
    * and verify what type of policies table is shown in the analysis if the table is the standard
    * this method must update the data of the hiden typologies to zero
    * -----------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 16/07/2019
    * @param recordId - Id of the current account has analisys object
    * @return String with the information to create the Tabs
    * @example getTemplatesByAccHasAnalysisJson(recordId)
    * -----------------------------------------------------------------------------------------------
    **/
    public static String getTemplatesByAccHasAnalysisJson(Id recordId) {
        final List<arce__Account_has_Analysis__c> accHasAnalysis = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<Id>{recordId});
        final List<arce__Sector__c> sector = Arc_Gen_Sector_Data.getSectorByDeveloperName(new List<String>{accHasAnalysis[0].arce__sector_rt_type__c});
        final List<Arc_Gen_TabSet_controller.WrapperTabSet> listWrapper = Arc_Gen_TabsetService_Helper.generateListWrapperTab(recordId, sector[0]);
        final String listJSON = JSON.serialize(listWrapper);
        accHasAnalysis[0].arce__sector_rt_id__c = sector[0].Id;
        accHasAnalysis[0].arce__outstanding_carrousel__c = Arc_Gen_TabsetService_Helper.getOutstandingFromTabClient(recordId);
        Arc_Gen_AccHasAnalysis_Data.upsertObjects(new List<arce__Account_has_Analysis__c>{accHasAnalysis[0]});
        if (accHasAnalysis[0].Arc_Gen_EEGRP__c == null || accHasAnalysis[0].Arc_Gen_EEGRP__c == '2') {
            Arc_Gen_TabsetService_Helper.update2zerohidenTypologies(recordId);
        }
        return listJSON;
    }
    /**
    * ---------------------------------------------------------------------------------------------------
    * @Description - Method that verify if you have permission to edit
    * ---------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 16/07/2019
    * @param blockPolicies - boolean with the information to make visible the policies tab
    * @param recordId - id of the current account has analysis object
    * @return a string with edit permission data
    * @example getPermissionToEdit(recordId)
    * ---------------------------------------------------------------------------------------------------
    **/
    public static String getPermissionToEdit(Id recordId) {
        Set<String> permissionSet = new Set<String>();
        final List<ID> records = new List<ID>();
        records.add(recordId);
        List<arce__Account_has_Analysis__c> arce = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<Id>{recordId});
        List<Id> lstIds = new List<Id>();
        lstIds.add(arce[0].arce__Analysis__c);
        final Type inter = Type.forName('arcealloc.Allocation_Service');
        final boolean custAlloc = String.isNotBlank(String.valueOf(inter)) ? ((Map<Id,boolean>) ((Callable) inter.newInstance()).call('checkBulkPrivileges', new Map<String, Object> {'accHasAnlysIdsLst' => records})).get(recordId) : true ;
        Map<String,Set<String>> permissionToEditMap = dwp_dace.DynamicActionsEngine_helper.getAvailableActions(lstIds,'arce__Analysis__c','Arc_Gen_Edition');
        if ( permissionToEditMap.get(arce[0].arce__Analysis__c) != null) {
            permissionSet.addAll(permissionToEditMap.get(arce[0].arce__Analysis__c));
        }
        return permissionSet.contains('Arc_Gen_Edit') && custAlloc ? 'true':'false';
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that change the status of an Arce Analysis
    * -----------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 29/07/2019
    * @param recordId - Id of the current account has analisys object
    * @return true/false
    * @example changeArceState(recordId)
    * -----------------------------------------------------------------------------------------------
    **/
    public static Boolean changeArceState(Id recordId) {
        return Arc_Gen_TabsetService_Helper.changeArceStateHelper(recordId);
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that change the unit of the record in the policy table
    * -----------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 28/10/2019
    * @param recordId - Id of the current account has analisys object
    * @return String
    * @example getUnitChangeResponse(recordId)
    * -----------------------------------------------------------------------------------------------
    **/
    public static String getUnitChangeResponse(Id recordId) {
        final String response;
        Arc_Gen_GenericUtilities.ConversionWrapper wrapperConversion = new Arc_Gen_GenericUtilities.ConversionWrapper();
        wrapperConversion.objData = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<String>{(String)recordId})[0];
        if (wrapperConversion.objData.get('arce__magnitude_unit_type__c') == wrapperConversion.objData.get('arce__prev_magnitude_unit_type__c')) {
            response = SAME_UNIT;
        } else {
            wrapperConversion.fieldNames = FIELD_API_NAMES;
            wrapperConversion.actualData = 'arce__magnitude_unit_type__c';
            wrapperConversion.previousData = 'arce__prev_magnitude_unit_type__c';
            wrapperConversion.objDataLts = Arc_Gen_LimitsExposures_Data.getExposureData(new List<Id>{recordId});
            response = Arc_Gen_GenericUtilities.convertUnits(wrapperConversion);
            arce__Account_has_Analysis__c accHasAn = (arce__Account_has_Analysis__c)wrapperConversion.objData;
            accHasAn.arce__prev_magnitude_unit_type__c = accHasAn.arce__magnitude_unit_type__c;
            Arc_Gen_AccHasAnalysis_Data.upsertObjects(new List<arce__Account_has_Analysis__c>{accHasAn});
        }
        return response;
    }
    /**
    * ---------------------------------------------------------------------------------------------------
    * @Description - Method that return the column configuration for policie table
    * ---------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 02/12/2019
    * @param recordId - id of the current account has analysis object
    * @return a string with configuration for policie table
    * @example getColumnReduction(recordId)
    * ---------------------------------------------------------------------------------------------------
    **/
    public static String getColumnReduction(Id recordId) {
        return Arc_Gen_ArceAnalysis_Data.getArceAnalysisData(new List<Id>{Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<String>{(String) recordId})[0].arce__Analysis__c})[0].arce__wf_status_id__c == '08' ? '7,1;0,0' : '0,0;0,0';
    }
}