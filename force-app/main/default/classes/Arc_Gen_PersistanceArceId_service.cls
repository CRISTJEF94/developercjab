/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_PersistanceArceId_service
* @Author  Ricardo Almanza Angeles  ricardo.almanza.contractor@bbva.com
* @Date     Created: 03/09/2019
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Uses IASO to conect to Risk Assesment service
* ------------------------------------------------------------------------------------------------
* @Changes
* 28/07/2019 ricardo.almanza.contractor@bbva.com
* Class creation.
* 4/11/2019 juanmanuel.perez.ortiz.contractor@bbva.com
* Add boolean parameter hasFinancialSponsor.
* 4/11/2019 juanmanuel.perez.ortiz.contractor@bbva.com
* Modified value in analysisId parameter.
* 6/12/2019 jhovanny.delacruz.cruz@bbva.com
* Encryption funcionality enabled
* 18/12/2019 franciscojavier.bueno@bbva.com
* Service Restructuring
* 13/01/2020 franciscojavier.bueno@bbva.com
* Risk Assessment Call Correction
* 29/01/2020 javier.soto.carrascosa@bbva.com
* Add fixes and manage risk assessment error
* 01/04/2020 javier.soto.carrascosa@bbva.com
* Support for local id
* -----------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_PersistanceArceId_service {
    /**
        * @Description: string with the service name
    */
        Static Final String SERVICE_NAME = 'riskassessments';
    /**
        * @Description: string with created resource response code (change to 200 if in scratch with mocks)
    */
        Static Final String COD_CREATED = '200';//ORIGINAL VALUE 201
    /**
    *-------------------------------------------------------------------------------
    * @description Method that sets the parameters to the persistance service
    --------------------------------------------------------------------------------
    * @author ricardo.almanza.contractor@bbva.com
    * @date 03/09/2019
    * @param String clientNumber
    * @return Arc_Gen_ServiceAndSaveResponse - Wrapper that contains the response of the called service and the DML operation
    * @example public static Arc_Gen_ServiceAndSaveResponse setupRiskAssessments(String analysisId,String clientNumber)
    **/
    public Arc_Gen_ServiceAndSaveResponse setupRiskAssHeader(String analysisId) {
        Arc_Gen_ServiceAndSaveResponse serviceAndSaveResp = new Arc_Gen_ServiceAndSaveResponse();
        final arce__Account_has_Analysis__c header = Arc_Gen_PersistanceArceId_data.persistanceDataAcc(analysisId);
        if (header.arce__risk_assessment_persisted__c == false) {
            serviceAndSaveResp = riskAssHeaderCallout(analysisId, header);
            serviceAndSaveResp = processRiskAss(serviceAndSaveResp, true, header.Id);
        } else {
            serviceAndSaveResp.serviceCode = COD_CREATED;
            serviceAndSaveResp.serviceMessage = 'Resource has already been created in Oracle';
            serviceAndSaveResp.updatefields = new Map<String,Object>();
            serviceAndSaveResp.updatefields.put('arce__risk_assessment_persisted__c', true);
        }
        return serviceAndSaveResp;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Method that sets the parameters to the persistance service
    --------------------------------------------------------------------------------
    * @author ricardo.almanza.contractor@bbva.com
    * @date 03/09/2019
    * @param String clientNumber
    * @return Arc_Gen_ServiceAndSaveResponse - Wrapper that contains the response of the called service and the DML operation
    * @example public static Arc_Gen_ServiceAndSaveResponse setupRiskAssessments(String analysisId,String clientNumber)
    **/
    public Arc_Gen_ServiceAndSaveResponse setupRiskAssessments(String analysisId, String customer) {
        Arc_Gen_ServiceAndSaveResponse serviceAndSaveResp = new Arc_Gen_ServiceAndSaveResponse();
        final arce__Account_has_Analysis__c accSubs = Arc_Gen_PersistanceArceId_data.persistanceDataS(analysisId,customer);
        if (accSubs.arce__risk_assessment_persisted__c == false) {
            serviceAndSaveResp = riskAssCallout(analysisId, customer, accSubs);
            serviceAndSaveResp = processRiskAss(serviceAndSaveResp, false, accSubs.Id);
        } else {
            serviceAndSaveResp.serviceCode = COD_CREATED;
            serviceAndSaveResp.serviceMessage = 'Resource has already been created in Oracle';
            serviceAndSaveResp.updatefields = new Map<String,Object>();
            serviceAndSaveResp.updatefields.put('arce__risk_assessment_persisted__c', true);
        }
        return serviceAndSaveResp;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Method that sets the parameters to the persistance service
    --------------------------------------------------------------------------------
    * @author ricardo.almanza.contractor@bbva.com
    * @date 03/09/2019
    * @param String analysisId
    * @param String clientNumber
    * @param arce__Account_has_Analysis__c accHas
    * @return Arc_Gen_ServiceAndSaveResponse - Wrapper that contains the response of the called service and the DML operation
    * @example riskAssCallout (String analysisId, String customer, arce__Account_has_Analysis__c accHas)
    **/
    public Arc_Gen_ServiceAndSaveResponse riskAssCallout (String analysisId, String customer, arce__Account_has_Analysis__c accSubs) {
        Arc_Gen_ServiceAndSaveResponse serviceAndSaveResp = new Arc_Gen_ServiceAndSaveResponse();
        Arc_Gen_getIASOResponse.serviceResponse iasoResponse = new Arc_Gen_getIASOResponse.serviceResponse();
        final arce__Account_has_Analysis__c header = Arc_Gen_PersistanceArceId_data.persistanceDataAcc(analysisId);
        final Map<Id, Arc_Gen_Account_Wrapper> mapWrap = Arc_Gen_Account_Locator.getAccountInfoById(new List<Id>{accSubs.arce__Customer__c});
        String encryptedClient = Arc_Gen_CallEncryptService.getEncryptedClient(mapWrap.get(accSubs.arce__Customer__c).accNumber);
        final String ChildARCE = accSubs.Name;
        final String ParentARCE = header.Name;
        final String NumClient = encryptedClient;
        final String TypeArc = mapWrap.get(accSubs.arce__Customer__c).participantType;
        final String NameClient = mapWrap.get(accSubs.arce__Customer__c).name;
        final String docNumber = mapWrap.get(accSubs.arce__Customer__c).docNumber;
        final String docType = mapWrap.get(accSubs.arce__Customer__c).docType;
        final String FS = header.arce__Customer__r.controlled_by_sponsor_type__c == '1' ? 'true' : 'false';
        iasoResponse = Arc_Gen_getIASOResponse.calloutIASO(SERVICE_NAME,'{"ChildARCE" : "' + ChildARCE +'","ParentARCE" : "' + ParentARCE +'","docNumber" : "' + docNumber +'","docType" : "' + docType +'","NumClient" : "' + NumClient +'","TypeArc" : "' + TypeArc +'","NameClient" : "' +  NameClient +'","FS" : "' + FS +'"}');
        serviceAndSaveResp.serviceCode = iasoResponse.serviceCode;
        serviceAndSaveResp.serviceMessage = iasoResponse.serviceMessage;
        serviceAndSaveResp.serviceHeaders = iasoResponse.serviceHeaders;
        return serviceAndSaveResp;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Method that sets the parameters to the persistance service
    --------------------------------------------------------------------------------
    * @author ricardo.almanza.contractor@bbva.com
    * @date 03/09/2019
    * @param String analysisId
    * @param String clientNumber
    * @param arce__Account_has_Analysis__c accHas
    * @return Arc_Gen_ServiceAndSaveResponse - Wrapper that contains the response of the called service and the DML operation
    * @example riskAssCallout (String analysisId, String customer, arce__Account_has_Analysis__c accHas)
    **/
    public Arc_Gen_ServiceAndSaveResponse riskAssHeaderCallout(String analysisId, arce__Account_has_Analysis__c header) {
        Arc_Gen_ServiceAndSaveResponse serviceAndSaveResp = new Arc_Gen_ServiceAndSaveResponse();
        Arc_Gen_getIASOResponse.serviceResponse iasoResponse = new Arc_Gen_getIASOResponse.serviceResponse();
        final Map<Id, Arc_Gen_Account_Wrapper> mapWrap = Arc_Gen_Account_Locator.getAccountInfoById(new List<Id>{header.arce__Customer__c});
        String encryptedClient = Arc_Gen_CallEncryptService.getEncryptedClient(mapWrap.get(header.arce__Customer__c).accNumber);
        final String ChildARCE = header.Name;
        final String ParentARCE = header.Name;
        final String NumClient = encryptedClient;
        final String TypeArc = mapWrap.get(header.arce__Customer__c).participantType;
        final String NameClient = mapWrap.get(header.arce__Customer__c).name;
        final String docNumber = mapWrap.get(header.arce__Customer__c).docNumber;
        final String docType = mapWrap.get(header.arce__Customer__c).docType;
        final String FS = header.arce__Customer__r.controlled_by_sponsor_type__c == '1' ? 'true' : 'false';
        iasoResponse = Arc_Gen_getIASOResponse.calloutIASO(SERVICE_NAME,'{"ChildARCE" : "' + ChildARCE +'","ParentARCE" : "' + ParentARCE +'","docNumber" : "' + docNumber +'","docType" : "' + docType +'","NumClient" : "' + NumClient +'","TypeArc" : "' + TypeArc +'","NameClient" : "' +  NameClient +'","FS" : "' + FS +'"}');
        serviceAndSaveResp.serviceCode = iasoResponse.serviceCode;
        serviceAndSaveResp.serviceMessage = iasoResponse.serviceMessage;
        serviceAndSaveResp.serviceHeaders = iasoResponse.serviceHeaders;
        return serviceAndSaveResp;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Method that sets the parameters to the persistance service
    --------------------------------------------------------------------------------
    * @author ricardo.almanza.contractor@bbva.com
    * @date 03/09/2019
    * @param String clientNumber
    * @return Arc_Gen_ServiceAndSaveResponse - Wrapper that contains the response of the called service and the DML operation
    * @example public static Arc_Gen_ServiceAndSaveResponse setupRiskAssessments(String analysisId,String clientNumber)
    **/
    public Arc_Gen_ServiceAndSaveResponse processRiskAss (Arc_Gen_ServiceAndSaveResponse riskAss, boolean updateData, Id ahaId) {
        String localField = Arc_Gen_Arceconfigs_locator.getConfigurationInfo('LocalIdentifier')[0].Value1__c;
        if (riskAss.serviceCode == COD_CREATED && updateData) {
            arce__account_has_analysis__c upAha = new arce__account_has_analysis__c(Id = ahaId);
            upAha.arce__risk_assessment_persisted__c = true;
            upAha = updateAhaLocalId(upAha, localField, riskAss);
            Arc_Gen_AccHasAnalysis_Data.updateAccHasAnalysis(new List<arce__account_has_analysis__c>{upAha});
        } else if (riskAss.serviceCode == COD_CREATED && !updateData) {
            riskAss.updatefields = new Map<String,Object>();
            riskAss.updatefields.put('arce__risk_assessment_persisted__c', true);
            updateMapLocalId(localField, riskAss);
        } else {
            CalloutException e = new CalloutException();
            e.setMessage(System.Label.Arc_Gen_OraclePersistenceError);
            throw e;
        }
        return riskAss;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Method that update account_has_analysis with local field
    --------------------------------------------------------------------------------
    * @author javier.soto.carrascosa@bbva.com
    * @date 01/04/2020
    * @param arce__account_has_analysis__c aha
    * @param String localField
    * @param Arc_Gen_ServiceAndSaveResponse riskAss
    * @return arce__account_has_analysis__c -updated account has analysis
    * @example private arce__account_has_analysis__c updateAhaLocalId (aha,localField,riskAss)
    **/
    private arce__account_has_analysis__c updateAhaLocalId (arce__account_has_analysis__c aha, string localField, Arc_Gen_ServiceAndSaveResponse riskAss) {
        if (Arc_Gen_ValidateInfo_utils.isFilled(localField)) {
            aha.put(localField,getIdFromHeader(riskAss));
        }
        return aha;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Method that updates Map with local field
    --------------------------------------------------------------------------------
    * @author javier.soto.carrascosa@bbva.com
    * @date 01/04/2020
    * @param String localField
    * @param Arc_Gen_ServiceAndSaveResponse riskAss
    * @return Arc_Gen_ServiceAndSaveResponse - Wrapper that contains the response of the called service and the DML operation
    * @example private Arc_Gen_ServiceAndSaveResponse updateMapLocalId (string localField, Arc_Gen_ServiceAndSaveResponse riskAss)
    **/
    private Arc_Gen_ServiceAndSaveResponse updateMapLocalId (string localField, Arc_Gen_ServiceAndSaveResponse riskAss) {
        if (Arc_Gen_ValidateInfo_utils.isFilled(localField)) {
            riskAss.updatefields.put(localField,getIdFromHeader(riskAss));
        }
        return riskAss;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Method that retrieves local Id from risk assessment header
    --------------------------------------------------------------------------------
    * @author javier.soto.carrascosa@bbva.com
    * @date 01/04/2020
    * @param Arc_Gen_ServiceAndSaveResponse riskAss
    * @return String
    * @example private String getIdFromHeader (Arc_Gen_ServiceAndSaveResponse riskAss)
    **/
    private String getIdFromHeader (Arc_Gen_ServiceAndSaveResponse riskAss) {
        final Map<String, String> header = riskAss.serviceHeaders;
        String localId = '';
        if(Arc_Gen_ValidateInfo_utils.hasInfoMap(header)) {
            final String riskAssLocation = header.get('location');
            if (Arc_Gen_ValidateInfo_utils.isFilled(riskAssLocation)) {
                localId = riskAssLocation.substringAfterLast('/');
            }
        }
        return localId;
    }
}