/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Workflow_test
* @Author   Juan Ignacio Hita Manso juanignacio.hita.contractor@bbva.com
* @Date     Created: 2019-07-01
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Controller APEX
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-12-03 juanignacio.hita.contractor@bbva.com
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
@isTest
public class Arc_Gen_Workflow_test {
    /**
    * --------------------------------------------------------------------------------------
    * @Description setup test
    * --------------------------------------------------------------------------------------
    * @Author   javier.soto.carrascosa@bbva.com
    * @Date     Created: 2020-01-08
    * @param void
    * @return void
    * @example setup()
    * --------------------------------------------------------------------------------------
    **/
    @testSetup static void setup() {
        Arc_UtilitysDataTest_tst.setupAcccounts();
        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{'G000001'});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get('G000001');
        arce__Analysis__c arceobj = new arce__Analysis__c();
        arceobj.Name = 'arce__Analysisobj';
        arceobj.arce__analysis_customer_relation_type__c = '01';
        arceobj.CurrencyIsoCode = 'EUR';
        arceobj.arce__Group__c = groupAccount.accId;
        arceobj.arce__Rating__c = 'Hot';
        arceobj.arce__Stage__c = '1';
        arceobj.arce__wf_status_id__c = '02';
        insert arceobj;
        arce__Account_has_Analysis__c arceAccounthasAnalysisobj = new arce__Account_has_Analysis__c();
        arceAccounthasAnalysisobj.currencyIsoCode = 'EUR';
        arceAccounthasAnalysisobj.arce__main_subsidiary_ind_type__c = true;
        arceAccounthasAnalysisobj.arce__InReview__c = true;
        arceAccounthasAnalysisobj.arce__Analysis__c = arceobj.Id;
        arceAccounthasAnalysisobj.arce__smes_eur_comuty_defn_type__c = '1';
        arceAccounthasAnalysisobj.arce__ll_before_adj_ind_type__c = '1';
        arceAccounthasAnalysisobj.arce__ll_before_adj_clsfn_type__c = 'NI';
        arceAccounthasAnalysisobj.arce__ll_after_adj_ind_type__c = '1';
        arceAccounthasAnalysisobj.arce__ll_after_adj_clsfn_type__c = 'NI';
        arceAccounthasAnalysisobj.arce__Customer__c = groupAccount.accId;
        insert arceAccounthasAnalysisobj;
    }
    /**
    * @Method:  test for method constructor
    * @Description: testing method.
    */
    @isTest
    static void testWorkFlowService500() {
        final Arc_Gen_Workflow_Interface workflowController = Arc_Gen_Workflow_Service.workflowClass();
        arce__Analysis__c analysis = [SELECT Id FROM arce__Analysis__c LIMIT 1];
        Test.startTest();
        final Arc_Gen_Delegation_Wrapper wrapper = workflowController.getDelegation('1', analysis.Id, 'OTRA');
        System.assertEquals(wrapper.ambit, '1', 'Workflow service test');
        Test.stopTest();
    }

}