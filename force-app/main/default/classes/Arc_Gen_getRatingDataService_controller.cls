/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_getRatingDataService_controller
* @Author   Eduardo Efraín Hernández Rendón  eduardoefrain.hernandez.contractor@bbva.com
* @Date     Created: 22/10/2019
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Obtains the response of the rating calculation service
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |28/10/2019 eduardoefrain.hernandez.contractor@bbva.com
*             Class creation.
* |23/7/2019  eduardoefrain.hernandez.contractor@bbva.com
*             Added comments.
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_getRatingDataService_controller {
    /**
    * @Description: Error Literal
    */
    private final static String ERROR_TEXT = 'Error';
    /**
    * @Description: False Literal
    */
    private final static String FALSE_TEXT = 'false';
/**
* @Class: ClientData
* @Description: Wrapper that contain all the rating service parameters
* @author BBVA
*/
    public class ClientData {
        /**
        * @Description: Indicates name of the customer
        */
        @AuraEnabled public String customerName {get;set;}
        /**
        * @Description: Indicates if the numeber of the customer
        */
        @AuraEnabled public String customerId {get;set;}
    }
/**
*-------------------------------------------------------------------------------
* @description Method that gets the client data
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId
* @return ClientData - A wrapper with the data of the client
* @example public static ClientData getCustomerData(String analysisId
**/
    @AuraEnabled
    public static ClientData getCustomerData(String analysisId) {
        ClientData data = new ClientData();
        List<String> customerData = new List<String>();
        customerData = Arc_Gen_getRatingDataService_service.getCustomerData(analysisId);
        data.customerName = customerData[0];
        data.customerId = customerData[1];
        Return data;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that gets the response FROM the service class
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId
* @param String customerId
* @return Arc_Gen_ServiceAndSaveResponse - A wrapper with the result of a DML operation and service call
* @example public static Arc_Gen_ServiceAndSaveResponse setupRating(String analysisId,String customerId)
**/
    @AuraEnabled
    public static Arc_Gen_ServiceAndSaveResponse setupRating(String analysisId, String customerId, String serviceMock) {
        Arc_Gen_ServiceAndSaveResponse response = new Arc_Gen_ServiceAndSaveResponse();
        try {
            if(serviceMock == ERROR_TEXT) {
                throw new CalloutException();
            }
            response = Arc_Gen_getRatingDataService_service.setupRating(analysisId, customerId, null, serviceMock);
        } catch(CalloutException ex) {
            response.saveStatus = FALSE_TEXT;
            response.serviceMessage = ex.getMessage();
        }
        Return response;
    }
}