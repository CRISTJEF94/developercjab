/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Validate_Customer_Data
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2019-05-22
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Data class for Arc_Gen_Validate_Customer_Service.
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-05-23 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* |2019-10-03 mariohumberto.ramirez.contractor@bbva.com
*             Change the method checkCustomerFlag, now return a List<arce__Account_has_Analysis__c>
* |2019-12-30 mariohumberto.ramirez.contractor@bbva.com
*             Added new method getFieldStageControl
* |2019-12-04 manuelhugo.castillo.contractor@bbva.com
*             Modify method 'getNameClient' replace Account to AccountWrapper
* |2020-17-01 mariohumberto.ramirez.contractor@bbva.com
*             Change arce__Customer__r.ParentId for arce__group_asset_header_type__c in queries
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_Validate_Customer_Data {

    /**
    * @Description: string with the value of preparing analysis status
    */
    static final string PENDING_SANCTION = '08';
    /**
    * @Description: string with value of contrasting analysis status
    */
    static final string IN_SANCTION = '2';
    /**
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_Validate_Customer_Data data = new Arc_Gen_Validate_Customer_Data()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_Validate_Customer_Data() {

    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Returns the import of the typologies.
    * --------------------------------------------------------------------------------------
    * @param accHasAId - Ids of the object arce__Account_has_Analysis__c
    * @return a List<arce__limits_exposures__c> that contain the import of the typologies
    * @example getImport(accHasAId)
    * --------------------------------------------------------------------------------------
    **/
    public static List<arce__limits_exposures__c> getExposures(List<string> accHasAId) {
        return [SELECT arce__limits_typology_id__c,arce__limits_typology_id__r.Name, arce__limits_typology_id__r.arce__risk_typology_parent_id__c, arce__limits_typology_id__r.arce__risk_typology_level_id__c, arce__Product_id__c, arce__Product_id__r.Name, arce__limits_exposures_parent_id__c, Id,  arce__account_has_analysis_id__r.arce__Customer__r.Name, arce__current_approved_amount__c, arce__curr_apprv_uncommited_amount__c, arce__curr_approved_commited_amount__c, arce__current_formalized_amount__c, arce__outstanding_amount__c, arce__current_proposed_amount__c, arce__last_approved_amount__c FROM arce__limits_exposures__c WHERE arce__account_has_analysis_id__c = :accHasAId AND arce__account_Id__c = null];
    }

    /**
    * -----------------------------------------------------------------------------------------
    * @Description check how many clients exist in the arce Analisys
    * -----------------------------------------------------------------------------------------
    * @param accHasAId Id of the object arce__Account_has_Analysis__c
    * @return a List<arce__Account_has_Analysis__c> that contain the id of the filial clients
    * @example checkGroup(accHasAId)
    * -----------------------------------------------------------------------------------------
    **/
    public static arce__Account_has_Analysis__c checkGroup(string accHasAId) {
        return [SELECT arce__Customer__c, arce__Customer__r.Name,arce__Customer__r.ParentId, arce__Analysis__c, arce__Analysis__r.arce__Group__c, arce__Analysis__r.arce__Group__r.Name FROM arce__Account_has_Analysis__c WHERE Id = :accHasAId];
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description return the map fields of the policy tab
    * --------------------------------------------------------------------------------------
    * @param section contains the name of the section in dynamic form
    * @return a List<dyfr__Field_Config__c> that contain the map fields of the policy tab
    * @example getFieldsBySection(accHasAId, section)
    * --------------------------------------------------------------------------------------
    **/
    public static List<dyfr__Field_Config__c> getFieldsBySection(List<string> section , String templateName) {
        return [SELECT Id, dyfr__Section_name__c, dyfr__DeveloperName__c, dyfr__Map_field__c, dyfr__Label__c, dyfr__Visibility_control_field__c, dyfr__Visibility_control_value__c, dyfr__Visibility_condition__c FROM dyfr__Field_Config__c WHERE dyfr__Section_name__c = :section AND dyfr__Tab__r.dyfr__Template__r.Name = :templateName AND dyfr__Type__c != 'lightning component' AND dyfr__Read_only__c = false AND dyfr__Type__c != 'blank'];
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description return the data of the stage control field
    * --------------------------------------------------------------------------------------
    * @param mapfield - api name of the field
    * @return a dyfr__Field_Config__c object
    * @example getFieldStageControl(accHasAId, section)
    * --------------------------------------------------------------------------------------
    **/
    public static dyfr__Field_Config__c getFieldStageControl(String mapField, String templateName) {
        return [SELECT Id, dyfr__Section_name__c, dyfr__DeveloperName__c, dyfr__Map_field__c, dyfr__Label__c, dyfr__Visibility_control_field__c, dyfr__Visibility_control_value__c, dyfr__Visibility_condition__c FROM dyfr__Field_Config__c WHERE dyfr__Map_field__c = :mapField AND dyfr__Tab__r.dyfr__Template__r.Name = :templateName AND dyfr__Type__c != 'lightning component' AND dyfr__Type__c != 'blank'];
    }

    /**
    * ----------------------------------------------------------------------------------------------
    * @Description return the real values in some fields of the policy tab
    * ----------------------------------------------------------------------------------------------
    * @param accHasAId Id of the object arce__Account_has_Analysis__c
    * @param mapfields contains the name of the map fields in dynamic form
    * @return a List<arce__Account_has_Analysis__c> that contains the real values in some fields of
    * the policy tab
    * @example getValues(accHasAId, mapfields)
    * ----------------------------------------------------------------------------------------------
    **/
    public static List<arce__Account_has_Analysis__c> getValues(string accHasAId, string mapfields) {
		System.debug('mapfields:'+mapfields);
        final string query = String.escapeSingleQuotes('SELECT ' + mapfields + ' FROM arce__Account_has_Analysis__c WHERE Id = :accHasAId');
        return Database.query(query);
    }

    /**
    * ----------------------------------------------------------------------------------------------
    * @Description return the ids of child accounts
    * ----------------------------------------------------------------------------------------------
    * @param parenId Account
    * @return a List<arce__Account_has_Analysis__c> that contains the child ids of a parent account
    * @example getIdsOfChildAccount(parenId)
    * ----------------------------------------------------------------------------------------------
    */
    public static List<arce__Analysis__c> getIdsOfChildAccount(Id groupId, Id arceId) {
        return [SELECT Id, (SELECT Id, arce__Customer__c, arce__Customer__r.Name From arce__Account_has_Analysis__r WHERE arce__InReview__c = true AND arce__group_asset_header_type__c = '2') FROM arce__Analysis__c WHERE arce__Group__c = :groupId AND Id = :arceId];
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description return the name of an account
    * --------------------------------------------------------------------------------------
    * @param id of arce__account_has_analysis_id__c
    * @return a string that contains the name of an account
    * @example getNameClient(id)
    * --------------------------------------------------------------------------------------
    **/
    public static string getNameClient(string id) {
        arce__Account_has_Analysis__c customer = [SELECT arce__Customer__c FROM arce__Account_has_Analysis__c WHERE Id = :id];
        List<Id> lstIds = new List<Id>{customer.arce__Customer__c};
        Map<Id, Arc_Gen_Account_Wrapper> accWrap = Arc_Gen_Account_Locator.getAccountInfoById(lstIds);
        string ret = accWrap.get(customer.arce__Customer__c).name;
        return ret;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Method that active a customer Flag
    * --------------------------------------------------------------------------------------
    * @param recordId Id of arce__account_has_analysis_id__c
    * @return void
    * @example activeCustomerFlag(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static void activeCustomerFlag(string recordId, string cases) {
        if (cases == 'Active') {
            arce__Account_has_Analysis__c arceAccHasAn = [SELECT id, arce__Analysis__c FROM arce__Account_has_Analysis__c WHERE Id = :recordId];
            arceAccHasAn.arce_ctmr_flag__c = true;
            upsert arceAccHasAn;
        } else {
            arce__Account_has_Analysis__c arceAccHasAn = [SELECT id, arce__Analysis__c FROM arce__Account_has_Analysis__c WHERE Id = :recordId];
            arceAccHasAn.arce_ctmr_flag__c = false;
            upsert arceAccHasAn;
        }
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description Method to consult the field arce_ctmr_flag__c
    * --------------------------------------------------------------------------------------
    * @param ids - List<Id> of arce__account_has_analysis_id__c
    * @return List<arce__Account_has_Analysis__c>
    * @example checkCustomerFlag(ids)
    * --------------------------------------------------------------------------------------
    **/
    public static List<arce__Account_has_Analysis__c> checkCustomerFlag(List<Id> ids) {
        return [SELECT id, arce_ctmr_flag__c, arce__Customer__r.Name FROM arce__Account_has_Analysis__c WHERE Id = :ids];
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description Method that change the status of an Arce Analysis
    * --------------------------------------------------------------------------------------
    * @param recordId Id of arce__account_has_analysis_id__c
    * @return void
    * @example changeStatusArce(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static void changeStatusArce(string recordId) {
        final Id arceId = [SELECT arce__Analysis__c FROM arce__Account_has_Analysis__c WHERE Id = :recordId].arce__Analysis__c;
        List<arce__Account_has_Analysis__c> accHasAnLts = Arc_Gen_GenericUtilities.getAccHasAnalysis(arceId);
        for (arce__Account_has_Analysis__c acchasAn: accHasAnLts) {
            acchasAn.arce_ctmr_flag__c = false;
            acchasAn.arce__anlys_wkfl_sbanlys_status_type__c = '3';
        }
        arce__Analysis__c analysis = [SELECT id,arce__wf_status_id__c,arce__Stage__c,Temporal_status__c FROM arce__Analysis__c WHERE id=:arceId];
        if(analysis.arce__wf_status_id__c == PENDING_SANCTION && analysis.arce__Stage__c == IN_SANCTION){
            analysis.arce__wf_status_id__c = '08';
        }else{
            analysis.arce__wf_status_id__c = '03';
        }
        analysis.Temporal_status__c = true;
        upsert analysis;
        upsert accHasAnLts;
    }
}