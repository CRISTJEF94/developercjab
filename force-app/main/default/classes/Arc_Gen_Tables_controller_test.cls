/*------------------------------------------------------------------
*Author:        Diego Miguel Tamarit - diego.miguel.contractor@bbva.com / dmiguelt@minsait.com
*Project:      	ARCE - BBVA Bancomer
*Description:   Test class for code coverage of:
Arc_Gen_Tables_controller
*_______________________________________________________________________________________
*Version    Date           Author                               Description
*1.0        11/04/2019     diego.miguel.contractor@bbva.com    	Creación.
-----------------------------------------------------------------------------------------*/
@isTest
public class Arc_Gen_Tables_controller_test {

    @isTest static void testGetTableJsonComponent() {
        String collectionType = String.valueOf((Math.random()*10000).round());
        String recordId = String.valueOf((Math.random()*10000).round());
        Test.startTest();
        Arc_Gen_Tables_controller tableController = new Arc_Gen_Tables_controller();
        String table = tableController.getTableJsonComponent(collectionType, recordId);
        Boolean isValid = table.contains(collectionType) && table.contains(recordId);
        system.assertEquals(true, isValid,'testGetTableJsonComponent json table do not contains correct values');
        Test.stopTest();
    }

}