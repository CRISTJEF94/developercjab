/**
* @File Name          : Arc_Gen_SetPoliciesVariables_test.cls
* @Description        : Test class for Gen_SetPoliciesVariables
* @Author             : luisruben.quinto.munoz@bbva.com
* @Group              : ARCE
* @Last Modified By   : luisruben.quinto.munoz@bbva.com
* @Last Modified On   : 23/7/2019 19:31:32
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    7/5/2019 23:44:32   diego.miguel.contractor@bbva.com     Added comments
* 1.1    12/7/2019 18:18:07   luisruben.quinto.munoz@bbva.com     Added No_de_Cliente__c
* 1.2    8/1/2020 18:21:09  javier.soto.carrascosa@bbva.com   Adapt test classess with account wrapper and setupaccounts
**/
@isTest
public class Arc_Gen_SetPoliciesVariables_test {
    /**
    * --------------------------------------------------------------------------------------
    * @Description setup test
    * --------------------------------------------------------------------------------------
    * @Author   javier.soto.carrascosa@bbva.com
    * @Date     Created: 2020-01-08
    * @param void
    * @return void
    * @example setup()
    * --------------------------------------------------------------------------------------
    **/
    @testSetup static void setup() {
    Arc_UtilitysDataTest_tst.setupAcccounts();
    }
    public static arce__Analysis__c setAnalysis(String name) {
        arce__Analysis__c analysis = new arce__Analysis__c(
            Name = name
        );
        Insert analysis;
        Return analysis;
    }
    public static arce__Account_has_Analysis__c setAnalyzedClient(String clientId,String analysisId,String validFs) {
        arce__Account_has_Analysis__c analyzedClient = new arce__Account_has_Analysis__c(
            arce__Customer__c = clientId,
            arce__Analysis__c = analysisId,
            arce__ffss_for_rating_id__c = validFs
        );
        Insert analyzedClient;
        Return analyzedClient;
    }
    public static arce__Account_has_Analysis__c getAnalyzedClient(String accNumber,String analysisName,String validFs) {
        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{accNumber});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(accNumber);
        arce__Analysis__c analysis = setAnalysis(analysisName);
        arce__Account_has_Analysis__c accHasAn = setAnalyzedClient(groupAccount.accId,analysis.Id,validFs);
        Return accHasAn;
    }
    public static arce__limits_typology__c setLimitsTypology(String name) {
        arce__limits_typology__c typology = new arce__limits_typology__c(
            Name = name,
            arce__risk_typology_level_id__c = System.Label.Arc_Gen_Typologic_TG_Id
        );
        Insert typology;
        Return typology;
    }
    public static arce__limits_exposures__c setLimitsExposures(String typologyId,String analysisId,Double proposedAmount) {
        arce__limits_exposures__c exposures = new arce__limits_exposures__c(
            arce__limits_typology_id__c = typologyId,
            arce__account_has_analysis_id__c = analysisId,
            arce__current_proposed_amount__c = proposedAmount
        );
        Insert exposures;
        Return exposures;
    }
    @isTest
    static void testingVariables() {
        arce__Account_has_Analysis__c accHasAn = getAnalyzedClient('G000001','Analysis Test',null);
        arce__limits_typology__c typology = setLimitsTypology('TOTAL GROUP');
        arce__limits_exposures__c exposures = setLimitsExposures(typology.Id,accHasAn.Id,150000000);
        List<String> ids = new List<String>();
        ids.add(accHasAn.Id);
        Arc_Gen_SetPoliciesVariables_service.setupPoliciesVariables(ids);
        arce__Account_has_Analysis__c currentAnalyzedClient = [SELECT id,arce__current_proposed_local_amount__c FROM arce__Account_has_Analysis__c WHERE Id =: accHasAn.Id][0];
        System.assertEquals(150000000, currentAnalyzedClient.arce__current_proposed_local_amount__c, 'Monto asociado a parrilla');
    }
}