/*------------------------------------------------------------------
* @File Name: Arc_Gen_ModalFinanStatemController.cls
* @Author:        ARCE Team
* @Group:      	ARCE - BBVA Bancomer
* @Description:   Controller to modal Arc_Gen_ModalFinanStatem.
* @Changes :
* Methods to get and process FinancialStatemnt from ASO

Service class: Arc_Gen_Balance_Tables_Service
Data class: Arc_Gen_Balance_Tables_data
*_______________________________________________________________________________________
*Version    Date           Author                               Description
*1.0        11/04/2019     ARCE TEAM   							Creación.
*1.1		24/04/2019	   diego.miguel.contractor@bbva.com		        Modified ratios function to parse Json response properly
*1.2		25/04/2019	   diego.miguel.contractor@bbva.com	            Modified ratios and eeff functions to handle error messages and display to users
*1.3		20/12/2019	   mariohumberto.ramirez.contractor@bbva.com    Added new wrapper FinancialDetailsWrapper and new method consultFSdetails
*1.4		30/01/2020	   juanmanuel.perez.ortiz.contractor@bbva.com	Add missing custom labels
-----------------------------------------------------------------------------------------*/
public with sharing class Arc_Gen_ModalFinanStatemController {
    /*
    * @Description String with the recordId of an object
    */
    private String recordId;
    /*
    * @Description String with the response of ASO
    */
    private list<String> responseASO;
    /*
    * @Description String with the response of the Engine
    */
    private list<String> responseEngine;
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description Wrapper that contain all the information to create the component
    * -----------------------------------------------------------------------------------------------
    * @param void
    * @return all the information to create the component
    * @example TabsWrapper tabswrapper = new TabsWrapper()
    * -----------------------------------------------------------------------------------------------
    **/
    public class FinancialDetailsWrapper {
        /**
        * @Description: Contain a message of error
        */
        @AuraEnabled public String gblResulError {get;set;}
        /**
        * @Description: Boolean to manage the result of an operation
        */
        @AuraEnabled public Boolean gblSuccessOperation {get;set;}
        /**
        * @Description: String to manage the servicecode of a service
        */
        @AuraEnabled public Integer gblRespServiceCode {get;set;}
        /**
        * @Description: Integer that contains the employess number
        */
        @AuraEnabled public Integer employeesNumber {get;set;}
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Fetches the financial statements from the engine.
    *--------------------------------------------------------------------------------
    * @author diego.miguel.contractor@bbva.com
    * @date 2019-04-11
    * @param varRecord recordId of the account_has_analysis.
    * @return list from financial statements.
    * @example public static List <arce__Financial_Statements__c> fetchFinancialStatements(String varRecord) {
    **/
    @AuraEnabled
    public static List<arce__Financial_Statements__c> fetchFinancialStatements(String varRecord) {
        try {
            return Arc_Gen_Balance_Tables_service.getFinancialStatementsFromEngine(varRecord, true);
        } catch (Exception ex) {
            throw new AuraHandledException(System.Label.Arc_Gen_ErrorService);   // NOSONAR
        }
    }
    /**
    *-------------------------------------------------------------------------------
    * @description method that call callEngineFinancialState and return the status of engine.
    *--------------------------------------------------------------------------------
    * @author diego.miguel.contractor@bbva.com
    * @date 2019-04-11
    * @param recordId recordId of the account_has_analysis and the list of elements selected from the table.
    * @param lista data for the tables
    * @return list with values of the response.
    * @example public static Arc_Gen_Balance_Tables_service.ratiosAndRatingResponse callEngineFinancialState(String recordId,String lista) {
    **/
    @AuraEnabled
    public static Arc_Gen_Balance_Tables_service.ratiosAndRatingResponse callEngineFinancialState(
            String recordId, List<String> financialIdList) {
        // Persist financial statements and ratios tables.
        final List<arce__Financial_Statements__c> financialStatements =
            Arc_Gen_Balance_Tables_service.getFinancialStatementsFromEngine(recordId, false);
        final List<arce__Financial_Statements__c> selectedFinancialStatements =
            Arc_Gen_Balance_Tables_service.filterFFSSByServiceId(financialStatements, financialIdList);
        final Arc_Gen_Balance_Tables_service.RatiosAndRatingResponse resp = Arc_Gen_Balance_Tables_service.buildTables(
            recordId, selectedFinancialStatements);
        return resp;
    }
    /**
    *----------------------------------------------------------------------------------------------------------------------------------------
    * @Description method that consult the financial statement details service and set the number of employess of the subsidiary
    *----------------------------------------------------------------------------------------------------------------------------------------
    * @Author   mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-12-19
    * @param recordId - Id of the account_has_analysis
    * @param fsServiceId - Id of the financial statement
    * @return FinancialDetailsWrapper - wrapper with the result of the operation
    * @example consultFSdetails(recordId, fsServiceId) {
    * ---------------------------------------------------------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static FinancialDetailsWrapper consultFSdetails(String recordId,String fsServiceId) {
        FinancialDetailsWrapper response =  new FinancialDetailsWrapper();
        try {
            response = Arc_Gen_FinStatDetails_Service.setNumberOfEmployees(recordId, fsServiceId);
            response.gblSuccessOperation = true;
        } catch (Exception e) {
            response.gblSuccessOperation = false;
            response.gblResulError = e.getMessage() + ' : ' + e.getLineNumber();
        }
        return response;
    }
}