/**
* @File Name          : public class Arc_Gen_Rating_Data.cls
* @Description        : Class that obtains the data of the object Rating
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE Team
* @Last Modified By   : eduardoefrain.hernandez.contractor@bbva.com
* @Last Modified On   : 16/07/2019 12:17:00
* @Changes  :
*==============================================================================
* Ver         Date                     Author                 Modification
*==============================================================================
* 1.0    16/07/2019 12:17:00   eduardoefrain.hernandez.contractor@bbva.com     Initial version
**/
public with sharing class Arc_Gen_Rating_data {
/**
*-------------------------------------------------------------------------------
* @description  Empty constructor
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 07/16/2019 14:50:32
* @example private Arc_Gen_Rating_data()
**/
    @TestVisible private Arc_Gen_Rating_data() {
    }
/**
*-------------------------------------------------------------------------------
* @description  get basic fields of rating
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 07/16/2019 14:50:32
* @param condition - String with the rating id
* @return List < ratingId >
* @example public static arce__rating__c generalRatingData(String ratingId)
**/
    public static arce__rating__c generalRatingData(String ratingId) {
        final String escapedCondition = '\''+String.escapeSingleQuotes(ratingId)+'\'';
        final String query = 'SELECT Id,arce__rating_id__c,arce__status_type__c,arce__rating_long_value_type__c,'
            +' arce__rating_short_value_type__c,arce__long_rating_value_type__c,arce__short_rating_value_type__c,'
            +' arce__total_rating_score_number__c,arce__pd_per__c,arce__rating_user_id__c'
            +' FROM arce__rating__c'
            +' WHERE Id = ';
        return Database.query(query + escapedCondition);
    }
}