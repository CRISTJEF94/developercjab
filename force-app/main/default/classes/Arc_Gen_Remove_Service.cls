/**
  * @File Name          : public with sharing class Arc_Gen_Remove_Service.cls
  * @Description        : Service class for remove function
  * @Author             : luisarturo.parra.contractor@bbva.com
  * @Group              : ARCE
  * @Last Modified By   : luisruben.quinto.munoz@bbva.com
  * @Last Modified On   : 23/7/2019 22:55:39
  * @Modification Log   :
  *==============================================================================
  * Ver         Date                     Author                 Modification
  *==============================================================================
  * 1.0    20/05/2019 12:50:32   luisarturo.parra.contractor@bbva.com     Initial Version
  * 1.1    21/06/2019 15:46:20   ismaelyovani.obregon.contractor@bbva.com     Sanction 1.0 Q3 Added Method that gets value of picklist.
  * 1.1    26/12/2019 14:50:29   juanmanuel.perez.ortiz.contractor@bbva.com   Add logic to send notifications.

  **/
public with sharing class Arc_Gen_Remove_Service {
    /**
        * @Description: String with value for FINALIZED_ANALYSIS in ARCE
    */
    static final String FINALIZED_ANALYSIS = '10';
    /**
        * @Description: String with value for STAGE_CLOSED in ARCE
    */
    static final String STAGE_CLOSED = '3';
    /**
    *-------------------------------------------------------------------------------
    * @description set new values for arce
    --------------------------------------------------------------------------------
    * @author luisarturo.parra.contractor@bbva.com
    * @date 20/05/2019
    * @param String record Id to be uptated
    * @param String reason to remove the arce
    * @return List < arce__Account_has_Analysis__c >
    * @example public static void updateStatusService(String recordId)
    **/
  public static void updateRemoveInfo(Id recordId,String reasonPick, String reasonDesc, String reasonPickLabel) {
    arce__Analysis__c arceUp = Arc_Gen_ArceAnalysis_Data.gerArce(recordId);
    final Arc_Gen_User_Wrapper wrpUser = Arc_Gen_User_Locator.getUserInfo(System.UserInfo.getUserId());
    arceUp.put('arce__wf_status_id__c',FINALIZED_ANALYSIS);
    arceUp.put('arce__Stage__c',STAGE_CLOSED);
    arceUp.put('arce__anlys_wkfl_discard_reason_id__c',reasonPick);
    arceUp.put('arce__anlys_wkfl_discard_reason_desc__c',reasonDesc);
    final List<arce__Analysis__c> arceList = New List<arce__Analysis__c>();
    arceList.add(arceUp);
    Arc_Gen_GenericUtilities.createNotifications(Arc_Gen_Notifications_Service.getUsersIds(arceUp.Id), arceUp.Id, System.Label.Arc_Gen_ArceRemove + ': ' + arceUp.Name);
    Arc_Gen_Traceability.saveEvent(arceUp.Id, System.Label.Arc_Gen_TraceabilityFinishArce, 'deny', System.Label.Arc_Gen_TraceabilityUserCode + ' : ' + system.UserInfo.getName() + ' | ' + System.Label.Arc_Gen_ExecRepStg + ' : ' + System.Label.Arc_Gen_Stage_03 + ' | ' + System.Label.Arc_Gen_TraceabilityReason + ' : ' + reasonPickLabel + ' | ' + System.Label.Arc_Gen_TraceabilityOffice + ' : ' + wrpUser.branchText);
    Arc_Gen_ArceAnalysis_Data.updateArce(arceList);
  }
    /**
    *-------------------------------------------------------------------------------
    * @description Method that gets value of picklist.
    --------------------------------------------------------------------------------
    * @author ismaelyovani.obregon.contractor@bbva.com
    * @date 21/06/2019
    * @params String varObject  name for the object
    * @params String varField name from the field to get pick list values
    * @return List<map<String,String>> of picklist values
    * @example public static List<map<String,String>> getPickListValue()
    **/
  public static List<map<String,String>> getDiscardReasons() {
      return Arc_Gen_GenericUtilities.getPicklistValuesLabels('arce__Analysis__c','arce__anlys_wkfl_discard_reason_id__c');
  }
}