/**
* @File Name          : Arc_Gen_ReturnButton_service.cls
* @Description        : Return an ARCE to the before status.
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE
* @Last Modified By   : juanmanuel.perez.ortiz.contractor@bbva.com
* @Last Modified On   : 26/12/2019 14:52:11
* @Changes
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    3/5/2019 13:47:37   eduardoefrain.hernandez.contractor@bbva.com     Initial Version
* 1.1    20/11/2019 13:47:37   javier.soto.carrascosa@bbva.com     Reference user interface
* 1.2    10/12/2019 12:47:40   juanmanuel.perez.ortiz.contractor@bbva.com     Add logic to return ARCE
* 1.3    26/12/2019 14:52:11   juanmanuel.perez.ortiz.contractor@bbva.com     Add logic to send notifications
**/
public without sharing class Arc_Gen_ReturnButton_service {
    /**
        * @Description: String with value corresponding to arce in preparation
    */
    static final String IN_PREPARATION = '1';
    /**
        * @Description: String with value corresponding to preparing analysis
    */
    static final String PREPARING_ANALYSIS = '02';
    /**
        * @Description: String with value corresponding to contrasting analysis
    */
    static final String CONTRASTING_ANALYSIS = '04';
/**
*-------------------------------------------------------------------------------
* @description Empty priavate constructor
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 2019-05-13
* @example private Arc_Gen_ReturnButton_controller ()
**/
    @TestVisible
    private Arc_Gen_ReturnButton_service () {
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Method that evaluate identification funcionality
    --------------------------------------------------------------------------------
    * @author juanignacio.hita.contractor@bbva.com
    * @date 2020-01-20
    * @param String accHasAnalysisId
    * @param Object wrapper
    * @param String userId
    * @return Arc_Gen_Delegation_Wrapper
    * @example public Arc_Gen_Delegation_Wrapper evaluateIdentification()
    **/
    public static Arc_Gen_Delegation_Wrapper evaluateIdentification(Id accHasAnalysisId, Object wrapper, String userId, String reason) {
		System.debug('accHasAnalysisId:'+accHasAnalysisId);
		System.debug('wrapper:'+wrapper);
		System.debug('userId:'+userId);
		System.debug('reason:'+reason);
        final arce__Analysis__c arceAnalysis = Arc_Gen_ArceAnalysis_Data.gerArce(accHasAnalysisId);
		System.debug('arceAnalysis:'+arceAnalysis);
        final Arc_Gen_User_Wrapper wrpUserCurrent = Arc_Gen_User_Locator.getUserInfo(System.UserInfo.getUserId());
		System.debug('wrpUserCurrent:'+wrpUserCurrent);
        final Arc_Gen_User_Wrapper wrpUser = Arc_Gen_User_Locator.getUserInfo(arceAnalysis.CreatedById);
		System.debug('wrpUser:'+wrpUser);

        Arc_Gen_Delegation_Wrapper wrapperSerialize = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) wrapper, Arc_Gen_Delegation_Wrapper.class);
        final String identification = Arc_Gen_Propose_Helper.initIdentification(wrapperSerialize.lstAmbits[0].get('value'), arceAnalysis.Id);
        final Arc_Gen_User_Wrapper targetUser = Arc_Gen_User_Locator.getUserInfo(identification);
        String comment = System.Label.Cls_arce_ReturnTo + ' ' + targetUser.userBasicInfo.Name + ' | ' + System.Label.Cls_arce_ReturnReason + ' ' + reason;
        Map<String, Object> fieldValueMap = new Map<String, Object>();
        fieldValueMap.put('arce__anlys_wkfl_return_reason_desc__c', reason);
        fieldValueMap.put('arce__anlys_wkfl_edit_br_level_type__c', wrapperSerialize.lstAmbits[0].get('value'));

        if (wrpUserCurrent.ambitUser == wrapperSerialize.lstAmbits[0].get('value')) {
            fieldValueMap.put('arce__wf_status_id__c',CONTRASTING_ANALYSIS);
            fieldValueMap.put('OwnerId',identification);
            wrapperSerialize.codStatus = 200;
        } else {
            if (wrpUser.ambitUser == wrapperSerialize.lstAmbits[0].get('value')) {
                if (arceAnalysis.arce__bbva_committees_type__c == '' || arceAnalysis.arce__bbva_committees_type__c == null) {
                    fieldValueMap.put('arce__Stage__c',IN_PREPARATION);
                    fieldValueMap.put('arce__wf_status_id__c',PREPARING_ANALYSIS);
                    fieldValueMap.put('OwnerId',identification);
                    wrapperSerialize.codStatus = 200;
                } else {
                    wrapperSerialize.codStatus = 500;
                    wrapperSerialize.msgInfo = System.Label.Arc_Gen_ReturnError;
                }
            } else {
                fieldValueMap.put('arce__wf_status_id__c',CONTRASTING_ANALYSIS);
                fieldValueMap.put('OwnerId',identification);
                wrapperSerialize.codStatus = 200;
            }

        }
        if (wrapperSerialize.codStatus == 200) {
            Arc_Gen_ArceAnalysis_Data.editAnalysisFields(arceAnalysis.Id, fieldValueMap);
            Arc_Gen_Traceability.saveEvent(arceAnalysis.Id, System.Label.Lc_arce_ReturnButton, 'sendBack', comment.abbreviate(255));
            Arc_Gen_GenericUtilities.createNotifications(Arc_Gen_Notifications_Service.getUsersIds(arceAnalysis.Id), arceAnalysis.Id, System.Label.Arc_Gen_ArceReturn + ': ' + arceAnalysis.Name);
        }
		System.debug('wrapperSerialize:'+wrapperSerialize);
        return wrapperSerialize;
    }

}