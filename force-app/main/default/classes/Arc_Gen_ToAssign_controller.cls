/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_ToAssign_controller
* @Author   Angel Fuertes Gomez - angel.fuertes2@bbva.com / BBVA DWP
* @Date     Created: 2019-05-04
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Class used for the button to Assign in the stage of Sanction.
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-05-04 angel.fuertes2@bbva.com
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
public class Arc_Gen_ToAssign_controller {
    /**
    *-------------------------------------------------------------------------------
    * @description Empty priavate constructor
    --------------------------------------------------------------------------------
    * @author eduardoefrain.hernandez.contractor@bbva.com
    * @date 2019-09-25
    * @example private Arc_Gen_ToAssign_controller ()
    **/
    @TestVisible
    private Arc_Gen_ToAssign_controller () {
    }
    /**
    * ---------------------------------------------------------------------------------------------------
    * @Description - Method to find users
    * ---------------------------------------------------------------------------------------------------
    * @Author   Angel Fuertes Gomez - angel.fuertes2@bbva.com / BBVA DWP
    * @Date     Created: 2019-05-04
    * @param inputTerm - String to find users
    * @return a list of map with the users
    * @example searchUser(inputTerm)
    * ---------------------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static List<map<String,String>> searchUser(String inputTerm) {
        return Arc_Gen_ToAssign_service.searchUser(inputTerm);
    }
    /**
    * ---------------------------------------------------------------------------------------------------
    * @Description - Method to assign the arce
    * ---------------------------------------------------------------------------------------------------
    * @Author   Angel Fuertes Gomez - angel.fuertes2@bbva.com / BBVA DWP
    * @Date     Created: 2019-05-04
    * @param userId - id of the user selected
    * @param recordId - id of the arce
    * @return a string
    * @example toAssign(userId, recordId)
    * ---------------------------------------------------------------------------------------------------
    **/
    @AuraEnabled
    public static String toAssign(Id userId, Id recordId) {
        return Arc_Gen_ToAssign_service.toAssign(userId,recordId);
    }
}