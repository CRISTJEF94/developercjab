/**
* @File Name          : Arc_Gen_TestLeverage_data.cls
* @Description        : Obtains the data to set the proposed limit of the group to a client field
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE Team
* @Last Modified By   : luisruben.quinto.munoz@bbva.com
* @Last Modified On   : 23/7/2019 21:00:44
* @Changes
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    30/4/2019 18:00:36   eduardoefrain.hernandez.contractor@bbva.com     Initial Version
* 1.1    7/5/2019 23:28:45   diego.miguel.contractor@bbva.com     Added comments
**/
public with sharing class Arc_Gen_SetPoliciesVariables_data {
/**
*-------------------------------------------------------------------------------
* @description Retrieves a list of all arce__Account_has_Analysis__c
* related with the same ARCE related to the given arce__Account_has_Analysis__c Id
*--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId
* @return List<arce__Account_has_Analysis__c>
* @example public List<arce__Account_has_Analysis__c> getGroupStructure(String analysisId)
**/
    public List<arce__Account_has_Analysis__c> getGroupStructure(String analysisId) {
        arce__Account_has_Analysis__c currentGroup = [SELECT id,arce__Analysis__c FROM arce__Account_has_Analysis__c WHERE Id =: analysisId];
        Return [SELECT id,arce__current_proposed_local_amount__c FROM arce__Account_has_Analysis__c WHERE arce__Analysis__c =: currentGroup.arce__Analysis__c];
    }
/**
*-------------------------------------------------------------------------------
* @description Retrieves a list of all arce__limits_exposures__c
* related with given arce__Account_has_Analysis__c Id and wich parent typology name is 'TOTAL GROUP'
*--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String analysisId
* @return arce__limits_exposures__c
* @example public arce__limits_exposures__c getLimitExposures(String analysisId)
**/
    public arce__limits_exposures__c getLimitExposures(String analysisId) {
        Return [SELECT Id,arce__current_proposed_amount__c,arce__limits_typology_id__c FROM arce__limits_exposures__c WHERE arce__account_has_analysis_id__c =: analysisId AND arce__limits_typology_id__r.arce__risk_typology_level_id__c =: System.Label.Arc_Gen_Typologic_TG_Id][0];
    }
/**
*-------------------------------------------------------------------------------
* @description Method that updates a list of sObject
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param List<sObject> recordsToUpdate
* @example public void updateRecords(List<sObject> recordsToUpdate)
**/
    public void updateRecords(List<sObject> recordsToUpdate) {
        update recordsToUpdate;
    }
}