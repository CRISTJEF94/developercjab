/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_FinStatDetails_Service_Helper
* @Author   mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 19/12/2019
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Helper class for Arc_Gen_FinStatDetails_Service
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |2019-12-19 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* |2020-01-29 juanmanuel.perez.ortiz.contractor@bbva.com
*             Modified getNumberOfEmployees() method, add Account Has Analysis Wrapper
* -----------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_FinStatDetails_Service_Helper {
    /**
        * @Description: String with the name of the service financial statement details
    */
    static final string FINAN_STAT_DETAILS = 'financialStatementDetails';
    /**
        * @Description: String with the name of the dummy financial statement
    */
    static final string DUMMY_FFSS = 'dummyFFSS';
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-12-19
    * @param void
    * @return void
    * @example Arc_Gen_FinStatDetails_Service_Helper helper = new Arc_Gen_FinStatDetails_Service_Helper()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_FinStatDetails_Service_Helper() {

    }
    /**
    *----------------------------------------------------------------------------------------------------------------------------------------
    * @Description method that consult the financial statement details service and return the number of employees of the subsidiary
    *----------------------------------------------------------------------------------------------------------------------------------------
    * @Author   mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-12-19
    * @param    recordId - Id of the account_has_analysis
    * @param    fsServiceId - Id of the financial statement
    * @return   Integer - Integer with the number of employees
    * @example  getNumberOfEmployes(recordId, recordId) {
    * ----------------------------------------------------------------------------------------------------------------------------------------
    **/
    public static Arc_Gen_ModalFinanStatemController.FinancialDetailsWrapper getNumberOfEmployees(String recordId,String fsServiceId) {
        final Arc_Gen_ModalFinanStatemController.FinancialDetailsWrapper responseWrapper = new Arc_Gen_ModalFinanStatemController.FinancialDetailsWrapper();
        final Arc_Gen_CustomServiceMessages serviceMessage = new Arc_Gen_CustomServiceMessages();
        if (fsServiceId == DUMMY_FFSS || fsServiceId == null) {
            responseWrapper.employeesNumber = null;
        } else {
            final String accountNumber = Arc_Gen_CallEncryptService.getEncryptedClient((String)Arc_Gen_AccHasAnalysis_Data.getAccountHasAnalysisAndCustomer(new List<String>{recordId})[0].accWrapperObj.accNumber);
            final String params = '{"accountNumber":"' + accountNumber + '","fsServiceId":"' + fsServiceId + '"}';
            final HttpResponse response = Arc_Gen_getIASOResponse.getServiceResponse(FINAN_STAT_DETAILS, params);
            if (response.getStatusCode() == serviceMessage.CODE_200) {
                responseWrapper.gblRespServiceCode = serviceMessage.CODE_200;
                Map<String, Object> mapResponse = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
                final List<Object> data = (List<Object>)mapResponse.get('data');
                for (integer i = 0; i < data.size(); i++) {
                    final Map<String, Object> dataMap = (Map<String, Object>)data[i];
                    responseWrapper.employeesNumber = (Integer)dataMap.get('employeesNumber');
                }
            } else {
                responseWrapper.gblRespServiceCode = response.getStatusCode();
            }
        }
        return responseWrapper;
    }
}