/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Validate_Content
* @Author   Juan Ignacio Hita
* @Date     Created: 2019-10-27
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Class for related records table manager validations
* ------------------------------------------------------------------------------------------------
* |2019-10-27 juanignacio.hita.contractor@bbva.com@bbva.com
*             Class creation.
* |2019-11-20 javier.soto.carrascosa@bbva.com@bbva.com
*             Fix sum percentage
* -----------------------------------------------------------------------------------------------
*/
global class Arc_Gen_ValidateContent_service implements rrtm.RelatedManager_Interface {
    /**
        * @Description: integer with maxYears
    */
    static Integer maxYears = 2;
    /**
        * @Description: integer with maximum percentage
    */
    static Integer maxPercent = 100;
    /**
    * --------------------------------------------------------------------------------------
    * @Description Wrapper class
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hita.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * --------------------------------------------------------------------------------------
    **/
    class Wrapper extends rrtm.RelatedRecord_WrapperValidation {}
    /**
    * --------------------------------------------------------------------------------------
    * @Description Method that validates the info coming from some tables
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hita.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param    List<Object> lstRecords for wrapper detail
    * @return   JSON String with validation result and messages updated
    * @example static String beforeSaveData(List<Object> lstRecords)
    * --------------------------------------------------------------------------------------
    **/
    public static String beforeSaveData(List<Object> lstRecords) {
        Wrapper wrapper = new Wrapper();
        wrapper.validation = true;
        wrapper.msgInfo = '';
        Map<String, Decimal> mapYearPercent = new Map<String, Decimal>();
        set<String> arcAcc = new Set<String>();

        if(!lstRecords.isEmpty()) {
            Integer ite = 0;
            for (Object obj : lstRecords) {
                String strJson = JSON.serialize(obj);
                Map<String, Object> mapObj = (Map<String, Object>)JSON.deserializeUntyped(strJson);
                ite++;
                arcAcc.add(String.valueOf(mapObj.get('arce__account_has_analysis_id__c')));

                String percent = String.valueOf(mapObj.get('arce__table_content_percentage__c'));
                String year = (String) mapObj.get('arce__table_content_year__c');

                if (Arc_Gen_ValidateInfo_utils.isFilled(percent)) {
                    mapYearPercent = Arc_Gen_ValidateInfo_utils.sumMapMethod(mapYearPercent,year,percent);
                } else {
                    wrapper.msgInfo += string.format(Label.Arc_Gen_CompleteField_Per, new List<String>{String.valueOf(ite)});
                    wrapper.validation = false;
                }
                if(!Arc_Gen_ValidateInfo_utils.isFilled(year)) {
                    wrapper.msgInfo += string.format(Label.Arc_Gen_CompleteField_Year, new List<String>{String.valueOf(ite)});
                    wrapper.validation = false;
                }
            }
        }
        if (mapYearPercent.size() <= maxYears ) {
            for(String key : mapYearPercent.keySet()) {
                if(mapYearPercent.get(key) != maxPercent) {
                    wrapper.validation = false;
                    wrapper.msgInfo += string.format(Label.Arc_Gen_InsertGeoAct_ErrorSumPercent, new List<String>{key});
                }
            }
        } else {
            wrapper.msgInfo += Label.Arc_Gen_InsertGeoAct_Error2years;
            wrapper.validation = false;
        }
        return JSON.serialize(wrapper);
    }
}