/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_AccHasAnalysis_Data
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 24/06/2019
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Data class for object arce__Account_has_Analysis__c
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |2019-06-24 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* |2019-10-03 mariohumberto.ramirez.contractor@bbva.com
*             Added new field 'arce__Analysis__r.arce__Stage__c' to retrieve in method
*             getAccHasRelation.
* |2019-10-03 mariohumberto.ramirez.contractor@bbva.com
*             Change of param in the method getRatingStatus, now is of type list
* |2019-10-11 mariohumberto.ramirez.contractor@bbva.com
*             Addes new field 'arce__Analysis__r.arce__analysis_risk_sanction_date__c' to retrieve
*             in method getAccHasAnalysis
* |2019-12-02 german.sanchez.perez.contractor@bbva.com | franciscojavier.bueno@bbva.com
*             api names modified with the correct name on business glossary
* |2019-12-02 manuelhugo.castillo.contractor@bbva.com
*             Include filed api arce__Customer__r.ParentId in query from method 'getAccHasAnalysis'
* |2019-12-19 german.sanchez.perez.contractor@bbva.com | franciscojavier.bueno@bbva.com
*             Added new field arce__Customer__r.AccountNumber in the query of the method
*             getAccHasRelation
* |2019-30-19 mariohumberto.ramirez.contractor@bbva.com
*             Added new fields (arce__prop_int_mod_rec_type__c,arce__risk_unit_confirm_desc__c,
*             arce__contrast_risk_unit_type__c,arce__contrast_risk_unit_neg_desc__c,
*             arce__contrast_risk_unit_posi_desc__c) to the query in the method getAccHasAnalysis
* |2020-17-01 mariohumberto.ramirez.contractor@bbva.com
*             Change arce__Customer__r.ParentId for arce__group_asset_header_type__c in queries
* |2019-01-20 juanmanuel.perez.ortiz.contractor@bbva.com
*             Added new field (arce__group_asset_header_type__c) to the query in the method getAccHasRelation
* |2020-01-28 mariohumberto.ramirez.contractor@bbva.com
*             Add new fields in the method getAccHasAnalysis() 'arce__call_limit_service__c, arce__last_update_policie__c,
*             arce__last_update_position__c, arce__outstanding_carrousel__c'
* |2019-02-08 ricardo.almanza.contractor@bbva.com
*             adedd fields to getAccHasAnFromArce
* |2020-03-16 juanignacio.hita.contractor@bbva.com
*             adedd new method "getAllAccFromArce"
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_AccHasAnalysis_Data {
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 24/06/2019
    * @param void
    * @return void
    * @example Arc_Gen_AccHasAnalysis_Data data = new Arc_Gen_AccHasAnalysis_Data()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_AccHasAnalysis_Data() {

    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that return a list of all fields in arce__Account_has_Analysis__c object
    * -----------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 24/06/2019
    * @param recordId - List of ids of the arce__Account_has_Analysis__c object to consult
    * @return List<arce__Account_has_Analysis__c> - List of all fields in arce__Account_has_Analysis__c object
    * @example getAccHasAnalysis(recordId)
    * -----------------------------------------------------------------------------------------------
    **/
    public static List<arce__Account_has_Analysis__c> getAccHasAnalysis(List<String> recordId) {
        return [SELECT id,arce__Analysis__r.OwnerId,arce__Analysis__r.arce__parent_analysis_id__c,arce__Analysis__r.arce__analysis_risk_sanction_date__c,
        arce__ffss_for_rating_id__r.arce__financial_statement_id__c,
        arce__ffss_for_rating_id__r.arce__rating_id__c,arce__Account_has_Analysis__c.arce__branch_id__c,
        arce__economic_activity_sector_desc__c,arce__gf_company_economic_actvy_id__c,
        arce__adj_long_rating_value_id__c,arce__adj_short_rating_value_id__c,arce__adj_total_rating_score_number__c,
        arce__ll_adj_ebitda_amount__c,arce__ll_adj_debt_amount__c,arce__sector_rt_type__c,arce__sector_rt_id__c,
        arce__Analysis__c,arce__contract_approval_date__c,arce__significant_variables_type__c,
        arce__limit_asset_allocation_amount__c,arce__data_adjusted_score_number__c,arce__ll_adj_debt_auditor_amount__c,
        arce__ll_adj_ebitda_auditor_amount__c,arce__cust_evolution_cyr_ind_type__c,arce__backlog_desc__c,arce__backlog_sales_desc__c,
        arce__bbva_financial_debt_share_per__c,arce__cust_budget_cyr_ind_type__c,arce__ffss_auditor_qlfn_type__c,
        arce__backlog_less_70_per_type__c,arce__backlog_less_70_per_desc__c,arce__enterprise_group_type__c,
        arce__enterprise_group_desc__c,arce__ll_test_scope_ind_type__c,arce__ebitda_less_70_peer_avg_desc__c,arce__ebitda_less_70_peer_avg_type__c,
        arce__level_concentration_geo_type__c,arce__sales_less_70_peer_avg_desc__c,arce__sales_less_70_peer_avg_type__c,
        arce__cust_public_private_cmprn_desc__c,arce__cust_proj_2yr_3yr_desc__c,arce__significant_variables_desc__c,
        arce__cust_evolution_cyr_desc__c,arce__Syn_expct_evol_short_term_txt__c,arce__off_balance_rlvnt_info_desc__c,
        arce__off_balance_treasury_desc__c,arce__Syn_client_situation_txt__c,arce__regulated_sector_desc__c,arce__exch_rate_vulnerability_desc__c,
        arce__competition_sector_desc__c,arce__level_conc_sector_desc__c,arce__level_concentration_geo_desc__c,
        arce__bbva_financial_debt_share_desc__c,arce__cust_budget_incl_ffss_desc__c,arce__co_actvy_prtcp_group_desc__c,
        arce__cust_contract_capacity_desc__c,arce__gf_covenant_desc__c,arce__fin_aggressiveness_mngmt_desc__c,
        arce__gntee_given_backlog_range_desc__c,arce__guarantee_given_desc__c,arce__struct_change_invst_desc__c,
        arce__shrhldr_Financialsponsor_profile_desc__c,arce__management_style_desc__c,arce__other_operation_desc__c,
        arce__other_contracted_product_desc__c,arce__shareholder_disagreement_desc__c,arce__capital_provision_desc__c,
        arce__rar_profitability_desc__c,arce__investment_relevant_fact_desc__c,arce__client_refinanced_desc__c,
        arce__shareholder_agreement_desc__c,arce__shrhldr_sig_movement_desc__c,arce__stage_collective_desc__c,
        arce__risk_position_summary_type__c,arce__shareholder_support_desc__c,arce__breakdown_activity_desc__c,
        arce__breakdown_geography_desc__c,arce__risk_position_summary_desc__c,arce__debt_comt_not_disb_amount__c,
        arce__debt_comt_not_disb_local_amount__c,arce__Completeness__c,arce__entity_covenants_type__c,
        CreatedById,CurrencyIsoCode,arce__outstanding_amount__c,arce__current_proposed_amount__c,
        arce__current_proposed_local_amount__c,arce__Customer__c,arce__customer_multi_country_type__c,arce__customer_multi_country_desc__c,
        arce_ctmr_flag__c,arce__cust_conc_clients_desc__c,arce__prev_magnitude_unit_type__c,arce__limit_asset_allocation_date__c,
        arce__exposure_at_default_date__c,arce__bbva_avg_prblty_default_date__c,arce__cust_prblty_default_date__c,
        arce__sector_avg_prblty_dflt_date__c,arce__ll_adj_deb_excl_amount__c,arce__ll_acquisition_debt_type__c,
        arce__competition_sector_type__c,arce__level_conc_sector_type__c,arce__market_situation_desc__c,arce__cust_conc_clients_type__c,
        arce__concentration_suppliers_type__c,arce__Funds_destination_type__c,arce__banking_pool_desc__c,arce__ead_amount__c,
        arce__ll_adj_ebitda_excl_amount__c,arce__economic_capital_amount__c,arce__ffss_submitted_type__c,Arc_Gen_EEGRP__c,
        arce__equity_total_asset_20_type__c,arce__Syn_equity_type__c,arce__equity_desc__c,arce__cost_structure_desc__c,
        arce__Syn_expct_evol_short_term_type__c,arce_Gen_ExceptionsParrilla__c,arce__ebitda_3yr_evolution_desc__c,
        arce__sales_evolution_desc__c,arce__account_payable_desc__c,arce__explantation_margin_desc__c,
        arce__export_range_year_type__c,arce__long_rating_value_id__c,arce__rating_short_value_type__c,
        arce__fin_covenants_breach_type__c,arce__fin_covenants_breach_desc__c,arce__Financial_Risk_Path__c,
        arce__fin_spnsr_relnshp_exprnc_type__c,arce__fin_spnsr_relnshp_exprnc_desc__c,arce__shrhldr_fin_sponsor_profl_type__c,
        arce__shrhldr_fin_sponsor_strat_type__c,arce__ffss_for_rating_id__c,arce__gl_fixed_asset_amount__c,
        arce__financial_flexibility_type__c,arce__manual_rating_value_type__c,arce__oper_guarantee_given_type__c,
        arce__conc_geography_activity_type__c,arce__gross_financial_debt_amount__c,arce__gross_financial_debt_local_amount__c,
        arce__gross_fin_debt_ebitda_number__c,arce__group_asset_header_type__c,arce__hats_risk_qualification_number__c,
        arce__Account_has_Analysis_Id__c,arce__sector_building_desc__c,arce__import_range_year_type__c,arce__InReview__c,
        arce__investment_relevant_fact_type__c,arce__off_balance_rlvnt_info_type__c,arce__ebitda_interest_number__c,
        arce__ebitda_interest_local_number__c,arce__invty_work_in_progress_desc__c,arce__struct_change_invst_type__c,LastModifiedById,
        arce__customer_last_projects_desc__c,arce__participant_leveraged_type__c,arce__ll_after_adj_ind_type__c,arce__ll_before_adj_ind_type__c,
        arce__ll_previous_anlys_ind_type__c,arce__ll_previous_anlys_clsfn_type__c,arce__ll_after_adj_clsfn_type__c,arce__ll_collective_type__c,
        arce__ll_before_adj_clsfn_type__c,arce__ll_test_manl_excl_rsn_type__c,arce__lgd_per__c,arce__financial_liquidity_desc__c,
        arce__main_affiliates_desc__c,arce__property_ownership_type__c,arce__main_subsidiary_ind_type__c,arce__years_experience_mngmt_type__c,
        arce__years_experience_mngmt_desc__c,arce__fin_aggressiveness_mngmt_type__c,arce__management_style_type__c,
        arce__market_growth_forecast_desc__c,arce__market_share_type__c,arce__maturity_date__c,
        arce__debt_maturity_available_type__c,Name,newFFSS_ind__c,arce__ffss_auditor_fullname_name__c,arce__number_entity_type__c,
        arce__dvrsfn_product_service_desc__c,arce__dvrsfn_product_service_type__c,arce__assessment_opportunities_desc__c,
        arce__ll_other_adj_debt_amount__c,arce__ll_other_adj_ebitda_amount__c,arce__other_contracted_product_type__c,
        arce__other_operation_type__c,arce__geography_outlook_type__c,arce__path__c,arce__bbva_avg_prblty_default_per__c,
        arce__probability_default_per__c,arce__sector_avg_prblty_default_per__c,arce__Peer_information_ind_type__c,
        arce__ffss_cnsld_perimeter_desc__c,arce__risk_policy_type__c,arce__off_balance_treasury_type__c,arce__portfolio_class_type__c,
        arce__shareholder_disagreement_type__c,arce__cust_budget_incl_ffss_ind_type__c,arce__pd_per__c,arce__property_ownership_desc__c,
        arce__capital_provision_per__c,arce__cust_proj_2yr_3yr_ind_type__c,arce__smes_eur_comuty_defn_type__c,arce__customer_qualitative_type__c,
        arce__rar_profitability_per__c,arce__Rating__c,arce__long_rating_value_type__c,arce__short_rating_value_type__c,arce__rating_model_type__c,
        arce__rating_scale_type__c,arce__total_rating_score_number__c,arce__ll_adj_debt_ebitda_number__c,arce__gntee_given_backlog_range_type__c,
        arce__total_debt_ebitda_per__c,arce__regulated_sector_type__c,arce__other_relevant_fact_type__c,arce__new_business_venture_type__c,
        arce__new_business_venture_desc__c,arce__other_relevant_fact_desc__c,arce__client_refinanced_type__c,arce__risk_request_desc__c,
        arce__risk_growth_amount__c,arce__Sector__c,arce__sector_desc__c,arce__sector_outlook_type__c,arce__rating_econ_sector_tmpl_id__c,
        arce__service_type_name__c,arce__cust_multi_business_line_type__c,arce__shareholder_agreement_type__c,
        arce__shrhldr_sig_movement_type__c,arce__client_situation_type__c,arce__Stage__c,arce__stage_collective_type__c,
        arce__Arc_Gen_Status_type__c,arce__assessment_strengths_desc__c,arce__subsector_desc__c,arce__shareholder_support_type__c,
        arce__Arc_Gen_TabAssessment__c,arce__Arc_Gen_TabCompBasicData__c,arce__Arc_Gen_TabCompBnkRelationship__c,arce__Arc_Gen_TabCompBusinessRisk__c,
        arce__Arc_Gen_TabCompFinancialRisk__c,arce__Arc_Gen_TabCompIndustryAnalysis__c,arce__Arc_Gen_TabRating__c,arce__cust_contract_capacity_type__c,
        arce__cust_contract_category_type__c,arce__assessment_threats_desc__c,arce__ffss_auditor_opinion_type__c,arce__ffss_certification_type__c,
        arce__ffss_auditor_opinion_desc__c,arce__currency_id__c,arce__total_asset_amount__c,arce__total_asset_local_amount__c,arce__gf_total_debt_amount__c,
        arce__gf_total_revenues_ytd_amount__c,arce__total_revenues_local_amount__c,arce__customer_product_usage_desc__c,arce__Institution_type__c,
        arce__magnitude_unit_type__c,arce__var_over_20_balance_desc__c,arce__var_vary_10_and_10_bal_desc__c,arce__variables_vary_20_balance_desc__c,
        arce__equity_vary_not_expln_desc__c,arce__ll_anlys_clsfn_var_rsn_id__c,arce__total_revenuesvariation_percentage__c,
        arce__exch_rate_vulnerability_type__c,arce__assessment_weaknesses_desc__c,arce__anlys_wkfl_sbanlys_status_type__c,WF_Stage_WF_Status__c,
        arce__gf_backlog_sales_cyr_number__c,arce__gf_backlog_sales_pyr_number__c,arce__gf_backlog_pending_cyr_amount__c,arce__gf_backlog_pending_pyr_amount__c,
        arce__prop_int_mod_rec_type__c,arce__risk_unit_confirm_desc__c,arce__contrast_risk_unit_type__c,arce__contrast_risk_unit_neg_desc__c,arce__contrast_risk_unit_posi_desc__c,
        arce__Analysis__r.arce__wf_status_id__c,arce__Analysis__r.arce__Stage__c,arce__call_limit_service__c,arce__last_update_policie__c,arce__last_update_position__c,arce__outstanding_carrousel__c,
        arce__gf_market_share_desc__c,arce__mngmt_aggressiveness_type__c,Is_Deleted__c, arce__debt_maturity_desc__c
                FROM arce__Account_has_Analysis__c
                WHERE Id = :recordId];
    }
    /**
    * -------------------------------------------------------------------------------------------------------
    * @Description - Method that return some related fields in arce__Account_has_Analysis__c
    * -------------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 24/06/2019
    * @param recordId - id of the arce__Account_has_Analysis__c object to consult
    * @return arce__Account_has_Analysis__c - Object with all related fields in arce__Account_has_Analysis__c
    * @example getAccHasAnalysis(recordId)
    * -------------------------------------------------------------------------------------------------------
    **/
    public static arce__Account_has_Analysis__c getAccHasRelation(Id recordId) {
        return [SELECT arce__Customer__c, arce__group_asset_header_type__c,arce__Customer__r.Name,arce__Customer__r.ParentId,arce__Customer__r.AccountNumber, arce__Analysis__c, arce__Analysis__r.arce__Group__c, arce__Analysis__r.arce__Group__r.Name, arce__Analysis__r.arce__Stage__c,arce__Analysis__r.arce__analysis_customer_relation_type__c FROM arce__Account_has_Analysis__c WHERE Id = :recordId];
    }
    /**
    * -------------------------------------------------------------------------------------------------------
    * @Description - Method that insert or update arce__Account_has_Analysis__c list
    * -------------------------------------------------------------------------------------------------------
    * @param ltsObjects - List of SObject to insert or update
    * @return void
    * @example upsertObjects(ltsObjects)
    * -------------------------------------------------------------------------------------------------------
    **/
    public static void upsertObjects (List<sObject> ltsObjects) {
        if ( !ltsObjects.isEmpty() ) {
            upsert ltsObjects;
        }
    }
    /**
    * -------------------------------------------------------------------------------------------------------
    * @Description - Method that returns the status of the rating
    * -------------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 12/07/2019
    * @param recordId - Id of the account has analysis object
    * @return arce__Account_has_Analysis__c - account has analysis object
    * @example getRatingStatus(recordId)
    * -------------------------------------------------------------------------------------------------------
    **/
    public static List<arce__Account_has_Analysis__c> getRatingStatus(List<Id> recordId) {
        return [SELECT Id, arce__Customer__c, arce__Customer__r.Name, arce__ffss_for_rating_id__r.arce__rating_id__r.arce__status_type__c FROM arce__Account_has_Analysis__c WHERE Id = :recordId];
    }
    /**
    * -------------------------------------------------------------------------------------------------------
    * @Description - Method that returns the status of the rating
    * -------------------------------------------------------------------------------------------------------
    * @Author   Luis Aturo Parra Rosas
    * @Date     Created: 12/07/2019
    * @param recordId - Id of the account has analysis object
    * @return arce__Account_has_Analysis__c - account has analysis object
    * @example   public static arce__Account_has_Analysis__c getAccForResume(Id recordId)
    * -------------------------------------------------------------------------------------------------------
    **/
    public static List<arce__Account_has_Analysis__c> getAccForResume(Set<Id> recordId) {
        return [SELECT Id FROM arce__Account_has_Analysis__c WHERE arce__Customer__c IN :recordId];
    }
    /**
    *-------------------------------------------------------------------------------
    * @description get customers who are in review
    --------------------------------------------------------------------------------
    * @author luisarturo.parra.contractor@bbva.com
    * @date 5/10/2019
    * @param String record Id to query
    * @return List < arce__Account_has_Analysis__c > list that match the condition
    * @example public static List < arce__Account_has_Analysis__c > getCustomersData()
    **/
    public static  arce__Account_has_Analysis__c getCustomersData(String customer) {
        arce__Account_has_Analysis__c aha = [SELECT Id,arce__Customer__c FROM arce__Account_has_Analysis__c WHERE arce__InReview__c = true AND Id = : customer limit 1 ];
        return aha;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that return a list of all fields in arce__Account_has_Analysis__c object with the variable InReview in True for an ARCE
    * -----------------------------------------------------------------------------------------------
    * @Author   Juan Ignacio Hita juanignacio.hita.contractor@bbva.com
    * @Date     Created: 06/11/2019
    * @return List<arce__Account_has_Analysis__c> - List of arce__Account_has_Analysis__c object
    * @example getAccHasAnFromArce(arceId)
    * -----------------------------------------------------------------------------------------------
    **/
    public static List<arce__Account_has_Analysis__c> getAccHasAnFromArce(Id arceId) {
        return [SELECT Id, arce__main_subsidiary_ind_type__c, arce__Customer__r.Name, arce__ffss_for_rating_id__r.arce__rating_final__c, arce__ffss_for_rating_id__c, arce__group_asset_header_type__c,arce__Customer__r.ParentId,arce__Customer__r.AccountNumber, arce__Customer__c, arce__Analysis__c, arce__Analysis__r.arce__analysis_customer_relation_type__c FROM arce__Account_has_Analysis__c WHERE arce__InReview__c = true AND arce__Analysis__c = :arceId];
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that return a list of all fields in arce__Account_has_Analysis__c object with the variable InReview in True for an ARCE
    * -----------------------------------------------------------------------------------------------
    * @Author   Juan Ignacio Hita juanignacio.hita.contractor@bbva.com
    * @Date     Created: 06/11/2019
    * @return List<arce__Account_has_Analysis__c> - List of arce__Account_has_Analysis__c object
    * @example getAccHasAnFromArce(arceId)
    * -----------------------------------------------------------------------------------------------
    **/
    public static List<arce__Account_has_Analysis__c> getAllAccFromArce(Id arceId) {
        return [SELECT Id, arce__main_subsidiary_ind_type__c, arce__Customer__r.Name, arce__ffss_for_rating_id__r.arce__rating_final__c, arce__ffss_for_rating_id__c, arce__group_asset_header_type__c,arce__Customer__r.ParentId,arce__Customer__r.AccountNumber, arce__Customer__c FROM arce__Account_has_Analysis__c WHERE arce__Analysis__c = :arceId];
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that update a list of arce_Account_has_Analysis__c
    * -----------------------------------------------------------------------------------------------
    * @Author   Juan Ignacio Hita juanignacio.hita.contractor@bbva.com
    * @Date     Created: 06/11/2019
    * @param lstAccHasAnalysis - List of arce__Account_has_Analysis__c object to update
    * @return void
    * @example updateAccHasAnalysis(lstAccHasAnalysis)
    * -----------------------------------------------------------------------------------------------
    **/
    public static void updateAccHasAnalysis(List<arce__Account_has_Analysis__c> lstAccHasAnalysis) {
        update lstAccHasAnalysis;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description return the ids of child accounts
    *--------------------------------------------------------------------------------
    * @date 21/11/2019
    * @author luisarturo.parra.contractor@bbva.com
    * @param parenId Account
    * @return a List<arce__Account_has_Analysis__c> that contains the child ids of a parent account
    * @example getIdsOfChildAccountNotSelected(Id groupId)
    */
    public static List<arce__Account_has_Analysis__c> getIdsOfChildAccountNotSelected(Id groupId) {
        return [SELECT Id, arce__Customer__c From arce__Account_has_Analysis__c WHERE arce__group_asset_header_type__c = '1' AND arce__InReview__c =: true];
    }
    /**
    *-------------------------------------------------------------------------------
    * @description returns a list of account_has_analysis related to the given ARCE id
    *--------------------------------------------------------------------------------
    * @date		10/12/2019
    * @author	manuelhugo.castillo.contractor@bbva.com
    * @param	String arceId - ARCE Id
    * @return	List<arce__Account_has_Analysis__c>
    * @example	public static List<arce__Account_has_Analysis__c> getAllAnalysis(String arceId)
    */
    public static List<Arc_Gen_Account_Has_Analysis_Wrapper> getAllAnalysis(String arceId) {
        Id arce = [SELECT arce__Analysis__c FROM arce__Account_has_Analysis__c WHERE Id =:arceId].arce__Analysis__c;
        List<arce__Account_has_Analysis__c> miniArceList = [SELECT Id,arce__Customer__c,arce__Customer__r.Name,arce__group_asset_header_type__c,arce__Customer__r.ParentId,arce__Customer__r.AccountNumber FROM arce__Account_has_Analysis__c WHERE arce__Analysis__c =: arce AND arce__InReview__c = true];
        Set<Id> setAccWithAHA = new Set<Id>();
        for(arce__Account_has_Analysis__c aha : miniArceList) {
            setAccWithAHA.add(aha.arce__Customer__c);
        }
        List<Id> lstAccsId = new List<Id>(setAccWithAHA);
        Map<Id, Arc_Gen_Account_Wrapper> mapAccount = Arc_Gen_Account_Locator.getAccountInfoById(lstAccsId);
        List<Arc_Gen_Account_Has_Analysis_Wrapper> lstAHAWrapper = new List<Arc_Gen_Account_Has_Analysis_Wrapper>();
        for(arce__Account_has_Analysis__c aha : miniArceList) {
            Arc_Gen_Account_Has_Analysis_Wrapper newAHAWrapper = new Arc_Gen_Account_Has_Analysis_Wrapper();
            newAHAWrapper.ahaObj = aha;
            newAHAWrapper.accWrapperObj = mapAccount.get(aha.arce__Customer__c);
            lstAHAWrapper.add(newAHAWrapper);
        }
        Return lstAHAWrapper;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Method that gets an AccountHasAnalysis record
    --------------------------------------------------------------------------------
    * @author eduardoefrain.hernandez.contractor@bbva.com
    * @date 28/05/2019
    * @param String analysisId - Analysis Id
    * @param String customerId - Customer Id
    * @return List<arce__Account_has_Analysis__c> - List of Account Has Analysis
    * @example public List<arce__Account_has_Analysis__c> getAccountHasAnalysis(String analysisId,String customerId)
    **/
    public static List<Arc_Gen_Account_Has_Analysis_Wrapper> getAccountHasAnalysis(String analysisId,String customerId) {
        List<arce__Account_has_Analysis__c> ltsAHAs = [SELECT id,Name,arce__path__c,arce__Customer__c,arce__Customer__r.AccountNumber FROM arce__Account_has_Analysis__c WHERE arce__Analysis__c =: analysisId AND arce__Customer__c =: customerId];
        return getListAHAWrapper(ltsAHAs);
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Method that gets an AccountHasAnalysis record
    --------------------------------------------------------------------------------
    * @author eduardoefrain.hernandez.contractor@bbva.com
    * @date 28/05/2019
    * @param String analysisId - Analysis Id
    * @param String customerId - Customer Id
    * @return List<arce__Account_has_Analysis__c> - List of Account Has Analysis
    * @example public List<arce__Account_has_Analysis__c> getAccountHasAnalysis(String analysisId,String customerId)
    **/
    public static List<Arc_Gen_Account_Has_Analysis_Wrapper> getAccountHasAnalysisAndCustomer(List<String> accHasAnalysisId) {
        return getListAHAWrapper(getAccHasAnalysis(accHasAnalysisId));
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Method 'getListAHAWrapper' that gets Account Has Analysis Wrapper List
    --------------------------------------------------------------------------------
    * @author manuelhugo.castillo.contractor@bbva.com
    * @date 11/12/2019
    * @param List<arce__Account_has_Analysis__c> ltsAHAs - Account Has Analysis List
    * @return List<Arc_Gen_Account_Has_Analysis_Wrapper> - List of Account Has Analysis wrapper
    * @example List<Arc_Gen_Account_Has_Analysis_Wrapper> getListAHAWrapper(List<arce__Account_has_Analysis__c> ltsAHAs)
    **/
    public static List<Arc_Gen_Account_Has_Analysis_Wrapper> getListAHAWrapper(List<arce__Account_has_Analysis__c> ltsAHAs) {
        Set<Id> setAccWithAHA = new Set<Id>();
        for (arce__Account_has_Analysis__c aha : ltsAHAs) {
            setAccWithAHA.add(aha.arce__Customer__c);
        }
        List<Id> lstAccsId = new List<Id>(setAccWithAHA);
        final Map<Id, Arc_Gen_Account_Wrapper> mapAccount = Arc_Gen_Account_Locator.getAccountInfoById(lstAccsId);
        List<Arc_Gen_Account_Has_Analysis_Wrapper> lstAHAWrapper = new List<Arc_Gen_Account_Has_Analysis_Wrapper>();
        for (arce__Account_has_Analysis__c aha : ltsAHAs) {
            Arc_Gen_Account_Has_Analysis_Wrapper newAHAWrapper = new Arc_Gen_Account_Has_Analysis_Wrapper();
            newAHAWrapper.ahaObj = aha;
            newAHAWrapper.accWrapperObj = mapAccount.get(aha.arce__Customer__c);
            lstAHAWrapper.add(newAHAWrapper);
        }
        return lstAHAWrapper;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description set delete logic - update the field Is_Deleted__c
    *--------------------------------------------------------------------------------
    * @date		05/02/2020
    * @author	juanignacio.hita.contractor@bbva.com
    * @param	List<Id> list of record id of account has analysis
    * @return	void
    * @example	Arc_Gen_AccHasAnalysis_Data.setDeleteLogic(new List<Id>{accHasId});
    */
    public static void setDeleteLogic(Map<String, String> lstIds) {
        List<String> lstKeys = new List<String>();
        lstKeys.addAll(lstIds.keySet());
        final List<arce__Account_has_Analysis__c> lstAccHasAnalysis = getAccHasAnalysis(lstKeys);
        for (arce__Account_has_Analysis__c acc : lstAccHasAnalysis) {
            acc.Is_Deleted__c = true;
            acc.arce__InReview__c = false;
        }
        upsertObjects(lstAccHasAnalysis);
    }
}