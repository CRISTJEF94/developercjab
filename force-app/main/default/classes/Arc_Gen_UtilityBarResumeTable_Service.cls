/**
* @File Name          : Arc_GenUtilityBarResume_Test.cls
* @Description        : test of table
* @Author             :luisarturo.parra.contractor@bbva.com
* @Group              : ARCE
* @Last Modified By   : luisarturo.parra.contractor@bbva.com
* @Last Modified On   : 15/10/2019 05:17
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author                 Modification
*==============================================================================
* 1.0    15/10/2019           luisarturo.parra.contractor@bbva.com     Initial Version
**/
public class Arc_Gen_UtilityBarResumeTable_Service {
    /**private constructor for sonar**/
    private Arc_Gen_UtilityBarResumeTable_Service(){}
    /**
    *-------------------------------------------------------------------------------
    * @description get data for the table
    *-------------------------------------------------------------------------------
    * @date 30/04/2019
    * @author luisarturo.parra.contractor@bbva.com
    * @param recordId analysisId of the account has analysis
    * @return  List < arce__limits_exposures__c >
    * @example public static List < arce__limits_exposures__c > getTableData(Id recordId) {
    */
    public static List<List<String>> getTableData(Id recordId) {
        List<arce__Account_has_Analysis__c> ahas = Arc_Gen_AccHasAnalysis_Data.getAccHasAnFromArce(recordId);
        set < Id > accountHAIds = new set < Id > ();
        for (arce__Account_has_Analysis__c childAHA: ahas) {
            if(childAHA.arce__group_asset_header_type__c  != '1')
                accountHAIds.add(childAHA.Id);
        }
        List < arce__limits_exposures__c > limits = Arc_Gen_LimitsExposures_Data.getExposureDatafromIds(accountHAIds);
        return orderdata(limits);
    }
    /**
*-------------------------------------------------------------------------------
* @description method to order the data
*-------------------------------------------------------------------------------
* @date 30/04/2019
* @author luisarturo.parra.contractor@bbva.com
* @param multidimensionarray array with wrapper objects serialized
* @param operationvalues wrapper object serialized
* @return String
* @example public static String orderdata(String multidimensionarray, String operationvalues) {
*/
    public static List<List<String>> orderdata(List < arce__limits_exposures__c > limits) {
        Map<String, List<String>> orderbyfieldmap = new Map<String,List<String>>();
        List<Arc_Gen_typologiesDataJunction> typowrap = new List<Arc_Gen_typologiesDataJunction>();
        for (arce__limits_exposures__c data : limits) {
            if(orderbyfieldmap.containsKey(data.arce__account_has_analysis_id__r.arce__Customer__c)) {
                Arc_Gen_typologiesDataJunction typos = new Arc_Gen_typologiesDataJunction();
                typos.typonames.typologyname = data.arce__limits_typology_id__r.Name;
                typos.typonames.typologyammount =  data.arce__current_proposed_amount__c;
                typos.typonames.typologykey = data.arce__limits_typology_id__r.arce__risk_typology_level_id__c;
                typos.customerinfo.customerName = data.arce__account_has_analysis_id__r.arce__Customer__r.Name;
                typos.customerinfo.customerId = data.arce__account_has_analysis_id__r.arce__Customer__c;
                orderbyfieldmap.get(data.arce__account_has_analysis_id__r.arce__Customer__c).add(JSON.serialize(typos));
            } else {
                List<String> typowraps = new List<String>();
                Arc_Gen_typologiesDataJunction typos = new Arc_Gen_typologiesDataJunction();
                typos.typonames.typologyname = data.arce__limits_typology_id__r.Name;
                typos.typonames.typologyammount =  data.arce__current_proposed_amount__c;
                typos.typonames.typologykey = data.arce__limits_typology_id__r.arce__risk_typology_level_id__c;
                typos.customerinfo.customerName = data.arce__account_has_analysis_id__r.arce__Customer__r.Name;
                typos.customerinfo.customerId = data.arce__account_has_analysis_id__r.arce__Customer__c;
                typowraps.add(JSON.serialize(typos));
                orderbyfieldmap.put(typos.customerinfo.customerId,typowraps );
            }
        }
        List<List<String>> fulldata = new List<List<String>>();
        for(String key : orderbyfieldmap.keyset()){
          fulldata.add(orderbyfieldmap.get(key));
        }
        return fulldata;
    }
}