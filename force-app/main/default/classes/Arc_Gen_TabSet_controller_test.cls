/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_TabSet_controller_test
* @Author   Mario Humberto Ramirez Lio - mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2019-07-17
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Test class for code coverage of: Arc_Gen_TabSet_controller, Arc_Gen_TabSet_service
* and  Arc_Gen_TabSet_ServiceHelper
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |11/04/2019 diego.miguel.contractor@bbva.com
*             Class creation.
* |2019-07-17 mariohumberto.ramirez.contractor@bbva.com
*             Documentation and refactor for change in completness.
* |2019-07-29 mariohumberto.ramirez.contractor@bbva.com
*             Add new lines of data in order to increment test coverage.
* |2019-08-08 mariohumberto.ramirez.contractor@bbva.com
*             Add new line Arc_UtilitysDataTest_tst.createIndustryAnalysisData()
*             in order to increment test coverage.
* |2019-08-09 mariohumberto.ramirez.contractor@bbva.com
*             Add new method "testContructorSector" in order to increment test coverage of the
*             Arc_Gen_Sector_Data class.
* |2019-11-20 mariohumberto.ramirez.contractor@bbva.com
*             Update test class
* |2020-01-09 javier.soto.carrascosa@bbva.com
*             Adapt test classess with account wrapper and setupaccounts
* -----------------------------------------------------------------------------------------------
*/
@SuppressWarnings('sf:TooManyMethods')
@isTest
public class Arc_Gen_TabSet_controller_test {
    /**
    * --------------------------------------------------------------------------------------
    * @Description Setup method for Arc_Gen_TabSet_controller_test
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio - mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-17
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.setup()
    * --------------------------------------------------------------------------------------
    **/
    @testSetup static void setup() {
        Arc_UtilitysDataTest_tst.setupAcccounts();
        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{'G000001'});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get('G000001');

        final arce__Sector__c newSector = arc_UtilitysDataTest_tst.crearSector('Generic', '100', 's-01', null);
        insert newSector;

        final arce__Analysis__c newArce = arc_UtilitysDataTest_tst.crearArceAnalysis('Arce Analysis', null, groupAccount.accId);
        newArce.arce__wf_status_id__c = '03';
        insert newArce;

        final arce__Account_has_Analysis__c newAnalysis = arc_UtilitysDataTest_tst.crearAccHasAnalysis(newSector.id, newArce.Id, groupAccount.accId, 's-01');
        insert newAnalysis;

        final arce__limits_typology__c typology = Arc_UtilitysDataTest_tst.crearLimitTypology('TOTAL Financial Risk ST & Commercial Risk', null, 'TP_0001');
        insert typology;

        Arc_UtilitysDataTest_tst createExposures = new Arc_UtilitysDataTest_tst();
        final arce__limits_exposures__c exposure1 = createExposures.crearExposures(typology.Id, newAnalysis.Id, 7);
        insert exposure1;

        Arc_UtilitysDataTest_tst.createBasicDataData();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Method that returns the JSON to build tabset component
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio - mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-17
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testGetTabsSuccess()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testGetTabsSuccess() {
        final arce__Account_has_Analysis__c accHasAnls = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
        Test.startTest();
        final String tabWrapper = Arc_Gen_TabSet_controller.getTabsJson(accHasAnls.Id);
        system.assertEquals(String.isEmpty(tabWrapper), false, 'The result is not empty');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Method that returns a message of error
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio - mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-17
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testGetTabsError()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testGetTabsError() {
        final Account acc = [SELECT Id FROM Account LIMIT 1];
        Test.startTest();
        final String tabWrapper2 = Arc_Gen_TabSet_controller.getTabsJson(acc.id);
        system.assertEquals(String.isEmpty(tabWrapper2), false, 'The result is not empty');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Method that returns the JSON to build tabset component
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio - mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-17
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testGetComple()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testGetComple() {
        final arce__Account_has_Analysis__c accHasAnls = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
        accHasAnls.arce__group_asset_header_type__c = '2';
        accHasAnls.arce__magnitude_unit_type__c = '1';
        accHasAnls.arce__prev_magnitude_unit_type__c = '2';
        upsert accHasAnls;

        final arce__Analysis__c arceAnalysis = [SELECT Id FROM arce__Analysis__c LIMIT 1];
        arceAnalysis.arce__Stage__c = '1';
        upsert arceAnalysis;

        Test.startTest();
        final Arc_Gen_TabSet_controller.TabsWrapper result = Arc_Gen_TabSet_controller.callTemplateAnalysisJson(accHasAnls.Id);
        system.assertEquals(String.isEmpty(result.gblDescriptionResponse), false, 'The result is not empty');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Method that returns the JSON to build tabset component
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio - mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-17
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testGetComple2()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testGetComple2() {
        final arce__Account_has_Analysis__c accHasAnls = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
        accHasAnls.arce__group_asset_header_type__c = '2';
        accHasAnls.arce__magnitude_unit_type__c = '1';
        accHasAnls.arce__prev_magnitude_unit_type__c = '3';
        upsert accHasAnls;

        final arce__Analysis__c arceAnalysis = [SELECT Id FROM arce__Analysis__c LIMIT 1];
        arceAnalysis.arce__Stage__c = '2';
        upsert arceAnalysis;

        Arc_UtilitysDataTest_tst.createIndustryAnalysisData();

        Test.startTest();
        final Arc_Gen_TabSet_controller.TabsWrapper result = Arc_Gen_TabSet_controller.callTemplateAnalysisJson(accHasAnls.Id);
        system.assertEquals(String.isEmpty(result.gblDescriptionResponse), false, 'The result is not empty');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Method that returns the JSON to build tabset component
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio - mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-17
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testMultifactor1()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testMultifactor1() {
        final arce__Account_has_Analysis__c accHasAnls = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
        accHasAnls.arce__group_asset_header_type__c = '2';
        accHasAnls.arce__magnitude_unit_type__c = '1';
        accHasAnls.arce__prev_magnitude_unit_type__c = '4';
        upsert accHasAnls;

        final arce__Analysis__c arceAnalysis = [SELECT Id FROM arce__Analysis__c LIMIT 1];
        arceAnalysis.arce__Stage__c = '2';
        upsert arceAnalysis;

        Arc_UtilitysDataTest_tst.createIndustryAnalysisData();

        Test.startTest();
        final Arc_Gen_TabSet_controller.TabsWrapper result = Arc_Gen_TabSet_controller.callTemplateAnalysisJson(accHasAnls.Id);
        system.assertEquals(String.isEmpty(result.gblDescriptionResponse), false, 'The result is not empty');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Method that returns the JSON to build tabset component
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio - mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-17
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testMultifactor2()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testMultifactor2() {
        final arce__Account_has_Analysis__c accHasAnls = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
        accHasAnls.arce__group_asset_header_type__c = '2';
        accHasAnls.arce__magnitude_unit_type__c = '4';
        accHasAnls.arce__prev_magnitude_unit_type__c = '3';
        upsert accHasAnls;

        final arce__Analysis__c arceAnalysis = [SELECT Id FROM arce__Analysis__c LIMIT 1];
        arceAnalysis.arce__Stage__c = '2';
        upsert arceAnalysis;

        Arc_UtilitysDataTest_tst.createIndustryAnalysisData();

        Test.startTest();
        final Arc_Gen_TabSet_controller.TabsWrapper result = Arc_Gen_TabSet_controller.callTemplateAnalysisJson(accHasAnls.Id);
        system.assertEquals(String.isEmpty(result.gblDescriptionResponse), false, 'The result is not empty');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Method that returns the JSON to build tabset component
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio - mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-17
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testMultifactor3()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testMultifactor3() {
        final arce__Account_has_Analysis__c accHasAnls = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
        accHasAnls.arce__group_asset_header_type__c = '2';
        accHasAnls.arce__magnitude_unit_type__c = '4';
        accHasAnls.arce__prev_magnitude_unit_type__c = '2';
        upsert accHasAnls;

        final arce__Analysis__c arceAnalysis = [SELECT Id FROM arce__Analysis__c LIMIT 1];
        arceAnalysis.arce__Stage__c = '2';
        upsert arceAnalysis;

        Arc_UtilitysDataTest_tst.createIndustryAnalysisData();

        Test.startTest();
        final Arc_Gen_TabSet_controller.TabsWrapper result = Arc_Gen_TabSet_controller.callTemplateAnalysisJson(accHasAnls.Id);
        system.assertEquals(String.isEmpty(result.gblDescriptionResponse), false, 'The result is not empty');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Method that returns the JSON to build tabset component
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio - mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-17
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testMultifactor4()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testMultifactor4() {
        final arce__Account_has_Analysis__c accHasAnls = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
        accHasAnls.arce__group_asset_header_type__c = '2';
        accHasAnls.arce__magnitude_unit_type__c = '4';
        accHasAnls.arce__prev_magnitude_unit_type__c = '1';
        upsert accHasAnls;

        final arce__Analysis__c arceAnalysis = [SELECT Id FROM arce__Analysis__c LIMIT 1];
        arceAnalysis.arce__Stage__c = '2';
        upsert arceAnalysis;

        Arc_UtilitysDataTest_tst.createIndustryAnalysisData();

        Test.startTest();
        final Arc_Gen_TabSet_controller.TabsWrapper result = Arc_Gen_TabSet_controller.callTemplateAnalysisJson(accHasAnls.Id);
        system.assertEquals(String.isEmpty(result.gblDescriptionResponse), false, 'The result is not empty');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Method that returns the JSON to build tabset component
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio - mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-17
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testMultifactor5()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testMultifactor5() {
        final arce__Account_has_Analysis__c accHasAnls = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
        accHasAnls.arce__group_asset_header_type__c = '2';
        accHasAnls.arce__magnitude_unit_type__c = '1';
        accHasAnls.arce__prev_magnitude_unit_type__c = null;
        upsert accHasAnls;

        final arce__Analysis__c arceAnalysis = [SELECT Id FROM arce__Analysis__c LIMIT 1];
        arceAnalysis.arce__Stage__c = '2';
        upsert arceAnalysis;

        Arc_UtilitysDataTest_tst.createIndustryAnalysisData();

        Test.startTest();
        final Arc_Gen_TabSet_controller.TabsWrapper result = Arc_Gen_TabSet_controller.callTemplateAnalysisJson(accHasAnls.Id);
        system.assertEquals(String.isEmpty(result.gblDescriptionResponse), false, 'The result is not empty');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Method that returns the JSON to build tabset component
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio - mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-17
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testUnitConversorError()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testUnitConversorError() {
        Test.startTest();
        final Arc_Gen_GenericUtilities.ConversionWrapper wrapper = new Arc_Gen_GenericUtilities.ConversionWrapper();
        final String result = Arc_Gen_GenericUtilities.convertUnits(wrapper);
        system.assertEquals(result.contains(System.Label.Arc_Gen_UpdateUnitError), true, 'there was a problem in the call to the conversor');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Setup method for Arc_Gen_TabSet_controller_test
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio - mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-17
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testGetCompleError2()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testGetCompleError2() {
        final Account account1 = [SELECT Id FROM Account LIMIT 1];
        upsert account1;

        Test.startTest();
        final Arc_Gen_TabSet_controller.TabsWrapper result = Arc_Gen_TabSet_controller.callTemplateAnalysisJson(account1.Id);
        system.assertEquals(String.isEmpty(result.gblDescriptionResponse), false, 'The result is not empty');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method for void constructor
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-17
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testContructorController()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testContructorController() {
        Test.startTest();
        final Arc_Gen_TabSet_controller controller = new Arc_Gen_TabSet_controller();
        system.assertEquals(controller, controller, 'The test to void contructor was successfull');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method for void contructor
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-17
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testContructorService()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testContructorService() {
        Test.startTest();
        final Arc_Gen_TabSet_service service = new Arc_Gen_TabSet_service();
        system.assertEquals(service, service, 'The test to void contructor was successfull');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method for void contructor
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-17
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testContructorHelper()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testContructorHelper() {
        Test.startTest();
        final Arc_Gen_TabsetService_Helper help = new Arc_Gen_TabsetService_Helper();
        system.assertEquals(help, help, 'The test to void contructor was successfull');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method for void contructor
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-29
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testContructorHelper()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testContructorData() {
        Test.startTest();
        final Arc_Gen_ArceAnalysis_Data data = new Arc_Gen_ArceAnalysis_Data();
        system.assertEquals(data, data, 'The test to void contructor was successfull');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method for void contructor
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-29
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testContructorData2()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testContructorData2() {
        Test.startTest();
        final Arc_Gen_AccHasAnalysis_Data data = new Arc_Gen_AccHasAnalysis_Data();
        system.assertEquals(data, data, 'The test to void contructor was successfull');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Test method for void contructor
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-08-09
    * @param void
    * @return void
    * @example Arc_Gen_TabSet_controller_test.testContructorSector()
    * --------------------------------------------------------------------------------------
    **/
    @isTest static void testContructorSector() {
        Test.startTest();
        final Arc_Gen_Sector_Data data = new Arc_Gen_Sector_Data();
        system.assertEquals(data, data, 'The test to the void contructor was successfull');
        Test.stopTest();
    }
}