/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_PotitionBankTable_Service
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2019-07-24
* @Group    ARCE
* --------------------------------------------------------------------------------------------------------------------
* @Description Service class for Arc_Gen_PotitionBankTable_Ctlr.
* --------------------------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-07-24 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* |2019-11-14 mariohumberto.ramirez.contractor@bbva.com
*             Added new param typoOfCustomer in getRowsGroup and getRowsFilial methods.
* |2019-11-27 mariohumberto.ramirez.contractor@bbva.com
*             Change the object limits exposure to risk position summary
* |2020-01-28 mariohumberto.ramirez.contractor@bbva.com
*             Added logic to manage automatic position
* --------------------------------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_PotitionBankTable_Service_Helper {
    /**
    * ----------------------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-24
    * @param void
    * @return void
    * @example Arc_Gen_PotitionBankTable_Service_Helper servicehelper = new Arc_Gen_PotitionBankTable_Service_Helper()
    * ----------------------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_PotitionBankTable_Service_Helper() {

    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description insert TOTAL if is missing
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-24
    * @param recordId - id of the acc has analysis object
    * @return void
    * @example insertTotal(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static Boolean insertTotal(Id recordId) {
        final arce__risk_position_summary__c riskPositionSummary = new arce__risk_position_summary__c();
        final arce__Account_has_Analysis__c accHasAnData = Arc_Gen_AccHasAnalysis_Data.getAccHasRelation(recordId);
        riskPositionSummary.arce__account_has_analysis_id__c = recordId;
        riskPositionSummary.recordTypeId = Arc_Gen_Risk_Position_summary_Data.getRecordTypeRiskPositionSum('Comments');
        riskPositionSummary.arce__account_Id__c = accHasAnData.arce__Customer__c;
        riskPositionSummary.arce__banrel_current_limit_name__c = 0;
        riskPositionSummary.arce__banrel_commitment_name__c = 0;
        riskPositionSummary.arce__banrel_uncommitment_name__c = 0;
        riskPositionSummary.arce__banrel_outstanding_name__c = 0;
        riskPositionSummary.arce__banrel_comments_desc__c = '';
        insert riskPositionSummary;
        return true;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return the column information of the table
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-24
    * @param recordId - id of the acc has analysis object
    * @return columns information of the potition bank table
    * @example getColumns(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static List<Arc_Gen_TableColumns> getColumns(Id recordId) {
        final String labels = Arc_Gen_GenericUtilities.getTypeOfCustomer(recordId) == System.Label.Cls_arce_Client ? System.Label.Arc_Gen_Product : System.Label.Arc_Gen_Subsidiate;
        Map<String,String> attributes = new Map<String,String>();
        attributes.put('alignment','center');
        Arc_Gen_TableColumns product = new Arc_Gen_TableColumns();
        product.type = 'text';
        product.fieldName = 'product';
        product.label = labels;
        product.initialWidth = 200;
        product.cellAttributes = attributes;
        Arc_Gen_TableColumns currentLimit = new Arc_Gen_TableColumns();
        currentLimit.type = 'number';
        currentLimit.fieldName = 'currentLimit';
        currentLimit.label = System.Label.Arc_Gen_Current_limit;
        currentLimit.cellAttributes = attributes;
        Arc_Gen_TableColumns commited = new Arc_Gen_TableColumns();
        commited.type = 'number';
        commited.fieldName = 'commited';
        commited.label = System.Label.Arc_Gen_Committed;
        commited.cellAttributes = attributes;
        Arc_Gen_TableColumns uncommited = new Arc_Gen_TableColumns();
        uncommited.type = 'number';
        uncommited.fieldName = 'uncommited';
        uncommited.label = System.Label.Arc_Gen_Non_committed;
        uncommited.cellAttributes = attributes;
        Arc_Gen_TableColumns outstanding = new Arc_Gen_TableColumns();
        outstanding.type = 'number';
        outstanding.fieldName = 'outstanding';
        outstanding.label = System.Label.Arc_Gen_Outstanding;
        outstanding.cellAttributes = attributes;
        Arc_Gen_TableColumns comments = new Arc_Gen_TableColumns();
        comments.type = 'text';
        comments.fieldName = 'comments';
        comments.label = System.Label.Arc_Gen_CommentsGuaranties;
        comments.initialWidth = 450;
        comments.cellAttributes = attributes;
        Arc_Gen_TableColumns currencyType = new Arc_Gen_TableColumns();
        currencyType.type = 'text';
        currencyType.fieldName = 'currencyType';
        currencyType.label = 'CURRENCY';
        currencyType.cellAttributes = attributes;
        Arc_Gen_TableColumns unit = new Arc_Gen_TableColumns();
        unit.type = 'text';
        unit.fieldName = 'unit';
        unit.label = 'UNIT';
        unit.cellAttributes = attributes;
        List<Arc_Gen_TableColumns> columns = new List<Arc_Gen_TableColumns>{product,currentLimit,commited,uncommited,outstanding,currencyType,unit,comments};
        return columns;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return the row information of the table
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-24
    * @param recordId -  Id of the account has analysis object
    * @return row information of the table
    * @example getColumns()
    * --------------------------------------------------------------------------------------
    **/
    public static List<Arc_Gen_TableRow> getRowsHelper(Id recordId) {
        final String typoOfCustomer = Arc_Gen_GenericUtilities.getTypeOfCustomer(recordId);
        return typoOfCustomer == 'Group' ? getRowsGroup(recordId, typoOfCustomer) : getRowsFilial(recordId, typoOfCustomer);
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return the row information of the table
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-24
    * @param recordId -  Id of the account has analysis object
    * @return row information of the table
    * @example getRowsFilial()
    * --------------------------------------------------------------------------------------
    **/
    public static List<Arc_Gen_TableRow> getRowsFilial(Id recordId, String typoOfCustomer) {
        List<Arc_Gen_TableRow> rowList = new List<Arc_Gen_TableRow>();
        double currentLimit = 0, commited = 0, uncommited = 0, outstanding = 0;
        Map<String,String> info = new Map<String,String>();
        final List<arce__risk_position_summary__c> riskPositionsummaryDataLts = Arc_Gen_Risk_Position_summary_Data.getPositionSummaryData(new List<Id>{recordId});
        for (arce__risk_position_summary__c summaryData: riskPositionsummaryDataLts) {
            if (summaryData.arce__Product_id__c != null) {
                Arc_Gen_TableRow row = new Arc_Gen_TableRow();
                info.put('clientType', typoOfCustomer);
                info.put('automatic', 'false');
                row.name = summaryData.Id;
                row.product = summaryData.arce__Product_id__r.Name;
                row.recordTypeId = summaryData.recordTypeId;
                row.currentLimit = summaryData.arce__banrel_current_limit_name__c;
                row.commited = summaryData.arce__banrel_commitment_name__c;
                row.uncommited = summaryData.arce__banrel_uncommitment_name__c;
                row.outstanding = summaryData.arce__banrel_outstanding_name__c;
                row.comments = summaryData.arce__banrel_comments_desc__c;
                row.info = info;
                currentLimit += summaryData.arce__banrel_current_limit_name__c;
                commited += summaryData.arce__banrel_commitment_name__c;
                uncommited += summaryData.arce__banrel_uncommitment_name__c;
                outstanding += summaryData.arce__banrel_outstanding_name__c;
                rowList.add(row);
            }
        }
        for (arce__risk_position_summary__c summaryData: riskPositionsummaryDataLts) {
            Arc_Gen_TableRow row = new Arc_Gen_TableRow();
            if (summaryData.arce__Product_id__c == null) {
                info.put('clientType', typoOfCustomer);
                info.put('automatic', 'false');
                row.name = summaryData.Id;
                row.recordTypeId = summaryData.recordTypeId;
                row.product = 'TOTAL';
                row.currentLimit = currentLimit;
                row.commited = commited;
                row.uncommited = uncommited;
                row.outstanding = outstanding;
                row.comments = summaryData.arce__banrel_comments_desc__c;
                row.info = info;
                rowList.add(row);
                summaryData.arce__banrel_current_limit_name__c = currentLimit;
                summaryData.arce__banrel_commitment_name__c = commited;
                summaryData.arce__banrel_uncommitment_name__c = uncommited;
                summaryData.arce__banrel_outstanding_name__c = outstanding;
            }
        }
        Arc_Gen_AccHasAnalysis_Data.upsertObjects(riskPositionsummaryDataLts);
        return rowList;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return the row information of the table
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-24
    * @param recordId -  Id of the account has analysis object
    * @return row information of the table
    * @example getRowsGroup(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static List<Arc_Gen_TableRow> getRowsGroup(Id recordId, String typoOfCustomer) {
        List<Arc_Gen_TableRow> rowList = new List<Arc_Gen_TableRow>();
        final List<arce__risk_position_summary__c> riskPositionsummaryDataGroup = Arc_Gen_Risk_Position_summary_Data.getPositionSummaryData(new List<Id>{recordId});
        final List<arce__risk_position_summary__c> allClientPositionSummDataGp = getAllClientRiskSummData(recordId);
        for (arce__risk_position_summary__c summaryData: allClientPositionSummDataGp) {
            if (summaryData.arce__account_Id__c != null) {
                Arc_Gen_TableRow row = new Arc_Gen_TableRow();
                Map<String,String> info = new Map<String,String>();
                info.put('clientType', typoOfCustomer);
                info.put('automatic', 'false');
                row.name = summaryData.Id;
                row.recordTypeId = summaryData.recordTypeId;
                row.product = summaryData.arce__account_Id__r.Name;
                row.currentLimit = summaryData.arce__banrel_current_limit_name__c;
                row.commited = summaryData.arce__banrel_commitment_name__c;
                row.uncommited = summaryData.arce__banrel_uncommitment_name__c;
                row.outstanding = summaryData.arce__banrel_outstanding_name__c;
                row.comments = summaryData.arce__banrel_comments_desc__c;
                row.info = info;
                rowList.add(row);
            }
        }
        for (arce__risk_position_summary__c summaryData: riskPositionsummaryDataGroup) {
            Arc_Gen_TableRow row = new Arc_Gen_TableRow();
            Map<String,String> info = new Map<String,String>();
            info.put('clientType', typoOfCustomer);
            info.put('automatic', 'false');
            row.name = summaryData.Id;
            row.recordTypeId = summaryData.recordTypeId;
            row.product = 'TOTAL';
            row.currentLimit = summaryData.arce__banrel_current_limit_name__c;
            row.commited = summaryData.arce__banrel_commitment_name__c;
            row.uncommited = summaryData.arce__banrel_uncommitment_name__c;
            row.outstanding = summaryData.arce__banrel_outstanding_name__c;
            row.comments = summaryData.arce__banrel_comments_desc__c;
            row.info = info;
            rowList.add(row);
        }
        return rowList;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return a list of arce__limits_exposures__c
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-07-25
    * @param recordId -  Id of the account has analysis object
    * @return List<arce__limits_exposures__c>
    * @example getAllClientExposureData(recordId)
    * --------------------------------------------------------------------------------------
    **/
    public static List<arce__risk_position_summary__c> getAllClientRiskSummData(Id recordId) {
        final Id arceAnalysisId = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<String>{(String)recordId})[0].arce__Analysis__c;
        final List<Id> arceAnalysisIdLts = Arc_Gen_GenericUtilities.getIdsOfSubsidiaries(arceAnalysisId);
        return Arc_Gen_Risk_Position_summary_Data.getPositionSummaryData(arceAnalysisIdLts);
    }
}