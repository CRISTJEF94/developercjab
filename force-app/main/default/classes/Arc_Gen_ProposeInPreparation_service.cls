/*------------------------------------------------------------------
* @File Name : Arc_Gen_ProposeInPreparation_Service
* @Author:      Luisarturo.parra.contractor@bbva.com
* @Project:     ARCE - BBVA Bancomer
* @Group:       ARCE
* @Description:   Test class for code coverage of:
* @Last Modified By   : luisruben.quinto.munoz@bbva.com
* @Last Modified On   : 28/1/2020 11:47:10
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |11/04/2019 luisarturo.parra.contractor@bbva.com
*             Class creation. /Refactorizacion
* |10/12/2019 juanmanuel.perez.ortiz.contractor@bbva.com
*             Add logic to assign previous and owner user
* |26/12/2019 juanmanuel.perez.ortiz.contractor@bbva.com
*             Add logic to send notifications
* |2020-01-24 luisruben.quinto.munoz@bbva.com
*             Change to without sharing and added field arce__proposeUser__c and fix comments
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_ProposeInPreparation_service {
    /**
    *-------------------------------------------------------------------------------
    * @description Constructor method for sonnar
    *--------------------------------------------------------------------------------
    * @author juanignacio.hita.contractor@bbva.com
    * @date   05/02/2020
    * @return void
    * @example  private Arc_Gen_ProposeInPreparation_service()
    **/
    @TestVisible
    private Arc_Gen_ProposeInPreparation_service() {
    }
    /**
    *-------------------------------------------------------------------------------
    * @Description  Methot that valid financial statements
    *-------------------------------------------------------------------------------
    * @date  01/10/2019
    * @author mariohumberto.ramirez.contractor@bbva.com
    * @param arceId - Id of the ARCE
    * @param sanction ambit chosen
    * @return wrp wrapper for propose in sanction
    *------------------------------------------------------------------------------
    */
    public static Boolean validateAllRatingsInArce(Id arceId) {
        Boolean ratingValid = true;
        final List<arce__Account_has_Analysis__c> accHasAn = Arc_Gen_AccHasAnalysis_Data.getAccHasAnFromArce(arceId);
        List<String> finStatementIds = new List<String>();
        for (arce__Account_has_Analysis__c accHas: accHasAn) {
            if (accHas.arce__group_asset_header_type__c != '1') { // no se valida para el FS del grupo, a modificar con lógica TD/BU
                finStatementIds.add(accHas.arce__ffss_for_rating_id__c);
            }
        }
        final List<arce__Financial_Statements__c> finStatementObjLts = Arc_Gen_FinancialStatements_locator.getFSInfo(finStatementIds);
        for (arce__Financial_Statements__c fs: finStatementObjLts) {
            if (!Arc_Gen_Balance_Tables_service.validateFSForRating(fs)) {
                ratingValid = false;
            }
        }
        return ratingValid;
    }
    /**
    *-------------------------------------------------------------------------------
    * @Description  Save the reason for the sanction
    *-------------------------------------------------------------------------------
    * @date  26/04/2019
    * @author angel.fuertes2@bbva.com
    * @Method:      Save the reason for the sanction
    * @param recordId record id (arce__Analysis__c)
    * @param sanction sanction
    * @return wrp
    */
    public static Arc_Gen_Delegation_Wrapper saveAction(Arc_Gen_Delegation_Wrapper wrapper, Id accHasAnalysisId, String ambit) {
        final Arc_Gen_User_Wrapper wrpUser = Arc_Gen_User_Locator.getUserInfo(System.UserInfo.getUserId());
        Arc_Gen_Delegation_Wrapper wrapperRet = new Arc_Gen_Delegation_Wrapper();
        if (ambit == wrpUser.ambitUser) {
            wrapperRet = Arc_Gen_Propose_Helper.evaluateDelegationA(wrapper, 'PREPARATION');
        } else {
            final List<String> valuesSelected = new List<String>{ambit};
            wrapper.lstAmbits =  Arc_Gen_GenericUtilities.getPicklistValuesLabels('arce__Analysis__c','arce__anlys_wkfl_edit_br_level_type__c', valuesSelected);
            wrapperRet = Arc_Gen_Propose_Helper.evaluateDelegationElement(wrapper, accHasAnalysisId, 'PREPARATION');
        }
        /**
        Arc_Gen_GenericUtilities.createNotifications(Arc_Gen_Notifications_Service.getUsersIds(arce.Id), arce.Id, System.Label.Arc_Gen_ArcePropose + ': ' + arce.Name);
        Arc_Gen_Traceability.saveEvent(arce.Id, System.Label.Arc_Gen_TraceabilityProposedBy, System.Label.Arc_Gen_TraceabilitySend, System.Label.Arc_Gen_TraceabilityUserCode + ' : ' + wrpUser.profileName + ' | ' + System.Label.Arc_Gen_ExecRepStg + ' : ' + System.Label.Arc_Gen_Stage_02 + ' | ' + System.Label.Arc_Gen_TraceabilityOffice + ' : ' + wrpUser.branchText);
        */
        return wrapperRet;
    }
    /**
    *-------------------------------------------------------------------------------
    * @Description  Methot that returns Account Has Analysis
    *-------------------------------------------------------------------------------
    * @Date   14/11/2019
    * @author manuelhugo.castillo.contractor@bbva.com
    * @param  String - Account Has Analysis recordId
    * @return Account has analysis object
    *------------------------------------------------------------------------------
    */
    public static arce__Account_Has_Analysis__c getAccountHasAnalysis(String id) {
        return Arc_Gen_AccHasAnalysis_Data.getAccHasRelation(id);
    }
    /**
    *-------------------------------------------------------------------------------
    * @Description associate reason for sanction
    *-------------------------------------------------------------------------------
    * @date  09/11/2019
    * @author javier.soto.carrascosa@bbva.com
    * @Method:      associate reason for sanction
    * @param:       Id recordid (arce__Analysis__c) to update reason odf sanction
    * @param:       sanction reason for sanction
    * @return arce__Analysis__c  with the reason of sanctions
    */
    public static arce__Analysis__c associateReasonSanction(Id recordId, String sanction) {
        String ownerId = [SELECT Id,OwnerId FROM arce__Analysis__c WHERE Id =: recordId].OwnerId;
        arce__Analysis__c upArce = new arce__Analysis__c(Id=recordId);
        upArce.put('arce__anlys_wkfl_edit_br_level_type__c',sanction);
        upArce.put('arce__Stage__c','2');
        upArce.put('arce__wf_status_id__c','04');
        upArce.put('arce__anlys_wkfl_prev_user_name__c',ownerId);
        upArce.put('OwnerId',ownerId);
        upArce.put('arce__proposeUser__c' , UserInfo.getUserId());
        return upArce;
    }
}