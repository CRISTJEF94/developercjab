/**
* @File Name          : Arc_Gen_ReturnButton_test.cls
* @Description        : Test Class that receives the response of the rating validation from the service
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE Team
* @Last Modified By   : luisruben.quinto.munoz@bbva.com
* @Last Modified On   : 23/7/2019 19:58:12
* @Changes
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    3/5/2019 18:00:36   eduardoefrain.hernandez.contractor@bbva.com     Initial Version
* 1.1   08/01/2020 13:12:00  javier.soto.carrascosa@bbva.com Add support for account wrapper and setupaccounts
**/
@isTest
public class Arc_Gen_ReturnButton_test {
    /**
    * --------------------------------------------------------------------------------------
    * @Description setup test
    * --------------------------------------------------------------------------------------
    * @Author   javier.soto.carrascosa@bbva.com
    * @Date     Created: 2020-01-08
    * @param void
    * @return void
    * @example setup()
    * --------------------------------------------------------------------------------------
    **/
    @testSetup static void setup() {
        Arc_UtilitysDataTest_tst.setupAcccounts();
        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{'G000001'});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get('G000001');
        arce__Analysis__c arceobj = new arce__Analysis__c();
        arceobj.Name = 'arce__Analysisobj';
        arceobj.arce__analysis_customer_relation_type__c = '01';
        arceobj.CurrencyIsoCode = 'EUR';
        arceobj.arce__Group__c = groupAccount.accId;
        arceobj.arce__Rating__c = 'Hot';
        arceobj.arce__Stage__c = '1';
        arceobj.arce__wf_status_id__c = '02';
        insert arceobj;
        arce__Account_has_Analysis__c arceAccounthasAnalysisobj = new arce__Account_has_Analysis__c();
        arceAccounthasAnalysisobj.currencyIsoCode = 'EUR';
        arceAccounthasAnalysisobj.arce__main_subsidiary_ind_type__c = true;
        arceAccounthasAnalysisobj.arce__InReview__c = true;
        arceAccounthasAnalysisobj.arce__Analysis__c = arceobj.Id;
        arceAccounthasAnalysisobj.arce__smes_eur_comuty_defn_type__c = '1';
        arceAccounthasAnalysisobj.arce__ll_before_adj_ind_type__c = '1';
        arceAccounthasAnalysisobj.arce__ll_before_adj_clsfn_type__c = 'NI';
        arceAccounthasAnalysisobj.arce__ll_after_adj_ind_type__c = '1';
        arceAccounthasAnalysisobj.arce__ll_after_adj_clsfn_type__c = 'NI';
        arceAccounthasAnalysisobj.arce__Customer__c = groupAccount.accId;
        insert arceAccounthasAnalysisobj;
    }
    /**
    * @Method:  test for method constructor
    * @Description: testing method.
    */
    @isTest
    static void testingConstructor() {
        Test.startTest();
        Arc_Gen_ReturnButton_controller contConstructor = new Arc_Gen_ReturnButton_controller();
        Arc_Gen_ReturnButton_service servConstructor = new Arc_Gen_ReturnButton_service();
        System.assertEquals(contConstructor, contConstructor,'Empty constructor');
        System.assertEquals(servConstructor, servConstructor,'Empty constructor');
        Test.stopTest();
    }
    /**
    * @Method:  test for method init delegation
    * @Description: testing method.
    */
    @isTest
    static void testInitDelegation() {
        arce__Account_has_Analysis__c aha = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
        Test.startTest();
        String ret = Arc_Gen_ReturnButton_controller.initDelegation(aha.Id);
        final Arc_Gen_Delegation_Wrapper wrapperSerialize = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) ret, Arc_Gen_Delegation_Wrapper.class);
        System.assertEquals(wrapperSerialize.ambit, '1', 'test init delegation');
        try {
            Arc_Gen_ReturnButton_controller.initDelegation(null);
        } catch (Exception ex) {
            System.assert(ex.getMessage().contains('Script'), 'Script-thrown exception');
        }
        Test.stopTest();
    }
    /**
    * @Method:  test for method init identification
    * @Description: testing method.
    */
    @isTest
    static void testInitIdentification() {
        Arc_Gen_Delegation_Wrapper wrapper = Arc_UtilitysDataTest_tst.crearDelegationWrapper();
        Test.startTest();
        String ret = Arc_Gen_ReturnButton_controller.initIdentification(JSON.serialize(wrapper));
        System.assertEquals(ret, UserInfo.getUserId(), 'init identification method test');
        try {
            Arc_Gen_ReturnButton_controller.initIdentification(null);
        } catch (Exception ex) {
            System.assert(ex.getMessage().contains('Script'), 'Script-thrown exception');
        }
        Test.stopTest();
    }
    /**
    * @Method:  test for method evaluate identification
    * @Description: testing method.
    */
    @isTest
    static void testEvaluateIdentificationA() {
        arce__Account_has_Analysis__c aha = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
        Arc_Gen_Delegation_Wrapper wrapper = Arc_UtilitysDataTest_tst.crearDelegationWrapper();
        Test.startTest();
        String retStr = Arc_Gen_ReturnButton_controller.evaluateIdentification(aha.Id, JSON.serialize(wrapper), UserInfo.getUserId(), '1');
        final Arc_Gen_Delegation_Wrapper wrapperRet = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) retStr, Arc_Gen_Delegation_Wrapper.class);
        System.assertEquals(wrapperRet.codStatus, 200, 'evaluate identification test method');
        try {
            Arc_Gen_ReturnButton_controller.evaluateIdentification(null, null, null, null);
        } catch (Exception ex) {
            System.assert(ex.getMessage().contains('Script'), 'Script-thrown exception');
        }
        Test.stopTest();
    }
    /**
    * @Method:  test for method evaluate identification
    * @Description: testing method.
    */
    @isTest
    static void testEvaluateIdentificationB() {
        arce__Account_has_Analysis__c aha = [SELECT Id FROM arce__Account_has_Analysis__c LIMIT 1];
        Arc_Gen_Delegation_Wrapper wrapper = Arc_UtilitysDataTest_tst.crearDelegationWrapper();
        List<String> valuesSelected = new List<String>{'2'};
        List<Map<String,String>> lstPicklist = Arc_Gen_GenericUtilities.getPicklistValuesLabels('arce__Analysis__c','arce__anlys_wkfl_edit_br_level_type__c', valuesSelected);
        wrapper.lstAmbits = lstPicklist;
        Test.startTest();
        String retStr = Arc_Gen_ReturnButton_controller.evaluateIdentification(aha.Id, JSON.serialize(wrapper), UserInfo.getUserId(), '1');
        final Arc_Gen_Delegation_Wrapper wrapperRet = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) retStr, Arc_Gen_Delegation_Wrapper.class);
        System.assertEquals(wrapperRet.codStatus, 200, 'evaluate identification test method');
        Test.stopTest();
    }
}