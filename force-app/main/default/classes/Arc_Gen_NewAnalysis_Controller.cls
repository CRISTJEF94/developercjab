/**
  * ------------------------------------------------------------------------------------------------
  * @Name     Arc_Gen_NewAnalysis_Controller
  * @Author   luisarturo.parra.contractor@bbva.com
  * @Date     Created: 2019-04-30
  * @Group    ARCE
  * ------------------------------------------------------------------------------------------------
  * @Description Controller of the new analysis clients generator
  * ------------------------------------------------------------------------------------------------
  * @Changes
  *
  * |2019-04-30 eduardoefrain.hernandez.contractor@bbva.com
  *             Class creation.
  * |2019-05-02 diego.miguel.contractor@bbva.com
  *             Added logic to redirect to valid ARCE
  * |2019-05-03 diego.miguel.contractor@bbva.com
  *             Added functions to save ARCE name AND subtype
  * |2019-05-09 diego.miguel.contractor@bbva.com
  *             Added functions to redirect acording to ARCE satatus AND update status
  * |2019-05-14 diego.miguel.contractor@bbva.com
  *             Added methods to groups ws support
  * |2019-05-28 eduardoefrain.hernandez.contractor@bbva.com
  *             Added method to call list customer service
  * |2019-08-13 mariohumberto.ramirez.contractor@bbva.com
  *             clientId param deleted in the setClientSector method
  * |2019-08-13 luisruben.quinto.munoz@bbva.com
  *             arce__Id__c deleted in the class
  * |2019-11-04 juanmanuel.perez.ortiz.contractor@bbva.com
  *             Remove FINAL statement in variable called response inside callPersistence method
  * |2019-11-27 mariohumberto.ramirez.contractor@bbva.com
  *             Added new param to the method setClientSector
  * |2019-11-27 luis.arturo.parra.contractor@bbva.com
  *             Refactorizacion
  * |2019-12-04 manuelhugo.castillo.contractor@bbva.com
  *             Modify method 'getaccdataforservices' replace Account to AccountWrapper
  * |2020-01-13 mariohumberto.ramirez.contractor@bbva.com
  *             Modify methods constructgroupstructure, getPreviousArce and setanalysis
  * |2020-01-09 javier.soto.carrascosa@bbva.com
  *             Remove unused class
  * |2020-01-29 javier.soto.carrascosa@bbva.com
  *             Block flow if risk assessment fails
  *             Remove unused class
  * |2020-02-04 juanmanuel.perez.ortiz.contractor@bbva.com
  *             Add two new parameters in setupPath() and created SetupPathWrapper to avoid 'long parameter lists'
  * |2020-02-08 ricardo.almanza.contractor@bbva.com
*               Added orphan
  * -----------------------------------------------------------------------------------------------
  */
public with sharing class Arc_Gen_NewAnalysis_Controller {
  /**
    * @Description: String with SUBSIDIARY string
    */
    static final string SUBSIDIARY = 'SUBSIDIARY';
    /**
    * @Description: String with GROUP string
    */
    static final string GROUP_NAME = 'GROUP';
  /**
  *-------------------------------------------------------------------------------
  * @Description wrapper for Sector response
  *-------------------------------------------------------------------------------
  * @Date 30/4/2019
  * @Author eduardoefrain.hernandez.contractor@bbva.com
  * @example SectorResponse response = new SectorResponse();
  *-------------------------------------------------------------------------------
  */
  public class SectorResponse {
    @AuraEnabled public boolean sectorResultResponse {
      get;
      set;
    }
    @AuraEnabled public String sectorDescriptionResponse {
      get;
      set;
    }
  }
  /**
  *-------------------------------------------------------------------------------
  * @description set the analysis in the response
  *--------------------------------------------------------------------------------
  * @date 02/11/2019
  * @author luisarturo.parra.contractor@bbva.com
  * @param recordId recordId - id of the account_has_analysis
  * @param String analysisType (arce__anlys_wkfl_sub_process_type__c)
  * @return AnalysisResponse - Response of the created ARCE
  * @example setupAnalysis(String recordId, String analysisType) {
  */
  @AuraEnabled
  public static Arc_Gen_Account_Wrapper getaccdataforservices(String recordId) {
    return Arc_Gen_NewAnalysis_data.getUniqueCustomerDatacl(recordId);
  }
  /**
  *-------------------------------------------------------------------------------
  * @description set the analysis in the response
  *--------------------------------------------------------------------------------
  * @date 02/11/2019
  * @author luisarturo.parra.contractor@bbva.com
  * @param recordId recordId - id of the account_has_analysis
  * @param String analysisType (arce__anlys_wkfl_sub_process_type__c)
  * @return AnalysisResponse - Response of the created ARCE
  * @example setupAnalysis(String recordId, String analysisType) {
  */
  @AuraEnabled
  public static String economicarticipants(String encryptedClient) {
    return JSON.serialize(Arc_Gen_CallEconomicParticipants.callEconomicParticipations(encryptedClient));
  }
  /**
  *-------------------------------------------------------------------------------
  * @description set the analysis in the response
  *--------------------------------------------------------------------------------
  * @date 02/11/2019
  * @author luisarturo.parra.contractor@bbva.com
  * @param recordId recordId - id of the account_has_analysis
  * @param String analysisType (arce__anlys_wkfl_sub_process_type__c)
  * @return AnalysisResponse - Response of the created ARCE
  * @example setupAnalysis(String recordId, String analysisType) {
  */
  @AuraEnabled
  public static String listparticipant(String encryptedgroup) {
    return JSON.serialize(Arc_Gen_CallListParticipant.callListParticipants(encryptedgroup));
  }
  /**
  *-------------------------------------------------------------------------------
  * @description set the analysis in the response
  *--------------------------------------------------------------------------------
  * @date 02/11/2019
  * @author luisarturo.parra.contractor@bbva.com
  * @param recordId recordId - id of the account_has_analysis
  * @param String analysisType (arce__anlys_wkfl_sub_process_type__c)
  * @return AnalysisResponse - Response of the created ARCE
  * @example setupAnalysis(String recordId, String analysisType) {
  */
  @AuraEnabled
  public static String constructgroupstructure(String listparticipant, String economicparticipant, String accountNumber, Boolean isOrphan) {
      Arc_Gen_CallEconomicParticipants.innertoreturn economicparticipants = new Arc_Gen_CallEconomicParticipants.innertoreturn();
      Arc_Gen_CallEconomicParticipants.Groupdata groupData = new Arc_Gen_CallEconomicParticipants.Groupdata();
	  System.debug('groupData:'+groupData);
      Boolean orphan = isOrphan;
      groupData.groupid = accountNumber;
      if (orphan == null) {
        orphan = false;
      }
      if(economicparticipant == null){
			System.debug('ENTER NULL:');
          economicparticipants.groupinfo = groupData;
      } else {
		System.debug('ENTER NO NULL:');
        economicparticipants = (Arc_Gen_CallEconomicParticipants.innertoreturn) JSON.deserialize(economicparticipant, Arc_Gen_CallEconomicParticipants.innertoreturn.Class);

      }
	  System.debug('economicparticipants:'+economicparticipants);
      Arc_Gen_CallListParticipant.innertoreturnlistp listparticipants = (Arc_Gen_CallListParticipant.innertoreturnlistp) JSON.deserialize(listparticipant, Arc_Gen_CallListParticipant.innertoreturnlistp.Class);
      Arc_Gen_NewGroups_service structure = new Arc_Gen_NewGroups_service();
	  System.debug('accountNumber:'+accountNumber);
      return JSON.serialize(structure.handleGroupStructureOnline(economicparticipants, listparticipants, accountNumber, orphan));
  }
  /**
  *-------------------------------------------------------------------------------
  * @description set the analysis in the response
  *--------------------------------------------------------------------------------
  * @date 02/11/2019
  * @author luisarturo.parra.contractor@bbva.com
  * @param recordId recordId - id of the account_has_analysis
  * @param String analysisType (arce__anlys_wkfl_sub_process_type__c)
  * @return AnalysisResponse - Response of the created ARCE
  * @example setupAnalysis(String recordId, String analysisType) {
  */
  @AuraEnabled
  public static String[] getPreviousArce(String customerId, String accountswraper) {
    return Arc_Gen_NewAnalysis_Service.getPreviousArceOnline(customerId,accountswraper);
  }
  /**
  *-------------------------------------------------------------------------------
  * @description set the analysis in the response
  *--------------------------------------------------------------------------------
  * @date 02/11/2019
  * @author luisarturo.parra.contractor@bbva.com
  * @param recordId recordId - id of the account_has_analysis
  * @param String analysisType (arce__anlys_wkfl_sub_process_type__c)
  * @return AnalysisResponse - Response of the created ARCE
  * @example setupAnalysis(String recordId, String analysisType) {
  */
  @AuraEnabled
  public static String setanalysis(String recordId, Boolean isorphan, String orphanNumber, String accounts) {
    Arc_Gen_NewAnalysis_Service.AnalysisResponse analysisresponse = new Arc_Gen_NewAnalysis_Service.AnalysisResponse();
    try {
      analysisresponse = Arc_Gen_NewAnalysis_Service.setanalysis(recordId, isorphan, orphanNumber, accounts);
    } catch (Exception ex) {
      throw new AuraHandledException(ex.getMessage());//NOSONAR
    }
    return JSON.serialize(analysisresponse);
  }
  /**
  *-------------------------------------------------------------------------------
  * @description set the analysis in the response
  *--------------------------------------------------------------------------------
  * @date 02/11/2019
  * @author luisarturo.parra.contractor@bbva.com
  * @param recordId recordId - id of the account_has_analysis
  * @param String analysisType (arce__anlys_wkfl_sub_process_type__c)
  * @return AnalysisResponse - Response of the created ARCE
  * @example setupAnalysis(String recordId, String analysisType) {
  */
  @AuraEnabled
  public static void updateArceToPreparing(String arceId) {
    Arc_Gen_NewAnalysis_Service.updateStatusArce(arceId, '02');
  }
  /**
  *-------------------------------------------------------------------------------
  * @description Method that calls the Persistance service.
  *-------------------------------------------------------------------------------
  * @date 09/04/2019
  * @author ricardo.almanza.contractor@bbva.com
  * @param String analysisId - Analysis Id
  * @return Arc_Gen_ServiceAndSaveResponse - Wrapper that contains the response of the called service and the DML operation
  * @example public static Arc_Gen_ServiceAndSaveResponse callPersistence(String customerId, String analysisId)
  */
  @AuraEnabled
  public static Arc_Gen_ServiceAndSaveResponse callPersistence(String analysisId) {
    Arc_Gen_PersistanceArceId_service service = new Arc_Gen_PersistanceArceId_service();
    Arc_Gen_ServiceAndSaveResponse response = new Arc_Gen_ServiceAndSaveResponse();
    try {
      response = service.setupRiskAssHeader(analysisId);
    } catch (NullPointerException ex) {
      response.serviceMessage = ex.getMessage();
    }
    Return response;
  }
  /**
  *-------------------------------------------------------------------------------
  * @Description set the sector of the client in the response
  *-------------------------------------------------------------------------------
  * @Author eduardoefrain.hernandez.contractor@bbva.com | 30/4/2019
  * @param analysisId analysisId of the account has analysis
  * @param selectedSector selected sector in the sector servide
  * @return response - Repsonse of the inserted sector
  * @example setClientSector(String analysisId,String selectedSector)
  *-------------------------------------------------------------------------------
  */
  @AuraEnabled
  public static SectorResponse setClientSector(String analysisId,String clientId,String selectedSector) {
      SectorResponse response = new SectorResponse();
      Arc_Gen_SetSector_Service.SectorResponse sectorResp = Arc_Gen_SetSector_Service.setClientSector(analysisId, clientId, selectedSector);
      response.sectorResultResponse = sectorResp.sectorResultResponse;
      response.sectorDescriptionResponse = sectorResp.sectorDescriptionResponse;
      Return response;
  }
  /**
  *-------------------------------------------------------------------------------
  * @description call the path of the service
  *-------------------------------------------------------------------------------
  * @date 30/04/2019
  * @author eduardoefrain.hernandez.contractor@bbva.com
  * @param analysisId analysisId of the account has analysis
  * @param customerId customerId Id of the client
  * @return Arc_Gen_ServiceAndSaveResponse - Wrapper that contains the response of the service
  * @example callPathService(String analysisId,String customerId,Boolean isorphan) {
  */
  @AuraEnabled
  public static Arc_Gen_ServiceAndSaveResponse callPathService(String analysisId, String customerId, Boolean isorphan) {
    Arc_Gen_getPathDataService_service.SetupPathWrapper pathParameters = new Arc_Gen_getPathDataService_service.SetupPathWrapper();
    String customerNumber = Arc_Gen_getPathDataService_service.getCustomerNumber(analysisId, customerId);
    Arc_Gen_getPathDataService_service service = new Arc_Gen_getPathDataService_service();
    Arc_Gen_ServiceAndSaveResponse response = new Arc_Gen_ServiceAndSaveResponse();
    pathParameters.analysisId = analysisId;
    pathParameters.customerId = customerId;
    pathParameters.clientNumber = customerNumber;
    pathParameters.subsidiary = (isorphan ? SUBSIDIARY : GROUP_NAME);
    pathParameters.saveobject = true;
    response = service.setupPath(pathParameters);
    Return response;
  }
}