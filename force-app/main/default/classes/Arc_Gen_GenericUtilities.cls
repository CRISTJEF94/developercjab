/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_GenericUtilities
* @Author   ismaelyovani.obregon.contractor@bbva.com
* @Date     Created: 2019-06-25
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Class of generic utilities
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-06-25 ismaelyovani.obregon.contractor@bbva.com
*             Class creation.
* |2019-06-25 mariohumberto.ramirez.contractor@bbva.com
*             Added new method getRecordTypeData
* |2019-07-16 mariohumberto.ramirez.contractor@bbva.com
*             Added new method getRecordTypeLimExp
* |2019-09-23 mariohumberto.ramirez.contractor@bbva.com
*             Added new method getTypeOfCustomer
*             Added new constants GROUP_CUSTOMER,ORPHAN_CUSTOMER,HEADER,SUBSIDIARY,GROUP,CLIENT
*             ORPHAN
* |2019-09-25 mariohumberto.ramirez.contractor@bbva.com
*             Added new methods getRecordTypeTask, getTypeOfCustomer, createTask, getProfileName
*             getIdsOfSubsidiaries and getAccHasAnalysis.
* |2019-10-21 manuelhugo.castillo.contractor@bbva.com
*             Added new methods getRecType,getAllSObjectRecType,getAllSObjectRecTypeByMapApiName
* |2019-10-28 mariohumberto.ramirez.contractor@bbva.com
*             Added new method getMultiplicationFactor()
* |2019-10-28 eduardoefrain.hernandez.contractor@bbva.com
*             Added method to set the static parameters for ASO services
* |2019-10-28 mariohumberto.ramirez.contractor@bbva.com
*             Added new method convertUnits()
* |2019-10-30 mariohumberto.ramirez.contractor@bbva.com
*             Added new method convertCurrency()
* |2019-10-30 javier.soto.carrascosa@bbva.com
*             Added method getLabelFromValue
* |2019-11-200 javier.soto.carrascosa@bbva.com
*             remove getProfileName
* |2019-12-26 juanmanuel.perez.ortiz.contractor@bbva.com
*             Add new static final variable and create new method to sent notifications
* |2020-13-01 mariohumberto.ramirez.contractor@bbva.com
*             Added new method getUpdateStructure()
*             Change arce__Customer__r.ParentId for arce__group_asset_header_type__c in queries
* |2020-01-24 juanmanuel.perez.ortiz.contractor@bbva.com
*             Remove logic static parameters to ASO services
* |2020-02-08 ricardo.almanza.contractor@bbva.com
*             Added config for oprhans testing
* -----------------------------------------------------------------------------------------------
*/
@SuppressWarnings('PMD.ExcessivePublicCount')
public without sharing class Arc_Gen_GenericUtilities {
    /**
        * @Description: String with the dev name of the recordtype
    */
    static final string TASK_DEV_NAME = 'Arc_Gen_ARCE_Task';
    /**
        * @Description: String with value "01"
    */
    static final string GROUP_CUSTOMER = '01';
    /**
        * @Description: String with value "02"
    */
    static final string ORPHAN_CUSTOMER = '02';
    /**
        * @Description: String with value "1"
    */
    static final string HEADER = '1';
    /**
        * @Description: String with value "2"
    */
    static final string SUBSIDIARY = '2';
    /**
        * @Description: String with value "Group"
    */
    static final string S_GROUP = 'Group';
    /**
        * @Description: String with value "Client"
    */
    static final string CLIENT = 'Client';
    /**
        * @Description: String with value "Orphan"
    */
    static final string ORPHAN = 'Orphan';
    /**
        * @Description: String with value "High"
    */
    static final string HIGH = 'High';
    /**
        * @Description: String with value "Not Started"
    */
    static final string NOT_STARTED = 'Not Started';
    /**
        * @Description: String with value "Other"
    */
    static final string OTHER = 'Other';
    /**
    * @Description: String with the dev name of the custom metadata update group structure
    */
    static final string UPDATE_GROUP_STRUCT = 'UpdateGroupStructure';
    /**
    * @Description: String with the dev name of the custom metadata update group structure
    */
    static final string GETORPHANANSSTRUCTURE = 'GetOrphanAnsStructure';
    /**
        * @Description: Record types Map
    */
    private static Map<String, Map<String,Id>> mapRecTypes = new Map<String, Map<String,Id>>();
    /**
        * @Description: Record types Map Aux
    */
    private static Map<String, Id> mapAuxObjRecType  = new Map<String, Id>();
    /**
        * @Description: Record type Ids Map
    */
    public class ConversionWrapper {
        /**
        * @Description: Previous Data
        */
        public String previousData {get; set;}
        /**
        * @Description: Actual Data
        */
        public String actualData {get; set;}
        /**
        * @Description: List of the field names
        */
        public List<String> fieldNames  {get; set;}
        /**
        * @Description: Name of an object
        */
        public String objectName {get; set;}
        /**
        * @Description: List of object Data
        */
        public List<SObject> objDataLts {get; set;}
        /**
        * @Description: Object Data
        */
        public SObject objData {get; set;}
    }
    public class Acchasanamap {
        public String ids{get;set;}
        public String grouporclient{get;set;}
    }
    public class Customerinfo{
      /**variable for CustomerName **/
      public String customerName {get; set;}
      /**customer Id **/
      public String customerId {get; set;}
    }
    private static Map<String, RecordType> mapIdRecordType  = new Map<String, RecordType>();
    static {
    List<RecordType> lstRecType = [SELECT Id, SobjectType, DeveloperName, Name FROM RecordType WHERE IsActive = true];
        for (RecordType objRecType : lstRecType) {
            mapAuxObjRecType = mapRecTypes.get(objRecType.SobjectType);
            if ( mapAuxObjRecType == null ) {
                mapAuxObjRecType = new Map<String, Id>();
                mapRecTypes.put(objRecType.SobjectType, mapAuxObjRecType);
            }
            mapAuxObjRecType.put(objRecType.DeveloperName, objRecType.Id);
            mapAuxObjRecType.put(objRecType.Name, objRecType.Id);
            mapIdRecordType.put(objRecType.id, objRecType);
        }
    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @author ismaelyovani.obregon.contractor@bbva.com
    * @date 2019-06-25
    * @param void
    * @return void
    * @example Arc_Gen_GenericUtilities generic = new Arc_Gen_GenericUtilities()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_GenericUtilities(){
    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that return's the values of any picklist.
    * ----------------------------------------------------------------------------------------------------
    * @author ismaelyovani.obregon.contractor@bbva.com
    * @date 2019-06-25
    * @param objectType object to get the field
    * @param selectedField to obtain picklist values
    * @return List<String> of pick list values
    * @example public static List<String> getPickListValue(String Object , String PicklistField)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static List<String> getValuesPickList(String objectType,String selectedField) {
        List<String> pickListValues = new List<String>();
        Schema.SObjectType convertToObj = Schema.getGlobalDescribe().get(objectType);
        Schema.DescribeSObjectResult res = convertToObj.getDescribe();
        Schema.DescribeFieldResult fieldResult = res.fields.getMap().get(selectedField).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple) {
            pickListValues.add(pickListVal.getLabel());
        }
        return pickListValues;
    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that return's the record type data
    * ----------------------------------------------------------------------------------------------------
    * @author ismaelyovani.obregon.contractor@bbva.com
    * @date 2019-06-25
    * @param recordTypeName - recordtype name
    * @return RecordType data queried
    * @example getRecordTypeData(recordTypeName)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static List<RecordType> getRecordTypeData(String recordTypeName) {
        return [SELECT Id, Name FROM RecordType WHERE DeveloperName = :recordTypeName];
    }

    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that return's a record type id of the Limits Exposure object
    * ----------------------------------------------------------------------------------------------------
    * @Author   mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-08-09
    * @param recordTypeDevName - developer name of the record type
    * @return Id of the recordType
    * @example getRecordTypeLimExp(recordTypeDevName)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static Id getRecordTypeLimExp(String recordTypeDevName) {
        return Schema.SObjectType.arce__limits_exposures__c.getRecordTypeInfosByDeveloperName().get(recordTypeDevName).getRecordTypeId();
    }

    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that return's a record type id of Task object
    * ----------------------------------------------------------------------------------------------------
    * @Author   mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-09-25
    * @param recordTypeDevName - developer name of the record type
    * @return Id of the recordType
    * @example getRecordTypeTask(recordTypeDevName)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static Id getRecordTypeTask(String recordTypeDevName) {
        return Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get(recordTypeDevName).getRecordTypeId();
    }

    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that return's if the client is group or filial
    * ----------------------------------------------------------------------------------------------------
    * @Author   mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-09-24
    * @param recordId - Id of the account has analysis object
    * @return String with value "Group" or "Client" or "Orphan"
    * @example getTypeOfCustomer(recordId)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static String getTypeOfCustomer(String recordId) {
        final String typeOfCustomer;
        final arce__Account_has_Analysis__c accHasAn = [SELECT Id,arce__Analysis__c,arce__Analysis__r.arce__analysis_customer_relation_type__c,arce__group_asset_header_type__c
                                                    FROM arce__Account_has_Analysis__c
                                                    WHERE Id = :recordId];
        if (accHasAn.arce__Analysis__r.arce__analysis_customer_relation_type__c == GROUP_CUSTOMER && accHasAn.arce__group_asset_header_type__c == HEADER) {
            typeOfCustomer = S_GROUP;
        } else if (accHasAn.arce__Analysis__r.arce__analysis_customer_relation_type__c == GROUP_CUSTOMER && accHasAn.arce__group_asset_header_type__c == SUBSIDIARY) {
            typeOfCustomer = CLIENT;
        } else if (accHasAn.arce__Analysis__r.arce__analysis_customer_relation_type__c == ORPHAN_CUSTOMER) {
            typeOfCustomer = ORPHAN;
        }
        return typeOfCustomer;
    }
    /**
    *-------------------------------------------------------------------------------
    * @description creates notifications for the user the ARCE is assign
    *-------------------------------------------------------------------------------
    * @author   juanmanuel.perez.ortiz.contractor@bbva.com
    * @date     26/12/2019
    * @Method:  createNotifications
    * @param:   ltsMembers list of member
    * @param:   recordId id of the ARCE
    * @param:   message mesage to the user
    */
    public static void createNotifications(List<User> ltsMembers, Id recordId, String message){
        list<Task> ltsTask = new list<Task>();
        for (User members: ltsMembers) {
            ltsTask.add(createTask(members.Id,recordId,message,TASK_DEV_NAME));
        }
        insert ltsTask;
    }

    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method to create a task
    * ----------------------------------------------------------------------------------------------------
    * @Author   angel.fuertes2@bbva.com
    * @Date     Created: 2019-03-12
    * @param ownerId - id of the owner
    * @param whatId - lookup
    * @param message - String with a custom message
    * @param devNameRT - developer name of the record type
    * @return a task object
    * @example createTask(ownerId,whatId,message,devNameRT)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static task createTask(Id ownerId, Id whatId, String message, String devNameRT) {
        Task tarea = new Task();
        tarea.OwnerId = ownerId;
        tarea.WhatId = whatId;
        tarea.Subject = message;
        tarea.Priority = HIGH;
        tarea.Status = NOT_STARTED;
        tarea.Type = OTHER;
        tarea.recordtypeId = getRecordTypeTask(devNameRT);
        tarea.ActivityDate = Date.today();
        return tarea;
    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that return a list of Ids of the subsidiaries in the arce analysis
    * ----------------------------------------------------------------------------------------------------
    * @Author   mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-09-25
    * @param arceAnalysisId - Id of the arce analysis object
    * @return List<Id> - list of Ids of account has analysis object
    * @example getIdsOfSubsidiaries(arceAnalysisId)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static List<Id> getIdsOfSubsidiaries(Id arceAnalysisId) {
        final List<Id> idAccHasAnLts = new List<Id>();
        final List<arce__Account_has_Analysis__c> acchasData = [SELECT Id, (SELECT Id, arce__Customer__c, arce__Customer__r.Name FROM arce__Account_has_Analysis__r WHERE arce__InReview__c = true AND arce__group_asset_header_type__c = '2')
                                                                FROM arce__Analysis__c
                                                                WHERE Id = :arceAnalysisId].arce__Account_has_Analysis__r;
        for (arce__Account_has_Analysis__c acchas: acchasData) {
            idAccHasAnLts.add(acchas.Id);
        }
        return idAccHasAnLts;
    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that return a list of account has analysis data
    * ----------------------------------------------------------------------------------------------------
    * @Author   mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-09-25
    * @param arceAnalysisId - Id of the arce analysis object
    * @return List<arce__Account_has_Analysis__c> - list of Ids of account has analysis object
    * @example getIdsOfAccHasAnalysis(arceAnalysisId)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static List<arce__Account_has_Analysis__c> getAccHasAnalysis(Id arceAnalysisId) {
        return [SELECT Id, Name, (SELECT Id, arce__contract_approval_date__c, arce__InReview__c, arce__Customer__r.OwnerId, arce__Analysis__c, arce_ctmr_flag__c, arce__anlys_wkfl_sbanlys_status_type__c FROM arce__Account_has_Analysis__r WHERE arce__InReview__c = true)
                                                                FROM arce__Analysis__c
                                                                WHERE Id = :arceAnalysisId].arce__Account_has_Analysis__r;
    }
    /*
    * @Description - Method that return record type Id queried by DeveloperName or Name
    * ----------------------------------------------------------------------------------------------------
    * @Author   manuelhugo.castillo.contractor@bbva.com
    * @Date     Created: 2019-10-21
    * @param    devNameObjeto - Object Developer Name
    * @param    devNameTipoRegistro - Developer Name Record Type Object
    * @return   Id - Record Type Id
    * @example  getRecType(developerNameOject,devNameTipoRegistro)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static id getRecType(String devNameObjeto, String devNameTipoRegistro) {
        Map<String, Id> mapObjRecType = mapRecTypes.get(devNameObjeto);
        return mapObjRecType.get(devNameTipoRegistro);
    }

    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that return a Ids set of the object record type informed
    * ----------------------------------------------------------------------------------------------------
    * @Author   manuelhugo.castillo.contractor@bbva.com
    * @Date     Created: 2019-10-21
    * @param    devNameObjeto - Object Developer Name
    * @return   Set<Id> - Set recordtype ids
    * @example  getAllSObjectRecType(devNameObjeto)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static Set<Id> getAllSObjectRecType(String devNameObjeto) {
        Set<Id> setIds = new Set<Id>();
        Map<String, Id> mapObjRecType = mapRecTypes.get(devNameObjeto);
        if(mapObjRecType != null) {
            setIds.addAll(mapObjRecType.values());
        }
        return setIds;
    }

    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that return a DeveloperName Map and Record Type Ids of the reported Object
    * ----------------------------------------------------------------------------------------------------
    * @Author   manuelhugo.castillo.contractor@bbva.com
    * @Date     Created: 2019-10-21
    * @param    devNameObjeto - Object Developer Name
    * @return   Map<Id, RecordType> - Id recordType and Developer Name
    * @example  getAllSObjectRecTypeByMapApiName(devNameObjeto)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static Map<Id, RecordType> getAllSObjectRecTypeByMapApiName(String devNameObjeto) {
        Map<Id, RecordType> mapRecordTypes = new Map<Id, RecordType>();
        for(RecordType iteraRT : mapIdRecordType.values()) {
            if(iteraRT.SobjectType.equals(devNameObjeto)) {
                mapRecordTypes.put(iteraRT.Id, iteraRT);
            }
        }
        return mapRecordTypes;
    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method that return a the multiplication factor to change units
    * ----------------------------------------------------------------------------------------------------
    * @Author   mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-10-28
    * @param    actualUnit - Api name of the field that contain the current type of unit
    * @param    previousUnit - Api name of the field that contain the previous type of unit
    * @param    accHasAnData - Account has analysis object
    * @return   Double - multiplication factor
    * @example  getMultiplicationFactor(actualUnit,previousUnit)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static Double getMultiplicationFactor(String actualUnit, String previousUnit, SObject obj) {
        Double multiplicationFactor = 1;
        final String combinationOfUnits = (String)obj.get(actualUnit) + ',' + (String)obj.get(previousUnit);
        switch on combinationOfUnits {
            when '1,2', '2,3', '3,4' {
                multiplicationFactor = 1000;
            }
            when '1,3', '2,4' {
                multiplicationFactor = 1000000;
            }
            when '1,4' {
                multiplicationFactor = 1000000000000L;
            }
            when '4,3', '3,2', '2,1' {
                multiplicationFactor = 0.001;
            }
            when '4,2', '3,1' {
                multiplicationFactor = 0.000001;
            }
            when '4,1' {
                multiplicationFactor = 0.000000000001;
            }
        }
        return multiplicationFactor;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description - Method that change the units
    * -----------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 01/11/2019
    * @param wrapperData - wrapper with the info to change the unit
    * @return a message
    * @example changeCurrency(objData, currencyInfo)
    * -----------------------------------------------------------------------------------------------
    **/
    public static String convertUnits (ConversionWrapper wrapperData) {
        final String response;
        try {
            Double multiplicationFactor = getMultiplicationFactor(wrapperData.actualData, wrapperData.previousData, wrapperData.objData);
            if (multiplicationFactor == 1) {
                response = 'SameUnit';
            } else {
                for (SObject obj: wrapperData.objDataLts) {
                    for (String field: wrapperData.fieldNames) {
                        if (String.valueOf(obj.getSObjectType().getDescribe().fields.getMap().get(field)) == field) {
                            obj.put(field, Double.valueOf(obj.get(field)) * multiplicationFactor);
                        }
                    }
                }
                Arc_Gen_AccHasAnalysis_Data.upsertObjects(wrapperData.objDataLts);
                response = System.Label.Arc_Gen_UpdateUnitSuccess;
            }
        } catch (Exception e) {
            response = System.Label.Arc_Gen_UpdateUnitError + e.getMessage() + ' : ' + e.getLineNumber();
        }
        return response;
    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method returns a list of map label -value for a given picklist field
    * ----------------------------------------------------------------------------------------------------
    * @author javier.soto.carrascosa@bbva.com
    * @date 2019-10-22
    * @param objectType Object where the picklist is located
    * @param selectedField api name of the picklist field
    * @return List<map<String,String>> of pick list values and labels
    * @example public static List<map<String,String>> getPicklistValuesLabels(String Object , String PicklistField)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static List<map<String,String>> getPicklistValuesLabels (String objectType,String selectedField) {
        final List<map<String,String>> ltsValue = new List<map<String,String>>();
        final Schema.SObjectType convertToObj = Schema.getGlobalDescribe().get(objectType);
        final Schema.DescribeSObjectResult res = convertToObj.getDescribe();
        final Schema.DescribeFieldResult fieldResult = res.fields.getMap().get(selectedField).getDescribe();
        final List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry value : ple) {
            ltsValue.add(new map<String,String>{'label' => value.getLabel() , 'value' => value.getValue()});
        }
        return ltsValue;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description Method that retries the label from value for a picklist
    * -----------------------------------------------------------------------------------------------
    * @author  angel.fuertes2@bbva.com
    * @date 2019-06-20
    * @Method:      getLabelFromValue
    * @param:       objObject to get the value of a picklist
    * @param:       fld field to get the picklist values
    * @param:       inputV value selevted to validaate
    * @return a String with the label
    * @example public static sObject associate(sObject objt, String fieldName, String value) {
    * -----------------------------------------------------------------------------------------------
    **/
    public static String getLabelFromValue (string objName, string fld, String inputV) {
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objName);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        String labelS = '';
        for( Schema.PicklistEntry value : fieldMap.get(fld).getDescribe().getPickListValues()) {
            if (inputV.equals(value.getValue())) {
                labelS = value.getLabel();
            }
        }
        return labelS;
    }
    /**
    * -----------------------------------------------------------------------------------------------
    * @Description Method that get All Analysis
    * -----------------------------------------------------------------------------------------------
    * @author  angel.fuertes2@bbva.com
    * @date 2019-06-20
    * @Method:      getLabelFromValue
    * @param:       objObject to get the value of a picklist
    * @param:       fld field to get the picklist values
    * @param:       inputV value selevted to validaate
    * @return a String with the label
    * @example public static sObject associate(sObject objt, String fieldName, String value) {
    * -----------------------------------------------------------------------------------------------
    **/
    public static List<Arc_Gen_GenericUtilities.Acchasanamap> getAllAnalysis(String arceId) {
        List<arce__Account_has_Analysis__c> miniArceList = new List<arce__Account_has_Analysis__c>();
        miniArceList = [SELECT Id, name, arce__Analysis__c FROM arce__Account_has_Analysis__c WHERE arce__Analysis__c =: arceId AND arce__InReview__c = true];
        List<Acchasanamap> innertoreturn = new List<Acchasanamap>();
        List<String> ids = new List<String>();
        for(arce__Account_has_Analysis__c aha : miniArceList){ids.add(aha.Id);}
        for(arce__Account_has_Analysis__c objCS : [SELECT Id,arce__group_asset_header_type__c  FROM arce__Account_has_Analysis__c  WHERE Id IN: ids]){
            Acchasanamap theahamap = new Acchasanamap();
            theahamap.ids = objCS.Id;
            theahamap.grouporclient = objCS.arce__group_asset_header_type__c;
            innertoreturn.add(theahamap);
        }
        Return innertoreturn;
    }
    /**
    *-------------------------------------------------------------------------------
    * @Description return true/false
    *-------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-13-01
    * @param:   void
    * @return   Boolean
    * @example  getUpdateStructure()
    * -----------------------------------------------------------------------------
    */
    public static Boolean getUpdateStructure() {
        return Boolean.valueOf(Arc_Gen_Arceconfigs_locator.getConfigurationInfo(UPDATE_GROUP_STRUCT)[0].Value1__c);
    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description - Method returns a list of map label -value for a given picklist field with a filter of values
    * ----------------------------------------------------------------------------------------------------
    * @author juanignacio.hita.contractor@bbva.com
    * @date 2020-01-07
    * @param objectType Object where the picklist is located
    * @param selectedField api name of the picklist field
    * @param selectedValues values selected for the filter
    * @return List<map<String,String>> of pick list values and labels
    * @example public static List<map<String,String>> getPicklistValuesLabels(String Object , String PicklistField, String selectedValues)
    * ----------------------------------------------------------------------------------------------------
    **/
    public static List<map<String,String>> getPicklistValuesLabels (String objectType, String selectedField, List<String> selectedValues) {
        List<Map<String,String>> mapPicklist = getPicklistValuesLabels(objectType, selectedField);
        List<Map<String,String>> mapPicklistSelected = new List<Map<String,String>>();

        for (Map<String,String> mapUnit : mapPicklist) {
            if (selectedValues.contains(mapUnit.get('value'))) {
                mapPicklistSelected.add(new map<String,String>{'label' => mapUnit.get('label') , 'value' => mapUnit.get('value')});
            }
        }

        return mapPicklistSelected;
    }
    /**
    *-------------------------------------------------------------------------------
    * @Description return true/false
    *-------------------------------------------------------------------------------
    * @Author   Ricardo Almanza Angeles  ricardo.almanza.contractor@bbva.com
    * @Date     Created: 2020-06-02
    * @param:   void
    * @return   Boolean
    * @example  getOrphanAnsStructure()
    * -----------------------------------------------------------------------------
    */
    public static Boolean getOrphanAnsStructure() {
        return Boolean.valueOf(Arc_Gen_Arceconfigs_locator.getConfigurationInfo(GETORPHANANSSTRUCTURE)[0].Value1__c);
    }
    /**
    *-------------------------------------------------------------------------------
    * @Description populate SObject with map values
    *-------------------------------------------------------------------------------
    * @Author   javier.soto.carrascosa@bbva.com
    * @Date     Created: 2020-04-10
    * @param:   SObject inputObj
    * @param:   Map<String,String> mapValues
    * @return   SObject
    * @example  populateObjFromMap(inputObj, mapValues)
    * -----------------------------------------------------------------------------
    */
    public static SObject populateObjFromMap(SObject inputObj, Map<String,Object> mapValues) {
        for (String fieldName : mapValues.keySet()) {
            inputObj.put(fieldName, mapValues.get(fieldName));
        }
        return inputObj;
    }
}