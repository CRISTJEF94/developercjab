global class AccountNormalizeDataBatch implements Database.Batchable<SObject>,Database.Stateful {
	/**
		Contiene los unique id de los clientes que no fueron encontrados en SF
	*/
	String uiClientsNotFound;
	/**
		Contiene los unique id de los clientes que no fueron encontrados en SF
	*/
	Map<Integer,String> mapClientNotFound;
	/**
		Contiene los unique code de los grupos economicos 
	*/
	List<String> lstUniqueCodeEconomicGroup;
	/**
		Contiene los unique code de los clientes
	*/
	List<String> lstUniqueCodeClients;
	/**
		Contiene los ruc de los clientes
	*/
	List<String> lstRucClients;
	/**
		Contiene los names de los clientes
	*/
	List<String> lstNamesClients;
	/**
		Contiene los unique id de los economic group de los clientes
	*/
	List<String> lstEconomicGroupClients;
	/**
		Contiene el tipo de operacion
		1 -> Operacion general, que se realiza con todos los registros de cuentas
		2 -> Operacion especifica , que se realiza en base a los registros dentro de un archivo csv
	*/
	Integer operation;

	global AccountNormalizeDataBatch(Integer op) {
		this.operation = op;
	}
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */ 
	global Database.QueryLocator start(Database.BatchableContext context) {
		System.debug('START AccountNormalizeDataBatch');
		uiClientsNotFound = 'COMPLETADO\n';
		/**
			Query que obtiene todos los registros de cuentas, usado como base para los dos tipos de operaciones
		*/
		String query = 'SELECT Id,unique_id__c,main_code_id__c,ParentId,Name FROM Account';
		/**
			Inicializacion del set que contiene los unique code de los grupos economicos
		*/
		if(this.operation==2){
			mapClientNotFound = new Map<Integer,String>();
			lstUniqueCodeEconomicGroup = new List<String>{'G0000176','G0006244', 'G0006079', 'G0000103', 'G0000106', 'G0000328', 'G0000349', 'G0006232', 'G0000104', 'G0000136', 'G0000232', 'G0000242', 'G0000266', 'G0000316', 'G0000403', 'G0000205', 'G0000209', 'G0000356', 'G0000403', 'G0000408', 'G0000423', 'G0000450', 'G0000607', 'G0000610', 'G0000638', 'G0000707', 'G0000787', 'G0000804', 'G0000814', 'G0000818', 'G0000908', 'G0000909', 'G0000917'};
			lstUniqueCodeClients = new List<String>{'00077178', '00893560', '00432024', '20412006', '10710073', '20518758', '20243587', '00007544', '00659363', '00422045', '00007447', '00298786', '03665402', '07910010', '20385476', '00422215', '02519526', '00051586', '01896296', '10252520', '20729908', '20523464', '00007510', '00634948', '00448133', '00961000', '04067754', '06969291', '01945777', '04208730', '00432687', '00663727', '01614339', '08150311', '00979309', '01202340', '02453622', '01846507', '15690607', '00247600', '00815446', '12904029', '20454026', '20691127', '20766822', '00962309', '01826409', '00722090', '01526987', '06197590', '20768878', '00433446', '00595993', '00079910', '00973181', '04271688', '08380945', '00726222', '01343297', '09113568', '09236740', '12054610', '00816973', '09213805', '00473960', '00980404', '13313024', '20121594', '20139763', '00432741', '01808028', '01460536', '04290488', '02517213', '04274695', '02330954', '04268903', '00620939', '20286192'};
			lstRucClients = new List<String>{'20771071709', '20788765097', '20788607335', '20786739897', '20616962699', '20296598349', '20293251135', '20787919156', '20788306751', '20723884374', '20788825782', '20788710918', '20780457385', '20729397751', '20732644378', '20782493079', '20782138058', '20788665084', '20722328885', '20722883749', '20924022613', '20201357935', '20788510251', '20788864117', '20788798297', '20764406070', '20726565849', '20786267898', '20210540149', '20779240771', '20787237062', '20787269789', '20788804203', '20778688680', '20628268348', '20788723653', '20753296744', '2078798303', '2065048054', '2078885827', '2078872350', '2060818352', '2078789214', '2077821770', '2064407826', '2072242739', '2078462732', '2078036072', '2077968307', '2076678707', '2092810676', '2078884773', '2078246048', '2078879980', '2078755444', '2074088779', '2074956474', '2078823005', '2078686164', '2077089779', '2077089712', '2066784954', '2078872329', '2078826106', '2077116092', '2078069967', '2060393692', '2021701048', '2025626506', '2078882998', '2078882391', '2072790388', '2071937269', '2072733350', '2072733770', '2072621203', '2078375179', '2078889370', '2078837409'};
			lstNamesClients = new List<String>{'RINES PREMIER', 'KANDIC INDUSTRIAL', 'CHIVILCOY PINTURAS', 'TLAPALERIA EL AGUILA', 'LA BODEGA', 'PORTEROS ELECTRICOS', 'ENTREPRISE GENERALE', 'PRUEBA REPO02', 'LINEA BLANCA NIEVES', 'PISCINAS CRISCI', 'ZUSANO SA DE CV', 'PORTEROS ELECTRICOS', 'ANDAMIOS TUBULARES P', 'CORAL CONSTRUCTORA', 'SISTEMAS ELECTRONICO', 'BARESA B.A. REAL EST', 'ASFALTOS CHOVA', 'NUNEZ OBRAS Y SERVIC', 'ACEROS DEL BAJIO A.C', 'LIB EL TIANGUIS', 'PROTECNIA', 'CORAL CONSTRUCTORA', 'FLORES ITA', 'ZAPATA RESTORANT', 'ZAPATA RESTORANT', 'MOTTESI MATERIALES', 'MADERERA LOS ALERCES', 'CERAMICA ZANON', 'RELIQUIA', 'MARMOLAR REVESTIMIEN', 'LAS BRASAS GRILL', 'VIDRIO Y ALUM ALUGLA', 'CASA NINO MADERERA', 'DIMENSION 2000', 'S.E.W. INGENIERIA', 'CHAPAS PLASTICAS', 'ACUAMATICA ZUMPANGO', 'AMOBLAMIENTOS MATERY', 'ZAPATA RESTORANT', 'TECNHIDRO CONSTRUCCI', 'ENTREPRISE GENERALE', 'LOMBARDI Y GRONDONA', 'CRISTAL PALERMO', 'ZAPATA RESTORANT', 'SALA DE BELLEZA SEXY', 'SUPERFUMERIA', 'ARQUITECTURA SOLAR', 'ALINSA SUMINISTROS P', 'ASCENSORES IBIZA', 'FERRETERIA ARGENTINA', 'RINES PREMIER', 'TABAQUERIA MARINA', 'WORLDLAND', 'SPACIOS', 'LA BODEGA', 'CONCRETO LACOSA', 'ESTUDIO BERTOLINO Y', 'RINES PREMIER', 'MARMOLAR REVESTIMIEN', 'DOCUMENTACIONES DIGI', 'PROTECNIA', 'RINES PREMIER', 'S.E.W. INGENIERIA', 'TRESPA INTERNATIONAL', 'CERRAMIENTOS SOLARIU', 'COSMETICA MANITA DE', 'LASCA SANITARIOS', 'FLORES ITA', 'PROYECTOS Y CONSTRUC', 'SPACIOS', 'CASO COMERCIAL', 'RINES PREMIER', 'OFITECH SA', 'PROTECNIA', 'GUERRIELLO INDUSTRIA', 'A Y LM CONSTRUCCIONE', 'GAETANO LA MARCA', 'CLAC Y ASOCIADOS', 'AHORRAMEX'};
			lstEconomicGroupClients = new List<String>{'G0000176', 'G0000176', 'G0006244', 'G0006244', 'G0006079', 'G0006079', 'G0006079', 'G0000103', 'G0000103', 'G0000103', 'G0000106', 'G0000106', 'G0000106', 'G0000106', 'G0000106', 'G0000328', 'G0000328', 'G0000349', 'G0000349', 'G0000349', 'G0006232', 'G0006232', 'G0000104', 'G0000104', 'G0000136', 'G0000136', 'G0000232', 'G0000232', 'G0000242', 'G0000242', 'G0000266', 'G0000266', 'G0000316', 'G0000316', 'G0000403', 'G0000403', 'G0000403', 'G0000205', 'G0000205', 'G0000209', 'G0000209', 'G0000209', 'G0000209', 'G0000209', 'G0000209', 'G0000356', 'G0000356', 'G0000408', 'G0000408', 'G0000408', 'G0000408', 'G0000423', 'G0000423', 'G0000450', 'G0000450', 'G0000607', 'G0000607', 'G0000610', 'G0000610', 'G0000638', 'G0000638', 'G0000638', 'G0000707', 'G0000707', 'G0000787', 'G0000787', 'G0000804', 'G0000804', 'G0000804', 'G0000814', 'G0000814', 'G0000818', 'G0000818', 'G0000908', 'G0000908', 'G0000909', 'G0000909', 'G0000917', 'G0000917'};
			/**
				modificacion de query por si el tipo de operacion es especifica, ya que no necesitariamos traer 
				todos los registros de account desde el metodo star
			*/	
			query += ' WHERE  (unique_id__c<>NULL AND unique_id__c IN :lstUniqueCodeEconomicGroup) ';
		}
		return Database.getQueryLocator(query);
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	global void execute(Database.BatchableContext context, List<Account> scope) {
		System.debug('EXECUTE AccountNormalizeDataBatch');
		if( this.operation==1 ){
			for(Integer i=0; i<scope.size() ;i++){
				//Completar con ceros en el lado izquierdo
				if(scope.get(i).unique_id__c <> NULL ){
					if( scope.get(i).unique_id__c.length()<8 ){
						Integer lostDigits = 8 - scope.get(i).unique_id__c.length();
						String digitsAddBefore = '';
						for( Integer j=0;j<lostDigits;j++ ){
							digitsAddBefore += '0';
						}
						scope.get(i).unique_id__c = digitsAddBefore + scope.get(i).unique_id__c;
					}
				}
				//Setear codigo unico y main code para que sean los mismo
				if( scope.get(i).unique_id__c != NULL &&  (scope.get(i).unique_id__c != scope.get(i).main_code_id__c) ){
					scope.get(i).main_code_id__c = scope.get(i).unique_id__c ;
				}else if( scope.get(i).main_code_id__c!=NULL && ( scope.get(i).main_code_id__c !=scope.get(i).unique_id__c )  ){
					scope.get(i).unique_id__c = scope.get(i).main_code_id__c;
				}
			}
			update scope;
		}else{
			System.debug('lstUniqueCodeClients:'+lstUniqueCodeClients);
			System.debug('scope:'+scope);
			List<Account> lstClientsAccounts = new List<Account>([ SELECT Id,Name, unique_id__c, ParentId FROM Account WHERE  (unique_id__c<>NULL AND unique_id__c IN :lstUniqueCodeClients) ]);	
			List<Account> lstClientsUpdate = new List<Account>();
			List<Account> lstAdaptClientsNotFound = new List<Account>();
			//Recorremos el set de codigos unicos de clientes
			for( Integer i=0 ; i<lstUniqueCodeClients.size() ; i++ ){
				Account accClientAux = new Account();
				Boolean encontrado = false;
				//Buscamos el unique code en la lista de clientes para ver si ese unique code fue encontrado
				//por la query
				for( Account accClient : lstClientsAccounts){
					if( accClient.unique_id__c == lstUniqueCodeClients[i] ){
						//Si fue encontrado hacemos una copia del registro
						encontrado = true;
						accClientAux = accClient;
						accClientAux.Name = lstNamesClients[i];
						//Buscamos en la lista de economic groups para obtener el id del 
						//economic group al que pertenece el cliente
						for( Account accEconomicGroup : scope ){
							if( accEconomicGroup.unique_id__c ==  lstEconomicGroupClients[i] ){
								//Si encontramos al economic group al que pertenece el cliente entonces
								//asignamos el id al ParentId 
								accClientAux.ParentId = accEconomicGroup.Id;
								lstClientsUpdate.add(accClientAux);
							}
						}
					}
				}
				if(!encontrado){
					uiClientsNotFound += lstUniqueCodeClients[i]+'\n';
					mapClientNotFound.put(i,lstUniqueCodeClients[i]);
				}
			}

			if( mapClientNotFound.size()>0 ){
				List<Account> lstClientsNotFoundAccounts = new List<Account>([ SELECT Id,Name, unique_id__c, AccountNumber,ParentId FROM Account WHERE unique_id__c NOT IN :lstUniqueCodeClients AND ParentId = NULL AND RecordType.Name = 'Client' LIMIT :mapClientNotFound.size() ]);	
				Integer i=0;
				for( Integer key : mapClientNotFound.keySet() ){
					lstClientsNotFoundAccounts[i].Name = lstNamesClients[key];
					lstClientsNotFoundAccounts[i].unique_id__c = mapClientNotFound.get(key);
					lstClientsNotFoundAccounts[i].main_code_id__c = mapClientNotFound.get(key);
					lstClientsNotFoundAccounts[i].AccountNumber = lstRucClients[key];
					for( Account accEconomicGroup : scope ){
						if( accEconomicGroup.unique_id__c ==  lstEconomicGroupClients[key] ){
							lstClientsNotFoundAccounts[i].ParentId = accEconomicGroup.Id;
							lstAdaptClientsNotFound.add(lstClientsNotFoundAccounts[i]);
						}
					}
					i++;
				}
			}

			if( lstClientsUpdate.size()>0 ){
				upsert lstClientsUpdate;
			}

			if( lstAdaptClientsNotFound.size()>0 ){
				upsert lstAdaptClientsNotFound;
			}

		}
	}
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
		System.debug('FINISH AccountNormalizeDataBatch');
		System.debug('uiClientsNotFound:'+uiClientsNotFound);
	}
}