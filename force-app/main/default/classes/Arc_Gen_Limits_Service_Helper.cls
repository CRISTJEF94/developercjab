/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Limits_Service_Helper
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2020-01-28
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Helper class to manage the service call of getLimits
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2020-01-28 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
public without sharing class Arc_Gen_Limits_Service_Helper {
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2019-01-28
    * @param void
    * @return void
    * @example Arc_Gen_Limits_Service service = new Arc_Gen_Limits_Service()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_Limits_Service_Helper() {

    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description get limit service data
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-01-28
    * @param mapResponse - service response
    * @return Map<String,Arc_Gen_Limits_Service.LimitsResponse> - Map with the info of the service
    * @example getServiceData(mapResponse)
    * --------------------------------------------------------------------------------------
    **/
    public static Map<String,Arc_Gen_Limits_Service.LimitsResponse> getServiceData(Map<String,Object> mapResponse) {
        final Map<String,Arc_Gen_Limits_Service.LimitsResponse> limitsRespMap = new Map<String,Arc_Gen_Limits_Service.LimitsResponse>();
        final Map<String,Object> data = (Map<String,Object>)mapResponse.get('data');
        final List<Object> ownerData = (List<Object>)data.get('owners');
        for(integer i = 0; i < ownerData.size(); i++) {
            final Map<String,Object> ownerMap = (Map<String,Object>)ownerData[i];
            final List<Object> limitsLts=(List<Object>)ownerMap.get('limits');
            for (integer j = 0; j < limitsLts.size(); j++) {
                Arc_Gen_Limits_Service.LimitsResponse limitWrapp = new Arc_Gen_Limits_Service.LimitsResponse();
                Map<String,Object> authorizedLimitMap = new Map<String,Object>();
                Map<String,Object> currentLimitMap = new Map<String,Object>();
                Map<String,Object> outstandingMap = new Map<String,Object>();
                Map<String,Object> commitedMap =  new Map<String,Object>();
                Map<String,Object> uncommitedMap =  new Map<String,Object>();
                Map<String,Object> limitMap = (Map<String,Object>)limitsLts[j];
                Map<String,Object> lineMap = (Map<String,Object>)limitMap.get('line');
                Map<String,Object> directRiskMap = (Map<String,Object>)limitMap.get('directRisk');
                authorizedLimitMap = getMap(limitMap,'authorizedLimit');
                currentLimitMap = getMap(directRiskMap,'formalized');
                outstandingMap = getMap(directRiskMap,'disposed');
                commitedMap = getMap(directRiskMap,'approvedCommitted');
                uncommitedMap = getMap(directRiskMap,'approvedUncommitted');
                limitWrapp.name = String.valueOf(lineMap.get('name'));
                limitWrapp.lastApproved = getAmount(authorizedLimitMap, 'amount');
                limitWrapp.currentLimit = getAmount(currentLimitMap, 'amount');
                limitWrapp.outstanding = getAmount(outstandingMap, 'amount');
                limitWrapp.commited = getAmount(commitedMap, 'amount');
                limitWrapp.uncommited = getAmount(uncommitedMap, 'amount');
                limitWrapp.currencyType = getCurrency(authorizedLimitMap, 'currency');
                limitsRespMap.put(String.valueOf(lineMap.get('id')), limitWrapp);
            }
        }
        return limitsRespMap;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validate Map
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-01-28
    * @param mapParam - (Map<String,Object>)
    * @param param - Map key
    * @return Map<String,Object>
    * @example getMap(mapParam, param)
    * --------------------------------------------------------------------------------------
    **/
    public static Map<String,Object> getMap(Map<String,Object> mapParam, String param) {
        return mapParam.get(param) == null ? new Map<String,Object>() : (Map<String,Object>)mapParam.get(param);
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validate Map
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-01-28
    * @param mapParam - (Map<String,Object>)
    * @param param - Map key
    * @return Map<String,Object>
    * @example getAmount(mapParam, param)
    * --------------------------------------------------------------------------------------
    **/
    public static Double getAmount(Map<String,Object> mapParam, String param) {
        return mapParam.get(param) == null ? 0 : Double.valueOf(mapParam.get(param));
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validate Map
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-01-28
    * @param mapParam - (Map<String,Object>)
    * @param param - Map key
    * @return Map<String,Object>
    * @example getCurrency(mapParam, param)
    * --------------------------------------------------------------------------------------
    **/
    public static String getCurrency(Map<String,Object> mapParam, String param) {
        return mapParam.get(param) == null ? 'N/A' : String.valueOf(mapParam.get(param));
    }
}