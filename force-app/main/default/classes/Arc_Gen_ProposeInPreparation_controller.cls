/**
* @File Name          : Arc_Gen_ProposeInPreparation_controller.cls
* @Description        : Refactorizacion
* @Author             : luisarturo.parra.contractor@bbva.com
* @Group              : ARCE
* @Last Modified By   : luisruben.quinto.munoz@bbva.com
* @Last Modified On   : 28/1/2020 11:40:10
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |21/11/2019 luisarturo.parra.contractor@bbva.com
*             Class creation. /Refactorizacion
* |18/12/2019 manuelhugo.castillo.contractor@bbva.com
*             Modify method 'getahas' replace arce__Account_has_Analysis__c to Arc_Gen_Account_Has_Analysis_Wrapper
* |2020-01-24 luisruben.quinto.munoz@bbva.com
*             Change to without sharing
* -----------------------------------------------------------------------------------------------
**/
public without sharing class Arc_Gen_ProposeInPreparation_controller {
    /**
    *-------------------------------------------------------------------------------
    * @description function that validate if the customer has a valid rating.
    *--------------------------------------------------------------------------------
    * @date		28/06/2019
    * @author	ismaelyovani.obregon.contractor@bbva.com
    * @param	recordId - account_has_analysis Id
    * @return	Boolean resp
    * @example	public static Boolean validateRatingInPreparation(String recordId)
    */
    @AuraEnabled
    public static Boolean validateRatingInPreparation(Id accHasAnalysisId) {
      try {
        final arce__Analysis__c arce = Arc_Gen_ArceAnalysis_Data.gerArce(accHasAnalysisId);
        return Arc_Gen_ProposeInPreparation_service.validateAllRatingsInArce(arce.Id);
      } catch (Exception e) {
        throw new AuraHandledException(System.Label.Arc_Gen_ApexCallError + e);
      }
    }
    /**
    *-------------------------------------------------------------------------------
    * @description initDelegation
    *--------------------------------------------------------------------------------
    * @date   09/01/2020
    * @author juanignacio.hita.contractor@bbva.com
    * @param  String : ambit
    * @param  Id : accHasId
    * @param  String : action
    * @return String
    * @example  String res = initDelegation(ambit, accHasId, action)
    */
    @AuraEnabled
    public static String initDelegation(Id accHasAnalysisId) {
      try {
        final arce__Analysis__c arce = Arc_Gen_ArceAnalysis_Data.gerArce(accHasAnalysisId);
        final Arc_Gen_User_Wrapper wrpUser = Arc_Gen_User_Locator.getUserInfo(System.UserInfo.getUserId());
        final Arc_Gen_Delegation_Wrapper wrapper = Arc_Gen_Propose_Helper.initDelegation(wrpUser.ambitUser, arce.Id, 'PROPOSE');
        Arc_Gen_Propose_Helper.updateSnctnType(wrapper);
        return JSON.serialize(wrapper);
      } catch (Exception e) {
        throw new AuraHandledException(System.Label.Arc_Gen_ApexCallError + e);
      }
    }
    /**
    *-------------------------------------------------------------------------------
    * @description evaluateDelegation
    *--------------------------------------------------------------------------------
    * @date   09/01/2020
    * @author juanignacio.hita.contractor@bbva.com
    * @param  Object : wrapper
    * @param  Id : accHasAnalysisId
    * @return String
    * @example  String res = evaluateDelegation(wrapper, accHasAnalysisId);
    */
    @AuraEnabled
    public static String evaluateDelegation(Object wrapper, Id accHasAnalysisId) {
      try {
        final Arc_Gen_Delegation_Wrapper wrapperSerialize = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) wrapper, Arc_Gen_Delegation_Wrapper.class);
        final Arc_Gen_Delegation_Wrapper wrapperRet = Arc_Gen_Propose_Helper.evaluateDelegation(wrapperSerialize, accHasAnalysisId, 'PREPARATION');
        return JSON.serialize(wrapperRet);
      } catch (Exception e) {
        throw new AuraHandledException(System.Label.Arc_Gen_ApexCallError + e);
      }
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Save the ambit
    *--------------------------------------------------------------------------------
    * @date 22/01/2020
    * @author juanignacio.hita.contractor@bbva.com
    * @param recordId : id of the record associated (arce__Analysis__c)
    * @param accHasAnalysisId : id of the account has analysis related with the analysis
    * @param ambit : String ambit value selected
    * @return returns list.
    * @example saveScopeOfSanction(Id recordId, String sanction)
    */
    @AuraEnabled
    public static String saveAction(Object wrapper, Id accHasAnalysisId, String ambit) {
      try {
        final Arc_Gen_Delegation_Wrapper wrapperSerialize = (Arc_Gen_Delegation_Wrapper) JSON.deserialize((String) wrapper, Arc_Gen_Delegation_Wrapper.class);
        final Arc_Gen_Delegation_Wrapper wrapperRet = Arc_Gen_ProposeInPreparation_service.saveAction(wrapperSerialize, accHasAnalysisId, ambit);
        return JSON.serialize(wrapperRet);
      } catch (Exception e) {
        throw new AuraHandledException(System.Label.Arc_Gen_ApexCallError + e);
      }
    }

}