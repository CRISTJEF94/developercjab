/**
* @File Name          : Arc_Gen_TestLeverage_controller_Test.cls
* @Description        : Test class for the test leverage section
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE - Test Leverage
* @Changes   :
*===================================================================================================
* Ver         Date                     Author                Modification
* =================================================================================================
* 1.0    30/4/2019 17:56:32   eduardoefrain.hernandez.contractor@bbva.com     Initial Version
* 1.1    23/7/2019 17:56:32   luisruben.quinto.munoz@bbva.com    Refactor Test clases Test Leveraged
* 1.1.1  2/12/2019 13:52:35   german.sanchez.perez.contractor@bbva.com
*                             franciscojavier.bueno@bbva.com     Api names modified with the correct name on business glossary
* 1.1.2    8/1/2020 18:21:09  javier.soto.carrascosa@bbva.com   Adapt test classess with account wrapper and setupaccounts
**/
@isTest
public class Arc_Gen_TestLeverage_controller_Test {
    /**
    * @Description: String with external id of test group
    */
    static final string GROUP_ID = 'G000001';
    /**
    * @Description: String with external id of test subsidiary
    */
    static final string SUBSIDIARY_ID = 'C000001';
@testSetup
    static void insertData() {
        Arc_UtilitysDataTest_tst.setupAcccounts();
        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{GROUP_ID,SUBSIDIARY_ID});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(GROUP_ID);
        Account accforArceHasnAlysis = new Account(Id=groupAccount.accId);
        accforArceHasnAlysis.CurrencyIsoCode = 'EUR';
        accforArceHasnAlysis.Rating = 'Hot';
        accforArceHasnAlysis.controlled_by_sponsor_type__c  =  '1';
        update accforArceHasnAlysis;

        final Arc_Gen_Account_Wrapper childAccount = groupAccWrapper.get(SUBSIDIARY_ID);
        Account kid = new Account(Id=childAccount.accId);
        kid.CurrencyIsoCode = 'EUR';
        kid.Rating = 'Hot';
        kid.controlled_by_sponsor_type__c = '1';
        upsert kid;

        arce__Sector__c sect = Arc_UtilitysDataTest_tst.crearSector('Executive Summ', '500', 'ExecSumm', null);
        Insert sect;

        arce__Analysis__c arceobj  =  Arc_UtilitysDataTest_tst.crearArceAnalysis('arce__Analysisobj', null, groupAccount.accId);
        arceobj.arce__analysis_customer_relation_type__c = '01';
        arceobj.CurrencyIsoCode = 'EUR';
        arceobj.arce__Rating__c = 'Hot';
        insert arceobj;

        arce__Account_has_Analysis__c arceAccounthasAnalysisobj  =  Arc_UtilitysDataTest_tst.crearAccHasAnalysis(sect.Id, arceobj.Id, groupAccount.accId, '  s-01');
        arceAccounthasAnalysisobj.currencyIsoCode = 'EUR';
        arceAccounthasAnalysisobj.arce__smes_eur_comuty_defn_type__c = '1';
        arceAccounthasAnalysisobj.arce__ll_before_adj_ind_type__c = '1';
        arceAccounthasAnalysisobj.arce__ll_before_adj_clsfn_type__c = 'NI';
        arceAccounthasAnalysisobj.arce__ll_after_adj_ind_type__c = '1';
        arceAccounthasAnalysisobj.arce__ll_after_adj_clsfn_type__c = 'NI';
        arceAccounthasAnalysisobj.arce__current_proposed_local_amount__c = 99999999999.0;
        insert arceAccounthasAnalysisobj;

        arce__rating__c ratingobj  =  Arc_UtilitysDataTest_tst.crearRating(null);
        ratingobj.arce__adj_long_rating_value_type__c = 'AAA';
        ratingobj.arce__adj_short_rating_value_type__c = 'AAA';
        ratingobj.arce__adj_total_rating_score_number__c = 1;
        ratingobj.CurrencyIsoCode = 'EUR';
        ratingobj.arce__rating_long_value_type__c = 'AAA';
        ratingobj.arce__rating_short_value_type__c = 'AAA';
        ratingobj.arce__manual_rating_value_type__c = 'AAA';
        ratingobj.arce__pd_per__c = 50;
        ratingobj.arce__long_rating_value_type__c = 'AAA';
        ratingobj.arce__short_rating_value_type__c = 'AAA';
        ratingobj.arce__total_rating_score_number__c = 1;
        ratingobj.arce__status_type__c = '1';
        ratingobj.arce__rating_user_id__c = '938098';
        ratingobj.arce__data_adjusted_score_number__c = 3.0;
        insert ratingobj;

        arce__Financial_Statements__c arceFinancialStatements  =  Arc_UtilitysDataTest_tst.crearFinStatement(accforArceHasnAlysis.id,arceAccounthasAnalysisobj.Id , ratingobj.Id, null);
        arceFinancialStatements.arce__ffss_adjusted_type__c = '1';
        arceFinancialStatements.arce__ffss_auditor_qlfn_type__c = '1';
        arceFinancialStatements.arce__ffss_auditor_opinion_type__c = 'S';
        arceFinancialStatements.arce__ffss_auditor_opinion_desc__c = '1';
        arceFinancialStatements.arce__ffss_certification_type__c = 'AUDITED';
        arceFinancialStatements.CurrencyIsoCode = 'EUR';
        arceFinancialStatements.arce__financial_statement_end_date__c = SYSTEM.TODAY()+30;
        arceFinancialStatements.arce__ffss_InReview__c = true;
        arceFinancialStatements.arce__financial_statement_id__c = 'id test';
        arceFinancialStatements.Name = '1';
        arceFinancialStatements.arce__status_type__c = '1';
        arceFinancialStatements.arce__ffss_submitted_type__c = '1';
        arceFinancialStatements.arce__ffss_valid_type__c = '1';
        arceFinancialStatements.arce__economic_month_info_number__c = '544';
        arceFinancialStatements.arce__financial_statement_start_date__c = SYSTEM.TODAY()+26;
        arceFinancialStatements.arce__financial_statement_start_date__c = SYSTEM.TODAY()+27;
        //arceFinancialStatements.arce__rating_final__c
        insert arceFinancialStatements;

        arceAccounthasAnalysisobj.arce__ffss_for_rating_id__c = arceFinancialStatements.Id;
        update arceAccounthasAnalysisobj;

        arce__limits_typology__c lt  =  Arc_UtilitysDataTest_tst.crearLimitTypology('TOTAL GROUP', null, null);
        insert lt;

        arce__limits_exposures__c le  = Arc_UtilitysDataTest_tst.crearLimitExposures(null,null,arceAccounthasAnalysisobj.Id,lt.Id);
        le.arce__current_proposed_amount__c  =  300000000;
        insert le;
    }
    @isTest
    static void testMethod1() {
        Test.startTest();
            List<arce__Account_has_Analysis__c> acc = [select Id from arce__Account_has_Analysis__c where currencyIsoCode = 'EUR'];
            Arc_Gen_TestLeverage_controller.setupLeverage(acc[0].Id);
            Arc_Gen_TestLeverage_service.leverageTestResponse leverage  =  new Arc_Gen_TestLeverage_service.leverageTestResponse();
            leverage.leveragedVariables = null;
            leverage.message = 'The leveraged  test cannot be done until the total limit for the group is set-up or the rating is calculated';
            leverage.status = 'false';
            system.assertEquals( 'false',leverage.status,'Status test Leverage');
        Test.stopTest();
    }
    @isTest
    static void testMethod2() {

            List<arce__Account_has_Analysis__c> acc = [select Id from arce__Account_has_Analysis__c where currencyIsoCode = 'EUR'];
            List<arce__rating__c> rating = [select Id from arce__rating__c where currencyIsoCode = 'EUR'];
            rating[0].arce__rating_short_value_type__c = 'CCC';
            update rating;
        Test.startTest();
            Arc_Gen_TestLeverage_controller.setupLeverage(acc[0].Id);
            Arc_Gen_TestLeverage_service.leverageTestResponse leverage  =  new Arc_Gen_TestLeverage_service.leverageTestResponse();
            leverage.leveragedVariables = null;
            leverage.message = 'The leveraged  test cannot be done until the total limit for the group is set-up or the rating is calculated';
            leverage.status = 'false';
            system.assertEquals( 'false',leverage.status,'Status test Leverage');
        Test.stopTest();
    }
    @isTest
    static void testMethod3() {
            final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{GROUP_ID,SUBSIDIARY_ID});
            final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(GROUP_ID);
            Account accG = new Account(Id=groupAccount.accId);
            accG.controlled_by_sponsor_type__c  =  '2';
            update accG;
            List<arce__Account_has_Analysis__c> acc = [select Id from arce__Account_has_Analysis__c where currencyIsoCode = 'EUR'];
            List<arce__rating__c> rating = [select Id from arce__rating__c where currencyIsoCode = 'EUR'];
            rating[0].arce__rating_short_value_type__c = 'BBB';
            update rating;
        Test.startTest();
            Arc_Gen_TestLeverage_controller.setupLeverage(acc[0].Id);
            Arc_Gen_TestLeverage_service.leverageTestResponse leverage  =  new Arc_Gen_TestLeverage_service.leverageTestResponse();
            leverage.leveragedVariables = null;
            leverage.message = 'The leveraged  test cannot be done until the total limit for the group is set-up or the rating is calculated';
            leverage.status = 'false';
            system.assertEquals( 'false',leverage.status,'Status test Leverage');
        Test.stopTest();
    }
    @isTest
    static void testMethod4() {
            final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{GROUP_ID,SUBSIDIARY_ID});
            final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(GROUP_ID);
            Account accG = new Account(Id=groupAccount.accId);
            accG.controlled_by_sponsor_type__c  =  '2';
            update accG;
            List<arce__Account_has_Analysis__c> acc = [select Id from arce__Account_has_Analysis__c where currencyIsoCode = 'EUR'];
            List<arce__rating__c> rating = [select Id from arce__rating__c where currencyIsoCode = 'EUR'];
            rating[0].arce__rating_short_value_type__c = 'CCC';
            update rating;
        Test.startTest();
            Arc_Gen_TestLeverage_controller.setupLeverage(acc[0].Id);
            Arc_Gen_TestLeverage_service.leverageTestResponse leverage  =  new Arc_Gen_TestLeverage_service.leverageTestResponse();
            leverage.leveragedVariables = null;
            leverage.message = 'The leveraged  test cannot be done until the total limit for the group is set-up or the rating is calculated';
            leverage.status = 'false';
            SYSTEM.assertEquals( 'false',leverage.status,'Status test Leverage');
        Test.stopTest();
    }
    @isTest
    static void testMethod5() {
            final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{GROUP_ID,SUBSIDIARY_ID});
            final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(GROUP_ID);
            Account accG = new Account(Id=groupAccount.accId);
            accG.controlled_by_sponsor_type__c  =  '2';
            update accG;
            List<arce__Account_has_Analysis__c> acc = [select Id,arce__smes_eur_comuty_defn_type__c from arce__Account_has_Analysis__c where currencyIsoCode = 'EUR'];
            acc[0].arce__smes_eur_comuty_defn_type__c  =  '2';
            update acc[0];
            List<arce__rating__c> rating = [select Id from arce__rating__c where currencyIsoCode = 'EUR'];
            rating[0].arce__rating_short_value_type__c = 'CCC';
            update rating;
        Test.startTest();
            Arc_Gen_TestLeverage_controller.setupLeverage(acc[0].Id);
            Arc_Gen_TestLeverage_service.leverageTestResponse leverage  =  new Arc_Gen_TestLeverage_service.leverageTestResponse();
            leverage.leveragedVariables = null;
            leverage.message = 'The leveraged  test cannot be done until the total limit for the group is set-up or the rating is calculated';
            leverage.status = 'false';
            system.assertEquals( 'false',leverage.status,'Status test Leverage');
        Test.stopTest();
    }
    @isTest
    static void testMethod6() {
            final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{GROUP_ID,SUBSIDIARY_ID});
            final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(GROUP_ID);
            Account accG = new Account(Id=groupAccount.accId);
            accG.controlled_by_sponsor_type__c  =  '2';
            update accG;
            List<arce__Account_has_Analysis__c> acc = [select Id,arce__smes_eur_comuty_defn_type__c from arce__Account_has_Analysis__c where currencyIsoCode = 'EUR'];
            acc[0].arce__smes_eur_comuty_defn_type__c  =  '2';
            update acc[0];
            List<arce__rating__c> rating = [select Id from arce__rating__c where currencyIsoCode = 'EUR'];
            rating[0].arce__rating_short_value_type__c = 'CCC';
            update rating;
        Test.startTest();
            Arc_Gen_TestLeverage_controller.setupLeverage(acc[0].Id);
            Arc_Gen_TestLeverage_service.leverageTestResponse leverage  =  new Arc_Gen_TestLeverage_service.leverageTestResponse();
            leverage.leveragedVariables = null;
            leverage.message = 'The leveraged  test cannot be done until the total limit for the group is set-up or the rating is calculated';
            leverage.status = 'false';
            system.assertEquals( 'false',leverage.status,'Status test Leverage');
        Test.stopTest();
    }
    @isTest
    static void testMethod7() {
            final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{GROUP_ID,SUBSIDIARY_ID});
            final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(GROUP_ID);
            Account accG = new Account(Id=groupAccount.accId);
            accG.controlled_by_sponsor_type__c  =  '2';
            update accG;
            List<arce__Account_has_Analysis__c> acc = [select Id,arce__smes_eur_comuty_defn_type__c from arce__Account_has_Analysis__c where currencyIsoCode = 'EUR'];
            acc[0].arce__smes_eur_comuty_defn_type__c  =  '2';
            acc[0].arce__debt_comt_not_disb_local_amount__c = 10000004.0;
            acc[0].arce__ebitda_interest_local_number__c = 10000004.0;
            acc[0].arce__ll_adj_deb_excl_amount__c = 10000004.0;
            acc[0].arce__ll_adj_debt_auditor_amount__c = 10000004.0;
            acc[0].arce__ll_adj_ebitda_auditor_amount__c = 10000004.0;
            acc[0].arce__ll_adj_ebitda_excl_amount__c = 10000004.0;
            acc[0].arce__gross_financial_debt_local_amount__c = 168678787778.0;
            acc[0].arce__ll_other_adj_debt_amount__c = 10000004.0;
            acc[0].arce__ll_other_adj_ebitda_amount__c = 10000004.0;
            acc[0].arce__total_asset_local_amount__c = 9999998.0;
            acc[0].arce__current_proposed_local_amount__c = 9999999999999999.0;
            update acc[0];
            List<arce__rating__c> rating = [select Id from arce__rating__c where currencyIsoCode = 'EUR'];
            rating[0].arce__rating_short_value_type__c = 'CCC';
            update rating;
        Test.startTest();
            Arc_Gen_TestLeverage_controller.setupLeverage(acc[0].Id);
            Arc_Gen_TestLeverage_service.leverageTestResponse leverage  =  new Arc_Gen_TestLeverage_service.leverageTestResponse();
            leverage.leveragedVariables = null;
            leverage.message = 'The leveraged  test cannot be done until the total limit for the group is set-up or the rating is calculated';
            leverage.status = 'false';
            system.assertEquals( 'false',leverage.status,'Status test Leverage');
        Test.stopTest();
    }
        @isTest
    static void testMethod8() {
            final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{GROUP_ID,SUBSIDIARY_ID});
            final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(GROUP_ID);
            Account accG = new Account(Id=groupAccount.accId);
            accG.controlled_by_sponsor_type__c  =  '2';
            update accG;
            List<arce__Account_has_Analysis__c> acc = [select Id,arce__smes_eur_comuty_defn_type__c from arce__Account_has_Analysis__c where currencyIsoCode = 'EUR'];
            acc[0].arce__smes_eur_comuty_defn_type__c  =  '2';
            acc[0].arce__debt_comt_not_disb_local_amount__c = 0;
            acc[0].arce__ebitda_interest_local_number__c = 2000000;
            acc[0].arce__ll_adj_deb_excl_amount__c = 0;
            acc[0].arce__ll_adj_debt_auditor_amount__c = 0;
            acc[0].arce__ll_adj_ebitda_auditor_amount__c = 0;
            acc[0].arce__ll_adj_ebitda_excl_amount__c = 0;
            acc[0].arce__gross_financial_debt_local_amount__c  =  9000000.0;
            acc[0].arce__ll_other_adj_debt_amount__c = 0;
            acc[0].arce__ll_other_adj_ebitda_amount__c = 0;
            acc[0].arce__total_asset_local_amount__c = 0;
            acc[0].arce__current_proposed_local_amount__c = 9999999999999999.0;
            update acc[0];
            List<arce__rating__c> rating = [select Id from arce__rating__c where currencyIsoCode = 'EUR'];
            rating[0].arce__rating_short_value_type__c = 'CCC';
            update rating;
        Test.startTest();
            Arc_Gen_TestLeverage_controller.setupLeverage(acc[0].Id);
            Arc_Gen_TestLeverage_service.leverageTestResponse leverage  =  new Arc_Gen_TestLeverage_service.leverageTestResponse();
            leverage.leveragedVariables = null;
            leverage.message = 'The leveraged  test cannot be done until the total limit for the group is set-up or the rating is calculated';
            leverage.status = 'false';
            system.assertEquals( 'false',leverage.status,'Status test Leverage');
        Test.stopTest();
    }
        @isTest
    static void testMethod9() {
            final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{GROUP_ID,SUBSIDIARY_ID});
            final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get(GROUP_ID);
            Account accG = new Account(Id=groupAccount.accId);
            accG.controlled_by_sponsor_type__c  =  '2';
            update accG;
            List<arce__Account_has_Analysis__c> acc = [select Id,arce__smes_eur_comuty_defn_type__c from arce__Account_has_Analysis__c where currencyIsoCode = 'EUR'];
            acc[0].arce__smes_eur_comuty_defn_type__c  =  '2';
            acc[0].arce__current_proposed_local_amount__c = 0.0;
            update acc[0];
            List<arce__rating__c> rating = [select Id from arce__rating__c where currencyIsoCode = 'EUR'];
            rating[0].arce__rating_short_value_type__c = 'CCC';
            update rating;
        Test.startTest();
            Arc_Gen_TestLeverage_controller.setupLeverage(acc[0].Id);
            Arc_Gen_TestLeverage_service.leverageTestResponse leverage  =  new Arc_Gen_TestLeverage_service.leverageTestResponse();
            leverage.leveragedVariables = null;
            leverage.message = 'The leveraged  test cannot be done until the total limit for the group is set-up or the rating is calculated';
            leverage.status = 'false';
            system.assertEquals( 'false',leverage.status,'Status test Leverage');
        Test.stopTest();
    }
    @isTest
    static void testMethod10() {
            List<arce__Account_has_Analysis__c> acc = [select Id,arce__smes_eur_comuty_defn_type__c from arce__Account_has_Analysis__c where currencyIsoCode = 'EUR'];
            acc[0].arce__current_proposed_local_amount__c = 3000000;
            update acc[0];
        Test.startTest();
            Arc_Gen_TestLeverage_controller.setupLeverage(acc[0].Id);
            Arc_Gen_TestLeverage_service.leverageTestResponse leverage  =  new Arc_Gen_TestLeverage_service.leverageTestResponse();
            leverage.leveragedVariables = null;
            leverage.message = 'The leveraged  test cannot be done until the total limit for the group is set-up or the rating is calculated';
            leverage.status = 'false';
            system.assertEquals( 'false',leverage.status,'Status test Leverage');
        Test.stopTest();
    }
    @isTest
    static void testMethod11() {
            arce__analysis__c acc2  =  new arce__analysis__c();
        acc2.Name  =  '12345678901234567890121234567890123456789012123456789012345678901212345678901234567890121234567890123456789012';
        Test.startTest();
            Arc_Gen_TestLeverage_data data  =  new Arc_Gen_TestLeverage_data();
            Arc_Gen_TestLeverage_data.saveResult updateResults  =  new Arc_Gen_TestLeverage_data.saveResult();
            updateResults  =  data.updateRecord(acc2);
            Arc_Gen_TestLeverage_service.leverageTestResponse leverage  =  new Arc_Gen_TestLeverage_service.leverageTestResponse();
            system.assertEquals( 'false',updateResults.status,'dml exception');
        Test.stopTest();
    }
}