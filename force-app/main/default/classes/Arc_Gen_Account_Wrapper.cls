/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Account_Interface
* @Author   juanignacio.hita.contractor@bbva.com
* @Date     Created: 15/11/2019
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Arc_Gen_Account_Wrapper that retrieves full account information
* -----------------------------------------------------------------------------------------------
* @Example Arc_Gen_Account_Wrapper wrapper = new Arc_Gen_Account_Wrapper();
* @Changes
*
* |2019-11-14 juanignacio.hita.contractor@bbva.com
*             Interface creation.
* |2020-01-06 mariohumberto.ramirez.contractor@bbva.com
*             Added new param updateStructure
* |2020-01-25 mariohumberto.ramirez.contractor@bbva.com
*             Added new params
* |2020-01-29 juanmanuel.perez.ortiz.contractor@bbva.com
*             Remove companyEcoAct,accActivity,rtDevName from Account Wrapper
* -----------------------------------------------------------------------------------------------
*/
@SuppressWarnings('PMD.ExcessivePublicCount, PMD.TooManyFields')
public class Arc_Gen_Account_Wrapper {
    /**
    *
    * @Description : Account Id
    */
    @AuraEnabled public Id accId {get;set;}
    /**
    *
    * @Description : String Account Name
    */
    @AuraEnabled public String name {get;set;}
    /**
    *
    * @Description : Account Number
    */
    @AuraEnabled public String accNumber {get;set;}
    /**
    *
    * @Description : Participant Type of the Account
    */
    @AuraEnabled public String participantType {get;set;}
    /**
    *
    * @Description : Participant Owner Id of the Account
    */
    @AuraEnabled public Id participantOwnerId {get;set;}
    /**
    *
    * @Description : Participant Parent Owner Id of the Account
    */
    @AuraEnabled public Id participantParentOwnerId {get;set;}
    /**
    *
    * @Description : Account Parent Id
    */
    @AuraEnabled public Id accParentId {get;set;}
    /**
    *
    * @Description : String Bank Id Account
    */
    @AuraEnabled public String bankId {get;set;}
    /**
    *
    * @Description : String Doc Number
    */
    @AuraEnabled public String docNumber {get;set;}
    /**
    *
    * @Description : String Doc Type
    */
    @AuraEnabled public String docType {get;set;}
    /**
    *
    * @Description : String Error
    */
    @AuraEnabled public String error {get;set;}
    /**
    *
    * @Description : Boolean to enable/disable the update of the group structure
    */
    @AuraEnabled public Boolean updateStructure {get;set;}
    /**
    *
    * @Description : value of the current limit amount
    */
    @AuraEnabled public Double currentLimit {get;set;}
    /**
    *
    * @Description : value of the commited omount
    */
    @AuraEnabled public Double commited {get;set;}
    /**
    *
    * @Description : value of non commited amount
    */
    @AuraEnabled public Double unCommited {get;set;}
    /**
    *
    * @Description : value of the outstanding amount
    */
    @AuraEnabled public Double outstanding {get;set;}
    /**
    *
    * @Description : value of the currency
    */
    @AuraEnabled public String currencyType {get;set;}
    /**
    *
    * @Description : value of the unit
    */
    @AuraEnabled public String unit {get;set;}
}