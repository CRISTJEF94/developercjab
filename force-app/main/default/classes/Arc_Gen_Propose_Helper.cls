/**
  * @File Name          : Arc_Gen_Propose_Helper.cls
  * @Description        :
  * @Author             : juanignacio.hita.contractor@bbva.com
  * @Group              :
  * @Last Modified By   : juanignacio.hita.contractor@bbva.com
  * @Last Modified On   : 15/01/2020
  * @Modification Log   :
  *==============================================================================
  * Ver         Date                     Author                 Modification
  *==============================================================================
  * 1.0    5/7/2019 12:50:32   juanignacio.hita.contractor@bbva.com     Initial Version
  **/
public with sharing class Arc_Gen_Propose_Helper {
  /*
    @Description: Status analysis : Arce Prepared
  */
  static final String ARCE_PREPARED = '03';
  /*
    @Description: Status analysis : Constrasting Analysis
  */
  static final String CONSTRA_ANALYSIS = '04';
  /*
    @Description: Status analysis : PENDING_SCHE_W_DELEGATION
  */
  static final String PENDING_SCHE_W_DELEGATION = '05';
  /*
    @Description: Status analysis : PENDING_DECI_W_DELEGATION
  */
  static final String PENDING_DECI_W_DELEGATION = '06';
  /*
    @Description: Status analysis : Pending Schedule in Committee
  */
  static final String PENDING_SCHE_COMMITEE = '07';
  /*
    @Description: Status analysis : Pending Sanction in Committee
  */
  static final String PENDING_SANC_COMMITEE = '08';
  /**
  *-------------------------------------------------------------------------------
  * @description Constructor method for sonnar
  *--------------------------------------------------------------------------------
  * @author juanignacio.hita.contractor@bbva.com
  * @date 15/01/2020
  * @return void
  * @example  private Arc_Gen_Propose_Helper()
  **/
  @TestVisible
  private Arc_Gen_Propose_Helper() {
  }
  /**
  *-------------------------------------------------------------------------------
  * @description  initDelegation
  --------------------------------------------------------------------------------
  * @author juanignacio.hita.contractor@bbva.com
  * @date   09/01/2020
  * @param  id : recordId id of the account has analysis
  * @param  String : ambit to propose
  * @param  String : action
  * @return Arc_Gen_Delegation_Wrapper
  * @example Arc_Gen_Delegation_Wrapper = initDelegation(ambit, accHasId, action);
  **/
  public static Arc_Gen_Delegation_Wrapper initDelegation(String ambit, Id analysisId, String action) {
    final Arc_Gen_Workflow_Interface workflowController = Arc_Gen_Workflow_Service.workflowClass();
    final Arc_Gen_Delegation_Wrapper wrapper = workflowController.getDelegation(ambit, analysisId, action);
    return wrapper;
  }
  /**
  *-------------------------------------------------------------------------------
  * @description  initIdentification
  --------------------------------------------------------------------------------
  * @author juanignacio.hita.contractor@bbva.com
  * @date   09/01/2020
  * @param  id : recordId id of the account has analysis
  * @param  String : ambit to propose
  * @param  String : action
  * @return Arc_Gen_Delegation_Wrapper
  * @example Arc_Gen_Delegation_Wrapper = initIdentification(ambit, accHasId);
  **/
  public static String initIdentification(String ambit, Id analysisId) {
    final Arc_Gen_Workflow_Interface workflowController = Arc_Gen_Workflow_Service.workflowClass();
    return workflowController.getIdentification(ambit, analysisId);
  }
  /**
  *-------------------------------------------------------------------------------
  * @description  updateSnctnType
  --------------------------------------------------------------------------------
  * @author juanignacio.hita.contractor@bbva.com
  * @date   09/01/2020
  * @param  Arc_Gen_Delegation_Wrapper : wrapper
  * @return void
  * @example updateSnctnType(wrapper);
  **/
  public static void updateSnctnType(Arc_Gen_Delegation_Wrapper wrapper) {
    Map<String, Object> fieldValueMap = new Map<String, Object>();
    fieldValueMap.put('arce__anlys_wkfl_snctn_br_level_type__c', wrapper.sanctionAmbit);
    Arc_Gen_ArceAnalysis_Data.editAnalysisFields(wrapper.analysisId, fieldValueMap);
  }
  /**
  *-------------------------------------------------------------------------------
  * @description  evaluateDelegation
  --------------------------------------------------------------------------------
  * @author juanignacio.hita.contractor@bbva.com
  * @date   09/01/2020
  * @param  Arc_Gen_Delegation_Wrapper : wrapper
  * @param  Id : accHasAnalysisId
  * @param  String : proposeType
  * @return Arc_Gen_Delegation_Wrapper
  * @example Arc_Gen_Delegation_Wrapper wrapper = evaluateDelegation(wrapper, accHasAnalysisId);
  **/
  public static Arc_Gen_Delegation_Wrapper evaluateDelegation(Arc_Gen_Delegation_Wrapper wrapper, Id accHasAnalysisId, String proposeType) {
    Arc_Gen_Delegation_Wrapper newWrapper = new Arc_Gen_Delegation_Wrapper();
    if (wrapper.hasDelegation == 'YES') {
      newWrapper = evaluateDelegationA(wrapper, proposeType);
    } else if (wrapper.hasDelegation == 'NO' || wrapper.hasDelegation == 'NOTCALCULATED') {
      newWrapper = evaluateDelegationB(wrapper, accHasAnalysisId, proposeType);
    } else {
      newWrapper.codStatus = 500;
      newWrapper.msgInfo = 'Configuration local proposeAction, hasDelegation value not found';
    }
    return newWrapper;
  }
  /**
  *-------------------------------------------------------------------------------
  * @description  evaluateDelegation
  --------------------------------------------------------------------------------
  * @author juanignacio.hita.contractor@bbva.com
  * @date   09/01/2020
  * @param  Arc_Gen_Delegation_Wrapper : wrapper
  * @param  String : proposeType
  * @return Arc_Gen_Delegation_Wrapper
  * @example Arc_Gen_Delegation_Wrapper wrapper = evaluateDelegationA(wrapper, accHasAnalysisId);
  **/
  public static Arc_Gen_Delegation_Wrapper evaluateDelegationA(Arc_Gen_Delegation_Wrapper wrapper, String proposeType) {
    Map<String, Object> fieldValueMap = new Map<String, Object>();
    String targetStatus = '';
    String targetStage = '';
    String action = '';
    final Arc_Gen_User_Wrapper currentUser = Arc_Gen_User_Locator.getUserInfo(System.UserInfo.getUserId());
    try {
      if (proposeType == 'SANCTION') {
        targetStatus = PENDING_SCHE_COMMITEE;
        targetStage = '2';
        action = 'Arc_Gen_ProposeInSanction';
      } else if (proposeType == 'PREPARATION') {
        targetStatus = PENDING_SCHE_COMMITEE;
        targetStage = '2';
        action = 'Arc_Gen_ProposeInPreparation';
      }
      fieldValueMap.put('arce__wf_status_id__c', targetStatus);
      fieldValueMap.put('arce__Stage__c', targetStage);
      fieldValueMap.put('arce__proposeUser__c',System.UserInfo.getUserId());
      Arc_Gen_ArceAnalysis_Data.editAnalysisFields(wrapper.analysisId, fieldValueMap);

      final string traceComments = System.Label.Arc_Gen_ProposeToTraceability.abbreviate(60) + ' ' + currentUser.userBasicInfo.Name + ' | ' + System.Label.Arc_Gen_TraceabilityAmbit + ': ' +  Arc_Gen_GenericUtilities.getLabelFromValue('arce__analysis__c', 'arce__anlys_wkfl_edit_br_level_type__c', wrapper.lstAmbits[0].get('value'));
      final Map<String, String> auditAttr = Arc_Gen_Traceability.genAuditAttr(System.Label.Arc_Gen_BtnPropose + ' - ' + Arc_Gen_GenericUtilities.getLabelFromValue('arce__analysis__c', 'arce__wf_status_id__c', targetStatus).abbreviate(60), 'sendBack', traceComments.abbreviate(255));
      final Map<String, String> auditWF = Arc_Gen_Traceability.genAuditWF(currentUser.ambitUser, '2', targetStatus, action);
      Arc_Gen_Traceability.saveTrace(wrapper.analysisId, auditAttr, auditWF);

      wrapper.codStatus = 201;
      wrapper.msgInfo = System.Label.Arc_Gen_SchedulePropose;
    } catch (Exception e) {
      wrapper.codStatus = 500;
      wrapper.msgInfo = e.getMessage();
    }
    return wrapper;
  }
  /**
  *-------------------------------------------------------------------------------
  * @description  evaluateDelegationB
  --------------------------------------------------------------------------------
  * @author juanignacio.hita.contractor@bbva.com
  * @date   09/01/2020
  * @param  Arc_Gen_Delegation_Wrapper : wrapper
  * @param  Id : accHasId
  * @param  String : proposeType
  * @return Arc_Gen_Delegation_Wrapper
  * @example Arc_Gen_Delegation_Wrapper wrapper = evaluateDelegationB(wrapper, accHasAnalysisId);
  **/
  public static Arc_Gen_Delegation_Wrapper evaluateDelegationB(Arc_Gen_Delegation_Wrapper wrapper, Id accHasId, String proposeType) {
    Arc_Gen_Delegation_Wrapper wrapperRet = wrapper;
    try {
      if (wrapper.lstAmbits.size() == 1) {
        wrapperRet = evaluateDelegationElement(wrapper, accHasId, proposeType);
      } else if (wrapper.lstAmbits.size() >= 1) {
        wrapperRet.codStatus = 200;
      }
    } catch (Exception e) {
      wrapperRet.codStatus = 500;
      wrapperRet.msgInfo = e.getMessage();
    }
    return wrapperRet;
  }
  /**
  *-------------------------------------------------------------------------------
  * @description  evaluateDelegationElement
  --------------------------------------------------------------------------------
  * @author juanignacio.hita.contractor@bbva.com
  * @date   09/01/2020
  * @param  Arc_Gen_Delegation_Wrapper : wrapper
  * @param  Id : accHasAnalysisId
  * @param  String : proposeType
  * @return Arc_Gen_Delegation_Wrapper
  * @example evaluateDelegationElement(wrapper, accHasAnalysisId);
  **/
  public static Arc_Gen_Delegation_Wrapper evaluateDelegationElement(Arc_Gen_Delegation_Wrapper wrapper, Id accHasAnalysisId, String proposeType) {
      final arce__Analysis__c arce = Arc_Gen_ArceAnalysis_Data.gerArce(accHasAnalysisId);
      final Arc_Gen_User_Wrapper currentUser = Arc_Gen_User_Locator.getUserInfo(System.UserInfo.getUserId());
      Map<String, Object> fieldValueMap = new Map<String, Object>();
      String identification = initIdentification(wrapper.lstAmbits[0].get('value'), wrapper.analysisId);
      final Arc_Gen_User_Wrapper nextUser = Arc_Gen_User_Locator.getUserInfo(identification);
      String targetStatus = '';
      String targetStage = '';
      String action = '';

      if (proposeType == 'SANCTION') {
        targetStatus = PENDING_SCHE_W_DELEGATION;
        targetStage = '2';
        action = 'Arc_Gen_ProposeInSanction';
      } else if (proposeType == 'PREPARATION') {
        targetStatus = CONSTRA_ANALYSIS;
        targetStage = '2';
        action = 'Arc_Gen_ProposeInPreparation';
      }
      fieldValueMap.put('arce__wf_status_id__c', targetStatus);
      fieldValueMap.put('arce__Stage__c', targetStage);
      fieldValueMap.put('arce__anlys_wkfl_edit_br_level_type__c', wrapper.lstAmbits[0].get('value'));
      fieldValueMap.put('OwnerId', identification);
      fieldValueMap.put('arce__proposeUser__c',System.UserInfo.getUserId());
      Arc_Gen_ArceAnalysis_Data.editAnalysisFields(arce.Id, fieldValueMap);

      final string traceComments = System.Label.Arc_Gen_ProposeToTraceability.abbreviate(60) + ' ' + nextUser.userBasicInfo.Name + ' | ' + System.Label.Arc_Gen_TraceabilityAmbit + ': ' +  Arc_Gen_GenericUtilities.getLabelFromValue('arce__analysis__c', 'arce__anlys_wkfl_edit_br_level_type__c', wrapper.lstAmbits[0].get('value'));
      final Map<String, String> auditAttr = Arc_Gen_Traceability.genAuditAttr(System.Label.Arc_Gen_BtnPropose + ' - ' + Arc_Gen_GenericUtilities.getLabelFromValue('arce__analysis__c', 'arce__wf_status_id__c', targetStatus).abbreviate(60), 'sendBack', traceComments);
      final Map<String, String> auditWF = Arc_Gen_Traceability.genAuditWF(currentUser.ambitUser, '2', targetStatus, action);
      Arc_Gen_Traceability.saveTrace(arce.Id, auditAttr, auditWF);

      Arc_Gen_GenericUtilities.createNotifications(Arc_Gen_Notifications_Service.getUsersIds(wrapper.analysisId), wrapper.analysisId, System.Label.Arc_Gen_ArcePropose + ': ' + arce.Name);

      wrapper.codStatus = 201;
      wrapper.msgInfo = System.Label.Arc_Gen_Record_Update_Success;
      return wrapper;
  }
}