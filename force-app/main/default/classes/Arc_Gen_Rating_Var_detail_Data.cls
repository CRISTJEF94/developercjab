/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_AccHasAnalysis_Data
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2020-01-13
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Data class for object arce__Account_has_Analysis__c
* -----------------------------------------------------------------------------------------------
* @Changes
*
* |2020-01-13 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* -----------------------------------------------------------------------------------------------
*/
public with sharing class Arc_Gen_Rating_Var_detail_Data {
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-01-13
    * @param void
    * @return void
    * @example Arc_Gen_Rating_Var_detail_Data data = new Arc_Gen_Rating_Var_detail_Data()
    * ----------------------------------------------------------------------------------------------------
    **/
    private Arc_Gen_Rating_Var_detail_Data() {

    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Set empty rating
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-01-13
    * @param accHasId -  Id of the account has analysis object
    * @return arce__rating_variables_detail__c
    * @example Arc_Gen_Rating_Var_detail_Data data = new Arc_Gen_Rating_Var_detail_Data()
    * ----------------------------------------------------------------------------------------------------
    **/
    public static arce__rating_variables_detail__c setEmptyRatingVariable(String accHasId) {
        return new arce__rating_variables_detail__c(arce__account_has_analysis_id__c = accHasId);
    }
    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Set empty rating
    * ----------------------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 2020-01-13
    * @param accHasId -  Id of the account has analysis object
    * @return arce__rating_variables_detail__c
    * @example Arc_Gen_Rating_Var_detail_Data data = new Arc_Gen_Rating_Var_detail_Data()
    * ----------------------------------------------------------------------------------------------------
    **/
    public static void insertRatingVariables(List<arce__rating_variables_detail__c> ratingVarLts) {
        if (!ratingVarLts.isEmpty()) {
            insert ratingVarLts;
        }
    }
}