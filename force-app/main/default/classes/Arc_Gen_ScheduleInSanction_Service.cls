/**
  * @File Name : Arc_Gen_ScheduleInSanction_Service.cls
  * @Description : Principal logic of schedule button
  * @Author : luisarturo.parra.externo@bbva.com
  * @Group : ARCE - BBVA Bancomer
  * @Last Modified By : juanmanuel.perez.ortiz.contractor@bbva.com
  * @Last Modified On : 26/12/2019 14:54:48
  * @Modification Log :
  *==============================================================================
  * Ver Date Author          Modification
  *==============================================================================
  * 1.0 5/10/2019 ARCE TEAM Creación.
  * 1.1 26/12/2019 juanmanuel.perez.ortiz.contractor@bbva.com   Add logic to send notifications.

  **/
public class Arc_Gen_ScheduleInSanction_Service {
    static final String PENDING_SCHE_W_DELEGATION = '05';
    static final String PENDING_DECI_W_DELEGATION = '06';
    static final String PENDING_SCHE_COMMITEE = '07';
    static final String PENDING_SANC_COMMITEE = '08';
    /**
    *-------------------------------------------------------------------------------
    * @description private method to avoid intances
    --------------------------------------------------------------------------------
    * @author luisarturo.parra.externo@bbva.com
    * @date 5/10/2019
    * @param none
    * @return  List < String >
    **/
    @TestVisible
    private Arc_Gen_ScheduleInSanction_Service() {
    }
    /**
    *-------------------------------------------------------------------------------
    * @description Method to change values and send arce to be updated
    --------------------------------------------------------------------------------
    * @author juanignacio.hita.contractor@bbva.com
    * @date 5/10/2019
    * @param String committeeValue
    * @param Id recordId
    * @param String committeeDesc
    * @return none
    **/
  public static void updateCommitteeService(String committeeValue, Id recordId, String committeeDesc) {
    arce__Analysis__c arceUp = Arc_Gen_ArceAnalysis_Data.gerArce(recordId);
    final Arc_Gen_User_Wrapper wrpUser = Arc_Gen_User_Locator.getUserInfo(System.UserInfo.getUserId());
    final List<arce__Analysis__c> arceList = New List<arce__Analysis__c>();
    String targetStatus = '';

    if (arceUp.arce__wf_status_id__c == PENDING_SCHE_W_DELEGATION) {
      targetStatus = PENDING_DECI_W_DELEGATION;
    } else {
      targetStatus = PENDING_SANC_COMMITEE;
    }
    arceUp.put('arce__wf_status_id__c', targetStatus);
    arceUp.put('arce__bbva_committees_type__c', committeeValue);
    arceList.add(arceUp);
    Arc_Gen_ArceAnalysis_Data.updateArce(arceList);

    String title = System.Label.Arc_Gen_TraceabilityScheduled + ' - ' + Arc_Gen_GenericUtilities.getLabelFromValue('arce__analysis__c', 'arce__wf_status_id__c', targetStatus).abbreviate(60);
    String comments = System.Label.Arc_Gen_TraceabilityCommittee.abbreviate(60) + ' : ' + committeeDesc.abbreviate(60);
    final Map<String, String> auditWF = Arc_Gen_Traceability.genAuditWF(wrpUser.ambitUser, '2', targetStatus, 'Arc_Gen_Schedule');
    Map<String, String> mapAuditAttr = Arc_Gen_Traceability.genAuditAttr(title, 'sendBack', comments);
    Arc_Gen_Traceability.saveTrace(arceUp.Id, mapAuditAttr, auditWF);

    Arc_Gen_GenericUtilities.createNotifications(Arc_Gen_Notifications_Service.getUsersIds(arceUp.Id), arceUp.Id, System.Label.Arc_Gen_ArceSchedule + ': ' +  arceUp.Name);

  }
}