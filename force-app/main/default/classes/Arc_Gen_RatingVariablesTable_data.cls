/**
* @File Name          : Arc_Gen_RatingVariablesTable_data.cls
* @Description        : Contains the datbase calls for the rating variables
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE Group
* @Last Modified By   : eduardoefrain.hernandez.contractor@bbva.com
* @Last Modified On   : 25/7/2019 9:59:41
* @Changes
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    30/4/2019 17:54:19   eduardoefrain.hernandez.contractor@bbva.com     Initial Version
* 1.1    24/7/2019 10:49:00   eduardoefrain.hernandez.contractor@bbva.com     Fix incidences
**/
public without sharing class Arc_Gen_RatingVariablesTable_data {
/**
*-------------------------------------------------------------------------------
* @description Method that obtains the level one variables
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String ratingId
* @return List<arce__rating_variables_detail__c>
* @example public List<arce__rating_variables_detail__c> getVariablesLevelOne(String ratingId)
**/
    public List<arce__rating_variables_detail__c> getVariablesByLevel(String ratingId, String level) {
        final String condition = 'arce__rating_Id__c = \''+String.escapeSingleQuotes(ratingId)+'\' AND arce__rating_variable_level_id__c = \''+String.escapeSingleQuotes(level)+'\' AND arce__rating_variable_name__c <> \'QUALITATIVE_BLOCK\' ORDER BY arce__rating_variable_id__c ASC';
        final String query = 'SELECT id,ToLabel(rating_variable_large_id__c),arce__rating_variable_name__c,arce__rating_var_value_amount__c,arce__rating_var_score_number__c,arce__rating_var_max_score_number__c,arce__parent_code_number__c FROM arce__rating_variables_detail__c WHERE ';
        return Database.query(query + condition);
    }
/**
*-------------------------------------------------------------------------------
* @description Method that obtains the qualitative variables
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 30/4/2019
* @param String ratingId
* @return List<arce__rating_variables_detail__c>
* @example public List<arce__rating_variables_detail__c> getVariablesLevelOne(String ratingId)
**/
    public List<arce__rating_variables_detail__c> getQualitativeVariables(String ratingId) {
        final String condition = 'arce__parent_code_number__c = \'2\' AND arce__rating_Id__c = \''+String.escapeSingleQuotes(ratingId)+'\' ORDER BY arce__rating_variable_id__c ASC';
        final String query = 'SELECT id,ToLabel(rating_variable_large_id__c),arce__rating_variable_name__c,arce__rating_var_value_amount__c,arce__rating_var_score_number__c,arce__rating_var_max_score_number__c,arce__parent_code_number__c FROM arce__rating_variables_detail__c WHERE ';
        return Database.query(query + condition);
    }
}