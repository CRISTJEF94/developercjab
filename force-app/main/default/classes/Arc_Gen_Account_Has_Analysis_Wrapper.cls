/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Account_Has_Analysis_Wrapper
* @Author   manuelhug.castillota.contractor@bbva.com
* @Date     Created: 150/12/2019
* @Group    ARCE
* -----------------------------------------------------------------------------------------------
* @Description Arc_Gen_Account_Has_Analysis_Wrapper that retrieves full account has analysis information
* -----------------------------------------------------------------------------------------------
* @Example Arc_Gen_Account_Has_Analysis_Wrapper wrapper = new Arc_Gen_Account_Has_Analysis_Wrapper();
* -----------------------------------------------------------------------------------------------
*/
public class Arc_Gen_Account_Has_Analysis_Wrapper {
    /**
    *
    * @Description : Account Has Analyisis Object
    */
    @AuraEnabled public arce__Account_has_Analysis__c ahaObj {get;set;}
    /**
    *
    * @Description : Account Wrapper Object
    */
    @AuraEnabled public Arc_Gen_Account_Wrapper accWrapperObj {get;set;}
}