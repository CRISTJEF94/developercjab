/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_Validate_Customer_Controller
* @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
* @Date     Created: 2019-05-22
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Service class for Arc_Gen_Validate_Customer_Controller.
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-05-23 mariohumberto.ramirez.contractor@bbva.com
*             Class creation.
* |2019-09-24 mariohumberto.ramirez.contractor@bbva.com
*             Added new call to Arc_Gen_GenericUtilities.getTypeOfCustomer(accHasAId) class
*             Added new constant S_GROUP
* |2019-09-30 mariohumberto.ramirez.contractor@bbva.com
*             Deleted lines that validate TOTAL CREDIT RISK
* |2019-09-30 mariohumberto.ramirez.contractor@bbva.com
*             Added new methods validateTypoSum and getIdsOfFilials
* |2019-10-03 mariohumberto.ramirez.contractor@bbva.com
*             Added new method validateRatingInSanction
* |2019-10-16 mariohumberto.ramirez.contractor@bbva.com
*             Added new method createSetapiFields
* |2019-10-29 mariohumberto.ramirez.contractor@bbva.com
*             Added new method getMultiplicationFactor
* |2019-12-30 mariohumberto.ramirez.contractor@bbva.com
*             Modify method validateFields
* |2020-01-28 mariohumberto.ramirez.contractor@bbva.com
*             Update class to manage automatic position validations
* |2020-02-08 ricardo.almanza.contractor@bbva.com
*             Modify to validate orphan
* -----------------------------------------------------------------------------------------------
*/
@SuppressWarnings('sf:TooManyMethods')
public with sharing class Arc_Gen_Validate_Customer_Service {
    /**
        * @Description: List of string with the name of some fields in arce__limits_exposure__c object
    */
    static final List<string> FIELD_LIST = new List<string>{'arce__current_proposed_amount__c'};
    /**
        * @Description: List of string with the label of some fields in arce__limits_exposure__c object
    */
    static final List<string> FIELD_LABEL = new List<string>{System.Label.Arc_Gen_Proposed};
    /**
        * @Description: String with the dev name of Total Corporate Risk tipology
    */
    static final string TOTAL_CORP_RISK = 'TP_0003';
    /**
        * @Description: List of string with the childs dev name of Total Corporate Risk tipology
    */
    static final List<string> TOTAL_CORPR_CHILD = new List<string>{'TP_0002', 'TP_0005', 'TP_0007', 'TP_0008'};
    /**
        * @Description: string with the name of a key of the map that return the method validateTable
    */
    static final string ID_CLIENTE = 'IdCliente';
    /**
        * @Description: string with the name of a key of the map that return the method validateTable
    */
    static final string FIELD_MISSING = 'FieldsMissing';
    /**
        * @Description: string with the name of a key of the map that return the method validateTable
    */
    static final string TYPOLOGY = 'Typology';
    /**
        * @Description: string with the name of a key of the map that return the method validateTable
    */
    static final string FIELD_NAME = 'FieldName';
    /**
        * @Description: string with the name of a key of the map that return the method validateTable
    */
    static final string ACTIVE = 'Active';
    /**
        * @Description: string with the name of a key of the map that return the method validateTable
    */
    static final string REMOVE = 'Remove';
    /**
        * @Description: String with value "Group"
    */
    static final string S_GROUP = 'Group';
    /**
        * @Description: String with value "Orphan"
    */
    static final string ORPHAN = 'Orphan';
    /**
        * @Description: String with value "Client"
    */
    static final string CLIENT = 'Client';
    /**
        * @Description: String with value "TP_0005"
    */
    static final string TOTAL_FIN_RISK_ST = 'TP_0005';
    /**
        * @Description: List String with value 'TP_0010','TP_0012'
    */
    static final List<string> TOTAL_FIN_ST_CHILD = new List<string>{'TP_0010','TP_0012'};
    /**
        * @Description: String with value "TP_0007"
    */
    static final string TOTAL_FIN_RISK_LT = 'TP_0007';
    /**
        * @Description: String with value 'TP_0011','TP_0014'
    */
    static final List<string> TOTAL_FIN_LT_CHILD = new List<string>{'TP_0011','TP_0014'};
    /**
        * @Description: String with value "3"
    */
    static final string VALIDATE = '3';
    /**
        * @Description: String with value "2"
    */
    static final string IN_SANCTION = '2';

    /**
    * ----------------------------------------------------------------------------------------------------
    * @Description Void Constructor to avoid warning in sonar qube
    * ----------------------------------------------------------------------------------------------------
    * @param void
    * @return void
    * @example Arc_Gen_Validate_Customer_Service service = new Arc_Gen_Validate_Customer_Service()
    * ----------------------------------------------------------------------------------------------------
    **/
    @TestVisible
    private Arc_Gen_Validate_Customer_Service() {

    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description Returns a List<Map<string,string>> that contain the error message of the
    * Policies table validations
    * --------------------------------------------------------------------------------------
    * @param accHasAId of the object arce__Account_has_Analysis__c
    * @return a List<Map<string,string>> that contain the error message of the
    * Policies table validations
    * @example validateTable(accHasAId)
    * --------------------------------------------------------------------------------------
    **/
    public static Arc_Gen_Validate_Customer_Controller.ResponseWrapper validateTable(string accHasAId) {
		System.debug('accHasAId:'+accHasAId);
        Arc_Gen_Validate_Customer_Controller.ResponseWrapper result = new Arc_Gen_Validate_Customer_Controller.ResponseWrapper();
        List<Map<string,string>> mapResult = new List<Map<string,string>>();
        final List<String> accHasA = new List<String>{accHasAId};
        final List<arce__limits_exposures__c> exposureData = Arc_Gen_Validate_Customer_Data.getExposures(accHasA);
        final arce__Account_has_Analysis__c acc = Arc_Gen_Validate_Customer_Data.checkGroup(accHasAId);
        final String typeOfCustomer = Arc_Gen_GenericUtilities.getTypeOfCustomer(accHasAId);
        final List<Map<string,string>> validateTypo = validateTypology(accHasAId, exposureData);
        final List<Map<string,string>> fields = validateFields(accHasAId);
        if (typeOfCustomer == S_GROUP) {
            final List<arce__Analysis__c> childsId = Arc_Gen_Validate_Customer_Data.getIdsOfChildAccount(acc.arce__Analysis__r.arce__Group__c, acc.arce__Analysis__c);
            final List<String> idOfClients = getIdsOfFilials(childsId);
            result.ratingStatus = validateRatingInSanction(accHasAId, typeOfCustomer, idOfClients);
            final List<String> flagType = checkValidateCustomer(idOfClients);
            switch on flagType[0] {
                when 'Active' {
                    final Double multiplicationFactorGP = getMultiplicationFactor((String)Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(accHasA)[0].arce__magnitude_unit_type__c);
                    final List<Map<string,string>> groupResult = getGroup(exposureData, idOfClients, multiplicationFactorGP);
                    mapResult = createFinalMap(validateTypo, fields, groupResult);
                    if (mapResult.isEmpty() && result.ratingStatus.isEmpty()) {
                        Arc_Gen_Validate_Customer_Data.changeStatusArce(accHasAId);
                    }
                }
                when else {
                    for (String flag: flagType) {
                        mapResult.add(new Map<string,string>{ID_CLIENTE => flag, FIELD_MISSING => System.Label.Arc_Gen_ClientNoValidated});
                    }
                }
            }
        } else {
            result.ratingStatus = validateRatingInSanction(accHasAId, typeOfCustomer, new List<String>{accHasAId});
            final List<Map<string,string>> productResult = validateProduct(exposureData);
            mapResult = createFinalMap(validateTypo, fields, productResult);
            if (mapResult.isEmpty() && result.ratingStatus.isEmpty()) {
                Arc_Gen_Validate_Customer_Data.activeCustomerFlag(accHasAId, ACTIVE);
                if(typeOfCustomer == ORPHAN){
                    Arc_Gen_Validate_Customer_Data.changeStatusArce(accHasAId);
                }
            } else {
                Arc_Gen_Validate_Customer_Data.activeCustomerFlag(accHasAId, REMOVE);
            }
        }
        result.gblResponse = mapResult;
        return result;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Returns a string with the name of the client no validated or a validation
    * flag
    * --------------------------------------------------------------------------------------
    * @param List<arce__Account_has_Analysis__c> childsId
    * @return a string with the name of the client no validated or a validation flag
    * @example checkValidateCustomer(childsId)
    * --------------------------------------------------------------------------------------
    **/
    public static List<String> checkValidateCustomer(List<String> ids) {
        List<String> resultFalse = new List<String>();
        List<String> result = new List<String>();
        final List<arce__Account_has_Analysis__c> accHasObjLts = Arc_Gen_Validate_Customer_Data.checkCustomerFlag(ids);
        for (arce__Account_has_Analysis__c acchas: accHasObjLts) {
            if (acchas.arce_ctmr_flag__c == false) {
                resultFalse.add((String)acchas.arce__Customer__r.Name);
            }
        }
        if (resultFalse.isEmpty()) {
            result.add(ACTIVE);
        } else {
            result = resultFalse;
        }
        return result;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Returns the ids of account has analysis related to group
    * --------------------------------------------------------------------------------------
    * @param List<arce__Analysis__c> childsId
    * @return a string with the name of the client no validated or a validation flag
    * @example getIdsOfFilials(childsId)
    * --------------------------------------------------------------------------------------
    **/
    public static List<String> getIdsOfFilials(List<arce__Analysis__c> childsId) {
        List<String> idOfChilds = new List<String>();
        for (arce__Analysis__c childId: childsId) {
            for (arce__Account_has_Analysis__c accId: childId.arce__Account_has_Analysis__r) {
                idOfChilds.add(accId.Id);
            }
        }
        return idOfChilds;
    }

    /**
    * ---------------------------------------------------------------------------------------------------
    * @Description Returns a List<Map<string,string>> that contain the final error message of the
    * Policies table validations
    * ---------------------------------------------------------------------------------------------------
    * @param validateTypo List<Map<string,string>> with the fields missing in typologies
    * @param fields List<Map<string,string>> with the fields missing in proposal and risk request section
    * @param groupResults List<Map<string,string>> with the fields missing if the client is a group
    * @return a List<Map<string,string>> with all the fields missing in policies table
    * @example createFinalMap(validateTypo, fields, groupResults)
    * ---------------------------------------------------------------------------------------------------
    **/
    public static List<Map<string,string>> createFinalMap(List<Map<string,string>> validateTypo, List<Map<string,string>> fields, List<Map<string,string>> groupResults) {
        List<Map<string,string>> finalMapResult = new List<Map<string,string>>();
        finalMapResult.addAll(validateTypo);
        finalMapResult.addAll(fields);
        if (!groupResults.isEmpty()) {
            finalMapResult.addAll(groupResults);
        }
        return finalMapResult;
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description Returns a List<Map<string,string>> with the fields missing in typologies
    * validations
    * --------------------------------------------------------------------------------------
    * @param accHasAId Id of the arce__Account_has_Analysis__c
    * @return a List<Map<string,string>> with the fields missing in typologies
    * validations
    * @example validateTypology(accHasAId)
    * --------------------------------------------------------------------------------------
    **/
    public static List<Map<string,string>> validateTypology(string accHasAId, List<arce__limits_exposures__c> exposureData) {
        List<Map<string,string>> mapResult = new List<Map<string,string>>();
        for (arce__limits_exposures__c expData: exposureData) {
            if (expData.arce__limits_typology_id__c != null && expData.arce__limits_typology_id__r.arce__risk_typology_parent_id__c != null) {
                final List<Map<string,string>> auxTypo = compareTypology(expData, expData.arce__limits_exposures_parent_id__c, exposureData);
                mapResult.addAll(auxTypo);
            }
            if (expData.arce__limits_typology_id__r.arce__risk_typology_level_id__c == TOTAL_FIN_RISK_ST) {
                final List<Map<string,string>> auxTypo3 = validateTypoSum(expData, exposureData, TOTAL_FIN_ST_CHILD);
                mapResult.addAll(auxTypo3);
            }
            if (expData.arce__limits_typology_id__r.arce__risk_typology_level_id__c == TOTAL_FIN_RISK_LT) {
                final List<Map<string,string>> auxTypo4 = validateTypoSum(expData, exposureData, TOTAL_FIN_LT_CHILD);
                mapResult.addAll(auxTypo4);
            }
            if (expData.arce__limits_typology_id__r.arce__risk_typology_level_id__c == TOTAL_CORP_RISK) {
                final List<Map<string,string>> auxTypo2 = validateTotals(expData, exposureData, TOTAL_CORPR_CHILD);
                mapResult.addAll(auxTypo2);
            }
        }
        return mapResult;
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description - Method that compare the amount of the parent typologies and the child
    * typologies
    * --------------------------------------------------------------------------------------
    * @param expData - Data of the child typology
    * @param typoParentId - Id of the parent typology
    * @param exposureData - Data of the arce__limits_exposures__c object
    * @return a List<Map<string,string>> with the information of the typologies that exeed
    * in amount to its parents
    * @example compareTypology(expData, typoParentId, exposureData)
    * --------------------------------------------------------------------------------------
    **/
    public static List<Map<string,string>> compareTypology(arce__limits_exposures__c expData, Id typoParentId, List<arce__limits_exposures__c> exposureData) {
        final List<Map<string,string>> mapRes = new List<Map<string,string>>();
        for (arce__limits_exposures__c exposureParent: exposureData) {
            if (exposureParent.Id == typoParentId) {
                for (integer i = 0; i < FIELD_LIST.size(); i++) {
                    if (Double.valueOf((Double)exposureParent.get(FIELD_LIST.get(i))) < Double.valueOf((Double)expData.get(FIELD_LIST.get(i)))) {
                        mapRes.add(new Map<String,String>{ID_CLIENTE => String.valueOf((String)expData.arce__account_has_analysis_id__r.arce__Customer__r.Name), TYPOLOGY => (String)expData.arce__limits_typology_id__r.Name, FIELD_NAME => FIELD_LABEL.get(i)});
                    }
                }
            }
        }
        return mapRes;
    }
    /*
    * --------------------------------------------------------------------------------------
    * @Description - Method that compare the amount of some Totals typologies and its childs
    * typologies
    * --------------------------------------------------------------------------------------
    * @param expData - Data of the Parent typology
    * @param exposureData - Data of the arce__limits_exposures__c object
    * @param iteraChilds - List string with the dev name of the childs
    * @return a List<Map<string,string>> with an error message
    * @example validateTypoSum(expData, exposureData, iteraChilds)
    * --------------------------------------------------------------------------------------
    **/
    public static List<Map<string,string>> validateTypoSum(arce__limits_exposures__c expData, List<arce__limits_exposures__c> exposureData, List<string> iteraChilds) {
        final List<Map<string,string>> mapRes = new List<Map<string,string>>();
        Map<string,double> fieldsValuesMap = new Map<string,double>();
        double aux = 0, flag = 0;
        for (integer i = 0; i < FIELD_LIST.size(); i++) {
            fieldsValuesMap.put(FIELD_LIST[i], 0);
        }
        for (arce__limits_exposures__c exposure: exposureData) {
            for (String child: iteraChilds) {
                if (exposure.arce__limits_typology_id__r.arce__risk_typology_level_id__c == child) {
                    flag++;
                    for (integer i = 0; i < FIELD_LIST.size(); i++) {
                        if (flag == 1) {
                            fieldsValuesMap.put(FIELD_LIST[i], (Double)exposure.get(FIELD_LIST.get(i)));
                        } else if (flag == 2) {
                            aux = fieldsValuesMap.get(FIELD_LIST[i]) + (Double)exposure.get(FIELD_LIST.get(i));
                            fieldsValuesMap.put(FIELD_LIST[i], aux);
                            aux = 0;
                            if (Double.valueOf((Double)expData.get(FIELD_LIST.get(i))) > fieldsValuesMap.get(FIELD_LIST[i])) {
                                mapRes.add(new Map<String,String>{ID_CLIENTE => String.valueOf((String)expData.arce__account_has_analysis_id__r.arce__Customer__r.Name), FIELD_MISSING => System.Label.Arc_Gen_SumOfChilds, TYPOLOGY => (String)expData.arce__limits_typology_id__r.Name, FIELD_NAME => FIELD_LABEL.get(i)});
                            }
                        }
                    }
                }
            }
        }
        return mapRes;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description - Method that compare the amount of some Totals typologies and its childs
    * typologies
    * --------------------------------------------------------------------------------------
    * @param expData - Data of the child typology
    * @param exposureData - Data of the arce__limits_exposures__c object
    * @param iteraChilds - List string with the dev name of the childs
    * @return a List<Map<string,string>> with the information of the typologies that exeed
    * in amount to its parents
    * @example validateTotals(expData, exposureData, iteraChilds)
    * --------------------------------------------------------------------------------------
    **/
    public static List<Map<string,string>> validateTotals(arce__limits_exposures__c expData, List<arce__limits_exposures__c> exposureData, List<string> iteraChilds) {
        final List<Map<string,string>> mapRes = new List<Map<string,string>>();
        for (arce__limits_exposures__c exposure: exposureData) {
            for (String child: iteraChilds) {
                if (exposure.arce__limits_typology_id__r.arce__risk_typology_level_id__c == child) {
                    for (integer i = 0; i < FIELD_LIST.size(); i++) {
                        if (Double.valueOf((Double)exposure.get(FIELD_LIST.get(i))) > Double.valueOf((Double)expData.get(FIELD_LIST.get(i)))) {
                            mapRes.add(new Map<String,String>{ID_CLIENTE => String.valueOf((String)expData.arce__account_has_analysis_id__r.arce__Customer__r.Name), FIELD_MISSING => 'Parent: ' + (String)expData.arce__limits_typology_id__r.Name, TYPOLOGY => (String)exposure.arce__limits_typology_id__r.Name, FIELD_NAME => FIELD_LABEL.get(i)});
                        }
                    }
                }
            }
        }
        return mapRes;
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description manage the validation of some fields in the policy tab
    * --------------------------------------------------------------------------------------
    * @param accHasAId Id of the arce__Account_has_Analysis__c
    * @return List<Map<string, string>> with the name of the missing fields
    * @example validateFields(accHasAId)
    * --------------------------------------------------------------------------------------
    **/
    public static List<Map<string,string>> validateFields(string accHasAId) {
        List<Map<string,string>> mapResult = new List<Map<string,string>>();
        final Set<String> mapFieldLts = new Set<String>();
        final Map<String,SObject> devFieldsConfigMap = new Map<String,SObject>();
        final Map<String,String> devFieldsValueMap = new Map<String,String>();
        final List<arce__Account_has_Analysis__c> accHasAnalysis = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(new List<Id>{accHasAId});
        final List<arce__Sector__c> sector = Arc_Gen_Sector_Data.getSectorByDeveloperName(new List<String>{accHasAnalysis[0].arce__sector_rt_type__c});
        final String clientName = Arc_Gen_Validate_Customer_Data.getNameClient(accHasAId);
		System.debug('System.Label.Arc_Gen_Proposal:'+System.Label.Arc_Gen_Proposal);
		System.debug('System.Label.Arc_Gen_Risk_request:'+System.Label.Arc_Gen_Risk_request);
		System.debug('sector[0].arce__developer_name__c:'+ (String)sector[0].arce__developer_name__c );
        final List<dyfr__Field_Config__c> fieldConfigData = Arc_Gen_Validate_Customer_Data.getFieldsBySection(new List<String>{System.Label.Arc_Gen_Proposal, System.Label.Arc_Gen_Risk_request}, (String)sector[0].arce__developer_name__c + '-500');
        final dyfr__Field_Config__c stageControl = Arc_Gen_Validate_Customer_Data.getFieldStageControl('arce__Analysis__r.arce__Stage__c', (String)sector[0].arce__developer_name__c + '-500');
        final String valueStageControl = (String)Arc_Gen_AccHasAnalysis_Data.getAccHasRelation((Id)accHasAId).arce__Analysis__r.arce__Stage__c;
        for (dyfr__Field_Config__c field: fieldConfigData) {
            mapFieldLts.add((String)field.dyfr__Map_field__c);
            devFieldsConfigMap.put((String)field.dyfr__DeveloperName__c, field);
        }
        devFieldsConfigMap.put((String)stageControl.dyfr__DeveloperName__c, stageControl);
        String mapFieldString = String.join(new List<String>(mapFieldLts), ',');
        final List<arce__Account_has_Analysis__c> fieldValues = Arc_Gen_Validate_Customer_Data.getValues(accHasAId, mapFieldString);
        for (dyfr__Field_Config__c field: fieldConfigData) {
            devFieldsValueMap.put((String)field.dyfr__DeveloperName__c, String.valueOf(fieldValues[0].get(field.dyfr__Map_field__c)));
        }
        devFieldsValueMap.put((String)stageControl.dyfr__DeveloperName__c,valueStageControl);
        for (dyfr__Field_Config__c field: fieldConfigData) {
            Boolean fieldFilled = validateField(field, devFieldsConfigMap, devFieldsValueMap);
            if (fieldFilled == false) {
                mapResult.add(new Map<string,string>{ID_CLIENTE => clientName, FIELD_MISSING => field.dyfr__Label__c});
            }
        }
        return mapResult;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description return true/false
    * --------------------------------------------------------------------------------------
    * @param field - dyfr__Field_Config__c object
    * @param devFieldsConfigMap - Map<String,SObject>
    * @param fieldValues - arce__Account_has_Analysis__c object list
    * @return boolean - true/false
    * @example validateField(field,devFieldsConfigMap,devFieldsValueMap)
    * --------------------------------------------------------------------------------------
    **/
    public static Boolean validateField(dyfr__Field_Config__c field, Map<String,SObject> devFieldsConfigMap, Map<String,String> devFieldsValueMap) {
        Boolean filledField;
        if (String.isEmpty(field.dyfr__Visibility_control_field__c)) {
            filledField = devFieldsValueMap.get(field.dyfr__DeveloperName__c) == null || devFieldsValueMap.get(field.dyfr__DeveloperName__c) == 'false' ? false : true;
        } else {
            filledField = field.dyfr__Visibility_control_field__c.contains(';') ? validateMultiDepField(field, devFieldsConfigMap, devFieldsValueMap) : validateDepField(field, devFieldsConfigMap, devFieldsValueMap);
        }
        return filledField;
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description return true/false
    * --------------------------------------------------------------------------------------
    * @param field - dyfr__Field_Config__c object
    * @param devFieldsConfigMap - Map<String,SObject>
    * @param fieldValues - arce__Account_has_Analysis__c object list
    * @return boolean - true/false
    * @example validateDepField(field,devFieldsConfigMap,devFieldsValueMap)
    * --------------------------------------------------------------------------------------
    **/
    public static Boolean validateDepField(dyfr__Field_Config__c field, Map<String,SObject> devFieldsConfigMap, Map<String,String> devFieldsValueMap) {
        Boolean fieldFilled;
        String valueControlField = devFieldsValueMap.get(field.dyfr__Visibility_control_field__c);
        if (valueControlField == field.dyfr__Visibility_control_value__c) {
            fieldFilled = devFieldsValueMap.get(field.dyfr__DeveloperName__c) == null || devFieldsValueMap.get(field.dyfr__DeveloperName__c) == 'false' ? false : true;
        }
        return fieldFilled;
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description return true/false
    * --------------------------------------------------------------------------------------
    * @param field - dyfr__Field_Config__c object
    * @param devFieldsConfigMap - Map<String,SObject>
    * @param fieldValues - arce__Account_has_Analysis__c object list
    * @return boolean - true/false
    * @example validateMultiDepField(field,devFieldsConfigMap,devFieldsValueMap)
    * --------------------------------------------------------------------------------------
    **/
    public static Boolean validateMultiDepField(dyfr__Field_Config__c field, Map<String,SObject> devFieldsConfigMap, Map<String,String> devFieldsValueMap) {
        List<Boolean> filledLts =  new List<Boolean>();
        Boolean isActive = false;
        Boolean filledField;
        integer k = 0;
        final List<String> fieldControlLts = String.valueOf(field.dyfr__Visibility_control_field__c).split(';');
        final List<String> fieldValuesLts = String.valueOf(field.dyfr__Visibility_control_value__c).split(';');
        for (integer i = 0; i < fieldControlLts.size(); i++) {
            filledLts.add(devFieldsValueMap.get(fieldControlLts[i]) == fieldValuesLts[i] ? true : false);
        }
        for (integer i = 0; i < filledLts.size(); i++) {
            if (filledLts[i] == true) {
                k++;
            }
        }
        if (k == filledLts.size()) {
            isActive = true;
        }
        filledField = devFieldsValueMap.get(field.dyfr__DeveloperName__c) == null && isActive == true ? false : true;
        return filledField;
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description manage the validation when there are some products in the policies table
    * --------------------------------------------------------------------------------------
    * @param accHasAId Id of the arce__Account_has_Analysis__c
    * @return List<Map<string,string>> with the name and the amount higher than the parent
    * typology
    * @example validateProduct(accHasAId)
    * --------------------------------------------------------------------------------------
    **/
    public static List<Map<String,String>> validateProduct(List<arce__limits_exposures__c> dataProducts) {
        final List<Map<string,string>> mapProduct = new List<Map<string,string>>();
        for (arce__limits_exposures__c dataProduct: dataProducts) {
            if (dataProduct.arce__Product_id__c != null) {
                List<Map<string,string>> auxProduct = compareProduct(dataProduct, dataProduct.arce__limits_exposures_parent_id__c, dataProducts);
                if (!auxProduct.isEmpty()) {
                    for (Map<string,string> auxMap: auxProduct) {
                        mapProduct.add(auxMap);
                    }
                }
            }
        }
        return mapProduct;
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description method that compare a product with its typology parent
    * --------------------------------------------------------------------------------------
    * @param product - arce__limits_exposures__c data of the product to compare
    * @param typoParentId - Id of the typology parent
    * @param dataProducts - all the data of the object arce__limits_exposures__c
    * @return List<Map<string,string>> with the name and the amount higher than the parent
    * typology
    * @example validateProduct(product, typoParentId, dataProducts)
    * --------------------------------------------------------------------------------------
    **/
    public static List<Map<string,string>> compareProduct(arce__limits_exposures__c product, Id typoParentId, List<arce__limits_exposures__c> dataProducts) {
        final List<Map<string,string>> mapRes = new List<Map<string,string>>();
        for (arce__limits_exposures__c dataP: dataProducts) {
            if (dataP.Id == typoParentId) {
                for (integer i = 0; i < FIELD_LIST.size(); i++) {
                    if (Double.valueOf((Double)dataP.get(FIELD_LIST.get(i))) < Double.valueOf((Double)product.get(FIELD_LIST.get(i)))) {
                        mapRes.add(new Map<String,String>{ID_CLIENTE => String.valueOf((String)product.arce__account_has_analysis_id__r.arce__Customer__r.Name), TYPOLOGY => (String)product.arce__Product_id__r.Name, FIELD_NAME => FIELD_LABEL.get(i)});
                    }
                }
            }
        }
        return mapRes;
    }

    /**
    * --------------------------------------------------------------------------------------
    * @Description manage the validation when a client is a group
    * --------------------------------------------------------------------------------------
    * @param exposureParentData - arce__limits_exposures__c data of the group
    * @param childsId - object that contains the the account has analysis Ids of the clients
    * @return List<Map<string,string>> with the error message
    * @example getGroup(exposureParentData, childsId)
    * --------------------------------------------------------------------------------------
    **/
    public static List<Map<string,string>> getGroup(List<arce__limits_exposures__c> exposureParentData, List<String> accHasIdList, Double multiplicationFactorGP) {
        final List<Map<String,String>> mapResult = new List<Map<String,String>>();
        Map<String,Double> multiplicationFactorChilds = new Map<String,Double>();
        final List<arce__Account_has_Analysis__c> accHasAnData = Arc_Gen_AccHasAnalysis_Data.getAccHasAnalysis(accHasIdList);
        for (arce__Account_has_Analysis__c accHasAn: accHasAnData) {
            multiplicationFactorChilds.put(accHasAn.Id, getMultiplicationFactor((String)acchasAn.arce__magnitude_unit_type__c));
        }
        final List<arce__limits_exposures__c> exposureChildData = Arc_Gen_Validate_Customer_Data.getExposures(accHasIdList);
        for (arce__limits_exposures__c exposureParentD: exposureParentData) {
            for (arce__limits_exposures__c exposureChildD: exposureChildData) {
                if (exposureParentD.arce__limits_typology_id__r.Name == exposureChildD.arce__limits_typology_id__r.Name) {
                    for (integer i = 0; i < FIELD_LIST.size(); i++) {
                        if (Double.valueOf((Double)exposureParentD.get(FIELD_LIST.get(i))) * multiplicationFactorGP < Double.valueOf((Double)exposureChildD.get(FIELD_LIST.get(i))) * multiplicationFactorChilds.get(exposureChildD.arce__account_has_analysis_id__c)) {
                            mapResult.add(new Map<String,String>{ID_CLIENTE => String.valueOf((String)exposureChildD.arce__account_has_analysis_id__r.arce__Customer__r.Name), TYPOLOGY => (String)exposureChildD.arce__limits_typology_id__r.Name, FIELD_NAME => FIELD_LABEL.get(i)});
                        }
                    }
                }
            }
        }
        return mapResult;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Return the multiplication factor to convert the unit selected in units
    * --------------------------------------------------------------------------------------
    * @Author   Mario Humberto Ramirez Lio  mariohumberto.ramirez.contractor@bbva.com
    * @Date     Created: 29/10/2019
    * @param unitSelected - unit selected in combo box
    * @return multiFactor - factor of multiplication to convert the unit selected in units
    * @example getMultiplicationFactor(unitSelected)
    * --------------------------------------------------------------------------------------
    **/
    public static Double getMultiplicationFactor(String unitSelected) {
        Double multiFactor = 1;
        switch on unitSelected {
            when '1' {
                multiFactor = 1;
            }
            when '2' {
                multiFactor = 1000;
            }
            when '3' {
                multiFactor = 1000000;
            }
            when '4' {
                multiFactor = 1000000000000L;
            }
        }
        return multiFactor;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description Valid rating in Sanction
    * --------------------------------------------------------------------------------------
    * @param recordId - id of the account has analysis object
    * @param typeOfCustomer - type of customer
    * @param idOfClients - List<Id> of account has analysis object
    * @return List<string> with name of the clients that do not has a valid rating
    * @example validateRatingInSanction(recordId, typeOfCustomer, idOfClients)
    * --------------------------------------------------------------------------------------
    **/
    public static List<String> validateRatingInSanction(String recordId, String typeOfCustomer, List<String> idOfClients) {
        final List<String> result = new List<String>();
        final List<Id> idLts = new List<Id>();
        final Id idAccHas = Id.valueOf(recordId);
        final arce__Account_has_Analysis__c stageOfArce = Arc_Gen_AccHasAnalysis_Data.getAccHasRelation(idAccHas);
        if (typeOfCustomer == S_GROUP && stageOfArce.arce__Analysis__r.arce__Stage__c == IN_SANCTION) {
            for (String idClient: idOfClients) {
                idLts.add(Id.valueOf(idClient));
            }
            final List<arce__Account_has_Analysis__c> ratingStatus = Arc_Gen_AccHasAnalysis_Data.getRatingStatus(idLts);
            for (arce__Account_has_Analysis__c ratingStat: ratingStatus) {
                if (ratingStat.arce__ffss_for_rating_id__r.arce__rating_id__r.arce__status_type__c != VALIDATE) {
                    result.add(ratingStat.arce__Customer__r.Name);
                }
            }
        } else if (typeOfCustomer == CLIENT && stageOfArce.arce__Analysis__r.arce__Stage__c == IN_SANCTION) {
            final List<arce__Account_has_Analysis__c> ratingStatus = Arc_Gen_AccHasAnalysis_Data.getRatingStatus(new List<Id>{recordId});
            if (ratingStatus[0].arce__ffss_for_rating_id__r.arce__rating_id__r.arce__status_type__c != VALIDATE) {
                result.add(ratingStatus[0].arce__Customer__r.Name);
            }
        }
        return result;
    }
}