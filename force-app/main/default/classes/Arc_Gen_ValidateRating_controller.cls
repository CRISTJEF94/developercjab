/**
* @File Name          : Arc_Gen_ValidateRating_data.cls
* @Description        : Class that receives the response of the rating validation from the service
* @Author             : eduardoefrain.hernandez.contractor@bbva.com
* @Group              : ARCE Team
* @Last Modified By   : luisruben.quinto.munoz@bbva.com
* @Last Modified On   : 23/7/2019 14:04:58
* @Changes
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    13/5/2019 18:00:36   eduardoefrain.hernandez.contractor@bbva.com     Initial Version
* 1.1    23/5/2019 18:00:36   eduardoefrain.hernandez.contractor@bbva.com     documentation
**/
public without sharing class Arc_Gen_ValidateRating_controller {
/**
*-------------------------------------------------------------------------------
* @description empty constructor for sonar
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 2019-05-13
* @param void
* @return void
* @example Arc_Gen_ValidateRating_controller()
**/
@TestVisible
    private Arc_Gen_ValidateRating_controller() {
    }
/**
*-------------------------------------------------------------------------------
* @description wrapper for the rating data from the service class
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 2019-05-13
* @example RatingData rData = new RatingData();
**/
    public class RatingData {
        @AuraEnabled public String ratingId {get;set;}
        @AuraEnabled public String ratingFinal {get;set;}
        @AuraEnabled public String ratingScore {get;set;}
    }
/**
*-------------------------------------------------------------------------------
* @description Method that obtains the rating data from the service class
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 2019-05-13
* @param String arce analysis Id
* @return RatingData - A wrapper with information of the rating
* @example public static RatingData getRatingData(String analysisId)
**/
    @AuraEnabled
    public static RatingData getRatingData(String analysisId) {
        Arc_Gen_ValidateRating_controller constructor = new Arc_Gen_ValidateRating_controller();
        RatingData rData = new RatingData();
        List<String> rDataString = Arc_Gen_ValidateRating_service.getRatingData(analysisId);
        rData.ratingId = rDataString[0];
        rData.ratingFinal = rDataString[1];
        rData.ratingScore = rDataString[2];
        Return rData;
    }
/**
*-------------------------------------------------------------------------------
* @description Method that obtains the response of the changing status from the service class
--------------------------------------------------------------------------------
* @author eduardoefrain.hernandez.contractor@bbva.com
* @date 2019-05-13
* @param String arce analysis Id
* @param String rating  Id
* @return Arc_Gen_ServiceAndSaveResponse - A wrapper with information of saving records
* @example public static Arc_Gen_ServiceAndSaveResponse changeStatus(String analysisId)
**/
    @AuraEnabled
    public static Arc_Gen_ServiceAndSaveResponse changeStatus(String analysisId, String ratingId) {
        Arc_Gen_ServiceAndSaveResponse response = Arc_Gen_ValidateRating_service.setupValidateRating(analysisId, ratingId, null);
        Return response;
    }
}