/**
* ------------------------------------------------------------------------------------------------
* @Name     Arc_Gen_ValidateClass_tst
* @Author   juan.ignacion.hit.contractor@bbva.com
* @Date     Created: 2019-11-04
* @Group    ARCE
* ------------------------------------------------------------------------------------------------
* @Description Test class for validation classes
* ------------------------------------------------------------------------------------------------
* @Changes
*
* |2019-07-24 juan.ignacion.hit.contractor@bbva.com
*             Class creation.
* |2019-12-02 german.sanchez.perez.contractor@bbva.com | franciscojavier.bueno@bbva.com
*             Api names modified with the correct name on business glossary
* |2020-01-09 javier.soto.carrascosa@bbva.com
*             Adapt test classess with account wrapper and setupaccounts
* -----------------------------------------------------------------------------------------------
*/
@SuppressWarnings('sf:TooManyMethods')
@isTest
public class Arc_Gen_ValidateClass_tst {
    /**
    * --------------------------------------------------------------------------------------
    * @Description load data method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void loadData
    * --------------------------------------------------------------------------------------
    **/
    @testSetup Static void loadData() {
        Arc_UtilitysDataTest_tst.setupAcccounts();
        final Map<String, Arc_Gen_Account_Wrapper> groupAccWrapper = Arc_Gen_Account_Locator.getAccountByAccNumber(new List<String>{'G000001'});
        final Arc_Gen_Account_Wrapper groupAccount = groupAccWrapper.get('G000001');

        final arce__Sector__c newSector = Arc_UtilitysDataTest_tst.crearSector('s-01', '100', 's-01', null);
        insert newSector;

        final arce__Analysis__c newArce = Arc_UtilitysDataTest_tst.crearArceAnalysis('Arce Analysis MB', null, groupAccount.accId);
        insert newArce;

        final arce__Account_has_Analysis__c newAnalysis = Arc_UtilitysDataTest_tst.crearAccHasAnalysis(newSector.id, newArce.Id, groupAccount.accId, 's-01');
        insert newAnalysis;
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateMainBanksOk
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    static void validateMainBanksOk() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__main_banks__c> lstMainBanks = new List<arce__main_banks__c>();
        final arce__main_banks__c mainBank1 = Arc_UtilitysDataTest_tst.crearMainBanks(newAnalysis.Id, null);
        mainBank1.arce__entity_quota_share_per__c = 10;
        mainBank1.arce__entity_name__c = 'BBVA';
        lstMainBanks.add(mainBank1);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateMainBanks_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstMainBanks);
        System.assertEquals(resBeforeSaveData, '{"validation":true,"msgInfo":""}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateMainBanksEmptyFieldKO
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    static void validateMainBanksEmptyFieldKO() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__main_banks__c> lstMainBanks = new List<arce__main_banks__c>();
        final arce__main_banks__c mainBank1 = Arc_UtilitysDataTest_tst.crearMainBanks(newAnalysis.Id, null);
        mainBank1.arce__entity_quota_share_per__c = 10;
        mainBank1.arce__entity_name__c = '';
        lstMainBanks.add(mainBank1);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateMainBanks_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstMainBanks);
        System.assertEquals(resBeforeSaveData, '{"validation":false,"msgInfo":"Error, please complete field Name for record 1."}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateMainBanksEmptyFieldShareKO
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    static void validateMainBanksEmptyFieldShareKO() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__main_banks__c> lstMainBanks = new List<arce__main_banks__c>();
        final arce__main_banks__c mainBank1 = Arc_UtilitysDataTest_tst.crearMainBanks(newAnalysis.Id, null);
        mainBank1.arce__entity_name__c = 'Test';
        lstMainBanks.add(mainBank1);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateMainBanks_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstMainBanks);
        System.assertEquals(resBeforeSaveData, '{"validation":false,"msgInfo":"Error, please complete field Percentage for record 1."}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateMainBanksEmptyRecordsOK
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateMainBanksEmptyRecordsOK() {
        final List<arce__main_banks__c> lstMainBanks = new List<arce__main_banks__c>();

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateMainBanks_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstMainBanks);
        System.assertEquals(resBeforeSaveData, '{"validation":true,"msgInfo":""}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateMainBanksPercentGreaterKO
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateMainBanksPercentGreaterKO() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__main_banks__c> lstMainBanks = new List<arce__main_banks__c>();
        final arce__main_banks__c mainBank1 = Arc_UtilitysDataTest_tst.crearMainBanks(newAnalysis.Id, null);
        mainBank1.arce__entity_quota_share_per__c = 120;
        mainBank1.arce__entity_name__c = 'BBVA';
        lstMainBanks.add(mainBank1);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateMainBanks_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstMainBanks);
        System.assertEquals(resBeforeSaveData, '{"validation":false,"msgInfo":"Error, the sum of percentages can\'t be greater than 100%."}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateContentOK
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateContentOK() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__Table_Content_per_Analysis__c> lstTableContent = new List<arce__Table_Content_per_Analysis__c>();
        final arce__Data_Collections__c dataCollection = Arc_UtilitysDataTest_tst.crearDataCollection('NameCollection 1', null, '05');
        insert dataCollection;

        final arce__Table_Content_per_Analysis__c tableContent1 = Arc_UtilitysDataTest_tst.crearTableContentAnalysis(newAnalysis.Id, dataCollection.Id, null, '2000');
        tableContent1.arce__table_content_percentage__c = 100;
        lstTableContent.add(tableContent1);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateContent_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstTableContent);
        System.assertEquals(resBeforeSaveData, '{"validation":true,"msgInfo":""}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateContentEmptyOK
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateContentEmptyOK() {
        final List<arce__Table_Content_per_Analysis__c> lstTableContent = new List<arce__Table_Content_per_Analysis__c>();

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateContent_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstTableContent);
        System.assertEquals(resBeforeSaveData, '{"validation":true,"msgInfo":""}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateContentEmptyFieldKO
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateContentEmptyFieldKO() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__Table_Content_per_Analysis__c> lstTableContent = new List<arce__Table_Content_per_Analysis__c>();
        final arce__Data_Collections__c dataCollection = Arc_UtilitysDataTest_tst.crearDataCollection('NameCollection 1', null, '05');
        insert dataCollection;

        final arce__Table_Content_per_Analysis__c tableContent1 = Arc_UtilitysDataTest_tst.crearTableContentAnalysis(newAnalysis.Id, dataCollection.Id, null, '2000');
        lstTableContent.add(tableContent1);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateContent_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstTableContent);
        System.assertEquals(resBeforeSaveData, '{"validation":false,"msgInfo":"Error, please complete field Percentage for record 1."}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateContentPercentFailKO
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateContentPercentFailKO() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__Table_Content_per_Analysis__c> lstTableContent = new List<arce__Table_Content_per_Analysis__c>();
        final arce__Data_Collections__c dataCollection = Arc_UtilitysDataTest_tst.crearDataCollection('NameCollection 1', null, '05');
        insert dataCollection;

        final arce__Table_Content_per_Analysis__c tableContent1 = Arc_UtilitysDataTest_tst.crearTableContentAnalysis(newAnalysis.Id, dataCollection.Id, null, '2000');
        tableContent1.arce__table_content_percentage__c = 120;
        lstTableContent.add(tableContent1);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateContent_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstTableContent);
        System.assertEquals(resBeforeSaveData, '{"validation":false,"msgInfo":"Error, the sum of percentages for year 2000 must be equal to 100%."}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateContentYearFailKO
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateContentYearFailKO() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__Table_Content_per_Analysis__c> lstTableContent = new List<arce__Table_Content_per_Analysis__c>();
        final arce__Data_Collections__c dataCollection = Arc_UtilitysDataTest_tst.crearDataCollection('NameCollection 1', null, '05');
        insert dataCollection;

        final arce__Table_Content_per_Analysis__c tableContent1 = Arc_UtilitysDataTest_tst.crearTableContentAnalysis(newAnalysis.Id, dataCollection.Id, null, '2000');
        final arce__Table_Content_per_Analysis__c tableContent2 = Arc_UtilitysDataTest_tst.crearTableContentAnalysis(newAnalysis.Id, dataCollection.Id, null, '2001');
        final arce__Table_Content_per_Analysis__c tableContent3 = Arc_UtilitysDataTest_tst.crearTableContentAnalysis(newAnalysis.Id, dataCollection.Id, null, '2002');
        final arce__Table_Content_per_Analysis__c tableContent4 = Arc_UtilitysDataTest_tst.crearTableContentAnalysis(newAnalysis.Id, dataCollection.Id, null, '2002');
        tableContent1.arce__table_content_percentage__c = 5;
        tableContent2.arce__table_content_percentage__c = 5;
        tableContent3.arce__table_content_percentage__c = 5;
        tableContent4.arce__table_content_percentage__c = 10;
        lstTableContent.add(tableContent1);
        lstTableContent.add(tableContent2);
        lstTableContent.add(tableContent3);
        lstTableContent.add(tableContent4);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateContent_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstTableContent);
        System.assertEquals(resBeforeSaveData, '{"validation":false,"msgInfo":"Error, you can only introduce up to two years of information."}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateContentEmptyYearKO
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateContentEmptyYearKO() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__Table_Content_per_Analysis__c> lstTableContent = new List<arce__Table_Content_per_Analysis__c>();
        final arce__Data_Collections__c dataCollection = Arc_UtilitysDataTest_tst.crearDataCollection('NameCollection 1', null, '05');
        insert dataCollection;

        final arce__Table_Content_per_Analysis__c tableContent1 = Arc_UtilitysDataTest_tst.crearTableContentAnalysis(newAnalysis.Id, dataCollection.Id, null, '2000');
        tableContent1.arce__table_content_percentage__c = 5;
        tableContent1.arce__table_content_year__c = '';
        lstTableContent.add(tableContent1);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateContent_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstTableContent);
        System.assertEquals(resBeforeSaveData, '{"validation":false,"msgInfo":"Error, please complete field Year for record 1.Error, the sum of percentages for year  must be equal to 100%."}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateThirdParticipantEmptyOK
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateThirdParticipantEmptyOK() {
        final List<arce__Third_Participant_Details__c> lstTableThird = new List<arce__Third_Participant_Details__c>();
        final arce__Third_Participant__c thirdParticipant =  Arc_UtilitysDataTest_tst.crearThirdParticipant(null);
        thirdParticipant.arce__third_participant_role_type__c = '03';
        insert thirdParticipant;

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateThirdParticipant_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstTableThird);
        System.assertEquals(resBeforeSaveData, '{"validation":true,"msgInfo":""}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateThirdParticipantPercentKO
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateThirdParticipantPercentKO() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__Third_Participant_Details__c> lstTableThird = new List<arce__Third_Participant_Details__c>();
        final arce__Third_Participant__c thirdParticipant =  Arc_UtilitysDataTest_tst.crearThirdParticipant(null);
        thirdParticipant.arce__third_participant_role_type__c = '03';
        insert thirdParticipant;

        final arce__Third_Participant_Details__c thirdParticipant1 = Arc_UtilitysDataTest_tst.crearThirdParticipantDetails(newAnalysis.Id, thirdParticipant.Id, null);
        thirdParticipant1.arce__third_participant_per__c = null;
        lstTableThird.add(thirdParticipant1);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateThirdParticipant_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstTableThird);
        System.assertEquals(resBeforeSaveData, '{"validation":false,"msgInfo":"Error, please complete field Percentage for record 1.The first column is mandatory, please review record 1."}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateThirdParticipantSumPercentKO
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateThirdParticipantSumPercentKO() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__Third_Participant_Details__c> lstTableThird = new List<arce__Third_Participant_Details__c>();
        final arce__Third_Participant__c thirdParticipant =  Arc_UtilitysDataTest_tst.crearThirdParticipant(null);
        thirdParticipant.arce__third_participant_role_type__c = '03';
        insert thirdParticipant;

        final arce__Third_Participant_Details__c thirdParticipant1 = Arc_UtilitysDataTest_tst.crearThirdParticipantDetails(newAnalysis.Id, thirdParticipant.Id, null);
        thirdParticipant1.arce__third_participant_per__c = 105;
        lstTableThird.add(thirdParticipant1);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateThirdParticipant_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstTableThird);
        System.assertEquals(resBeforeSaveData, '{"validation":false,"msgInfo":"The first column is mandatory, please review record 1.Error, the sum of percentages can\'t be greater than 100%."}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateShareholdersEmptyOK
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateShareholdersEmptyOK() {
        final List<arce__Third_Participant_Details__c> lstTableThird = new List<arce__Third_Participant_Details__c>();
        final arce__Third_Participant__c thirdParticipant =  Arc_UtilitysDataTest_tst.crearThirdParticipant(null);
        thirdParticipant.arce__third_participant_role_type__c = '03';
        insert thirdParticipant;
        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateShareholders_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstTableThird);
        System.assertEquals(resBeforeSaveData, '{"validation":true,"msgInfo":""}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateShareholdersTypeOK
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateShareholdersTypeOK() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__Third_Participant_Details__c> lstTableThird = new List<arce__Third_Participant_Details__c>();
        final arce__Third_Participant__c thirdParticipant =  Arc_UtilitysDataTest_tst.crearThirdParticipant(null);
        thirdParticipant.arce__third_participant_role_type__c = '03';
        insert thirdParticipant;
        final arce__Third_Participant__c thirdParticipantTR1 = [SELECT Id FROM arce__Third_Participant__c LIMIT 1];

        final arce__Third_Participant_Details__c thirdParticipant1 = Arc_UtilitysDataTest_tst.crearThirdParticipantDetails(newAnalysis.Id, thirdParticipant.Id, null);
        thirdParticipant1.arce__third_participant_per__c = 20;
        thirdParticipant1.arce__shareholder_sponsor_year_id__c = '2000';
        thirdParticipant1.arce__shrhldr_financial_sponsor_type__c = '02';
        thirdParticipant1.arce__Third_Participant_id__c = thirdParticipantTR1.Id;
        lstTableThird.add(thirdParticipant1);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateShareholders_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstTableThird);
        System.assertEquals(resBeforeSaveData, '{"validation":false,"msgInfo":"Year should be completed only for financial sponsors, please review record 1.The first column is mandatory, please review record 1."}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateShareholdersFSSumPerKOK
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateShareholdersFSSumPerKOK() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__Third_Participant_Details__c> lstTableThird = new List<arce__Third_Participant_Details__c>();
        final arce__Third_Participant__c thirdParticipant =  Arc_UtilitysDataTest_tst.crearThirdParticipant(null);
        thirdParticipant.arce__third_participant_role_type__c = '03';
        insert thirdParticipant;

        final arce__Third_Participant_Details__c thirdParticipant1 = Arc_UtilitysDataTest_tst.crearThirdParticipantDetails(newAnalysis.Id, thirdParticipant.Id, null);
        thirdParticipant1.arce__third_participant_per__c = 120;
        thirdParticipant1.arce__shareholder_sponsor_year_id__c = '';
        thirdParticipant1.arce__shrhldr_financial_sponsor_type__c = '02';
        lstTableThird.add(thirdParticipant1);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateShareholders_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstTableThird);
        System.assertEquals(true, resBeforeSaveData.contains('false'), 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateShareholdersFieldEmptyOK
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateShareholdersFieldEmptyOK() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__Third_Participant_Details__c> lstTableThird = new List<arce__Third_Participant_Details__c>();
        final arce__Third_Participant__c thirdParticipant =  Arc_UtilitysDataTest_tst.crearThirdParticipant(null);
        thirdParticipant.arce__third_participant_role_type__c = '03';
        insert thirdParticipant;

        final arce__Third_Participant_Details__c thirdParticipant1 = Arc_UtilitysDataTest_tst.crearThirdParticipantDetails(newAnalysis.Id, thirdParticipant.Id, null);
        thirdParticipant1.arce__third_participant_per__c = null;
        thirdParticipant1.arce__shareholder_sponsor_year_id__c = null;
        thirdParticipant1.arce__shrhldr_financial_sponsor_type__c = '01';
        lstTableThird.add(thirdParticipant1);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateShareholders_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstTableThird);
        System.assertEquals(resBeforeSaveData, '{"validation":false,"msgInfo":"Error, please complete field Percentage for record 1.Year should be completed only for financial sponsors, please review record 1.The first column is mandatory, please review record 1."}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateMaturityContentAmountKO
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateMaturityContentAmountKO() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__Table_Content_per_Analysis__c> lstTableContent = new List<arce__Table_Content_per_Analysis__c>();
        final arce__Data_Collections__c dataCollection = Arc_UtilitysDataTest_tst.crearDataCollection('Bonds', null, '01');
        insert dataCollection;

        final arce__Table_Content_per_Analysis__c tableContent1 = Arc_UtilitysDataTest_tst.crearTableContentAnalysis(newAnalysis.Id, dataCollection.Id, null, '2000');
        tableContent1.arce__table_content_value__c = null;
        lstTableContent.add(tableContent1);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateContentMaturity_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstTableContent);
        System.assertEquals(resBeforeSaveData, '{"validation":false,"msgInfo":"Error, please complete field Amount for record 1.The first column is mandatory, please review record 1."}', 'Success');
        Test.stopTest();
    }
    /**
    * --------------------------------------------------------------------------------------
    * @Description validation test method
    * --------------------------------------------------------------------------------------
    * @Author   juan.ignacion.hit.contractor@bbva.com
    * @Date     Created: 2019-11-04
    * @param void
    * @return void
    * @example Static void validateMaturityContentYearKO
    * --------------------------------------------------------------------------------------
    **/
    @isTest
    Static void validateMaturityContentYearKO() {
        final arce__Account_has_Analysis__c newAnalysis = [SELECT Id, arce__sector_rt_type__c FROM arce__Account_has_Analysis__c LIMIT 1];
        final List<arce__Table_Content_per_Analysis__c> lstTableContent = new List<arce__Table_Content_per_Analysis__c>();
        final arce__Data_Collections__c dataCollection = Arc_UtilitysDataTest_tst.crearDataCollection('Bonds', null, '01');
        insert dataCollection;

        final arce__Table_Content_per_Analysis__c tableContent1 = Arc_UtilitysDataTest_tst.crearTableContentAnalysis(newAnalysis.Id, dataCollection.Id, null, '2000');
        tableContent1.arce__table_content_value__c = 100;
        tableContent1.arce__table_content_year__c = '';
        lstTableContent.add(tableContent1);

        Test.startTest();
        final System.Type objType = Type.forName('Arc_Gen_ValidateContentMaturity_service');
        String resBeforeSaveData = '';
        final rrtm.RelatedManager_Interface tableManagerClass = (rrtm.RelatedManager_Interface)objType.newInstance();
        resBeforeSaveData = tableManagerClass.beforeSaveData(lstTableContent);
        System.assertEquals(resBeforeSaveData, '{"validation":false,"msgInfo":"Error, please complete field Year for record 1.The first column is mandatory, please review record 1."}', 'Success');
        Test.stopTest();
    }

}